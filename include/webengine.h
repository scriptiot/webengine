/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef EWEBENGINE_H
#define EWEBENGINE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "quickvm.h"

typedef struct we_html_canvas_t {
    void *obj;
    void *buffer;
    int x;
    int y;
    double angle;
    int w;
    int h;
    int tx;
    int ty;
    void (*stroke)(struct we_html_canvas_t *canvas);
    void (*destroy)(struct we_html_canvas_t *canvas);
    void *user_data;
} we_html_canvas_t;

//html property registry
typedef struct we_html_property_reg_t {
    const char *name;
    qvm_native_t set_api;
    qvm_native_t get_api;
    qvm_hash_t key;
} we_html_property_reg_t;

//html object registry
typedef struct we_html_object_reg_t {
    const char *name;
    qvm_native_t api;
    we_html_property_reg_t *regs;
} we_html_object_reg_t;

#define we_log(fmt,...) printf("[I]: " fmt ,##__VA_ARGS__)
#define we_warning(fmt,...) printf("[W]: " fmt ,##__VA_ARGS__)
#define we_error(fmt,...) printf("[E]: " fmt ,##__VA_ARGS__)

#ifdef WE_USE_DEBUG
    #define we_debug(fmt, ...) printf("[D]: " fmt ,##__VA_ARGS__)
#else
    #define we_debug(fmt, ...)
#endif

//register system print
extern void we_register_print(void *p);

//system memory manipulation
extern   void * we_sys_malloc(size_t size);
extern   void we_sys_free(void *mem);

//memory manipulation
extern void *we_malloc(int size);
extern void we_free(void * mem);
extern void *we_realloc(void *p, size_t size);
extern qvm_state_t *we_get_runtime(void);
extern const char *we_get_root_dir(void);
extern int we_start(int argc, char *argv[]);
extern int we_restart(int argc, char *argv[]);
extern uint8_t we_system_memory_usage(void);
extern void we_create_runtime(void);
extern int we_init(void);
extern void we_get_realpath(char *path, const char *name);
extern void we_poll(qvm_state_t *e);

//page component manipulation
extern int we_page_manager_add_component(qvm_state_t *e, void *addr, qvm_value_t html_object);
extern int we_page_manager_remove_component(qvm_state_t *e, void *addr);
extern qvm_value_t we_page_manager_find_component(qvm_state_t *e, void *addr);
extern qvm_value_t we_page_get_current_index_object(qvm_state_t *e);
extern qvm_value_t we_page_get_current_css_object(qvm_state_t *e);
extern qvm_value_t we_page_get_current_html_object(qvm_state_t *e);
extern qvm_value_t we_page_get_current_default(qvm_state_t *e);

//html object manipulation
extern void we_html_object_set_obj_data(qvm_value_t object, void *obj);
extern void *we_html_object_get_obj_data(qvm_value_t o);
extern void we_html_object_set_style_data(qvm_value_t object, void *obj);
extern void *we_html_object_get_style_data(qvm_value_t o);
extern void we_html_object_set_user_data(qvm_value_t object, void *obj);
extern void *we_html_object_get_user_data(qvm_value_t o);
extern void we_html_object_set_destroy(qvm_value_t object, qvm_native_t fn);
extern void we_html_ui_object_register(qvm_state_t * e, we_html_object_reg_t *regs);
extern void we_html_ui_style_register(qvm_state_t * e, we_html_property_reg_t *regs);
extern void *we_get_user_data(qvm_state_t *e, qvm_value_t object);
extern void we_set_user_data(qvm_state_t *e, qvm_value_t object, void *data);

we_html_canvas_t *we_canvas_init(qvm_state_t *e, qvm_value_t p, int color_depth, void (*stroke)(we_html_canvas_t *));
typedef void (*we_process_handler) (qvm_state_t *e, int type, void* ptr);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
