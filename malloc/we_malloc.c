#include "we_malloc.h"

void we_memcpy(void *des,void *src,uint32_t n)
{  
    uint8_t *xdes=(uint8_t *)des;
    uint8_t *xsrc=(uint8_t *)src;
	
	while(n--)*xdes++ = *xsrc++;  
}  

void we_memset(void *s,uint8_t c,uint32_t count)
{  
    uint8_t *xs = (uint8_t *)s;
	while(count--) *xs++ = c;  
}

void we_heap_init(struct we_heap_t *heap, void * table, size_t table_size, void *mem, size_t size)
{
    heap->table = (uint32_t *)table;
    heap->tb_size = table_size;

    heap->base = (uint8_t  *)mem;
    we_memset(table, 0, heap->tb_size * sizeof (uint32_t));
    we_memset(mem, 0, size);
}   

void *we_heap_alloc(struct we_heap_t *heap, size_t size)
{
	uint32_t tb_size = 0;
	uint32_t cnt = 0;
	uint32_t i;  
		
	if(size==0)
		return NULL;
	
    tb_size = size / MEM_BLOCK_SIZE;  	
	if(size % MEM_BLOCK_SIZE) tb_size++; 
	
	uint32_t offset=0;  
	for(offset = 0; offset < heap->tb_size; offset++)
	{     
		if(!heap->table[offset]) {
            cnt++;
        } else {
			cnt = 0;
			continue;
		}
		
		if(cnt == tb_size)						
		{
			for(i=0;i<tb_size;i++)  				
			{  
				heap->table[offset-i] = tb_size;  
			}  
            return  (void*)((intptr_t)heap->base + (offset+1-tb_size)*MEM_BLOCK_SIZE);
		}
    }
	return NULL;
}

void we_heap_free(struct we_heap_t *heap,void *mem)
{
	if(mem==NULL) return;
    uint32_t i = 0;
	uint32_t offset = (uint32_t)mem - (uint32_t)heap->base;  
    uint32_t maxsize = (uint32_t)heap->base + heap->tb_size*sizeof(uint32_t);

	if(offset < maxsize)
	{  
        int index = offset / MEM_BLOCK_SIZE;			
        uint32_t size = heap->table[index];
        for(i=0; i< size; i++)  					
        {  
            heap->table[index+i] = 0;  
        }  
	}
}

uint8_t we_heap_usage(struct we_heap_t *heap)
{  
	uint32_t used=0;  
	uint32_t i;  
	
	for(i=0;i<heap->tb_size;i++)  
	{  
		if(heap->table[i])
            used++; 
	} 
	
    return (used*100)/(heap->tb_size);
} 
