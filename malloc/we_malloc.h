/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_MALLOC_H
#define WE_MALLOC_H

#include <stdlib.h>
#include <stdint.h>

#define MEM_BLOCK_SIZE			32

typedef struct we_heap_t
{
    uint8_t  *base;
    uint32_t *table;
    uint32_t tb_size;
} we_heap_t;

void we_memcpy(void *des,void *src,uint32_t n);
void we_memset(void *s,uint8_t c,uint32_t count);
void we_heap_init(struct we_heap_t *heap, void *table, size_t table_size, void *mem, size_t size);
void *we_heap_alloc(struct we_heap_t *heap, size_t size);
void we_heap_free(struct we_heap_t *heap,void *mem);
uint8_t we_heap_usage(struct we_heap_t *heap);

#endif
