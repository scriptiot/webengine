# webengine

## 介绍
WebEngine是一款针对嵌入式单片机设计的浏览器、小程序应用框架。

### 主要特点
1. 支持Javascript、CSS、和类vue语言。
2. 支持不同GUI引擎适配。
3. 支持标签扩展。
4. 支持第三方库扩展。

### core目录结构
+ base64: base64算法JS库扩展
+ binding: 标签数据绑定
+ compress: heatshrink压缩解压库
+ css: css解析
+ dom: dom api扩展
+ evue: 类vue标签解析
+ fileapi: 文件操作
+ graphics: 2D、3D接口封装
+ html: 类html标签解析
+ image: 图片解码、管理
+ javascript: js接口扩展
+ md5: md5算法封装
+ network: socket库，http协议解析
+ page: 页面管理
+ process: 跨进程、线程管理
+ rendering: 渲染管理
+ router: 页面路由管理

### 其它目录结构
+ component: 标签控件封装
+ malloc: 内存管理
+ simulator: 基于QT的linux平台模拟器
+ tests: 测试项


## 构建运行

git clone https://gitee.com/scriptiot/evm.git
进入evm，git checkout quickvm-1.0
注意，evm目录和webengine在同一级目录

Qt Creator打开**simulator/webengine.pro**

Command line arguments: `--width 240 --height 240 --dir ../tests/test/` 

### Linux

依赖：curl/SDL2/mbedtls

```
sudo apt-get install libcurl4-openssl-dev
sudo apt-get install libsdl2-dev
sudo apt-get install libmbedtls-dev
```
### Windows

模拟器依赖都已经存放在**simulator**目录下，编译时会自动链接。

如果提示mbedtls编译报错，那么则需要克隆mbedtls源码编译出库，然后替换**simulator/mbedtls**目录下所有文件，再次编译

```
git clone https://github.com/ARMmbed/mbedtls.git
cd mbedtls
git checkout v2.16.9
mingw32-make CC=gcc WINDOWS_BUILD=1
```

将下面两个目录下以下几个文件拷贝到**simulator/mbedtls**

**mbedtls/library**

```
libmbedcrypto.a
libmbedtls.a
libmbedx509.a
```

**mbedtls/include**

*所有文件*

参考：
+ https://segmentfault.com/a/1190000037795535
+ https://github.com/armmbed/mbedtls/issues/231


## 开发文档

https://www.yuque.com/books/share/42351a1a-9ea1-46c2-87f6-c14e51724977?# 《WebEngine》