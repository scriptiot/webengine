HEADERS += \
    $$PWD/../webengine/core/binding/we_binding.h \
    $$PWD/../webengine/core/css/we_css.h \
    $$PWD/../webengine/core/dom/we_document.h \
    $$PWD/../webengine/core/evue/we_evue.h \
    $$PWD/../webengine/core/fileapi/we_file.h \
    $$PWD/../webengine/core/html/we_html.h \
    $$PWD/../webengine/core/javascript/we_js_builtin.h \
    $$PWD/../webengine/core/page/we_page.h \
    $$PWD/../webengine/core/rendering/we_render.h \
    $$PWD/../webengine/core/router/we_router.h \
    $$PWD/../webengine/core/compress/wrap_heatshrink.h \
    $$PWD/../webengine/core/compress/heatshrink_encoder.h \
    $$PWD/../webengine/core/compress/heatshrink_decoder.h \
    $$PWD/../webengine/core/compress/heatshrink_config.h \
    $$PWD/../webengine/core/compress/heatshrink_common.h \
    $$PWD/../webengine/core/compress/compress_heatshrink.h \
    $$PWD/../webengine/core/base64/we_base64.h \
    $$PWD/../webengine/core/epk/we_epk.h \
    $$PWD/../webengine/core/md5/we_md5.h

SOURCES += \
    $$PWD/../webengine/core/image/we_image.c \
    $$PWD/../webengine/core/image/lodepng.c

SOURCES += \
    $$PWD/../webengine/core/process/we_process.c

SOURCES += \
    $$PWD/../webengine/core/network/we_socket.c \
    $$PWD/../webengine/core/network/we_http_parser.c \
    $$PWD/../webengine/core/network/http-parser/http_parser.c

SOURCES += \
    $$PWD/../webengine/core/binding/we_binding.c \
    $$PWD/../webengine/core/binding/we_binding_native.c \
    $$PWD/../webengine/core/binding/we_binding_watcher.c \
    $$PWD/../webengine/core/css/we_css.c \
    $$PWD/../webengine/core/dom/we_document.c \
    $$PWD/../webengine/core/evue/we_evue.c \
    $$PWD/../webengine/core/fileapi/we_file.c \
    $$PWD/../webengine/core/html/we_html_object.c \
    $$PWD/../webengine/core/html/we_html_register_ui.c \
    $$PWD/../webengine/core/javascript/we_js_builtin.c \
    $$PWD/../webengine/core/javascript/we_js_builtin_dom.c \
    $$PWD/../webengine/core/javascript/we_js_builtin_dom_element.c \
    $$PWD/../webengine/core/page/we_page.c \
    $$PWD/../webengine/core/page/we_page_manager.c \
    $$PWD/../webengine/core/page/we_page_state.c \
    $$PWD/../webengine/core/page/we_page_state_machine.c \
    $$PWD/../webengine/core/rendering/we_render.c \
    $$PWD/../webengine/core/rendering/we_render_template.c \
    $$PWD/../webengine/core/router/we_router.c \
    $$PWD/../webengine/core/compress/wrap_heatshrink.c \
    $$PWD/../webengine/core/compress/heatshrink_encoder.c \
    $$PWD/../webengine/core/compress/heatshrink_decoder.c \
    $$PWD/../webengine/core/compress/compress_heatshrink.c \
    $$PWD/../webengine/core/base64/we_base64.c \
    $$PWD/../webengine/core/graphics/we_graphics.c \
    $$PWD/../webengine/core/graphics/we_tk.c \
    $$PWD/../webengine/core/md5/we_md5.c \
    $$PWD/../webengine/core/webengine.c

SOURCES += \
    $$PWD/../webengine/core/graphics/PicoGL/src/api.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/arrays.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/clear.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/clip.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/error.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/get.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/image_util.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/init.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/light.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/list.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/matrix.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/memory.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/misc.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/msghandling.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/oscontext.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/select.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/specbuf.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/texture.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/vertex.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/zbuffer.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/zdither.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/zline.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/zmath.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/ztriangle.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/glu/glu.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/glu/glu_cylinder.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/glu/glu_disk.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/glu/glu_perspective.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/glu/glu_sphere.c \
    $$PWD/../webengine/core/graphics/PicoGL/src/glu/glu_torus.c
