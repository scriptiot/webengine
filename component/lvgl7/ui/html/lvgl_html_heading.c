#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_heading_h1(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }
    obj = (lv_obj_t*)lv_label_create(parent, NULL);
    if( !obj )
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    return qvm_undefined;
}

qvm_value_t html_obj_heading_h2(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }
    obj = (lv_obj_t*)lv_ta_create(parent, NULL);
    if( !obj )
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    return qvm_undefined;
}

qvm_value_t html_obj_heading_h3(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }
    obj = (lv_obj_t*)lv_ta_create(parent, NULL);
    if( !obj )
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    return qvm_undefined;
}

qvm_value_t html_obj_heading_h4(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }
    obj = (lv_obj_t*)lv_ta_create(parent, NULL);
    if( !obj )
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    return qvm_undefined;
}

qvm_value_t html_obj_heading_h5(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }
    obj = (lv_obj_t*)lv_ta_create(parent, NULL);
    if( !obj )
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    return qvm_undefined;
}

qvm_value_t html_obj_heading_h6(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }
    obj = (lv_obj_t*)lv_ta_create(parent, NULL);
    if( !obj )
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    return qvm_undefined;
}

qvm_value_t html_obj_heading_h1_set_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        lv_label_set_text(obj, qvm_to_string(e, v[0]));
    }else if ( argc >= 1 && qvm_is_integer(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        char text[32];
        sprintf(text, "%d", qvm_to_int32(e, v[0]));
        lv_ta_set_text(obj, text);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_heading_h2_set_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        lv_label_set_text(obj, qvm_to_string(e, v[0]));
    }else if ( argc >= 1 && qvm_is_integer(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        char text[32];
        sprintf(text, "%d", qvm_to_int32(e, v[0]));
        lv_label_set_text(obj, text);
    }
    return qvm_undefined;
}


qvm_value_t html_obj_heading_h3_set_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        lv_label_set_text(obj, qvm_to_string(e, v[0]));
    }else if ( argc >= 1 && qvm_is_integer(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        char text[32];
        sprintf(text, "%d", qvm_to_int32(e, v[0]));
        lv_label_set_text(obj, text);
    }
    return qvm_undefined;
}


qvm_value_t html_obj_heading_h4_set_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        lv_label_set_text(obj, qvm_to_string(e, v[0]));
    }else if ( argc >= 1 && qvm_is_integer(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        char text[32];
        sprintf(text, "%d", qvm_to_int32(e, v[0]));
        lv_label_set_text(obj, text);
    }
    return qvm_undefined;
}


qvm_value_t html_obj_heading_h5_set_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        lv_label_set_text(obj, qvm_to_string(e, v[0]));
    }else if ( argc >= 1 && qvm_is_integer(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        char text[32];
        sprintf(text, "%d", qvm_to_int32(e, v[0]));
        lv_label_set_text(obj, text);
    }
    return qvm_undefined;
}


qvm_value_t html_obj_heading_h6_set_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        lv_label_set_text(obj, qvm_to_string(e, v[0]));
    }else if ( argc >= 1 && qvm_is_integer(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        char text[32];
        sprintf(text, "%d", qvm_to_int32(e, v[0]));
        lv_label_set_text(obj, text);
    }
    return qvm_undefined;
}

