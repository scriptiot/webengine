#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_TabView(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }

    obj = (lv_obj_t*)lv_slider_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    return qvm_undefined;
}

qvm_value_t html_obj_Tabview_set_currentIndex(qvm_state_t *e, qvm_value_t *p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);
    if ( argc >= 1 && qvm_is_integer(v[0]) ){
        int currentIndex = qvm_to_int32(e, v[0]);
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_Tabview_set_loopon(qvm_state_t *e, qvm_value_t *p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);
    if ( argc >= 1 && qvm_is_string(v[0]) ){
        const char * loopon = qvm_to_string(e, v[0]);
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_Tabview_get_currentIndex(qvm_state_t *e, qvm_value_t *p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    qvm_hash_t key = qvm_to_int32(e, v[1]);
    qvm_add_property(e, v, key, qvm_mk_number(lv_tabview_get_tab_act(obj)));
    return qvm_undefined;
}

qvm_value_t html_obj_Tabview_get_loopon(qvm_state_t *e, qvm_value_t *p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    qvm_hash_t key = qvm_to_int32(e, v[1]);
    return qvm_undefined;
}
