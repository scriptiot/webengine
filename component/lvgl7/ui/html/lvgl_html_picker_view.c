#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_PickerView(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }

    obj = (lv_obj_t*)lv_roller_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    return qvm_undefined;
}

qvm_value_t html_obj_PickerView_get_selected(qvm_state_t *e, qvm_value_t *p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    qvm_hash_t key = qvm_to_int32(e, v[1]);
    qvm_add_property(e, v, key, qvm_mk_number(lv_roller_get_selected(obj)));
    return qvm_undefined;
}

qvm_value_t html_obj_PickerView_set_selected(qvm_state_t *e, qvm_value_t *p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);
    uint16_t selected = 0;
    if ( argc >= 1 && qvm_is_string(v[0]) ){
        selected = (uint16_t)atoi(qvm_to_string(e, v[0]));
    }else if (argc >= 1 && qvm_is_integer(v[0])){
        selected = qvm_to_int32(e, v[0]);
    }
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    lv_roller_set_selected(obj, selected, false);
    return qvm_undefined;
}

qvm_value_t html_obj_PickerView_set_options(qvm_state_t *e, qvm_value_t *p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);
    if ( argc >= 1 && qvm_is_array(v[0]) ){
        qvm_value_t * option = v;
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);

        int count = qvm_get_array_length(e, (option);
        int len = 0;
        for(int i=0; i < count; i++){
            len += qvm_string_length(e, qvm_get_array(e, option, i));
        }

        char options[len + count];
        memset(options, 0, len + count);
        for(int i=0; i < count; i++){
            strcat(options, qvm_to_string(e, qvm_get_array(e, option, i)));
            strcat(options, "\n");
        }
        options[len + count - 1] = 0;
        lv_roller_set_options(obj, options);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_PickerView_set_visible_count(qvm_state_t *e, qvm_value_t *p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);
    uint16_t count = 0;
    if ( argc >= 1 && qvm_is_string(v[0]) ){
        count = (uint16_t)atoi(qvm_to_string(e, v[0]));
    }else if (argc >= 1 && qvm_is_integer(v[0])){
        count = qvm_to_int32(e, v[0]);
    }
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    lv_roller_set_visible_row_count(obj, count);
    return qvm_undefined;
}
