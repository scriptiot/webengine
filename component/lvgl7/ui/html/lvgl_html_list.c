#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_definition_list(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }

    obj = (lv_obj_t*)lv_cont_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    return qvm_undefined;
}

qvm_value_t html_obj_definition_term(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }

    obj = (lv_obj_t*)lv_cont_create(parent, NULL);
    if( !obj )
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    return qvm_undefined;
}

qvm_value_t html_obj_definition_description(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }

    obj = (lv_obj_t*)lv_cont_create(parent, NULL);
    if( !obj )
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    return qvm_undefined;
}

qvm_value_t html_obj_definition_set_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        lv_obj_t *text_obj = lv_ta_create(obj, NULL);
        lv_label_set_text(text_obj, qvm_to_string(e, v[0]));
    }
    return qvm_undefined;
}
