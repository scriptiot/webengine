#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_Image(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }

    obj = (lv_obj_t*)lv_img_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    return qvm_undefined;
}

qvm_value_t html_obj_Image_set_src(qvm_state_t *e, qvm_value_t *p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);
    qvm_value_t * src = v;
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    if( argc >= 1 && qvm_is_string(src) ){
        const char * filename = qvm_to_string(e, src);
        if (strstr(filename, ".bin") || strstr(filename, ".png")){
            lv_img_set_src(obj, filename);
        }
    }
    return qvm_undefined;
}

qvm_value_t html_obj_Image_get_src(qvm_state_t *e, qvm_value_t *p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    qvm_hash_t key = qvm_to_int32(e, v[1]);

    const char * src = lv_img_get_file_name(obj);

    qvm_add_property(e, v, key, *evm_heap_string_create(e, src, 0));
    return qvm_undefined;
}
