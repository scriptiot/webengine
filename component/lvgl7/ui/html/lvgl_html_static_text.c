#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_static_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1 && p) {
        parent = we_html_object_get_obj_data(p);
    }

    obj = (lv_obj_t*)lv_label_create(parent, NULL);
    if( !obj )
        return qvm_undefined;

    if( argc >= 1 && qvm_is_string(v[0]) ){
        lv_label_set_text(obj, qvm_to_string(e, v[0]));
    }
    return qvm_undefined;
}
