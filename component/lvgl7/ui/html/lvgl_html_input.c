#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_Input(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }

    obj = (lv_obj_t*)lv_btn_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    return qvm_undefined;
}
