#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"
#include "qvm_conf.h"

#if LV_USE_GIF
#include "lv_lib_gif/lv_gif.h"
#endif

qvm_value_t html_obj_ImageButton(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }
	
#if LV_USE_GIF
    qvm_value_t *attributes = qvm_get_property(e, p, "attributes", 0);
    qvm_value_t *prop;
    char full_path[128];
    const char *path;
    if( attributes ) {
        prop = qvm_get_property(e, attributes, "src", 0);
        if( prop && qvm_is_string(prop) ) {
            path = qvm_to_string(e, prop);
            if( strstr(path, ".gif") ) {
                sprintf(full_path, "C:/%s/%s", we_get_root_dir(), path);
                obj = lv_gif_create_from_file(parent, full_path);
                if( !obj )
                    return qvm_undefined;
                we_html_object_set_obj_data(p, obj);
                lvgl_style_utils_add(obj, p);
                lvgl_html_basic_add_component(e, obj, p);
                return qvm_undefined;
            }
        }
    }
#endif

    obj = (lv_obj_t*)lv_imgbtn_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    lv_style_t * style = lvgl_style_utils_add(obj, p);
    lvgl_html_basic_add_component(e, obj, p);

    lv_style_set_text_color(style, LV_STATE_DEFAULT, lv_color_make(0x00, 0x00, 0x00));
    lv_style_set_image_recolor(style, LV_STATE_DEFAULT, lv_color_make(0xff, 0xff, 0xff));
    lv_style_set_image_recolor_opa(style, LV_STATE_DEFAULT, 0);
    lv_style_set_image_opa(style, LV_STATE_DEFAULT, 255);

    //Write style state: LV_STATE_PRESSED for style_home_imgbtncopy_main
        lv_style_set_text_color(style, LV_STATE_PRESSED, lv_color_make(0xFF, 0x33, 0xFF));
        lv_style_set_image_recolor(style, LV_STATE_PRESSED, lv_color_make(0x00, 0x00, 0x00));
        lv_style_set_image_recolor_opa(style, LV_STATE_PRESSED, 0);

        //Write style state: LV_STATE_CHECKED for style_home_imgbtncopy_main
        lv_style_set_text_color(style, LV_STATE_CHECKED, lv_color_make(0xFF, 0x33, 0xFF));
        lv_style_set_image_recolor(style, LV_STATE_CHECKED, lv_color_make(0x00, 0x00, 0x00));
        lv_style_set_image_recolor_opa(style, LV_STATE_CHECKED, 0);

    return qvm_undefined;
}

qvm_value_t html_obj_ImageButton_set_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v, lv_btn_state_t state)
{
    QVM_UNUSED(e);
    qvm_value_t * src = v;
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    if( argc >= 1 && qvm_is_string(src[0]) ){
        char path[128];
        const char * filename = qvm_to_string(e, src[0]);
        if (strstr(filename, ".bin") || strstr(filename, ".png")){
            if(check_file_isvaild(filename,path) == LV_FS_RES_OK)
            {
                lv_imgbtn_set_src(obj, state, path);
            }
        }
    }
    return qvm_undefined;
}

qvm_value_t html_obj_ImageButton_set_pressed_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    return html_obj_ImageButton_set_src(e, p, argc, v, LV_BTN_STATE_PRESSED);
}

qvm_value_t html_obj_ImageButton_set_released_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    return html_obj_ImageButton_set_src(e, p, argc, v, LV_BTN_STATE_RELEASED);
}

qvm_value_t html_obj_ImageButton_set_disabled_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    return html_obj_ImageButton_set_src(e, p, argc, v, LV_BTN_STATE_DISABLED);
}

qvm_value_t html_obj_ImageButton_set_checked_pressed_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    return html_obj_ImageButton_set_src(e, p, argc, v, LV_BTN_STATE_CHECKED_PRESSED);
}

qvm_value_t html_obj_ImageButton_set_checked_released_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    return html_obj_ImageButton_set_src(e, p, argc, v, LV_BTN_STATE_CHECKED_RELEASED);
}

qvm_value_t html_obj_ImageButton_set_checked_disabled_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    return html_obj_ImageButton_set_src(e, p, argc, v, LV_BTN_STATE_CHECKED_DISABLED);
}

