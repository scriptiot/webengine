#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"
#include "lvgl/src/lv_misc/lv_debug.h"
#include "lvgl/src/lv_misc/lv_math.h"
#include "lvgl/src/lv_draw/lv_draw.h"
#include "lvgl/src/lv_core/lv_refr.h"
#include "lvgl/src/lv_themes/lv_theme.h"

static qvm_value_t html_canvas_destroy(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    QVM_UNUSED(argc);
    QVM_UNUSED(v);
    we_html_canvas_t *obj = (we_html_canvas_t *)we_html_object_get_user_data(p);
    if( !obj )
        return qvm_undefined;
    if( obj->destroy ) {
        obj->destroy(obj);
    }
    we_free(obj);
    return qvm_undefined;
}

static void stroke(we_html_canvas_t *canvas) {
    lv_obj_invalidate(canvas->obj);
}

qvm_value_t html_obj_Canvas(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }
    obj = (lv_obj_t*)lv_canvas_create(parent, NULL);
    if( !obj )
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    we_html_object_set_destroy(p, html_canvas_destroy);
    lvgl_style_utils_add(obj, p);
    lvgl_html_basic_add_component(e, obj, p);
    we_html_canvas_t * canvas = we_canvas_init(e, p, LV_COLOR_DEPTH, stroke);
    QVM_ASSERT(canvas);
    lv_canvas_set_buffer(obj, canvas->buffer, canvas->w, canvas->h, LV_IMG_CF_TRUE_COLOR);
    return qvm_undefined;
}
