#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"
#include "graphics/we_graphics.h"

static qvm_value_t html_canvas3d_init(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    we_html_canvas_t *obj = (we_html_canvas_t *)we_get_user_data(e,p);
    lv_coord_t w = (lv_coord_t)qvm_to_int32(e, v[0]);
    lv_coord_t h = (lv_coord_t)qvm_to_int32(e, v[1]);
    if( obj->buffer ) {
        we_free(obj->buffer);
    }
    obj->buffer = (lv_color_t*)we_malloc(w * h * LV_COLOR_DEPTH / 8);
    if( !obj->buffer ){
        printf("failed ot create canvas buffer\n");
    }
    we_html_object_set_user_data(p, obj->buffer);
    lv_canvas_set_buffer(obj->obj, obj->buffer, w, h, LV_IMG_CF_TRUE_COLOR);
    we_graphics_3d_init(e, w, h, LV_COLOR_DEPTH, obj->buffer);
    return qvm_undefined;
}

static qvm_value_t html_canvas3d_stroke(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    we_html_canvas_t *obj = (we_html_canvas_t *)we_get_user_data(e,p);
    lv_obj_invalidate(obj->obj);
    return qvm_undefined;
}

qvm_value_t html_obj_canvas3d(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }
    obj = (lv_obj_t*)lv_canvas_create(parent, NULL);
    if( !obj )
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    lvgl_style_utils_add(obj, p);
    lvgl_html_basic_add_component(e, obj, p);

    qvm_value_t context = qvm_new_object(e);
    we_html_canvas_t *html_canvas = we_malloc(sizeof(we_html_canvas_t));
    html_canvas->obj = obj;
    html_canvas->buffer = NULL;
    qvm_add_property(e, context, "init", qvm_mk_native(html_canvas3d_init));
    qvm_add_property(e, context, "stroke", qvm_mk_native(html_canvas3d_stroke));
    qvm_add_property(e, p, "context", context);
    we_set_user_data(e, context, html_canvas);
    return qvm_undefined;
}
