#ifndef EUI_FONT_H
#define EUI_FONT_H

#include <stdint.h>

typedef struct eui_font_t
{
    const uint8_t *table;
    uint16_t width;
    uint16_t height;
} eui_font_t;

extern eui_font_t Font24x32;
extern eui_font_t Font16x24;
extern eui_font_t Font8x16;

#endif
