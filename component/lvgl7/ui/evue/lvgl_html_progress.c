#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"

qvm_value_t html_obj_Progress(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }

    obj = (lv_obj_t*)lv_bar_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    lvgl_style_utils_add(obj, p);
    lvgl_html_basic_add_component(e, obj, p);
    lv_bar_set_range(obj, 0, 100);
    return qvm_undefined;
}

qvm_value_t html_obj_Progress_set_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(e);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    if( argc >= 1 && qvm_is_integer(v[0]) && obj ){
        int value = qvm_to_int32(e, v[0]);
        lv_bar_set_value(obj, value, LV_ANIM_ON);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_Progress_get_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);

    if (obj == NULL) return qvm_undefined;

    qvm_hash_t key = qvm_to_int32(e, v[1]);

    int16_t value = lv_bar_get_value(obj);

//    qvm_add_property(e, v[0], key, qvm_mk_number(value));
    return qvm_undefined;
}

qvm_value_t html_obj_Progress_set_start_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(e);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    if( argc >= 1 && qvm_is_integer(v[0]) && obj ){
        int value = qvm_to_int32(e, v[0]);
        lv_bar_set_start_value(obj, value, LV_ANIM_ON);
    }

    return qvm_undefined;
}

qvm_value_t html_obj_Progress_get_start_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);

    if (obj == NULL) return qvm_undefined;

    qvm_hash_t key = qvm_to_int32(e, v[1]);

    int16_t value = lv_bar_get_start_value(obj);

//    qvm_add_property(e, v[0], key, qvm_mk_number(value));
    return qvm_undefined;
}

qvm_value_t html_obj_Progress_set_duration(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(e);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    if( argc >= 1 && qvm_is_integer(v[0]) && obj ){
        int value = qvm_to_int32(e, v[0]);
        lv_bar_set_anim_time(obj, value);
    }

    return qvm_undefined;
}

qvm_value_t html_obj_Progress_get_duration(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);

    if (obj == NULL) return qvm_undefined;

    qvm_hash_t key = qvm_to_int32(e, v[1]);

    int16_t value = lv_bar_get_min_value(obj);

//    qvm_add_property(e, v[0], key, qvm_mk_number(value));
    return qvm_undefined;
}
