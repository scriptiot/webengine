#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"
#include "page/we_page.h"

typedef struct html_tabview_t {
    lv_point_t *points;
    uint16_t size;
} html_tabview_t;

qvm_value_t html_obj_TabView(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }

    obj = (lv_obj_t*)lv_tileview_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;

    we_html_object_set_obj_data(p, obj);
    lvgl_style_utils_add(obj, p);
    lvgl_html_basic_add_component(e, obj, p);

    html_tabview_t *info = we_malloc(sizeof(html_tabview_t));
    if( !info )
        return qvm_undefined;
    memset(info, 0, sizeof(html_tabview_t));
    we_html_object_set_user_data(p, info);
    lv_tileview_set_edge_flash(obj, true);
    return qvm_undefined;
}
