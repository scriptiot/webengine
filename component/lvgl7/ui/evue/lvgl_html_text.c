#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"

qvm_value_t html_obj_Text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }

    obj = (lv_obj_t*)lv_label_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    lvgl_style_utils_add(obj, p);
    lvgl_html_basic_add_component(e, obj, p);
    lv_label_set_long_mode(obj, LV_LABEL_LONG_BREAK);
    return qvm_undefined;
}

qvm_value_t html_obj_Text_set_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        lv_label_set_text(obj, qvm_to_string(e, v[0]));
    } else if ( argc >= 1 && qvm_is_integer(v[0]) ){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        char text[32];
        sprintf(text, "%d", qvm_to_int32(e, v[0]));
        lv_label_set_text(obj, text);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_Text_get_text(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    const char *key = qvm_to_string(e, v[1]);
    char * text = lv_label_get_text(obj);
    qvm_add_property(e, v[0], key, qvm_new_string(e, text));
    return qvm_undefined;
}
