#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"

qvm_value_t html_obj_Option(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    lv_obj_t *parent = NULL;
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    } else {
        return qvm_undefined;
    }
    qvm_value_t value = qvm_get_property(e, p, "value");
    const char *option = qvm_to_string(e, value);
    int pos = lv_dropdown_get_option_cnt(parent);
    lv_dropdown_add_option(parent, option, pos);
    return qvm_undefined;
}
