#ifndef LVGL_HTML_BASIC_H
#define LVGL_HTML_BASIC_H

#include "we_gui.h"
#include "lvgl.h"

int lvgl_html_basic_is_obj_type(lv_obj_t * obj, const char* type_name);
void lvgl_html_basic_add_component(qvm_state_t *e, lv_obj_t * obj, qvm_value_t html_object);
void lvgl_html_basic_set_destroy(qvm_state_t *e, qvm_value_t *p, qvm_native_t destroy);
void lvgl_html_run_callback(qvm_state_t * e, lv_obj_t * obj, qvm_value_t index_object, const char * name, int argc, qvm_value_t * args);
#endif
