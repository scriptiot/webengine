#include "ui/evue/lvgl_html_basic.h"
#include "page/we_page.h"

int lvgl_html_basic_is_obj_type(lv_obj_t * obj, const char* type_name)
{
    lv_obj_type_t type;
    lv_obj_get_type(obj, &type);
    uint8_t cnt;
    for(cnt = 0; cnt < LV_MAX_ANCESTOR_NUM; cnt++) {
        if(type.type[cnt] == NULL) break;
        if(!strcmp(type.type[cnt], type_name)) return true;
    }
    return false;
}


void lvgl_html_run_callback(qvm_state_t * e, lv_obj_t * obj, qvm_value_t index_object, const char * name, int argc, qvm_value_t * args){
    QVM_UNUSED(obj);
    qvm_value_t m_clicked_fn = qvm_get_property(e, index_object, name);
    qvm_call(e, m_clicked_fn, index_object, argc, args);
}

void lvgl_html_basic_handler(lv_obj_t * obj, lv_signal_t event)
{
    if( !lv_indev_is_dragging(lv_indev_get_act()) &&
            event != LV_EVENT_PRESSED  &&
            event != LV_EVENT_RELEASED  &&
            event != LV_EVENT_PRESSING  &&
            event != LV_EVENT_CLICKED ) {
        return;
    }
    qvm_state_t * e = we_get_runtime();

    qvm_value_t html_object = we_page_manager_find_component(e, obj);
    qvm_value_t def = we_page_get_current_default(e);

    if( qvm_is_undefined(html_object) )
        return;

    qvm_value_t attributes = qvm_get_property(e, html_object, "attributes");
    if( qvm_is_undefined(attributes) )
        return;

    qvm_value_t onclick = qvm_get_property(e, attributes, "onclick");
    qvm_value_t onmousedown = qvm_get_property(e, attributes, "onmousedown");
    qvm_value_t onmouseup = qvm_get_property(e, attributes, "onmouseup");

    qvm_value_t args[5];
    args[0] = html_object;
    lv_point_t globalClickPoint;
    lv_indev_get_point(lv_indev_get_act(), &globalClickPoint);
    lv_coord_t x = globalClickPoint.x;
    x = obj->coords.x1;
    lv_coord_t y = globalClickPoint.y;
    y = obj->coords.y1;
    args[1] = qvm_mk_number(globalClickPoint.x - x);
    args[2] = qvm_mk_number(globalClickPoint.y - y);

    switch (event) {
        case LV_EVENT_DRAG_BEGIN: {
            qvm_value_t on_event = qvm_get_property(e, attributes, "ondragstart");
            if (qvm_is_string(on_event) ){
                lvgl_html_run_callback(e, obj, def, qvm_to_string(e, on_event), 3, args);
            }
        }break;
        case LV_EVENT_DRAG_END: {
            qvm_value_t on_event = qvm_get_property(e, attributes, "ondragend");
            if (qvm_is_string(on_event)){
                lvgl_html_run_callback(e, obj, def, qvm_to_string(e, on_event), 3, args);
            }
        }break;
        case LV_EVENT_PRESSING: {
            qvm_value_t on_event = qvm_get_property(e, attributes, "onmousemove");
            if (qvm_is_string(on_event)){
                lvgl_html_run_callback(e, obj, def, qvm_to_string(e, on_event), 3, args);
            }
        }break;
        case LV_EVENT_PRESSED:{
            if (qvm_is_string(onmousedown)){
                lvgl_html_run_callback(e, obj, def, qvm_to_string(e, onmousedown), 3, args);
            }
            break;
        }
        case LV_EVENT_RELEASED:{
            if (qvm_is_string(onmouseup)){
                lvgl_html_run_callback(e, obj, def, qvm_to_string(e, onmouseup), 3, args);
            }
            break;
        }
        case LV_EVENT_CLICKED:{
            if (qvm_is_string(onclick)){
                lvgl_html_run_callback(e, obj, def, qvm_to_string(e, onclick), 3, args);
            }
            break;
        }
        default:
        break;
    }
}

void lvgl_html_basic_add_component(qvm_state_t *e, lv_obj_t * obj, qvm_value_t html_object) {

    qvm_value_t attributes = qvm_get_property(e, html_object, "attributes");
    if( qvm_is_undefined(attributes) )
        return;

    qvm_value_t onclick = qvm_get_property(e, attributes, "onclick");
    qvm_value_t onmousedown = qvm_get_property(e, attributes, "onmousedown");
    qvm_value_t onmouseup = qvm_get_property(e, attributes, "onmouseup");
    qvm_value_t ondrag = qvm_get_property(e, attributes, "ondrag");

    if ( qvm_is_undefined(onclick ) || qvm_is_undefined(onmousedown ) || qvm_is_undefined(onmouseup ) || qvm_is_undefined(ondrag ) ){
        lv_obj_set_click(obj, true);
        lv_obj_set_event_cb(obj, lvgl_html_basic_handler);
    }else{
        lv_obj_set_parent_event(obj, true);
    }
    we_page_manager_add_component(e, obj, html_object);
    we_html_object_set_destroy(html_object, lvgl_html_basic_destroy);
}

qvm_value_t lvgl_html_basic_show(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    QVM_UNUSED(argc);
    QVM_UNUSED(v);
    lv_obj_t * obj = NULL;
    obj = we_html_object_get_obj_data(p);
    if(obj){
        lv_obj_set_parent(obj, lv_scr_act());
        lv_obj_set_hidden(obj, false);
    }
    return qvm_undefined;
}

qvm_value_t lvgl_html_basic_hide(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    QVM_UNUSED(argc);
    QVM_UNUSED(v);
    lv_obj_t * obj = NULL;
    obj = we_html_object_get_obj_data(p);
    if(obj){
        lv_obj_set_hidden(obj, true);
    }
    return qvm_undefined;
}

qvm_value_t lvgl_html_basic_destroy(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    QVM_UNUSED(argc);
    QVM_UNUSED(v);
    lv_obj_t *obj = we_html_object_get_obj_data(p);
    if(obj){
        lv_obj_del(obj);
    }
    return qvm_undefined;
}

void we_gui_destroy_all(void) {
    lv_obj_clean(lv_scr_act());
}
