#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"

qvm_value_t html_obj_List(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }
    obj = (lv_obj_t*)lv_page_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    lvgl_style_utils_add(obj, p);
    lvgl_html_basic_add_component(e, obj, p);
    lv_page_set_scrollable_fit2(obj, LV_FIT_PARENT, LV_FIT_TIGHT);
    lv_page_set_scrl_layout(obj, LV_LAYOUT_CENTER);
    lv_page_set_scrollbar_mode(obj, LV_SCROLLBAR_MODE_DRAG);
    lv_page_set_edge_flash(obj, true);
    return qvm_undefined;
}
