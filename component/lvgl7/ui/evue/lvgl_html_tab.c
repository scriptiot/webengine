#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"

typedef struct html_tabview_t {
    lv_point_t *points;
    uint16_t size;
} html_tabview_t;

qvm_value_t html_obj_Tab(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    if( argc < 1 ) {
        return qvm_undefined;
    }
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    qvm_value_t name = qvm_get_property(e, v[0], "name");
    const char *nodename = qvm_to_string(e, name);
    parent = we_html_object_get_obj_data(v[0]);
    html_tabview_t *info = we_html_object_get_user_data(v[0]);
    if( !strcmp(nodename, "tabview") && info ) {
        obj = lv_cont_create(parent, NULL);
        if( !obj )
            return qvm_undefined;
        we_html_object_set_obj_data(p, obj);
        lvgl_style_utils_add(obj, p);
        lvgl_html_basic_add_component(e, obj, p);
        lv_obj_set_size(obj, LV_HOR_RES, LV_VER_RES);
        lv_obj_set_pos(obj, LV_HOR_RES * info->size, 0);
        if( info->size == 0 ){
            lv_point_t *points = we_malloc(sizeof(lv_point_t));
            info->points = points;
            points->x = 0;
            points->y = 0;
            info->size++;
        } else {
            info->size++;
            lv_point_t *points = we_malloc(sizeof(lv_point_t) * (info->size) );
            for(int i = 0; i < info->size; i++){
                points[i].x = i;
                points[i].y = 0;
            }
            we_free(info->points);
            info->points = points;
        }
        lv_tileview_set_valid_positions(parent, info->points, info->size);
        lv_tileview_add_element(parent, obj);

    }
    return qvm_undefined;
}
