#include "ui/evue/lvgl_html_basic.h"
#include "page/we_page.h"
#include "style/lvgl_style_utils.h"

void lvgl_slider_handler(lv_obj_t * obj, lv_signal_t event)
{
    if( !lv_indev_is_dragging(lv_indev_get_act()) &&
            event != LV_EVENT_PRESSED  &&
            event != LV_EVENT_RELEASED  &&
            event != LV_EVENT_PRESSING  &&
            event != LV_EVENT_CLICKED ) {
        return;
    }
    qvm_state_t * e = we_get_runtime();

    qvm_value_t html_object = we_page_manager_find_component(e, obj);
    if( qvm_is_undefined(html_object) )
        return;

    qvm_value_t attributes = qvm_get_property(e, html_object, "attributes");
    if( qvm_is_undefined(attributes) )
        return;

    qvm_value_t onclick = qvm_get_property(e, attributes, "onchange");

    qvm_value_t args[1];
    args[0] = qvm_mk_number( lv_slider_get_value(obj) );

    switch (event) {
        case LV_EVENT_CLICKED:
        case LV_EVENT_PRESSING:
        case LV_EVENT_PRESSED:{
            if (qvm_is_string(onclick)){
                lvgl_html_run_callback(e, obj, we_page_get_current_default(e), qvm_to_string(e, onclick), 1, args);
            }
            break;
        }
        default:
        break;
    }
}

qvm_value_t html_obj_Slider(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }

    obj = (lv_obj_t*)lv_slider_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;

    qvm_value_t attributes = qvm_get_property(e, p, "attributes");
    qvm_value_t min = qvm_get_property(e, attributes, "min");
    qvm_value_t max = qvm_get_property(e, attributes, "max");
    qvm_value_t value = qvm_get_property(e, attributes, "value");

    lv_slider_set_range(obj, lv_slider_get_min_value(obj), 100);

    if (qvm_is_number(min)){
        lv_slider_set_range(obj, (lv_coord_t)qvm_to_int32(e, min), lv_slider_get_max_value(obj));
    }

    if ( qvm_is_number(max)){
        int32_t _max = qvm_to_int32(e, max);
        if (_max == lv_slider_get_min_value(obj)){
            _max = _max + 1;
        }
        lv_slider_set_range(obj, lv_slider_get_min_value(obj), (lv_coord_t)_max);
    }

    if (qvm_is_number(value)){
        lv_slider_set_value(obj, (lv_coord_t)qvm_to_int32(e, value), LV_ANIM_ON);
    }

    we_html_object_set_obj_data(p, obj);
    lvgl_style_utils_add(obj, p);
    lvgl_html_basic_add_component(e, obj, p);
    lv_obj_set_event_cb(obj, lvgl_slider_handler);
    return qvm_undefined;
}

qvm_value_t html_obj_Slider_set_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(e);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    if( argc >= 1 && obj ){
        int value = qvm_to_int32(e, v[0]);
        lv_slider_set_value(obj, (lv_coord_t)value, LV_ANIM_ON);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_Slider_get_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);

    if (obj == NULL) return qvm_undefined;

    const char *key = qvm_to_string(e, v[1]);

    int16_t value = lv_slider_get_value(obj);

    qvm_add_property(e, v[0], key, qvm_mk_number(value));
    return qvm_undefined;
}

qvm_value_t html_obj_Slider_set_start_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(e);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    if( argc >= 1 && obj ){
        int value = qvm_to_int32(e, v[0]);
        lv_slider_set_value(obj, (lv_coord_t)value, LV_ANIM_ON);
    }

    return qvm_undefined;
}

qvm_value_t html_obj_Slider_get_start_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);

    if (obj == NULL) return qvm_undefined;

    const char *key = qvm_to_string(e, v[1]);

    int16_t value = lv_slider_get_value(obj);

    qvm_add_property(e, v[0], key, qvm_mk_number(value));
    return qvm_undefined;
}

qvm_value_t html_obj_Slider_set_duration(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(e);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    if( argc >= 1 && obj ){
        int value = qvm_to_int32(e, v[0]);
        lv_slider_set_anim_time(obj, (lv_coord_t)value);
    }

    return qvm_undefined;
}

qvm_value_t html_obj_Slider_get_duration(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);

    if (obj == NULL) return qvm_undefined;

    const char *key = qvm_to_string(e, v[1]);

    int16_t value = lv_slider_get_anim_time(obj);

    qvm_add_property(e, v[0], key, qvm_mk_number(value));
    return qvm_undefined;
}
