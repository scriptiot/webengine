#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"
#include "page/we_page.h"

void lvgl_html_select_handler(lv_obj_t * obj, lv_signal_t event)
{
    qvm_state_t * e = we_get_runtime();
    qvm_value_t html_object = we_page_manager_find_component(e, obj);
    qvm_value_t index_object = we_page_get_current_index_object(e);

    if( qvm_is_undefined(html_object) )
        return;

    qvm_value_t attributes = qvm_get_property(e, html_object, "attributes");
    if( qvm_is_undefined(attributes) )
        return;

    qvm_value_t indexChanged = qvm_get_property(e, attributes, "indexChanged");

    if ( qvm_is_undefined(indexChanged))
        return;

    qvm_value_t args[2];
    args[0] = html_object;
    uint16_t currentIndex = lv_dropdown_get_selected(obj);
    args[1] = qvm_mk_number(currentIndex);

    if (event == LV_EVENT_VALUE_CHANGED){
        lvgl_html_run_callback(e, obj, index_object, qvm_to_string(e, indexChanged), 2, args);
    }
}

qvm_value_t  html_obj_Select(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }
    obj = (lv_obj_t*)lv_dropdown_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;

    lv_dropdown_clear_options(obj);

    we_html_object_set_obj_data(p, obj);
    lvgl_style_utils_add(obj, p);
    lvgl_html_basic_add_component(e, obj, p);
    return qvm_undefined;
}
