#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"
#include "ui/evue/eui_font.h"

typedef struct html_eui_t {
    lv_obj_t *obj;
    lv_color_t *buffer;
    lv_coord_t x;
    lv_coord_t y;
    lv_coord_t w;
    lv_coord_t h;
    lv_coord_t x1;
    lv_coord_t y1;
    lv_coord_t x2;
    lv_coord_t y2;
}html_eui_t;

static lv_color_t _canvas_color;

typedef lv_color_t eui_color_t;
typedef lv_coord_t eui_coord_t;

static void eui_set_clip_rect(html_eui_t *obj, eui_coord_t x1, eui_coord_t y1, eui_coord_t x2, eui_coord_t y2) {
    obj->x1 = x1;
    obj->y1 = y1;
    obj->x2 = x2;
    obj->y2 = y2;
}

static void eui_set_point(html_eui_t *obj, eui_coord_t x, eui_coord_t y, eui_color_t c) {
    if( (x >= obj->x1 && x <= obj->x2) && (y >= obj->y1 && y <= obj->y2) )
        lv_canvas_set_px(obj->obj, x, y, c);
}

static eui_color_t eui_get_point(html_eui_t *obj, eui_coord_t x, eui_coord_t y) {
    return lv_canvas_get_px(obj->obj, x, y);
}

static void eui_draw_line(html_eui_t *obj, eui_coord_t x1, eui_coord_t y1, eui_coord_t x2, eui_coord_t y2, eui_color_t color) {
    int t;
    int xerr=0,yerr=0,delta_x,delta_y,distance;
    eui_coord_t incx,incy,uRow,uCol;

    delta_x=x2-x1;
    delta_y=y2-y1;
    uRow=x1;
    uCol=y1;

    if(delta_x>0) incx=1;
    else if(delta_x==0)incx=0;
    else
    {
        incx = -1;
        delta_x = -delta_x;
    }
    if(delta_y>0)incy=1;
    else if(delta_y==0)incy=0;
    else
    {
        incy=-1;
        delta_y = -delta_y;
    }
    if( delta_x>delta_y)distance=delta_x;
    else distance=delta_y;

    for(t=0;t<=distance+1;t++ )
    {
        eui_set_point(obj, uRow, uCol, color);
        xerr = xerr + delta_x ;
        yerr = yerr + delta_y ;
        if(xerr>distance)
        {
            xerr = xerr - distance;
            uRow = uRow + incx;
        }
        if(yerr>distance)
        {
            yerr = yerr - distance;
            uCol = uCol + incy;
        }
    }
}

static void eui_fill_rect(html_eui_t *obj, eui_coord_t x, eui_coord_t y, eui_coord_t w, eui_coord_t h, eui_color_t color) {
    for(eui_coord_t i = 0; i < h; i++) {
        eui_draw_line(obj, x, y + i, x + w - 1, y + i, color);
    }
}

static void eui_draw_rect(html_eui_t *obj, eui_coord_t x1, eui_coord_t y1, eui_coord_t x2, eui_coord_t y2, eui_color_t color)
{
    int i;
    for(i=0;i<=x2-x1;i++)
    {
        eui_set_point(obj, x1 + i, y1, color);
        eui_set_point(obj, x1 + i, y2, color);
    }

    for(i=0;i<=y2-y1;i++)
    {
        eui_set_point(obj, x1,y1+i,color);
        eui_set_point(obj, x2,y1+i,color);
    }
}

static void eui_draw_circle(html_eui_t *obj, eui_coord_t xc, eui_coord_t yc, eui_coord_t r, eui_color_t c)
{
    eui_coord_t x = 0, y = r, yi, d;
    d = 3 - 2 * r;
    while (x <= y)
    {
        eui_set_point(obj, xc + x, yc + y, c);
        eui_set_point(obj, xc - x, yc + y, c);
        eui_set_point(obj, xc + x, yc - y, c);
        eui_set_point(obj, xc - x, yc - y, c);
        eui_set_point(obj, xc + y, yc + x, c);
        eui_set_point(obj, xc - y, yc + x, c);
        eui_set_point(obj, xc + y, yc - x, c);
        eui_set_point(obj, xc - y, yc - x, c);
        if (d < 0)
        {
            d = d + 4 * x + 6;
        }
        else
        {
            d = d + 4 * (x - y) + 10;
            y --;
        }
        x++;
    }

}

static void eui_fill_circle(html_eui_t *obj, eui_coord_t xc, eui_coord_t yc, eui_coord_t r, eui_color_t c)
{
    eui_coord_t x = 0, y = r, yi, d;
    d = 3 - 2 * r;
    while (x <= y)
    {
        for (yi = x; yi <= y; yi ++)
        {
            eui_set_point(obj, xc + x, yc + yi, c);
            eui_set_point(obj, xc - x, yc + yi, c);
            eui_set_point(obj, xc + x, yc - yi, c);
            eui_set_point(obj, xc - x, yc - yi, c);
            eui_set_point(obj, xc + yi, yc + x, c);
            eui_set_point(obj, xc - yi, yc + x, c);
            eui_set_point(obj, xc + yi, yc - x, c);
            eui_set_point(obj, xc - yi, yc - x, c);
        }

        if (d < 0) {
            d = d + 4 * x + 6;
        } else {
            d = d + 4 * (x - y) + 10;
            y --;
        }
        x++;
    }
}

void eui_draw_char (html_eui_t *obj, uint16_t usX, uint16_t usY, eui_font_t *font, eui_color_t color, const char cChar )
{
    uint8_t  byteCount, bitCount, fontLength;
    uint16_t ucRelativePositon;
    uint8_t *Pfont;

    ucRelativePositon = cChar - ' ';

    fontLength = (font->width * font->height) / 8;

    Pfont = (uint8_t *)&font->table[ucRelativePositon * fontLength];
    byteCount = 0;
    for (int i = 0; i < font->height; i++ ) {
        for (int j = 0; j < font->width / 8; j++ ) {
            for ( bitCount = 0; bitCount < 8; bitCount++ )
            {
                if ( Pfont[byteCount] & (0x80>>bitCount) )
                    eui_set_point (obj, usX + j * 8 + bitCount, usY + i, color );
            }
            byteCount++;
        }
    }
}

void eui_draw_text (html_eui_t *obj, uint16_t usX ,uint16_t usY, eui_font_t *font, eui_color_t color, char * pStr )
{
    while ( * pStr != '\0' )
    {

        eui_draw_char (obj, usX, usY, font, color, *pStr);

        pStr ++;

        usX += font->width;

    }

}

static qvm_value_t html_canvas_init(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    html_eui_t *obj = (html_eui_t *)we_get_user_data(e,p);
    lv_coord_t w = (lv_coord_t)qvm_to_int32(e, v[0]);
    lv_coord_t h = (lv_coord_t)qvm_to_int32(e, v[1]);
    if( obj->buffer ) {
        we_free(obj->buffer);
    }
    obj->buffer = (lv_color_t*)we_malloc(w * h * LV_COLOR_DEPTH / 8);
    we_html_object_set_user_data(p, obj->buffer);
    lv_canvas_set_buffer(obj->obj, obj->buffer, w, h, LV_IMG_CF_TRUE_COLOR);
    obj->x = 0;
    obj->y = 0;
    obj->w = w;
    obj->h = h;

    obj->x1 = 0;
    obj->y1 = 0;
    obj->x2 = w - 1;
    obj->y2 = h - 1;
    return qvm_undefined;
}
//setClipRect(x, y, w, h)
static qvm_value_t html_canvas_setClipRect(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    html_eui_t *obj = (html_eui_t *)we_get_user_data(e,p);
    obj->x1 = (lv_coord_t)qvm_to_int32(e, v[0]);
    obj->y1 = (lv_coord_t)qvm_to_int32(e, v[1]);
    obj->x2 = obj->x1 + (lv_coord_t)qvm_to_int32(e, v[2]) - 1;
    obj->y2 = obj->y1 + (lv_coord_t)qvm_to_int32(e, v[3]) - 1;
    return qvm_undefined;
}

static qvm_value_t html_canvas_destroy(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    QVM_UNUSED(argc);
    QVM_UNUSED(v);
    html_eui_t *obj = (html_eui_t *)we_get_user_data(e,p);
    if( obj->buffer ) {
        we_free(obj->buffer);
        obj->buffer = NULL;
    }
    return qvm_undefined;
}
//rect(x, y, w, h, color)
static qvm_value_t html_canvas_rect(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    html_eui_t *obj = (html_eui_t *)we_get_user_data(e,p);
    lv_color_t color;
    lv_coord_t x1 = (lv_coord_t)qvm_to_int32(e, v[0]);
    lv_coord_t y1 = (lv_coord_t)qvm_to_int32(e, v[1]);
    lv_coord_t x2 = (lv_coord_t)qvm_to_int32(e, v[2]);
    lv_coord_t y2 = (lv_coord_t)qvm_to_int32(e, v[3]);


    if( qvm_is_string(v + 4) ){
        color = lvgl_style_utils_get_color(v + 4);
    } else if(qvm_is_number(v[4]) )
        color.full = (uint16_t)qvm_to_int32(e, v + 4);

    eui_draw_line(obj, x1 , y1, x2, y2, color);
    return qvm_undefined;
}
//fillRect(x, y, w, h, color)
static qvm_value_t html_canvas_fillRect(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    html_eui_t *obj = (html_eui_t *)we_get_user_data(e,p);
    lv_color_t color;
    lv_coord_t x = (lv_coord_t)qvm_to_int32(e, v[0]);
    lv_coord_t y = (lv_coord_t)qvm_to_int32(e, v[1]);
    lv_coord_t w = (lv_coord_t)qvm_to_int32(e, v[2]);
    lv_coord_t h = (lv_coord_t)qvm_to_int32(e, v[3]);

    if( qvm_is_string(v + 4) ){
        color = lvgl_style_utils_get_color(v + 4);
    } else if(qvm_is_number(v[4]) )
        color.full = (uint16_t)qvm_to_int32(e, v + 4);
    eui_fill_rect(obj, x, y, w, h, color);
    return qvm_undefined;
}
//circle(x, y, r, color)
static qvm_value_t html_canvas_circle(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    html_eui_t *obj = (html_eui_t *)we_get_user_data(e,p);
    lv_coord_t x = (lv_coord_t)qvm_to_int32(e, v[0]);
    lv_coord_t y = (lv_coord_t)qvm_to_int32(e, v[1]);
    lv_coord_t r = (lv_coord_t)qvm_to_int32(e, v[2]);
    lv_color_t color;
    if( qvm_is_string(v + 3) ){
        color = lvgl_style_utils_get_color(v + 3);
    } else if(qvm_is_number(v[3]) )
        color.full = (uint16_t)qvm_to_int32(e, v[3]);
    eui_draw_circle(obj, x, y, r, color);
    return qvm_undefined;
}
//fillCircle(x, y, r, color)
static qvm_value_t html_canvas_fillCircle(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    html_eui_t *obj = (html_eui_t *)we_get_user_data(e,p);
    lv_coord_t x = (lv_coord_t)qvm_to_int32(e, v[0]);
    lv_coord_t y = (lv_coord_t)qvm_to_int32(e, v[1]);
    lv_coord_t r = (lv_coord_t)qvm_to_int32(e, v[2]);
    lv_color_t color;
    if( qvm_is_string(v + 3) ){
        color = lvgl_style_utils_get_color(v + 3);
    } else if(qvm_is_number(v[3]) )
        color.full = (uint16_t)qvm_to_int32(e, v[3]);
    eui_fill_circle(obj, x, y, r, color);
    return qvm_undefined;
}
//line(x1, y1, x2, y2, color, width)
static qvm_value_t html_canvas_line(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    html_eui_t *obj = (html_eui_t *)we_get_user_data(e,p);
    lv_color_t color;
    lv_coord_t x1 = (lv_coord_t)qvm_to_int32(e, v[0]);
    lv_coord_t y1 = (lv_coord_t)qvm_to_int32(e, v[1]);

    lv_coord_t x2 = (lv_coord_t)qvm_to_int32(e, v[2]);
    lv_coord_t y2 = (lv_coord_t)qvm_to_int32(e, v[3]);

    if( qvm_is_string(v + 4) ){
        color = lvgl_style_utils_get_color(v + 4);
    } else if(qvm_is_number(v[4]) )
        color.full = (uint16_t)qvm_to_int32(e, v + 4);

    eui_draw_line(obj, x1, y1, x2, y2, color);
    return qvm_undefined;
}

//text(x, y, w, str, align)
static qvm_value_t  html_eui_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    html_eui_t *obj = (html_eui_t *)we_get_user_data(e,p);
    lv_coord_t x = (lv_coord_t)qvm_to_int32(e, v[0]);
    lv_coord_t y = (lv_coord_t)qvm_to_int32(e, v[1]);
    lv_coord_t w = (lv_coord_t)qvm_to_int32(e, v[2]);
    const char *str = qvm_to_string(e, v + 3);
    lv_draw_label_dsc_t dsc;
    lv_draw_label_dsc_init(&dsc);
    dsc.color = _canvas_color;
    lv_label_align_t align = LV_LABEL_ALIGN_CENTER;
    if( argc > 4 && qvm_is_string(v + 4) ) {
        const char *align_str = qvm_to_string(e, v + 4);
        if( !strcmp(align_str, "left") ) {
            align = LV_LABEL_ALIGN_LEFT;
        } else if( !strcmp(align_str, "center") ) {
            align = LV_LABEL_ALIGN_CENTER;
        } else if( !strcmp(align_str, "right") ) {
            align = LV_LABEL_ALIGN_RIGHT;
        } else if( !strcmp(align_str, "auto") ) {
            align = LV_LABEL_ALIGN_AUTO;
        }
    }

    eui_draw_text(obj, x, y, &Font8x16, _canvas_color, str);
    return qvm_undefined;
}

static qvm_value_t  html_canvas_setColor(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    _canvas_color = lvgl_style_utils_get_color(v[0]);
    return qvm_undefined;
}

qvm_value_t html_obj_eui(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }
    obj = (lv_obj_t*)lv_canvas_create(parent, NULL);
    if( !obj )
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    lvgl_style_utils_add(obj, p);
    lvgl_html_basic_add_component(e, obj, p);

    qvm_value_t *context = evm_object_create(e, GC_OBJECT, 10, 0);
    if( context ) {
        html_eui_t *html_canvas = evm_malloc(sizeof(html_eui_t));
        html_canvas->obj = obj;
        html_canvas->buffer = NULL;
        evm_prop_append(e, context, "init", evm_mk_native((intptr_t)html_canvas_init));
        evm_prop_append(e, context, "destroy", evm_mk_native((intptr_t)html_canvas_destroy));
        evm_prop_append(e, context, "rect", evm_mk_native((intptr_t)html_canvas_rect));
        evm_prop_append(e, context, "fillRect", evm_mk_native((intptr_t)html_canvas_fillRect));
        evm_prop_append(e, context, "circle", evm_mk_native((intptr_t)html_canvas_circle));
        evm_prop_append(e, context, "line", evm_mk_native((intptr_t)html_canvas_line));
        evm_prop_append(e, context, "fillCircle", evm_mk_native((intptr_t)html_canvas_fillCircle));
        evm_prop_append(e, context, "text", evm_mk_native((intptr_t)html_eui_text));
        evm_prop_append(e, context, "setColor", evm_mk_native((intptr_t)html_canvas_setColor));
        evm_prop_append(e, context, "setClipRect", evm_mk_native((intptr_t)html_canvas_setClipRect));
        evm_prop_push(e, p, "context", context);
        evm_object_set_ext_data(context, (intptr_t)html_canvas);
        _canvas_color = LV_COLOR_BLACK;
    }
    return qvm_undefined;
}
