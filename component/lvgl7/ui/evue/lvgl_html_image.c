#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"

#include "qvm_conf.h"
#include "fileapi/we_file.h"
#include "image/we_image.h"
#include "image/lodepng.h"

void we_convert_color_depth(uint8_t *img, uint32_t px_cnt, int alpha)
{
#if WE_COLOR_DEPTH == 16
    lv_color32_t * img_argb = (lv_color32_t*)img;
    lv_color_t c;
    uint32_t i;
    if( alpha ) {
        for(i = 0; i < px_cnt; i++) {
            c = LV_COLOR_MAKE(img_argb[i].ch.blue, img_argb[i].ch.green, img_argb[i].ch.red);
            img[i*3 + 2] = img_argb[i].ch.alpha;
            img[i*3 + 1] = c.full >> 8;
            img[i*3 + 0] = c.full & 0xFF;
        }
    } else {
        for(i = 0; i < px_cnt; i++) {
            c = LV_COLOR_MAKE(img_argb[i].ch.blue, img_argb[i].ch.green, img_argb[i].ch.red);
            img[i*2 + 1] = c.full >> 8;
            img[i*2 + 0] = c.full & 0xFF;
        }
    }
#endif
}

qvm_value_t html_obj_Image(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }


    obj = (lv_obj_t*)lv_img_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;

    we_html_object_set_obj_data(p, obj);
    we_html_object_set_user_data(p, we_malloc(sizeof (lv_img_dsc_t)));
    lvgl_style_utils_add(obj, p);
    lvgl_html_basic_add_component(e, obj, p);
    lv_img_set_auto_size(obj, true);
    return qvm_undefined;
}

qvm_value_t html_obj_Image_get_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    return qvm_undefined;
}

static void html_obj_image_destroyer(lv_img_dsc_t *dsc){
    lv_mem_free(dsc);
}

qvm_value_t html_obj_Image_set_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);
    qvm_value_t * src = v;
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    if( argc >= 1 && qvm_is_string(src[0]) ){
        const char * filename = qvm_to_string(e, src[0]);
        if( strstr(filename, ".png") ) {
            qvm_value_t image_obj = we_image_append(e, filename);
            if( qvm_is_undefined(image_obj) ) {
                return qvm_undefined;
            }
            qvm_value_t wv = qvm_get_property(e, image_obj, "width");
            qvm_value_t hv = qvm_get_property(e, image_obj, "height");
            int png_width = qvm_to_int32(e, wv);
            int png_height = qvm_to_int32(e, hv);

            we_image_info_t *info = (we_image_info_t *)we_get_user_data(e,image_obj);
            if( !info ) {
                return qvm_undefined;
            }

            lv_img_dsc_t *png_dsc = lv_mem_alloc(sizeof(lv_img_dsc_t));
            if ( !png_dsc ){
                return qvm_undefined;
            }
            memset(png_dsc, 0, sizeof(lv_img_dsc_t));
            png_dsc->header.always_zero = 0;
            png_dsc->header.cf = LV_IMG_CF_TRUE_COLOR_ALPHA;
            png_dsc->header.w = png_width;
            png_dsc->header.h = png_height;
            png_dsc->data_size =png_width * png_height * 4;
            png_dsc->data = info->decoded_data;
            info->user_data = png_dsc;
            lv_img_set_src(obj, png_dsc);
        }
    }
    return qvm_undefined;
}

qvm_value_t html_obj_Image_get_angle(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    qvm_hash_t key = qvm_to_int32(e, v[1]);

    uint16_t angle = lv_img_get_angle(obj);

//    qvm_add_property(e, v[0], key, qvm_mk_number(angle));
    return qvm_undefined;
}

qvm_value_t html_obj_Image_set_angle(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    if( argc >= 1 && qvm_is_integer(v[0]) ){
        int16_t angle = qvm_to_int32(e, v[0]);
        lv_img_set_angle(obj, angle);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_Image_get_pivot(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    qvm_hash_t key = qvm_to_int32(e, v[1]);

    lv_point_t * pivot = NULL;

    lv_img_get_pivot(obj, pivot);

    //qvm_add_property(e, v[0], key, (intptr_t)pivot);
    return qvm_undefined;
}

qvm_value_t html_obj_Image_set_pivot(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    if( argc >= 1 && qvm_is_object(v[0]) ){
        qvm_value_t x = qvm_get_property(e, v[0], "x");
        qvm_value_t y = qvm_get_property(e, v[0], "y");

        int16_t angle_x = qvm_to_int32(e, x);
        int16_t angle_y = qvm_to_int32(e, y);

        lv_img_set_pivot(obj, angle_x, angle_y);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_Image_get_zoom(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(argc);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    qvm_hash_t key = qvm_to_int32(e, v[1]);

    uint16_t zoom = lv_img_get_zoom(obj);

//    qvm_add_property(e, v[0], key, qvm_mk_number(zoom));
    return qvm_undefined;
}

qvm_value_t html_obj_Image_set_zoom(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);
    lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    if( argc >= 1 && qvm_is_integer(v[0]) ){
        uint16_t zoom = qvm_to_int32(e, v[0]);
        lv_img_set_zoom(obj, zoom);
    }
    return qvm_undefined;
}
