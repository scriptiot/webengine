#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"

qvm_value_t html_obj_Line(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    lv_obj_t *parent = lv_scr_act();
    lv_obj_t *obj = NULL;
    if(argc == 1) {
        parent = we_html_object_get_obj_data(v[0]);
    }
    obj = (lv_obj_t*)lv_line_create(parent, NULL);
    if( !obj ) 
        return qvm_undefined;
    we_html_object_set_obj_data(p, obj);
    lvgl_style_utils_add(obj, p);
    lvgl_html_basic_add_component(e, obj, p);
    lv_style_t * style = we_html_object_get_style_data(p);
    lv_style_set_line_width(style, LV_STATE_DEFAULT, 1);
    lv_style_set_line_color(style, LV_STATE_DEFAULT, LV_COLOR_BLACK);
    return qvm_undefined;
}

qvm_value_t html_obj_Line_set_line_width(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 &&  qvm_is_number(v[0]) ) {
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_style_set_line_width(style, LV_STATE_DEFAULT, qvm_to_int32(e, v[0]));
    }
    return qvm_undefined;
}

qvm_value_t html_obj_Line_set_line_color(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && (qvm_is_string(v[0]) || qvm_is_number(v[0])) ) {
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_style_set_line_color(style, LV_STATE_DEFAULT, lvgl_style_utils_get_color(v[0]));
    }
    return qvm_undefined;
}

qvm_value_t html_obj_Line_set_points(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_array(v[0]) ) {
        uint16_t size = (uint16_t)qvm_get_array_length(e, v[0]);
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        lv_point_t *points = (lv_point_t*)we_html_object_get_user_data(p);
        if( points ) {
            we_free(points);
        }
        points = we_malloc(size * sizeof(lv_point_t));
        for(int i = 0; i < size; i++) {
            qvm_value_t point = qvm_get_array(e, v[0], i);
            if( qvm_is_array( point ) && qvm_get_array_length(e, point) > 1) {
                qvm_value_t xv = qvm_get_array(e, point, 0);
                qvm_value_t yv = qvm_get_array(e, point, 1);
                int x = qvm_to_int32(e, xv);
                int y = qvm_to_int32(e, yv);
                points[i].x = x;
                points[i].y = y;
            }
        }
        lv_line_set_points(obj, points, size);
    }
    return qvm_undefined;
}
