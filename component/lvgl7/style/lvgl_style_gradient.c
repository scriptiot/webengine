#include "we_gui.h"
#include "style/lvgl_style_utils.h"

qvm_value_t we_css_linear_gradient(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    if( argc > 2 && qvm_is_string(v[0])) {
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_style_set_bg_opa(style, LV_STATE_DEFAULT, LV_OPA_COVER);
        lv_style_set_bg_color(style, LV_STATE_DEFAULT, lvgl_style_utils_get_color(v[1]));
        lv_style_set_bg_grad_color(style, LV_STATE_DEFAULT, lvgl_style_utils_get_color(v[2]));
        if( !strcmp(qvm_to_string(e, v[0]), "horizontal") ) {
            lv_style_set_bg_grad_dir(style, LV_STATE_DEFAULT, LV_GRAD_DIR_HOR);
        } else if( !strcmp(qvm_to_string(e, v[0]), "vertical") )  {
            lv_style_set_bg_grad_dir(style, LV_STATE_DEFAULT, LV_GRAD_DIR_VER);
        }

        lv_style_set_bg_main_stop(style, LV_STATE_DEFAULT, 0);
        lv_style_set_bg_grad_stop(style, LV_STATE_DEFAULT, 255);
        lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_background(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    return we_css_linear_gradient(e, p, argc, v);
}

qvm_value_t html_obj_set_gradient_color(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 2 && qvm_is_string(v[0])){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    }
    return qvm_undefined;
}
