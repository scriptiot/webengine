#include "style/lvgl_style_utils.h"
#include "qvm_conf.h"

#define DEFAULT_FONT_FAMILY "Montserrat"
#define DEFAULT_FONT_SIZE 20

static const char * currentfont_family = DEFAULT_FONT_FAMILY;
static uint32_t currentfont_size = DEFAULT_FONT_SIZE;
static qvm_value_t current_html_obj;



#define LV_COLOR_HEX(c) LV_COLOR_MAKE((uint8_t) ((uint32_t)((uint32_t)c >> 16) & 0xFF), \
                                (uint8_t) ((uint32_t)((uint32_t)c >> 8) & 0xFF), \
                                (uint8_t) ((uint32_t) c & 0xFF))

#define FONT(size) if (font_size == size){ \
                       return &lv_font_montserrat_##size; \
                   }

lv_style_t *lvgl_style_utils_add(lv_obj_t * obj, qvm_value_t v) {
    lv_style_t * style = we_malloc(sizeof(lv_style_t));
    lv_style_init(style);
    lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
    we_html_object_set_style_data(v, style);
    return style;
}

lv_style_t *lvgl_style_utils_get(qvm_value_t v){
    return we_html_object_get_style_data(v);
}

lv_font_t * lvgl_style_utils_get_font(qvm_value_t p, const char *font_family, int32_t font_size){
    if (current_html_obj.u.object != p.u.object){
        currentfont_family = DEFAULT_FONT_FAMILY;
        currentfont_size = DEFAULT_FONT_SIZE;
    }

    if (font_family != NULL){
        currentfont_family = font_family;
        current_html_obj = p;
    }

    if (font_size != -1){
        currentfont_size = font_size;
        current_html_obj = p;
    }

#if defined (__WIN32) || defined (_WIN64) || defined (__linux)
    if (currentfont_family != NULL && !strcmp(currentfont_family, "simsun")){
        if (currentfont_size == 20){
            return &simsun_20;
        }
        if (currentfont_size == 30){
            return &simsun_30;
        }
    }
#endif

#if LV_FONT_MONTSERRAT_12
    FONT(12)
#endif
#if LV_FONT_MONTSERRAT_14
    FONT(14)
#endif
#if LV_FONT_MONTSERRAT_16
    FONT(16)
#endif
#if LV_FONT_MONTSERRAT_18
    FONT(18)
#endif
#if LV_FONT_MONTSERRAT_20
    FONT(20)
#endif
#if LV_FONT_MONTSERRAT_22
    FONT(22)
#endif
#if LV_FONT_MONTSERRAT_24
    FONT(24)
#endif
#if LV_FONT_MONTSERRAT_26
    FONT(26)
#endif
#if LV_FONT_MONTSERRAT_28
    FONT(28)
#endif
#if LV_FONT_MONTSERRAT_30
    FONT(30)
#endif
#if LV_FONT_MONTSERRAT_32
    FONT(32)
#endif
#if LV_FONT_MONTSERRAT_34
    FONT(34)
#endif
#if LV_FONT_MONTSERRAT_36
    FONT(36)
#endif
#if LV_FONT_MONTSERRAT_38
    FONT(38)
#endif
#if LV_FONT_MONTSERRAT_40
    FONT(40)
#endif
#if LV_FONT_MONTSERRAT_42
    FONT(42)
#endif
#if LV_FONT_MONTSERRAT_44
    FONT(44)
#endif
#if LV_FONT_MONTSERRAT_46
    FONT(46)
#endif
#if LV_FONT_MONTSERRAT_48
    FONT(48)
#endif
    return LV_THEME_DEFAULT_FONT_NORMAL;
}

lv_opa_t lvgl_style_utils_get_opa(int opacity){
    opacity = 10 * opacity / 10 ;
    switch (opacity) {
        case 0: return LV_OPA_TRANSP;
        case 10: return LV_OPA_10;
        case 20: return LV_OPA_20;
        case 30: return LV_OPA_30;
        case 40: return LV_OPA_40;
        case 50: return LV_OPA_50;
        case 60: return LV_OPA_60;
        case 70: return LV_OPA_70;
        case 80: return LV_OPA_80;
        case 90: return LV_OPA_90;
        case 100: return LV_OPA_COVER;
        default:return LV_OPA_COVER;
    }
}

lv_color_t lvgl_style_utils_get_color(qvm_value_t v){
    if( qvm_is_string(v)) {
        const char * s = qvm_to_string(we_get_runtime(), v);
        if( !strcmp(s, "white") ) return LV_COLOR_WHITE;
        if( !strcmp(s, "silver") ) return LV_COLOR_SILVER;
        if( !strcmp(s, "gray") ) return LV_COLOR_GRAY;
        if( !strcmp(s, "black") ) return LV_COLOR_BLACK;
        if( !strcmp(s, "red") ) return LV_COLOR_RED;
        if( !strcmp(s, "maroon") ) return LV_COLOR_MAROON;
        if( !strcmp(s, "yellow") ) return LV_COLOR_YELLOW;
        if( !strcmp(s, "olive") ) return LV_COLOR_OLIVE;
        if( !strcmp(s, "lime") ) return LV_COLOR_LIME;
        if( !strcmp(s, "green") ) return LV_COLOR_GREEN;
        if( !strcmp(s, "cyan") ) return LV_COLOR_CYAN;
        if( !strcmp(s, "aqua") ) return LV_COLOR_AQUA;
        if( !strcmp(s, "teal") ) return LV_COLOR_TEAL;
        if( !strcmp(s, "blue") ) return LV_COLOR_BLUE;
        if( !strcmp(s, "green") ) return LV_COLOR_GREEN;
        if( !strcmp(s, "navy") ) return LV_COLOR_NAVY;
        if( !strcmp(s, "magenta") ) return LV_COLOR_MAGENTA;
        if( !strcmp(s, "purple") ) return LV_COLOR_PURPLE;
        if( !strcmp(s, "orange") ) return LV_COLOR_ORANGE;
        if( !strcmp(s, "darkblue") ) return LV_COLOR_MAKE(0, 51, 102);
        if( !strcmp(s, "lightblue") ) return LV_COLOR_MAKE(46, 203, 203);
    }
    int i = qvm_to_int32(we_get_runtime(), v);
    return LV_COLOR_HEX(i);
}

const char * get_html_type(qvm_state_t *e, qvm_value_t hml){
    qvm_value_t _name = qvm_get_property(e, hml, "name");
    if (qvm_is_string(_name)){
        return qvm_to_string(e, _name);
    }
    return "";
}


bool is_html_type(qvm_state_t *e, qvm_value_t hml, const char *name)
{
    qvm_value_t _name = qvm_get_property(e, hml, "name");
    const char * bname = qvm_to_string(e, _name);
    if (!strcmp(bname, name)){
        return true;
    }
    return false;
}

lv_res_t check_file_isvaild(const char *filename, char *path)
{
    if(filename == NULL || path == NULL)
        return LV_RES_INV;

    lv_fs_file_t file;
    sprintf(path, "%s/%s", WE_CONF_ROOT_DIR, filename);
    lv_fs_res_t res = lv_fs_open(&file, path, LV_FS_MODE_RD);
    if(res == LV_FS_RES_OK){
        lv_fs_close(&file);
        return res;
    }

    memset(path, 0, strlen(path));
    sprintf(path, "%s/%s", LV_RAMFS_DIR, filename);
    res = lv_fs_open(&file, path, LV_FS_MODE_RD);
    if(res == LV_FS_RES_OK){
        lv_fs_close(&file);
        return res;
    }

    return LV_RES_INV;
}
