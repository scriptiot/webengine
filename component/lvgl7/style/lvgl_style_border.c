#include "we_gui.h"
#include "style/lvgl_style_utils.h"

qvm_value_t  html_obj_set_borderopacity(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_style_set_border_opa(style, LV_STATE_DEFAULT, (lv_coord_t)qvm_to_int32(e, v[0]));
        lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
    }
    return qvm_undefined;
}

qvm_value_t  html_obj_set_bordercolor(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_style_set_border_color(style, LV_STATE_DEFAULT, lvgl_style_utils_get_color(v[0]));
    }
    return qvm_undefined;
}

qvm_value_t  html_obj_set_borderwidth(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_style_set_border_width(style, LV_STATE_DEFAULT, (lv_coord_t)qvm_to_int32(e, v[0]));
        lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
    }
    return qvm_undefined;
}

qvm_value_t  html_obj_set_borderradius(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_style_set_radius(style, LV_STATE_DEFAULT, (lv_coord_t)qvm_to_int32(e, v[0]));
        lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
    }
    return qvm_undefined;
}
