#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_set_top(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0])){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        if (!strcmp(qvm_to_string(e, v[0]), "none")){
            lv_obj_set_hidden(obj, true);
        }else{
            lv_obj_set_hidden(obj, false);
        }
    }
    return qvm_undefined;
}
