#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_set_alignitems(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0])){
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        if (!strcmp(qvm_to_string(e, v[0]), "center")){
            lv_cont_set_layout(obj, LV_LAYOUT_CENTER);
        } else if (!strcmp(qvm_to_string(e, v[0]), "flex-start")){
            lv_cont_set_layout(obj, LV_LABEL_ALIGN_LEFT);
        } else if (!strcmp(qvm_to_string(e, v[0]), "flex-end")){
            lv_cont_set_layout(obj, LV_LAYOUT_ROW_BOTTOM);
        } else if (!strcmp(qvm_to_string(e, v[0]), "pretty")){
            lv_cont_set_layout(obj, LV_LAYOUT_PRETTY_MID);
        } else if (!strcmp(qvm_to_string(e, v[0]), "grid")){
            lv_cont_set_layout(obj, LV_LAYOUT_GRID);
        } else if (!strcmp(qvm_to_string(e, v[0]), "off")){
            lv_cont_set_layout(obj, LV_LAYOUT_OFF);
        }
    }
    return qvm_undefined;
}
