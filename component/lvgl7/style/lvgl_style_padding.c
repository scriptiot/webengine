#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_set_padding(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_style_set_pad_all(style, LV_STATE_DEFAULT, (lv_coord_t)qvm_to_int32(e, v[0]));
        lv_style_set_pad_inner(style, LV_STATE_DEFAULT, 0);
        lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_padding_left(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_style_set_pad_left(style, LV_STATE_DEFAULT, (lv_coord_t)qvm_to_int32(e, v[0]));
        lv_style_set_pad_inner(style, LV_STATE_DEFAULT, 0);
        lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_padding_top(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_style_set_pad_top(style, LV_STATE_DEFAULT, (lv_coord_t)qvm_to_int32(e, v[0]));
        lv_style_set_pad_inner(style, LV_STATE_DEFAULT, 0);
        lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_padding_right(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_style_set_pad_right(style, LV_STATE_DEFAULT, (lv_coord_t)qvm_to_int32(e, v[0]));
        lv_style_set_pad_inner(style, LV_STATE_DEFAULT, 0);
        lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_padding_bottom(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_style_set_pad_bottom(style, LV_STATE_DEFAULT, (lv_coord_t)qvm_to_int32(e, v[0]));
        lv_style_set_pad_inner(style, LV_STATE_DEFAULT, 0);
        lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
    }
    return qvm_undefined;
}
