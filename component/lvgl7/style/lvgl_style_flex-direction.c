#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_set_flexdirection(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0])){
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        if (!strcmp(qvm_to_string(e, v[0]), "row")){
            lv_cont_set_layout(obj, LV_LAYOUT_ROW_MID);
        }else if (!strcmp(qvm_to_string(e, v[0]), "column")){
            lv_cont_set_layout(obj, LV_LAYOUT_COLUMN_MID);
        }
    }
    return qvm_undefined;
}
