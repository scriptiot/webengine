#ifndef LVGL_STYLE_UTILS_H
#define LVGL_STYLE_UTILS_H

#include "we_gui.h"
#include "lvgl.h"

lv_style_t *lvgl_style_utils_add(lv_obj_t * obj, qvm_value_t v);
lv_style_t *lvgl_style_utils_get(qvm_value_t v);
lv_font_t *lvgl_style_utils_get_font(qvm_value_t p, const char* font_family, int32_t font_size);
lv_opa_t lvgl_style_utils_get_opa(int opacity);
lv_color_t lvgl_style_utils_get_color(qvm_value_t v);

const char * get_html_type(qvm_state_t *e, qvm_value_t hml);
bool is_html_type(qvm_state_t *e, qvm_value_t hml, const char *name);

lv_res_t check_file_isvaild(const char * filename, char * path);
#endif
