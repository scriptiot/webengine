#include "we_gui.h"
#include "style/lvgl_style_utils.h"

qvm_value_t html_obj_set_opacity(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_style_set_bg_opa(style, LV_STATE_DEFAULT, lvgl_style_utils_get_opa(qvm_to_int32(e, v[0]) * 100));
        lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
    }
    return qvm_undefined;
}
