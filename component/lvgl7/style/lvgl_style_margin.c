#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_set_margin(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_style_set_margin_all(style, LV_STATE_DEFAULT, (lv_coord_t)qvm_to_int32(e, v[0]));
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_margin_left(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_style_set_margin_left(style, LV_STATE_DEFAULT, (lv_coord_t)qvm_to_int32(e, v[0]));
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_margin_top(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_style_set_margin_top(style, LV_STATE_DEFAULT, (lv_coord_t)qvm_to_int32(e, v[0]));
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_margin_right(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_style_set_margin_right(style, LV_STATE_DEFAULT, (lv_coord_t)qvm_to_int32(e, v[0]));
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_margin_bottom(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_style_set_margin_bottom(style, LV_STATE_DEFAULT, (lv_coord_t)qvm_to_int32(e, v[0]));
    }
    return qvm_undefined;
}
