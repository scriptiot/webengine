#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_set_draggable(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0])){
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        if (!strcmp(qvm_to_string(e, v[0]), "true")){
            lv_obj_set_drag(obj, true);
        } else if (!strcmp(qvm_to_string(e, v[0]), "false")){
            lv_obj_set_drag(obj, false);
        }
    }
    return qvm_undefined;
}
