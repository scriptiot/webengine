#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_set_fade_in(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 2 && qvm_is_number(v[0]) && qvm_is_number(v[1])) {
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_fade_out(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 2 && qvm_is_number(v[0]) && qvm_is_number(v[1])) {
        lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
    }
    return qvm_undefined;
}
