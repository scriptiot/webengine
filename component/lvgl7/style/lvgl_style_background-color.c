#include "we_gui.h"
#include "style/lvgl_style_utils.h"

qvm_value_t  html_obj_set_backgroundcolor(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 && (qvm_is_string(v[0]) || qvm_is_number(v[0])) ){
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_style_t * style = we_html_object_get_style_data(p);
        if( qvm_is_string(v[0]) && strcmp(qvm_to_string(e, v[0]), "transparent") == 0 ) {
            lv_style_set_bg_opa(style, LV_STATE_DEFAULT, LV_OPA_TRANSP);
        } else {
            lv_style_set_bg_color(style, LV_STATE_DEFAULT, lvgl_style_utils_get_color(v[0]));
        }
        lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
    }
    return qvm_undefined;
}
