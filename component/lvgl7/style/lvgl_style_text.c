#include "ui/evue/lvgl_html_basic.h"
#include "style/lvgl_style_utils.h"

qvm_value_t html_obj_set_text_color(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if (argc != 1 || !(qvm_is_string(v[0]) || qvm_is_number(v[0])))
        return qvm_undefined;

    if (is_html_type(e, p, "text") || is_html_type(e, p, "textarea")){
        lv_style_t * style = we_html_object_get_style_data(p);
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_color_t color = lvgl_style_utils_get_color(v[0]);
        lv_style_set_text_color(style, LV_STATE_DEFAULT, color);
        lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
    }

    return qvm_undefined;
}

qvm_value_t html_obj_set_text_align(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0])){
        if (is_html_type(e, p, "text") || is_html_type(e, p, "textarea")){
            lv_style_t * style = we_html_object_get_style_data(p);
            lv_obj_t * obj = we_html_object_get_obj_data(p);
            if (!strcmp(qvm_to_string(e, v[0]), "left")){
                lv_label_set_align(obj, LV_LABEL_ALIGN_LEFT);
            }else if (!strcmp(qvm_to_string(e, v[0]), "center")){
                lv_label_set_align(obj, LV_LABEL_ALIGN_CENTER);
            }else if (!strcmp(qvm_to_string(e, v[0]), "right")){
                lv_label_set_align(obj, LV_LABEL_ALIGN_RIGHT);
            }else{
                lv_label_set_align(obj, LV_LABEL_ALIGN_AUTO);
            }
        }
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_text_font_size(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_integer(v[0])){
        if (is_html_type(e, p, "text") || is_html_type(e, p, "textarea")){
            lv_style_t * style = we_html_object_get_style_data(p);
            lv_obj_t * obj = we_html_object_get_obj_data(p);
            int32_t font_size = qvm_to_int32(e, v[0]);
            lv_style_set_text_font(style, LV_STATE_DEFAULT, lvgl_style_utils_get_font(p, NULL, font_size));
            lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
        }
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_text_letter_space(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_integer(v[0])){
        if (is_html_type(e, p, "text") || is_html_type(e, p, "textarea")){
            lv_style_t * style = we_html_object_get_style_data(p);
            lv_obj_t * obj = we_html_object_get_obj_data(p);
            int letter_space = qvm_to_int32(e, v[0]);
            lv_style_set_text_letter_space(style, LV_STATE_DEFAULT, letter_space);
            lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
        }
    }
    return qvm_undefined;
}

LV_FONT_DECLARE(lv_font_simsun_16_cjk)
qvm_value_t html_obj_set_text_font_family(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0])){
        if (is_html_type(e, p, "text") || is_html_type(e, p, "textarea")){
            lv_style_t * style = we_html_object_get_style_data(p);
            lv_obj_t * obj = we_html_object_get_obj_data(p);
            const char * font_family = qvm_to_string(e, v[0]);
            lv_style_set_text_font(style, LV_STATE_DEFAULT, lvgl_style_utils_get_font(p, font_family, -1));
            lv_obj_add_style(obj, LV_OBJ_PART_MAIN, style);
        }
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_text_overflow(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_string(v[0])){
        if (is_html_type(e, p, "text")){
            lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
            if (lvgl_html_basic_is_obj_type(obj, "lv_label")){
                if (!strcmp(qvm_to_string(e, v[0]), "clip")){
                    lv_label_set_long_mode(obj, LV_LABEL_LONG_CROP);
                }else if (!strcmp(qvm_to_string(e, v[0]), "ellipsis")){
                    lv_label_set_long_mode(obj, LV_LABEL_LONG_DOT);
                }else if (!strcmp(qvm_to_string(e, v[0]), "longexpand")){
                    lv_label_set_long_mode(obj, LV_LABEL_LONG_EXPAND);
                }else if (!strcmp(qvm_to_string(e, v[0]), "longbreak")){
                    lv_label_set_long_mode(obj, LV_LABEL_LONG_BREAK);
                }else if (!strcmp(qvm_to_string(e, v[0]), "scroll")){
                    lv_label_set_long_mode(obj, LV_LABEL_LONG_SROLL);
                    lv_label_set_anim_speed(obj, 40);
                }else if (!strcmp(qvm_to_string(e, v[0]), "scrollcircle")){
                    lv_label_set_long_mode(obj, LV_LABEL_LONG_SROLL_CIRC);
                    lv_label_set_anim_speed(obj, 40);
                }
            }
        }
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_text_animation_speed(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 && qvm_is_integer(v[0])){
        if (is_html_type(e, p, "text") || is_html_type(e, p, "textarea")){
            lv_obj_t * obj = (lv_obj_t*)we_html_object_get_obj_data(p);
        }
    }
    return qvm_undefined;
}
