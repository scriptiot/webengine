#include "we_gui.h"
#include "lvgl.h"

qvm_value_t html_obj_set_width(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_obj_set_width(obj, (lv_coord_t)qvm_to_int32(e, v[0]));
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_height(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_obj_set_height(obj, (lv_coord_t)qvm_to_int32(e, v[0]));
    }
    return qvm_undefined;
}
