#include "we_gui.h"
#include "lvgl.h"

qvm_value_t  html_obj_set_y(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_obj_set_y(obj, (lv_coord_t)qvm_to_int32(e, v[0]));
    }
    return qvm_undefined;
}

qvm_value_t html_obj_set_x(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
    QVM_UNUSED(e);
    if( argc >= 1 ){
        lv_obj_t * obj = we_html_object_get_obj_data(p);
        lv_obj_set_x(obj, (lv_coord_t)qvm_to_int32(e, v[0]));
    }
    return qvm_undefined;
}

