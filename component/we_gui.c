#include "we_gui.h"
#include "page/we_page.h"

#define HTML_OBJ_PROPERTIES {"x", (qvm_native_t)html_obj_set_x, NULL, QVM_INVALID_HASH}, \
                            {"y", (qvm_native_t)html_obj_set_y, NULL, QVM_INVALID_HASH}, \
                            {"left", (qvm_native_t)html_obj_set_x, NULL, QVM_INVALID_HASH},\
                            {"top", (qvm_native_t)html_obj_set_y, NULL, QVM_INVALID_HASH},\
                            {"width", (qvm_native_t)html_obj_set_width, NULL, QVM_INVALID_HASH}, \
                            {"height", (qvm_native_t)html_obj_set_height, NULL, QVM_INVALID_HASH}, \
                            {"opacity", (qvm_native_t)html_obj_set_opacity, NULL, QVM_INVALID_HASH},\
                            {"display", (qvm_native_t)html_obj_set_display, NULL, QVM_INVALID_HASH},\
                            {"align-items", (qvm_native_t)html_obj_set_alignitems, NULL, QVM_INVALID_HASH},\
                            {"flex_direction", (qvm_native_t)html_obj_set_flexdirection, NULL, QVM_INVALID_HASH},\
                            {"background-color", (qvm_native_t)html_obj_set_backgroundcolor, NULL, QVM_INVALID_HASH},\
                            {"background", (qvm_native_t)html_obj_set_background, NULL, QVM_INVALID_HASH},\
                            {"opacity", (qvm_native_t)html_obj_set_opacity, NULL, QVM_INVALID_HASH},\
                            {"border-color", (qvm_native_t)html_obj_set_bordercolor, NULL, QVM_INVALID_HASH},\
                            {"border-width", (qvm_native_t)html_obj_set_borderwidth, NULL, QVM_INVALID_HASH},\
                            {"border-radius", (qvm_native_t)html_obj_set_borderradius, NULL, QVM_INVALID_HASH},\
                            {"border-opacity", (qvm_native_t)html_obj_set_borderopacity, NULL, QVM_INVALID_HASH},\
                            {"margin", (qvm_native_t)html_obj_set_margin, NULL, QVM_INVALID_HASH},\
                            {"margin-left", (qvm_native_t)html_obj_set_margin_left, NULL, QVM_INVALID_HASH},\
                            {"margin-top", (qvm_native_t)html_obj_set_margin_top, NULL, QVM_INVALID_HASH},\
                            {"margin-right", (qvm_native_t)html_obj_set_margin_right, NULL, QVM_INVALID_HASH},\
                            {"margin-bottom", (qvm_native_t)html_obj_set_margin_bottom, NULL, QVM_INVALID_HASH},\
                            {"padding", (qvm_native_t)html_obj_set_padding, NULL, QVM_INVALID_HASH},\
                            {"padding-left", (qvm_native_t)html_obj_set_padding_left, NULL, QVM_INVALID_HASH},\
                            {"padding-top", (qvm_native_t)html_obj_set_padding_top, NULL, QVM_INVALID_HASH},\
                            {"padding-right", (qvm_native_t)html_obj_set_padding_right, NULL, QVM_INVALID_HASH},\
                            {"padding-bottom", (qvm_native_t)html_obj_set_padding_bottom, NULL, QVM_INVALID_HASH},\
                            {"color", (qvm_native_t)html_obj_set_text_color, NULL, QVM_INVALID_HASH},\
                            {"text-align", (qvm_native_t)html_obj_set_text_align,NULL,  QVM_INVALID_HASH},\
                            {"font-size", (qvm_native_t)html_obj_set_text_font_size, NULL, QVM_INVALID_HASH},\
                            {"letter-spacing", (qvm_native_t)html_obj_set_text_letter_space, NULL, QVM_INVALID_HASH},\
                            {"font-family", (qvm_native_t)html_obj_set_text_font_family, NULL, QVM_INVALID_HASH},\
                            {"text-overflow", (qvm_native_t)html_obj_set_text_overflow, NULL, QVM_INVALID_HASH},\
                            {"draggable", (qvm_native_t)html_obj_set_draggable, NULL, QVM_INVALID_HASH}, \

static we_html_property_reg_t html_obj_properties[] = {
    HTML_OBJ_PROPERTIES
    {NULL, NULL, NULL, QVM_INVALID_HASH}
};

static we_html_property_reg_t html_input_properties[] = {
    HTML_OBJ_PROPERTIES
    {"value", (qvm_native_t)html_obj_Input_set_text, (qvm_native_t)html_obj_Input_get_text, QVM_INVALID_HASH},
    {"placeholder", (qvm_native_t)html_obj_Input_set_placeholder, (qvm_native_t)html_obj_Input_get_placeholder, QVM_INVALID_HASH},
    {NULL, NULL, NULL, QVM_INVALID_HASH}
};

static we_html_property_reg_t html_text_properties[] = {
    HTML_OBJ_PROPERTIES
    {"text", (qvm_native_t)html_obj_Text_set_text, (qvm_native_t)html_obj_Text_get_text, QVM_INVALID_HASH},
    {NULL, NULL, NULL, QVM_INVALID_HASH}
};

static we_html_property_reg_t html_textarea_properties[] = {
    HTML_OBJ_PROPERTIES
    {"text", (qvm_native_t)html_obj_Textarea_set_text, (qvm_native_t)html_obj_Textarea_get_text, QVM_INVALID_HASH},
    {NULL, NULL, NULL, QVM_INVALID_HASH}
};

static we_html_property_reg_t html_progress_properties[] = {
    HTML_OBJ_PROPERTIES
    {"percent", (qvm_native_t)html_obj_Progress_set_value, (qvm_native_t)html_obj_Progress_get_value, QVM_INVALID_HASH},
    {"duration", (qvm_native_t)html_obj_Progress_set_duration, (qvm_native_t)html_obj_Progress_get_duration, QVM_INVALID_HASH},
    {"value", (qvm_native_t)html_obj_Progress_set_start_value, (qvm_native_t)html_obj_Progress_get_start_value, QVM_INVALID_HASH},
    {NULL, NULL, NULL, QVM_INVALID_HASH}
};

static we_html_property_reg_t html_slider_properties[] = {
    HTML_OBJ_PROPERTIES
    {"duration", (qvm_native_t)html_obj_Slider_set_duration, (qvm_native_t)html_obj_Slider_get_duration, QVM_INVALID_HASH},
    {"value", (qvm_native_t)html_obj_Slider_set_value, (qvm_native_t)html_obj_Slider_get_value, QVM_INVALID_HASH},
    {NULL, NULL, NULL, QVM_INVALID_HASH}
};

static we_html_property_reg_t html_image_properties[] = {
    HTML_OBJ_PROPERTIES
    {"src", (qvm_native_t)html_obj_Image_set_src, (qvm_native_t)html_obj_Image_get_src, QVM_INVALID_HASH},
    {"rotate", (qvm_native_t)html_obj_Image_set_pivot, (qvm_native_t)html_obj_Image_get_pivot, QVM_INVALID_HASH},
    {"angle", (qvm_native_t)html_obj_Image_set_angle, (qvm_native_t)html_obj_Image_get_angle, QVM_INVALID_HASH},
    {"zoom", (qvm_native_t)html_obj_Image_set_zoom, (qvm_native_t)html_obj_Image_get_zoom, QVM_INVALID_HASH},
    {NULL, NULL, NULL, QVM_INVALID_HASH}
};

static we_html_property_reg_t html_imagebutton_properties[] = {
    HTML_OBJ_PROPERTIES
    {"pressed_src", (qvm_native_t)html_obj_ImageButton_set_pressed_src, NULL, QVM_INVALID_HASH},
    {"released_src", (qvm_native_t)html_obj_ImageButton_set_released_src, NULL, QVM_INVALID_HASH},
    {"disabled_src", (qvm_native_t)html_obj_ImageButton_set_disabled_src, NULL, QVM_INVALID_HASH},
    {"checked_pressed_src", (qvm_native_t)html_obj_ImageButton_set_checked_pressed_src, NULL, QVM_INVALID_HASH},
    {"checked_released_src", (qvm_native_t)html_obj_ImageButton_set_checked_released_src, NULL, QVM_INVALID_HASH},
    {"checked_disabled_src", (qvm_native_t)html_obj_ImageButton_set_checked_disabled_src, NULL, QVM_INVALID_HASH},
    {NULL, NULL, NULL, QVM_INVALID_HASH}
};

static we_html_property_reg_t html_tabview_properties[] = {
    HTML_OBJ_PROPERTIES
    {NULL, NULL, NULL, QVM_INVALID_HASH}
};

static we_html_property_reg_t html_select_properties[] = {
    HTML_OBJ_PROPERTIES
    {NULL, NULL, NULL, QVM_INVALID_HASH}
};

static we_html_property_reg_t html_line_properties[] = {
    HTML_OBJ_PROPERTIES
    {"points", (qvm_native_t)html_obj_Line_set_points, NULL, QVM_INVALID_HASH},
    {"line-width", (qvm_native_t)html_obj_Line_set_line_width, NULL, QVM_INVALID_HASH},
    {"line-color", (qvm_native_t)html_obj_Line_set_line_color, NULL, QVM_INVALID_HASH},
    {NULL, NULL, NULL, QVM_INVALID_HASH}
};

static we_html_property_reg_t html_style_properties[] = {
    {"display", (qvm_native_t)html_obj_set_display, NULL, QVM_INVALID_HASH},
    {"left", (qvm_native_t)html_obj_set_x, NULL, QVM_INVALID_HASH},
    {"top", (qvm_native_t)html_obj_set_y, NULL, QVM_INVALID_HASH},
    {"width", (qvm_native_t)html_obj_set_width, NULL, QVM_INVALID_HASH},
    {"height", (qvm_native_t)html_obj_set_height, NULL, QVM_INVALID_HASH},
    {"align-items", (qvm_native_t)html_obj_set_alignitems, NULL, QVM_INVALID_HASH},
    {"flex-direction", (qvm_native_t)html_obj_set_flexdirection, NULL, QVM_INVALID_HASH},
    {"background-color", (qvm_native_t)html_obj_set_backgroundcolor, NULL, QVM_INVALID_HASH},
    {"background", (qvm_native_t)html_obj_set_background, NULL, QVM_INVALID_HASH},
    {"opacity", (qvm_native_t)html_obj_set_opacity, NULL, QVM_INVALID_HASH},
    {"border-color", (qvm_native_t)html_obj_set_bordercolor, NULL, QVM_INVALID_HASH},
    {"border-width", (qvm_native_t)html_obj_set_borderwidth, NULL, QVM_INVALID_HASH},
    {"border-radius", (qvm_native_t)html_obj_set_borderradius, NULL, QVM_INVALID_HASH},
    {"border-opacity", (qvm_native_t)html_obj_set_borderopacity, NULL, QVM_INVALID_HASH},
    {"margin", (qvm_native_t)html_obj_set_margin, NULL, QVM_INVALID_HASH},
    {"margin-left", (qvm_native_t)html_obj_set_margin_left, NULL, QVM_INVALID_HASH},
    {"margin-top", (qvm_native_t)html_obj_set_margin_top, NULL, QVM_INVALID_HASH},
    {"margin-right", (qvm_native_t)html_obj_set_margin_right, NULL, QVM_INVALID_HASH},
    {"margin-bottom", (qvm_native_t)html_obj_set_margin_bottom, NULL, QVM_INVALID_HASH},
    {"padding", (qvm_native_t)html_obj_set_padding, NULL, QVM_INVALID_HASH},
    {"padding-left", (qvm_native_t)html_obj_set_padding_left, NULL, QVM_INVALID_HASH},
    {"padding-top", (qvm_native_t)html_obj_set_padding_top, NULL, QVM_INVALID_HASH},
    {"padding-right", (qvm_native_t)html_obj_set_padding_right, NULL, QVM_INVALID_HASH},
    {"padding-bottom", (qvm_native_t)html_obj_set_padding_bottom, NULL, QVM_INVALID_HASH},
    {"color", (qvm_native_t)html_obj_set_text_color, NULL, QVM_INVALID_HASH},
    {"text-align", (qvm_native_t)html_obj_set_text_align,NULL,  QVM_INVALID_HASH},
    {"font-size", (qvm_native_t)html_obj_set_text_font_size, NULL, QVM_INVALID_HASH},
    {"letter-spacing", (qvm_native_t)html_obj_set_text_letter_space, NULL, QVM_INVALID_HASH},
    {"font-family", (qvm_native_t)html_obj_set_text_font_family, NULL, QVM_INVALID_HASH},
    {"text-overflow", (qvm_native_t)html_obj_set_text_overflow, NULL, QVM_INVALID_HASH},
    {NULL, NULL, NULL, 0}
};

static we_html_object_reg_t html_objects[] = {
    {"div",(qvm_native_t)html_obj_Div, html_obj_properties},
    {"list", (qvm_native_t)html_obj_List, html_obj_properties},
    {"list-item", (qvm_native_t)html_obj_ListItem, html_obj_properties},
    {"image", (qvm_native_t)html_obj_Image, html_image_properties},
    {"imagebutton", (qvm_native_t)html_obj_ImageButton, html_imagebutton_properties},
    {"bar", (qvm_native_t)html_obj_Progress, html_progress_properties},
    {"progress", (qvm_native_t)html_obj_Progress, html_progress_properties},
    {"text", (qvm_native_t)html_obj_Text, html_text_properties},
    {"textarea", (qvm_native_t)html_obj_Textarea, html_textarea_properties},
    {"input", (qvm_native_t)html_obj_Input, html_input_properties},
    {"button", (qvm_native_t)html_obj_Button, html_obj_properties},
    {"slider", (qvm_native_t)html_obj_Slider, html_slider_properties},
    {"tab", (qvm_native_t)html_obj_Tab, html_obj_properties},
    {"tabview", (qvm_native_t)html_obj_TabView, html_tabview_properties},
    {"select",(qvm_native_t)html_obj_Select, html_select_properties},
    {"option",(qvm_native_t)html_obj_Option, html_obj_properties},
    {"line",(qvm_native_t)html_obj_Line, html_line_properties},
#ifdef WE_USE_CANVAS
    {"canvas", (qvm_native_t)html_obj_Canvas, html_obj_properties},
#endif
    {NULL, NULL, NULL}
};

int gui_init(qvm_state_t *e) {
    gui_init_ex(e);
    we_html_ui_object_register(e, html_objects);
    we_html_ui_style_register(e, html_style_properties);
    we_page_register_basic(lvgl_html_basic_show, lvgl_html_basic_hide);
    return 1;
}
