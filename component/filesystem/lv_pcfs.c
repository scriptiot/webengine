#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include "lvgl/src/lv_misc/lv_fs.h"
#include "we_gui.h"
#include "fileapi/we_file.h"
#include "evm_vfs_file.h"
#include "evm_vfs_types.h"
typedef evm_vfs_file_t *pc_file_t;
typedef DIR *pc_dir_t;

static int pcfs_inited = 0;

/**
 * Open a file from the PC
 * @param file_p pointer to a FILE* variable
 * @param fn name of the file.
 * @param mode element of 'fs_mode_t' enum or its 'OR' connection (e.g. FS_MODE_WR | FS_MODE_RD)
 * @return LV_FS_RES_OK: no error, the file is opened
 *         any error from lv_fs_res_t enum
 */
static lv_fs_res_t pcfs_open(struct _lv_fs_drv_t * drv, void * file_p, const char * fn, lv_fs_mode_t mode)
{
    uint32_t fmode = 0;

    if (mode == LV_FS_MODE_WR) fmode |= WE_FS_WRITE;
    else if (mode == LV_FS_MODE_RD) fmode |= WE_FS_READ;
    else if (mode == (LV_FS_MODE_WR | LV_FS_MODE_RD)) fmode |= WE_FS_READ | WE_FS_WRITE;
    else fmode |= WE_FS_READ;

    fmode |= WE_FS_BIN;

    char buf[256];
    sprintf(buf, "%s/%s", we_get_root_dir(), fn);

    int32_t fd = we_file_open(buf, fmode);
    if (fd <= 0)
        return LV_FS_RES_UNKNOWN;

    we_file_seek(fd, 0, SEEK_SET);
    evm_vfs_file_t * vfd = evm_vfs_file_get(fd);
    pc_file_t * fp = file_p;        /*Just avoid the confusing casings*/
    *fp = vfd;


    return LV_FS_RES_OK;
}

/**
 * Close an opened file
 * @param file_p pointer to a FILE* variable. (opened with lv_ufs_open)
 * @return LV_FS_RES_OK: no error, the file is read
 *         any error from lv__fs_res_t enum
 */
static lv_fs_res_t pcfs_close(struct _lv_fs_drv_t * drv, void * file_p)
{
    pc_file_t *vfp = (pc_file_t *)(file_p);      /*Just avoid the confusing casings*/

    int32_t fd = evm_vfs_fd_get(*vfp);
    we_file_close(fd);
    return LV_FS_RES_OK;
}

/**
 * remove file
 * @param file_p pointer to a FILE* variable. (opened with lv_ufs_open)
 * @return LV_FS_RES_OK: no error, the file is read
 *         any error from lv__fs_res_t enum
 */
static lv_fs_res_t pcfs_remove(struct _lv_fs_drv_t * drv, const char * path)
{
         /*Just avoid the confusing casings*/
    int res = we_file_remove(path);
    if (res == 0){
        return LV_FS_RES_OK;
    }else{
        return LV_FS_RES_FS_ERR;
    }
}

/**
 * rename file
 * @param file_p pointer to a FILE* variable. (opened with lv_ufs_open)
 * @return LV_FS_RES_OK: no error, the file is read
 *         any error from lv__fs_res_t enum
 */
static lv_fs_res_t pcfs_rename(struct _lv_fs_drv_t * drv, const char * oldname, const char * newname)
{
         /*Just avoid the confusing casings*/
    int res = we_file_rename(oldname, newname);
    if (res == 0){
        return LV_FS_RES_OK;
    }else{
        return LV_FS_RES_FS_ERR;
    }
}

/**
 * Read data from an opened file
 * @param file_p pointer to a FILE variable.
 * @param buf pointer to a memory block where to store the read data
 * @param btr number of Bytes To Read
 * @param br the real number of read bytes (Byte Read)
 * @return LV_FS_RES_OK: no error, the file is read
 *         any error from lv__fs_res_t enum
 */
static lv_fs_res_t pcfs_read(struct _lv_fs_drv_t * drv, void * file_p, void * buf, uint32_t btr, uint32_t * br)
{
    pc_file_t *vfp = (pc_file_t *)(file_p);      /*Just avoid the confusing casings*/

    int32_t fd = evm_vfs_fd_get(*vfp);

    we_file_read(fd, buf, btr, br);

    return LV_FS_RES_OK;
}

/**
 * Set the read write pointer. Also expand the file size if necessary.
 * @param file_p pointer to a FILE* variable. (opened with lv_ufs_open )
 * @param pos the new position of read write pointer
 * @return LV_FS_RES_OK: no error, the file is read
 *         any error from lv__fs_res_t enum
 */
static lv_fs_res_t pcfs_seek(struct _lv_fs_drv_t * drv, void * file_p, uint32_t pos)
{
    pc_file_t *vfp = (pc_file_t *)(file_p);      /*Just avoid the confusing casings*/

    int32_t fd = evm_vfs_fd_get(*vfp);

    we_file_seek(fd, pos, SEEK_SET);

    return LV_FS_RES_OK;
}

/**
 * Give the position of the read write pointer
 * @param file_p pointer to a FILE* variable.
 * @param pos_p pointer to to store the result
 * @return LV_FS_RES_OK: no error, the file is read
 *         any error from lv__fs_res_t enum
 */
static lv_fs_res_t pcfs_tell(struct _lv_fs_drv_t * drv, void * file_p, uint32_t * pos_p)
{
    pc_file_t *vfp = (pc_file_t *)(file_p);      /*Just avoid the confusing casings*/

    int32_t fd = evm_vfs_fd_get(*vfp);

    we_file_tell(fd, pos_p);

    return LV_FS_RES_OK;
}

static lv_fs_res_t pcfs_write (struct _lv_fs_drv_t * drv, void * file_p, const void * buf, uint32_t btw, uint32_t * bw)
{
    pc_file_t *vfp = (pc_file_t *)(file_p);      /*Just avoid the confusing casings*/

    int32_t fd = evm_vfs_fd_get(*vfp);

    we_file_write(fd, (void *)buf,  btw, (int32_t *)bw);

    return LV_FS_RES_OK;
}

static bool pcfs_ready(struct _lv_fs_drv_t * drv)
{
    return (pcfs_inited == 1);
}

/**
 * Initialize a lv_ufs_read_dir_t variable to directory reading
 * @param rddir_p pointer to a 'ufs_dir_t' variable
 * @param path uFS doesn't support folders so it has to be ""
 * @return LV_FS_RES_OK or any error from lv__fs_res_t enum
 */
static lv_fs_res_t pcfs_dir_open(struct _lv_fs_drv_t * drv, void *rddir_p, const char *path)
{
    pc_dir_t open_dir;
    pc_dir_t *pc_dir = rddir_p;

    open_dir = opendir(path);

    if (open_dir == NULL) {
        return LV_FS_RES_NOT_EX;       /*Must be "" */
    } else {
        *pc_dir = open_dir;
        return LV_FS_RES_OK;
    }
}

/**
 * Read the next file name
 * @param dir_p pointer to an initialized 'ufs_dir_t' variable
 * @param fn pointer to buffer to sore the file name
 * @return LV_FS_RES_OK or any error from lv__fs_res_t enum
 */
static lv_fs_res_t pcfs_dir_read(struct _lv_fs_drv_t * drv, void *rddir_p, char *fn)
{
    pc_dir_t *pc_dir = rddir_p;
    struct dirent *ent;

    ent = readdir(*pc_dir);
    if (ent != NULL) {
        strcpy(fn, ent->d_name);
        return LV_FS_RES_OK;
    } else {
        return LV_FS_RES_NOT_EX;
    }

    return LV_FS_RES_OK;
}

/**
 * Close the directory reading
 * @param rddir_p pointer to an initialized 'ufs_dir_t' variable
 * @return LV_FS_RES_OK or any error from lv__fs_res_t enum
 */
static lv_fs_res_t pcfs_dir_close(struct _lv_fs_drv_t * drv, void *rddir_p)
{
    pc_dir_t *pc_dir = rddir_p;

    closedir(*pc_dir);

    return LV_FS_RES_OK;
}

static lv_fs_res_t pcfs_size(struct _lv_fs_drv_t * drv, void *file_p, uint32_t * size_p) {
    pc_file_t *vfp = (pc_file_t *)(file_p);      /*Just avoid the confusing casings*/
    int32_t fd = evm_vfs_fd_get(*vfp);
    evm_vfs_stat_t st;

    evm_vfs_fstat((int)fd, &st);

    *size_p = st.st_size;

    return LV_FS_RES_OK;
}

int lv_pcfs_init(void)
{
    lv_fs_drv_t pcfs_drv;

    if (pcfs_inited) {
        printf("pcfs has been inited\n");
        return 0;
    }

    lv_fs_drv_init(&pcfs_drv);

    pcfs_drv.file_size = sizeof(pc_file_t);
    pcfs_drv.rddir_size = sizeof(pc_dir_t);
    pcfs_drv.open_cb = pcfs_open;
    pcfs_drv.close_cb = pcfs_close;
    pcfs_drv.remove_cb = pcfs_remove;
    pcfs_drv.rename_cb = pcfs_rename;
    pcfs_drv.read_cb = pcfs_read;
    pcfs_drv.write_cb = pcfs_write;
    pcfs_drv.seek_cb = pcfs_seek;
    pcfs_drv.tell_cb = pcfs_tell;
    pcfs_drv.size_cb = pcfs_size;
    pcfs_drv.ready_cb = pcfs_ready;



    pcfs_drv.letter = 'C';  //for flash
    /* only sd card fs support dir */
    pcfs_drv.dir_open_cb = pcfs_dir_open;
    pcfs_drv.dir_read_cb = pcfs_dir_read;
    pcfs_drv.dir_close_cb = pcfs_dir_close;

    lv_fs_drv_register(&pcfs_drv);

    pcfs_inited = 1;

    return 0;
}
