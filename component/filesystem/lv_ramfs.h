/**
 * @file lv_flashfs.h
 * Implementation of flash file system which do NOT support directories.
 * The API is compatible with the lv_fs_int module.
 */

#ifndef LV_FLASHFS_H
#define LV_FLASHFS_H

#ifdef __cplusplus
extern "C"
{
#endif


/*********************
 *      INCLUDES
 *********************/
#include "lv_conf.h"


/*********************
 *      DEFINES
 *********************/
#define RAMFSFS_LETTER      'R'

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 * GLOBAL PROTOTYPES
 **********************/

/**
 * Create a driver for flashfs and initialize it.
 */
void lv_ramfs_init(void);

/**********************
 *      MACROS
 **********************/

#endif /*USE_LV_FILESYSTEM*/

#ifdef __cplusplus
} /* extern "C" */
#endif

