/**
 * @file lv_ramfs.c
 * Implementation of flash file system which do NOT support directories.
 * The API is compatible with the lv_fs_int module.
 */
/*********************
 *      INCLUDES
 *********************/
 
 
#include "lv_ramfs.h"
#include "src/lv_misc/lv_fs.h"
#include "evm_vfs_api.h"
#include "evm_k_api.h"
#include "evm_ramfs.h"
#include "qvm_conf.h"
#include <string.h>
#include <stdio.h>
#include <errno.h>

/*********************
 *      DEFINES
 *********************/

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/
/**********************
 *  STATIC VARIABLES
 **********************/
static int  ramfs_inited = 0;

/**************************************************************************************************************
 *      MACROS
 **************************************************************************************************************/

/**************************************************************************************************************
 *   STATIC FUNCTIONS
 **************************************************************************************************************/
/**
 * Give the state of the ramfs
 * @return true if ramfs is initialized and can be used else false
 */
static bool lv_ramfs_ready(struct _lv_fs_drv_t * drv)
{
    return ramfs_inited == 1;
}

/**
 * Open a file in ramfs
 * @param file_p pointer to a lv_ramfs_file_t variable
 * @param fname name of the file. There are no directories so e.g. "myfile.txt"
 * @param mode element of 'fs_mode_t' enum or its 'OR' connection (e.g.
 * FS_MODE_WR | FS_MODE_RD)
 * @return LV_FS_RES_OK: no error, the file is opened
 *         any error from lv__fs_res_t enum
 */
static lv_fs_res_t lv_ramfs_size(struct _lv_fs_drv_t * drv, void *file_p, uint32_t *size_p);

static lv_fs_res_t lv_ramfs_open(struct _lv_fs_drv_t * drv, void *file_p, const char *fname, lv_fs_mode_t mode)
{
    int32_t flags;
    int32_t *fd = (int32_t *)file_p;

    if (mode == LV_FS_MODE_WR) flags = EVM_O_RDWR;
    else if (mode == LV_FS_MODE_RD) flags = EVM_O_RDONLY;
    else if (mode == (LV_FS_MODE_WR | LV_FS_MODE_RD)) flags = EVM_O_RDWR;
    else flags = EVM_O_RDWR;

    char fpath[strlen(fname) + 4];
    sprintf(fpath, "%c:/%s", RAMFSFS_LETTER, fname);
    *fd = evm_vfs_open(fpath, flags);

    if (*fd <= 0) {
        return LV_FS_RES_UNKNOWN;
    }
	
	uint32_t len = 0;
	lv_ramfs_size(drv, fname,&len);

    return LV_FS_RES_OK;
}

/**
 * Close an opened file
 * @param file_p pointer to an 'ramfs_file_t' variable. (opened with lv_ramfs_open)
 * @return LV_FS_RES_OK: no error, the file is read
 *         any error from lv__fs_res_t enum
 */
static lv_fs_res_t lv_ramfs_close(struct _lv_fs_drv_t * drv, void *file_p)
{
    int32_t fd = *(int32_t *)file_p;
    int32_t ret = 0;

    if (fd == 0) {
        return LV_FS_RES_OK;
    }

    ret = evm_vfs_close(fd);
    if (ret < 0) {
        return LV_FS_RES_INV_PARAM;
    }

    return LV_FS_RES_OK;
}

/**
 * Remove a file. The file can not be opened.
 * @param fname '\0' terminated string
 * @return LV_FS_RES_OK: no error, the file is removed
 *         LV_FS_RES_DENIED: the file was opened, remove failed
 */
static lv_fs_res_t lv_ramfs_remove(struct _lv_fs_drv_t * drv, const char *fname)
{
    int32_t ret = 0;

    char fpath[strlen(fname) + 4];
    sprintf(fpath, "%c:/%s", RAMFSFS_LETTER, fname);
    ret= evm_vfs_remove(fname);
    if (ret < 0) {
        return LV_FS_RES_UNKNOWN;
    }

    return LV_FS_RES_OK;
}

/**
 * Read data from an opened file
 * @param file_p pointer to an 'ramfs_file_t' variable. (opened with lv_ramfs_open )
 * @param buf pointer to a memory block where to store the read data
 * @param btr number of Bytes To Read
 * @param br the real number of read bytes (Byte Read)
 * @return LV_FS_RES_OK: no error, the file is read
 *         any error from lv__fs_res_t enum
 */
static lv_fs_res_t lv_ramfs_read(struct _lv_fs_drv_t * drv, void *file_p, void *buf, uint32_t btr, uint32_t *br)
{
    int32_t fd = *(int32_t *)file_p;
    int32_t ret = 0;

    *br = 0;
    if (fd == 0) {
        return LV_FS_RES_INV_PARAM;
    }
    ret= evm_vfs_read(fd, buf,  btr);
    char* b = (char*)buf;
    *br = ret;
    if (ret != btr) {
         return LV_FS_RES_UNKNOWN;
    }

    return LV_FS_RES_OK;
}

/**
 * Write data to an opened file
 * @param file_p pointer to an 'ramfs_file_t' variable. (opened with lv_ramfs_open)
 * @param buf pointer to a memory block which content will be written
 * @param btw the number Bytes To Write
 * @param bw The real number of written bytes (Byte Written)
 * @return LV_FS_RES_OK: no error, the file is read
 *         any error from lv__fs_res_t enum
 */
static lv_fs_res_t lv_ramfs_write(struct _lv_fs_drv_t * drv, void *file_p, const void *buf, uint32_t btw, uint32_t *bw)
{
    uint32_t fd = *(uint32_t *)file_p;
    int32_t ret = 0;

    *bw = 0;
    if (fd == 0) {
        return LV_FS_RES_INV_PARAM;
    }

    ret = evm_vfs_write(fd, buf, btw);
    *bw = ret;
    if (ret != btw) {
        *bw = 0;
        return LV_FS_RES_UNKNOWN;
    }

    return LV_FS_RES_OK;
}

/**
 * Set the read write pointer. Also expand the file size if necessary.
 * @param file_p pointer to an 'ramfs_file_t' variable. (opened with lv_ramfs_open )
 * @param pos the new position of read write pointer
 * @return LV_FS_RES_OK: no error, the file is read
 *         any error from lv__fs_res_t enum
 */
static lv_fs_res_t lv_ramfs_seek(struct _lv_fs_drv_t * drv, void *file_p, uint32_t pos)
{
    int32_t fd = *(int32_t *)file_p;
    int32_t ret = 0;

    if (fd == 0) {
        return LV_FS_RES_INV_PARAM;
    }

    ret = evm_vfs_lseek(fd, pos, SEEK_SET);
    if (ret < 0)
        return LV_FS_RES_UNKNOWN;

    return LV_FS_RES_OK;
}

/**
 * Give the size of the file in bytes
 * @param file_p file_p pointer to an 'ramfs_file_t' variable. (opened with
 * lv_ramfs_open )
 * @param size_p pointer to store the size
 * @return LV_FS_RES_OK: no error, the file is read
 *         any error from lv__fs_res_t enum
 */
static lv_fs_res_t lv_ramfs_size(struct _lv_fs_drv_t * drv, void *file_p, uint32_t *size_p)
{
    int32_t fd = *(int32_t *)file_p;

    if (fd == 0) {
        return LV_FS_RES_INV_PARAM;
    }
    evm_vfs_stat_t st;
    evm_vfs_fstat(fd, &st);

    *size_p = st.st_size;

    return LV_FS_RES_OK;
}

/**
 * Rename flash file (C:)
 * @param oldname
 * @param newname
 * @return LV_FS_RES_OK: no error, the file is read
 *         any error from lv__fs_res_t enum
 */
static lv_fs_res_t lv_ramfs_rename(struct _lv_fs_drv_t * drv, const char * oldname, const char * newname)
{
    int32_t ret = 0;

    char oldfpath[strlen(oldname) + 4];
    sprintf(oldfpath, "%c:/%s", RAMFSFS_LETTER, oldname);

    char newfpath[strlen(newname) + 4];
    sprintf(newfpath, "%c:/%s", RAMFSFS_LETTER, newname);

    ret = evm_vfs_rename(oldfpath, newfpath);
    if (ret != 0)
        return LV_FS_RES_UNKNOWN;

    return LV_FS_RES_OK;
}

/**************************************************************************************************************
 *   GLOBAL FUNCTIONS
 **************************************************************************************************************/
/**
 * Create a driver for ramfs and initialize it.
 */

void lv_ramfs_init(void)
{
    lv_fs_drv_t ramfs_drv;

    if (ramfs_inited) {
        return;
    }

    evm_vfs_init();
    evm_ramfs_register(LV_RAMFS_DIR);

    memset(&ramfs_drv, 0, sizeof(lv_fs_drv_t));

    ramfs_drv.file_size  = sizeof(uint32_t);
    ramfs_drv.letter     = RAMFSFS_LETTER;
    ramfs_drv.ready_cb   = lv_ramfs_ready;

    ramfs_drv.open_cb    = lv_ramfs_open;
    ramfs_drv.close_cb   = lv_ramfs_close;
    ramfs_drv.remove_cb  = lv_ramfs_remove;
    ramfs_drv.read_cb    = lv_ramfs_read;
    ramfs_drv.write_cb   = lv_ramfs_write;
    ramfs_drv.seek_cb    = lv_ramfs_seek;
    ramfs_drv.size_cb    = lv_ramfs_size;
    ramfs_drv.rename_cb  = lv_ramfs_rename;

    lv_fs_drv_register(&ramfs_drv);
    ramfs_inited = 1;
}

