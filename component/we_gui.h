/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_GUI_H
#define WE_GUI_H

#include "webengine.h"

extern int gui_init_ex(qvm_state_t *e);
extern int gui_init(qvm_state_t *e);
extern void we_gui_destroy_all(void);

//style
qvm_value_t html_obj_set_alignitems(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_backgroundcolor(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_borderopacity(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_bordercolor(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_borderwidth(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_borderradius(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_display(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_fade_in(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_fade_out(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_flexdirection(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_gradient_color(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_margin(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_margin_left(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_margin_top(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_margin_right(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_margin_bottom(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_opacity(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_padding(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_padding_left(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_padding_top(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_padding_right(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_padding_bottom(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_y(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_x(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_text_color(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_text_align(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_text_font_size(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_text_letter_space(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_text_font_family(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_text_overflow(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_text_animation_speed(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_width(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_height(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_background(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_set_draggable(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);

//basic
qvm_value_t lvgl_html_basic_show(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t lvgl_html_basic_hide(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t lvgl_html_basic_destroy(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);

//Button
qvm_value_t html_obj_Button(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);

//Canvas
qvm_value_t html_obj_Canvas(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);

//Div
qvm_value_t html_obj_Div(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);

//Image
qvm_value_t html_obj_Image(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_Image_set_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Image_get_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Image_set_angle(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Image_get_angle(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Image_set_zoom(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Image_get_zoom(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Image_get_pivot(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Image_set_pivot(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);

//ImageButton
qvm_value_t html_obj_ImageButton(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_ImageButton_set_pressed_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_ImageButton_set_released_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_ImageButton_set_disabled_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_ImageButton_set_checked_pressed_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_ImageButton_set_checked_released_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_ImageButton_set_checked_disabled_src(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
//Input
qvm_value_t html_obj_Input(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_Input_set_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_Input_get_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_Input_set_placeholder(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_Input_get_placeholder(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);

//List
qvm_value_t html_obj_List(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);

//ListItem
qvm_value_t html_obj_ListItem(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);

//Progress
qvm_value_t html_obj_Progress(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_Progress_set_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Progress_get_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Progress_set_duration(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Progress_get_duration(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Progress_set_start_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Progress_get_start_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);

//Select
qvm_value_t html_obj_Select(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
//Option
qvm_value_t html_obj_Option(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
//Slider
qvm_value_t html_obj_Slider(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_Slider_set_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Slider_get_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Slider_set_duration(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Slider_get_duration(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Slider_set_start_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Slider_get_start_value(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);

//Text
qvm_value_t html_obj_Text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_Text_set_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Text_get_text(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);

//Textarea
qvm_value_t html_obj_Textarea(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_Textarea_set_text(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t *v);
qvm_value_t html_obj_Textarea_get_text(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);

//TabView
qvm_value_t html_obj_TabView(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);

//Tab
qvm_value_t html_obj_Tab(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v);

//Line
qvm_value_t html_obj_Line(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_Line_set_points(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_Line_set_line_width(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);
qvm_value_t html_obj_Line_set_line_color(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v);


#endif
