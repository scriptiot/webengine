---
summary: 系统宏编译定义
version: 1.0
date: 2022.1.16
---

### WE_USE_ECMA_FUNCTION
引擎使用function内置对象

### WE_USE_ECMA_ARRAY
引擎使用array内置对象

### WE_USE_ECMA_STRING
引擎使用string内置对象

### WE_USE_ECMA_MATH
引擎使用math内置对象

### WE_USE_ECMA_NUMBER
引擎使用number内置对象

### WE_USE_ECMA_BOOLEAN
引擎使用boolean内置对象

### WE_USE_ECMA_DATE
引擎使用date内置对象

### WE_USE_ECMA_JSON
引擎使用JSON内置对象

### WE_USE_BASE64
引擎使用base64库

### WE_USE_HEATSHRINK
引擎使用heatshrink压缩解压库

### WE_USE_OPENGL
引擎使用opengl代替canvas

### WE_USE_SOCKET
引擎使用socket库

### WE_USE_HTTP
引擎使用http协议解析库

### WE_USE_PROCESS
引擎使用process库
