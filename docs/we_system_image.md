---
system: @system.image
summary: 图片资源管理操作
version: 1.0
date: 2022.1.16
---

--------------------------------------------------
# image.append

```javascript
image.append(path)
```

添加图片资源到image缓存

## 参数表

Name | Type | Description
-----|------|--------------
`path`|`string`| path 图片路径，根据后缀类型进行图片解码，目前仅支持png

## 返回值

> `object`: 成功返回图片缓存对象

## 调用示例

```javascript
var png_data = image.append('test.png');
console.log(png_data.type);
console.log(png_data.width);
console.log(png_data.height);
```


--------------------------------------------------
# image.remove

```javascript
image.remove(path)
```

从image缓存中删除图片资源

## 参数表

Name | Type | Description
-----|------|--------------
`path`|`string`| path 图片路径

## 返回值

> `boolean`: 成功返回true

## 调用示例

```javascript
image.remove('test.png');
```

--------------------------------------------------
# image.get

```javascript
image.get(path)
```

从image缓存中获取图片对象

## 参数表

Name | Type | Description
-----|------|--------------
`path`|`string`| path 图片路径

## 返回值

> `object`: 成功返回图片缓存对象

## 调用示例

```javascript
var png_data = image.get('test.png');
```
