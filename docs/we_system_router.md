---
system: @system.router
summary: router操作
version: 1.0
date: 2022.1.16
---

--------------------------------------------------
# router.push

```javascript
router.push(object)
```

router 切换新页面并缓存

## 参数表

```javascript
object = {
    path: 'evue_new_page' //evue文件名称
}
```

## 返回值

> *无返回值*

## 调用示例

```javascript
router.push({
    path: 'evue_new_page'
})
```


--------------------------------------------------
# router.repalce

```javascript
router.repalce(object)
```

router 切换新页面，不缓存

## 参数表

```javascript
object = {
    path: 'evue_new_page' //evue文件名称
}
```


## 返回值

> *无返回值*

## 调用示例

```javascript
router.replace({
    path: 'evue_new_page'
})
```


--------------------------------------------------
# router.launch

```javascript
router.launch(object)
```

router 切换新页面，并重新启动引擎

## 参数表

```javascript
object = {
    path: 'evue_new_page' //evue文件名称
}
```


## 返回值

> *无返回值*

## 调用示例

```javascript
router.launch({
    path: 'evue_new_page'
})


