---
system: @system.process
summary: 进程操作
version: 1.0
date: 2022.1.16
---

--------------------------------------------------
# process.nextTick

```javascript
process.nextTick(callback, [...args]);
```

base64编码

## 参数表

Name | Type | Description
-----|------|--------------
`callback`|`function`| callback 回调函数
`[...args]`|`arguments`| callback 回调函数的传参

## 返回值

> *无返回值*

## 调用示例

```javascript
process.nextTick(function(a, b){}, [1, 2]);
```
