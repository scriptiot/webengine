---
system: @system.socket
summary: socket操作
version: 1.0
date: 2022.1.16
---

--------------------------------------------------
# socket.connect

```javascript
socket.connect(host, port, rx_buf_size, tx_buf_size)
```

socket tcp网络链接

## 参数表

Name | Type | Description
-----|------|--------------
`host`|`string`| host 服务器域名或ip
`port`|`int`| port 服务器端口
`rx_buf_size`|`int`| rx_buf_size 可选，接收缓存大小
`tx_buf_size`|`int`| tx_buf_size 可选，发送缓存大小

## 返回值

> `object`: 成功返回socket对象

## 调用示例

```javascript
//参考socket的说明, 并查阅demo
```


--------------------------------------------------
# socketObj.close

```javascript
socketObj.close()
```

关闭socket对象

## 参数表

Name | Type | Description
-----|------|--------------


## 返回值

> *无返回值*

## 调用示例

```javascript
//参考socket的说明, 并查阅demo
```


--------------------------------------------------
# socketObj.write

```javascript
socketObj.write(buffer|string, len)
```

通过socket对象发送数据

## 参数表

Name | Type | Description
-----|------|--------------
`buffer|string`|`buffer|string`| buffer|string buffer数组或者字符串
`len`|`int`| len 可选，写入的数据长度

## 返回值

> `int`: 返回写入的数据长度

## 调用示例

```javascript
socketObj.write('hello evm') 
```


--------------------------------------------------
# socketObj.read

```javascript
socketObj.read(size)
```

通过socket对象读取数据

## 参数表

Name | Type | Description
-----|------|--------------
`size`|`int`| pin 读取数据长度

## 返回值

> `buffer`: 返回读取的数据buffer数组

## 调用示例

```javascript
var data = socketObj.read(100)
```


--------------------------------------------------
# socketObj.on

```javascript
socketObj.on(event, callback)
```

通过socket对象注册事件回调函数

## 参数表

Name | Type | Description
-----|------|--------------
`event`|`string`| event 事件名称，尽支持'recv', 'connected', 'disconnected', 'close'
`callback`|`function`| callback 事件触发的回调函数

## 返回值

> *无返回值*

## 调用示例

```javascript
socketObj.on('recv', function(code, data) {
    console.log(data);
})
```


--------------------------------------------------
# socketObj.destroy

```javascript
socketObj.destroy()
```

销毁socket对象资源

## 参数表

Name | Type | Description
-----|------|--------------


## 返回值

> *无返回值*

## 调用示例

```javascript
socketObj.destroy();
```

