---
system: @system.http_parser
summary: http_parser操作
version: 1.0
date: 2022.1.16
---

--------------------------------------------------
# http_parser.create

```javascript
http_parser.create(type)
```

创建http_parser对象

## 参数表

Name | Type | Description
-----|------|--------------
`type`|`int`| host http解析类型，可以是http_parser.REQUEST, http_parser.RESPONSE

## 返回值

> `object`: 成功返回http_parser对象

## 调用示例

```javascript
var parser = http_parser.create(http_parser.RESPONSE);
```


--------------------------------------------------
# parser.execute

```javascript
parser.execute(data)
```

执行parser解析

## 参数表

Name | Type | Description
-----|------|--------------
`data`|`buffer`| data 待解析的buffer数组数据


## 返回值

> `object`: 解析成功后返回解析内容对象

## 调用示例

```javascript
var res = parser.execute(data);
console.log(res.code);
console.log(res.body);
```

--------------------------------------------------
# parser.destroy

```javascript
parser.destroy()
```

释放parser资源

## 参数表

Name | Type | Description
-----|------|--------------


## 返回值

> *无返回值*

## 调用示例

```javascript
parser.destroy();
```
