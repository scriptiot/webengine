---
system: @system.heatshrink
summary: heatshrink压缩解压
version: 1.0
date: 2022.1.16
---

--------------------------------------------------
# heatshrink.compress

```javascript
heatshrink.compress(buffer|string)
```

base64编码

## 参数表

Name | Type | Description
-----|------|--------------
`buffer|string`|`buffer|string`| buffer|string 待压缩的数据

## 返回值

> `buffer`: 成功返回压缩后的buffer数组

## 调用示例

```javascript
var data = heatshrink.compress('hello evm');
```


--------------------------------------------------
# heatshrink.decompress

```javascript
heatshrink.decompress(buffer|string)
```

base64编码

## 参数表

Name | Type | Description
-----|------|--------------
`buffer|string`|`buffer|string`| buffer|string 待解压的数据

## 返回值

> `buffer`: 成功返回解压后的buffer数组

## 调用示例

```javascript
var data = heatshrink.decompress('hello evm');
```
