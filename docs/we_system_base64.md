---
system: @system.base64
summary: base64编解码操作
version: 1.0
date: 2022.1.16
---

--------------------------------------------------
# base64.encode

```javascript
base64.encode(buffer|string)
```

base64编码

## 参数表

Name | Type | Description
-----|------|--------------
`buffer|string`|`buffer|string`| buffer|string 待编码的数据

## 返回值

> `string`: 成功返回编码后的字符串

## 调用示例

```javascript
var data = base64.encode('hello evm');
```


--------------------------------------------------
# base64.decode

```javascript
base64.decode(buffer|string)
```

base64编码

## 参数表

Name | Type | Description
-----|------|--------------
`buffer|string`|`buffer|string`| buffer|string 待解码的数据

## 返回值

> `buffer`: 成功返回编码后的buffer数组

## 调用示例

```javascript
var data = base64.decode('hello evm');
```
