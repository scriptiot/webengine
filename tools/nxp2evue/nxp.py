# -*- coding: utf-8 -*-
import sys
import os
import json
from typing import (
    Any,
    Optional,
    Dict,
    Mapping,
    List,
    Tuple,
    Match,
    Callable,
    Type,
    Sequence,
)
import fs
from PIL import Image
from jinja2 import Environment, FileSystemLoader
from jinja2.utils import generate_lorem_ipsum
import logging
from logging.handlers import RotatingFileHandler

#log write in console
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
#log formatter
formatter = logging.Formatter(
    '%(asctime)s %(levelname)8s [%(filename)25s%(funcName)20s%(lineno)06s] %(message)s')
ch.setFormatter(formatter)

logger = logging.root
logger.setLevel(logging.INFO)
logger.addHandler(ch)

class ObjectDict(Dict[str, Any]):
    """Makes a dictionary behave like an object, with attribute-style access."""

    def __getattr__(self, name: str) -> Any:
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)

    def __setattr__(self, name: str, value: Any) -> None:
        self[name] = value


ROOT = os.path.abspath(os.getcwd())
jinja_env = Environment(loader=FileSystemLoader(os.path.join(ROOT, 'templates')))

class Evue(ObjectDict):

    __types__ = {
        'cont': 'div',
        'btn': 'button',
        'label': 'text',
        'btn': 'text',
        'slider': 'slider',
        'img': 'image',
        'imgbtn': 'img',
        'tabview': 'tabview',
        'tab': 'tab',
        'page': 'list',
        'progress': 'bar',
        'bar': 'bar',
    }

    def __init__(self, fpath, output):
        super().__init__()
        self.guiguider = os.path.abspath(fpath)
        self.output = os.path.abspath(output)
        self.projectDir = os.path.dirname(fpath)
        self.resourceDir = os.sep.join([self.projectDir, 'import'])

    def run(self):
        with open(  self.guiguider, "r", encoding='utf-8') as f:
            self.update(json.loads(f.read()))
        
        self.create_evue_project()
        for pitem in self.FrontJson:
            for item in pitem['list']:
                if item['type'] == 'imgbtn':
                    item['type'] = 'img'
                    item['img_path'] = item['src_released']
                if item['type'] == 'img' or item['type'] == 'imgbtn':
                    fpath = os.sep.join([self.resourceDir, item['img_path']])
                    image_name = item['img_path'].split('.')[0] + '_' + str(item['width']) + '_' + str(item['height']) + '.' + item['img_path'].split('.')[1]
                    newfpath = os.sep.join([self.output, image_name])
                    im = Image.open(fpath)
                    out = im.resize((item['width'], item['height']),Image.ANTIALIAS)
                    out.save(newfpath)
                    item['img_path'] = image_name
            self.create_evue(pitem)

        if self.FrontJson:
            self.create_app_js(self.FrontJson[0])

    def create_evue_project(self):
        if not os.path.exists(self.output):
            os.makedirs(self.output)
    
    def evue_type(self, lv_type='cont'):
        if lv_type in self.__types__:
            return self.__types__[lv_type]
        else:
            return 'div'
    
    def addEvueAttrs(self, item):
        item['evue_type'] = self.evue_type(item['type'])
        item['evue_id'] =  item['type'] + '_' + item['name'] + '_' + item['id']

        if 'bg_color' not in item:
            item['bg_color'] = 'transparent'

        if 'border_color' not in item:
            item['border_color'] = 'transparent'

        if 'border_width' not in item:
            item['border_width'] = 0
        
        if 'paddingLeft' not in item:
            item['paddingLeft'] = 0
        if 'paddingTop' not in item:
            item['paddingTop'] = 0
        if 'paddingRight' not in item:
            item['paddingRight'] = 0
        if 'paddingBottom' not in item:
            item['paddingBottom'] = 0
        
        if 'bg_opa' not in item:
            item['bg_opa'] = 0
        
        if 'radius' not in item:
            item['radius'] = 0
        
        if 'opacity' not in item:
            item['opacity'] = 1
        
        
        if 'layout' in item:
            if item['layout'] == 'LV_LAYOUT_OFF':
                item['align-items'] = 'off'
            elif item['layout'] == 'LV_LAYOUT_CENTER':
                item['align-items'] = 'center'
            elif item['layout'] == 'LV_LAYOUT_ROW_BOTTOM':
                item['align-items'] = 'flex-end'
            elif item['layout'] == 'LV_LAYOUT_PRETTY_MID':
                item['align-items'] = 'pretty'
            elif item['layout'] == 'LV_LAYOUT_GRID':
                item['align-items'] = 'grid'
            elif item['layout'] == 'LV_LAYOUT_GRID':
                item['align-items'] = 'off'
        else:
            item['align-items'] = 'center'
        
        if item['type'] == 'imgbtn':
            item['img_path'] = item['src_released']

        
        if  item['evue_type'] == 'text':
            if 'text_align' not in item:
                item['text_align'] = 'center'
            if 'letter_space' not in item:
                item['letter_space'] = 0

            if 'long_mode' not in item:
                item['long_mode'] = 'LV_LABEL_LONG_BREAK'

            if item['long_mode'] == 'LV_LABEL_LONG_CROP':
                item['text_overflow'] = 'clip'
            elif item['long_mode'] == 'LV_LABEL_LONG_DOT':
                item['text_overflow'] = 'ellipsis'
            elif item['long_mode'] == 'LV_LABEL_LONG_EXPAND':
                item['text_overflow'] = 'longexpand'
            elif item['long_mode'] == 'LV_LABEL_LONG_BREAK':
                item['text_overflow'] = 'longbreak'
            elif item['long_mode'] == 'LV_LABEL_LONG_SROLL':
                item['text_overflow'] = 'scroll'
            elif item['long_mode'] == 'LV_LABEL_LONG_SROLL_CIRC':
                item['text_overflow'] = 'scrollcircle'
            else:
                item['text_overflow'] = 'longbreak'

        return item
    
    def item_2_obj(self, item):
        for key in item:
            if isinstance(item[key], dict):
                item[key] = ObjectDict(item[key])
            if isinstance(item[key], list):
                list_items = []
                for litem in item[key]:
                    obj = ObjectDict(litem)
                    list_items.append(self.addEvueAttrs(obj))
                item[key] = list_items

        _item = ObjectDict(item)
        return _item

    def create_evue(self, item):
        item['type'] = 'cont'
        item['left'] = 0
        item['top'] = 0
        item['width'] = self.lvConf['LV_HOR_RES_MAX'][1:-1]
        item['height'] = self.lvConf['LV_VER_RES_MAX'][1:-1]

        self.addEvueAttrs(item)
        _name = item['name']
        _path = os.sep.join([self.output, '%s.evue' % _name])

        self.jinja_template = jinja_env.get_template('evue.template')
        h = self.jinja_template.render({
                        "obj": self.item_2_obj(item)
                    })
        with open(_path, 'w', encoding='utf-8') as f:
            f.write(h)
    
    def creat_evue_html(self, item):
        pass
    
    def create_evue_js(self, item):
        pass

    def create_evue_css(self, item):
        pass
    
    def create_app_js(self, item):
        self.jinja_template = jinja_env.get_template('app.js.template')
        h = self.jinja_template.render({
                        "obj": self.item_2_obj(item)
                    })
        path = os.sep.join([self.output, "app.js"])
        with open(path, 'w', encoding='utf-8') as f:
            f.write(h)
    
    def copyResource(self):
        from fs.copy import copy_fs
        copy_fs(self.resourceDir, self.output)

        home_fs = fs.open_fs(self.output)
        files = []
        for jspath in home_fs.glob('*.png', namespaces=['details']):
            fpath = os.sep.join([self.output, jspath.info.name])
            im = Image.open(fpath)
            out = im.resize((width, height),Image.ANTIALIAS)
            out.save(fpath)

def json2evue(fpath, output):
    evue = Evue(fpath, output)
    evue.run()

if __name__ == '__main__':
    json2evue(sys.argv[1], sys.argv[2])
