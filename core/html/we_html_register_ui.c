#include "html/we_html.h"

static we_html_object_reg_t *_we_html_object_reg;
static we_html_property_reg_t *_we_html_style_reg;
static uint32_t _we_html_object_regs_len;
static uint32_t _we_html_style_regs_len;

static int _we_html_regs_len(we_html_object_reg_t * regs){
    if(regs == NULL) return 0;
    int i = 0;
    while(1){
        if( regs[i].name == NULL) return i;
        i++;
    }
    return i;
}

static int _we_html_value_reg_len(we_html_property_reg_t * regs) {
    int i = 0;
    if(regs == NULL) 
        return 0;
    while(1) {
        if( regs[i].name == NULL ) 
            return i;
        i++;
    }
    return i;
}

we_html_property_reg_t *we_html_reg_property_get(we_html_object_t *object, const char *name) {
    int i = 0;
    we_html_property_reg_t * o = object->regs;
    if( o == NULL )
        return NULL;
    while(o->name) {
        if( !strcmp(o->name, name) )
            return o;
        o = object->regs + i;
        i++;
    }
    return NULL;
}

void we_html_ui_object_register(qvm_state_t * e, we_html_object_reg_t *regs) {
    int objects_len;
    int j;
    we_html_property_reg_t *properties;
    if( !regs ) 
        return;
    _we_html_object_reg = regs;
    _we_html_object_regs_len = objects_len = _we_html_regs_len(regs);
    for(int i = 0; i < objects_len; i++){
        j = 0;
        properties = regs[i].regs;
        while(properties) {
            if( properties[j].name == NULL ) 
                break;
            properties[j].key = qvm_str_insert(e, properties[j].name, 0);
            j++;
        }
    }
}

void we_html_ui_style_register(qvm_state_t * e, we_html_property_reg_t *regs) {
    if( !regs ) 
        return;
    int i = 0;
    while(1){
        if( regs[i].name == NULL) 
            break;
        regs[i].key = qvm_str_insert(e, regs[i].name, 0);
        i++;
    }
    _we_html_style_reg = regs;
    _we_html_style_regs_len = i;
}

we_html_object_reg_t * we_html_get_object_reg(const char * name) {
    for(uint32_t i = 0; i < _we_html_object_regs_len; i++) {
        we_html_object_reg_t * o = _we_html_object_reg + i;
        if( !strcmp(o->name, name) )
            return o;
    }
    return NULL;
}

we_html_property_reg_t * we_html_get_style_reg(qvm_hash_t key) {
    for(uint32_t i = 0; i < _we_html_style_regs_len; i++) {
        we_html_property_reg_t *o = _we_html_style_reg + i;
        if( key == o->key )
            return o;
    }
    return NULL;
}

void we_html_object_copy_from_reg(we_html_object_t *object, we_html_object_reg_t *reg) {
    object->name = reg->name;
    object->regs = reg->regs;
    object->regs_len = _we_html_value_reg_len(reg->regs);
    object->create = reg->api;
}

