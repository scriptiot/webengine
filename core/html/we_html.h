/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_HTML_H
#define WE_HTML_H

#ifdef __cplusplus
extern "C" {
#endif

#include "webengine.h"

typedef struct we_html_object_t {
    const char *name;
    we_html_property_reg_t *regs;
    int regs_len;
    qvm_native_t create;
    qvm_native_t destroy;
    void *obj_data;
    void *user_data;
    void *style_data;
    struct we_html_object_t *parent;
} we_html_object_t;

we_html_property_reg_t *we_html_reg_property_get(we_html_object_t *object, const char *name);

qvm_value_t we_html_object_get_attributes(qvm_state_t *e, qvm_value_t object);
qvm_value_t we_html_object_get_elements(qvm_state_t *e, qvm_value_t object);
qvm_value_t we_html_object_get_value(qvm_state_t *e, qvm_value_t object);
qvm_value_t we_html_object_get_name(qvm_state_t *e, qvm_value_t object);
qvm_value_t we_html_object_get_id(qvm_state_t *e, qvm_value_t object);
qvm_value_t we_html_object_get_class(qvm_state_t *e, qvm_value_t object);
qvm_value_t we_html_object_get_style(qvm_state_t *e, qvm_value_t object);


we_html_object_reg_t *we_html_get_object_reg(const char *name);
we_html_property_reg_t *we_html_get_style_reg(qvm_hash_t key);
void we_html_object_copy_from_reg(we_html_object_t *object, we_html_object_reg_t *reg);

int we_html_init(qvm_state_t *e);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
