#include "html/we_html.h"


qvm_value_t we_html_object_get_attributes(qvm_state_t *e, qvm_value_t object) {
    return qvm_get_property(e, object, "attritubes");
}

qvm_value_t we_html_object_get_elements(qvm_state_t *e, qvm_value_t object) {
    return qvm_get_property(e, object, "childNodes");
}

qvm_value_t we_html_object_get_value(qvm_state_t *e, qvm_value_t object) {
    return qvm_get_property(e, object, "value");
}

qvm_value_t we_html_object_get_name(qvm_state_t *e, qvm_value_t object) {
    return qvm_get_property(e, object, "name");
}

qvm_value_t we_html_object_get_id(qvm_state_t *e, qvm_value_t object) {
    return qvm_get_property(e, object, "id");
}

qvm_value_t we_html_object_get_class(qvm_state_t *e, qvm_value_t object) {
    return qvm_get_property(e, object, "class");
}

qvm_value_t we_html_object_get_style(qvm_state_t *e, qvm_value_t object) {
    qvm_value_t attributes = we_html_object_get_attributes(e, object);
    return qvm_get_property(e, attributes, "style");
}

void we_html_object_set_obj_data(qvm_value_t object, void *obj) {
    we_html_object_t * html_obj = (we_html_object_t*)qvm_to_invoke(we_get_runtime(), qvm_get_property(we_get_runtime(), object, ".data"));
    if( html_obj == NULL )
        return;
    html_obj->obj_data = obj;
}

void *we_html_object_get_obj_data(qvm_value_t o) {
    we_html_object_t * html_obj = (we_html_object_t*)qvm_to_invoke(we_get_runtime(), qvm_get_property(we_get_runtime(), o, ".data"));
    if( html_obj != NULL )
        return html_obj->obj_data;
    return NULL;
}

void we_html_object_set_style_data(qvm_value_t object, void *obj) {
    we_html_object_t * html_obj = (we_html_object_t*)qvm_to_invoke(we_get_runtime(), qvm_get_property(we_get_runtime(), object, ".data"));
    if( html_obj == NULL )
        return;
    html_obj->style_data = obj;
}

void *we_html_object_get_style_data(qvm_value_t o) {
    we_html_object_t * html_obj = (we_html_object_t*)qvm_to_invoke(we_get_runtime(), qvm_get_property(we_get_runtime(), o, ".data"));
    if( html_obj != NULL )
        return html_obj->style_data;
    return NULL;
}

void we_html_object_set_user_data(qvm_value_t object, void *obj) {
    we_html_object_t * html_obj = (we_html_object_t*)qvm_to_invoke(we_get_runtime(), qvm_get_property(we_get_runtime(), object, ".data"));
    if( html_obj == NULL )
        return;
    html_obj->user_data = obj;
}

void *we_html_object_get_user_data(qvm_value_t o) {
    we_html_object_t * html_obj = (we_html_object_t*)qvm_to_invoke(we_get_runtime(), qvm_get_property(we_get_runtime(), o, ".data"));
    if( html_obj != NULL )
        return html_obj->user_data;
    return NULL;
}

void we_html_object_set_destroy(qvm_value_t object, qvm_native_t fn) {
    we_html_object_t * html_obj = (we_html_object_t*)qvm_to_invoke(we_get_runtime(), qvm_get_property(we_get_runtime(), object, ".data"));
    if( html_obj == NULL )
        return;
    html_obj->destroy = fn;
}
