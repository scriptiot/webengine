#include "rendering/we_render.h"
#include "css/we_css.h"
#include "binding/we_binding.h"
#include "page/we_page.h"


we_html_object_t *we_render_html_object(qvm_state_t *e, qvm_value_t parent_object, qvm_value_t object, qvm_value_t scope, int refresh);
void we_render_html_object_value(qvm_state_t *e, qvm_value_t object, qvm_value_t scope);
void we_render_html_object_attributes(qvm_state_t *e, qvm_value_t object, qvm_value_t scope, int refresh);
void we_render_html_component_attributes(qvm_state_t *e, qvm_value_t object, qvm_value_t scope, int refresh);
void we_render_html_object_elements(qvm_state_t *e, qvm_value_t object, qvm_value_t scope, int refresh);

we_html_object_t *we_render_html_object(qvm_state_t *e, qvm_value_t parent_object, qvm_value_t object, qvm_value_t scope, int refresh) {
    QVM_UNUSED(scope);
    QVM_UNUSED(refresh);

    we_html_object_t *html_object = NULL;
    html_object = (we_html_object_t *)qvm_malloc(e, sizeof(we_html_object_t));
    if( !html_object ) {
        qvm_throw(e, qvm_error(e, "Insufficient system memory"));
        return NULL;
    }
    memset(html_object, 0, sizeof(we_html_object_t));
    qvm_add_property(e, object, ".data", qvm_mk_invoke(html_object));

    qvm_value_t label_name_val = qvm_get_property(e, object, "name");
    if ( qvm_is_undefined(label_name_val) ){
        return NULL;
    }

    const char *label_name = qvm_to_string(e, label_name_val );
    we_html_object_reg_t *object_reg = we_html_get_object_reg(label_name);
    if( object_reg ) {
        we_html_object_copy_from_reg(html_object, object_reg);
        if( html_object->create ) {
            if( we_get_user_data(e, parent_object) == 0 ) {
                html_object->create(e, object, 0, NULL);
            } else {
                html_object->create(e, object, 1, &parent_object);
            }
        }
        //rendering css style
        we_css_run(e, object);
        //rendering html value
        we_render_html_object_value(e, object, scope);
        //rendering elements
        we_render_html_object_elements(e, object, scope, refresh);
        //rendering html attributes
        we_render_html_object_attributes(e, object, scope, refresh);
    } else if( !strcmp(label_name, "template") ) {
        we_render_template(e, parent_object, object, scope, refresh);
    } else {
        //load evue file as component
        qvm_value_t index_object = we_page_load_evue_file(label_name);
        if( !qvm_is_undefined(index_object) ) {
            qvm_push(e, index_object);
            qvm_value_t html_object = qvm_get_property(e, index_object, "html_object");
            qvm_value_t def = qvm_get_property(e, index_object, "default");

            we_binding_define_property(e, def);

            qvm_add_property(e, object, "index_object", index_object);
            //rendering object
            if( qvm_is_object(html_object)) {
                we_render_html_object(e, parent_object, html_object, def, refresh);
            }
            //call 'onCreate' function
            qvm_value_t onCreate = qvm_get_property(e, def, "onCreate");
            if( !qvm_is_undefined(onCreate) ) {
                //onCreate(self, parent)
                qvm_value_t args[2];
                args[0] = object;
                args[1] = parent_object;
                qvm_call(e, onCreate, def, 2, args);
            }

            //rendering html attributes
            we_render_html_component_attributes(e, object, def, refresh);

            if( qvm_is_object(html_object)) {
                //rendering elements
                we_render_html_object_elements(e, object, def, refresh);
            }
        }
    }
    return html_object;
}

void we_render_html_object_value(qvm_state_t *e, qvm_value_t object, qvm_value_t scope) {
    QVM_UNUSED(scope);
    we_html_object_t *html_object = (we_html_object_t*)we_get_user_data(e, object);
    if( !html_object )
        return;
    qvm_value_t value = qvm_get_property(e, object, "value");
    if( qvm_is_undefined(value) )
        return;
    we_binding_compile(e, object, value, "text", scope);
}

void we_render_html_object_attributes(qvm_state_t *e, qvm_value_t object, qvm_value_t scope, int refresh) {
    QVM_UNUSED(scope);
    we_html_object_t *html_object = (we_html_object_t*)we_get_user_data(e,object);
    qvm_value_t attributes = qvm_get_property(e, object, "attributes");
    if( !qvm_is_object(attributes) )
        return;
    //dynamic rendering
    if( refresh ) {
        attributes = qvm_copy(e, attributes);
    }

    qvm_push(e, attributes);

    qvm_value_t next = qvm_first_property(e, attributes);
    while(qvm_is_valid(next)){
        const char *key = qvm_get_propertyname(e, next);
        if( key[0] == ':') {
            we_html_property_reg_t * reg = we_html_reg_property_get(html_object, key + 1);
            if (reg && reg->set_api) {
                we_binding_watcher_create(e, object, attributes, qvm_mk_string(key), reg->set_api, reg->get_api);
                const char *content = qvm_to_string(e, next);
                qvm_build_js_rule(e);
                qvm_value_t run = qvm_parse_string(e, content);
                qvm_value_t res = qvm_call(e, run, we_page_get_current_default(e), 0, NULL);
                we_binding_watcher_clear_current(e);
                qvm_print_value(e, res);
                qvm_value_t args[2];
                args[0] = res;
                args[1] = qvm_mk_string(key);
                reg->set_api(e, object, 2, args);
            }
        } else {
            we_html_property_reg_t * reg = we_html_reg_property_get(html_object, key);
            if (reg && reg->set_api) {
                reg->set_api(e, object, 1, &next);
            }
        }
        next = qvm_next_property(e, attributes, next);
    }
}

static qvm_value_t component_attribute_setter(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v){
    qvm_value_t index_object = qvm_get_property(e, p, "index_object");
    qvm_value_t def = qvm_get_property(e, index_object, "default");
    qvm_value_t newvalue = v[0];
    qvm_value_t key = v[1];

    const char* name = qvm_to_string(e, key);
    qvm_value_t method = qvm_get_property(e, def, name);
    if( qvm_is_script(method) ){
        qvm_call(e, method, def, 1, &newvalue);
    }
    return qvm_undefined;
}

void we_render_html_component_attributes(qvm_state_t *e, qvm_value_t object, qvm_value_t scope, int refresh) {
    QVM_UNUSED(scope);
    qvm_value_t attributes = qvm_get_property(e, object, "attributes");
    if( !qvm_is_object(attributes) )
        return;
    //dynamic rendering
    if( refresh ) {
        attributes = qvm_copy(e, attributes);
    }

    qvm_push(e, attributes);

    qvm_value_t next = qvm_first_property(e, attributes);
    while(qvm_is_valid(next)){
        const char *key = qvm_get_propertyname(e, next);
        if( key[0] == ':') {
            we_binding_watcher_create(e, object, attributes, qvm_mk_string(key + 1), component_attribute_setter, NULL);
            const char *content = qvm_to_string(e, next);
            qvm_build_js_rule(e);
            qvm_value_t run = qvm_parse_string(e, content);
            qvm_value_t res = qvm_call(e, run, we_page_get_current_default(e), 0, NULL);
            we_binding_watcher_clear_current(e);
            qvm_print_value(e, res);
            qvm_value_t args[2];
            args[0] = res;
            args[1] = qvm_mk_string(key + 1);
            component_attribute_setter(e, object, 2, args);
        }
        next = qvm_next_property(e, attributes, next);
    }
}

void we_render_html_object_elements(qvm_state_t *e, qvm_value_t object, qvm_value_t scope, int refresh) {
    qvm_value_t elements = qvm_get_property(e, object, "childNodes");
    if( !qvm_is_array(elements) )
        return;
    if( refresh ) {
        elements = qvm_copy(e, elements);
    }
    if( qvm_is_undefined(elements) )
        return;
    qvm_push(e, elements);
    for(int i = 0; i < qvm_get_array_length(e, elements); i++ ) {
        qvm_value_t local_v = qvm_get_array(e, elements, i);
        if( qvm_is_object(local_v) ){
            if( refresh ) {
                local_v = qvm_copy(e, local_v);
                qvm_set_array(e, elements, i, local_v);
            }
            we_render_html_object(e, object, local_v, scope, refresh);
        }
    }
}

int we_render_run(qvm_state_t *e) {
    qvm_value_t *sp = qvm_save_sp(e);
    qvm_value_t index_object = we_page_get_current_index_object(e);
    qvm_value_t hml_object = we_page_get_current_html_object(e);
    qvm_value_t css_object = we_page_get_current_css_object(e);
    qvm_value_t def = we_page_get_current_default(e);

    if( qvm_is_undefined(index_object) || qvm_is_undefined(hml_object) || qvm_is_undefined(css_object) || qvm_is_undefined(def) )
        return 0;

    we_binding_define_property(e, we_page_get_current_default(e));
    qvm_add_property(e, hml_object, "index_object", index_object);
    we_page_manager_create_components(e);
    qvm_value_t first_node = we_page_get_current_top_html_object(e);
    we_render_html_object(e, qvm_undefined, first_node, def, 0);
    qvm_restore_sp(e, sp);
    return 1;
}
