#include "rendering/we_render.h"
#include "page/we_page.h"
#include "binding/we_binding.h"

void _render_template_remove_elements(qvm_state_t *e, qvm_value_t p) {
    qvm_value_t elements = qvm_get_property(e, p, "templates");
    if( qvm_is_array(elements) ) {
        for(int i = 0; i < qvm_get_array_length(e, elements); i++ ) {
            qvm_value_t local_v = qvm_get_array(e, elements, i);
            if( qvm_is_object(local_v) ){
                we_html_object_t *html_object = (we_html_object_t *)we_get_user_data(e, local_v);
                if( html_object && html_object->destroy ) {
                    we_page_manager_remove_component(e, html_object->obj_data);
                    html_object->destroy(e, local_v, 0, NULL);
                }
                if( html_object && html_object->style_data )
                    we_free(html_object->style_data);
                we_free(html_object);
                we_set_user_data(e, local_v, NULL);
                _render_template_remove_elements(e, local_v);
            }
        }
    }
}

static void render_template_remove_elements(qvm_state_t *e, qvm_value_t p) {
    qvm_value_t elements = qvm_get_property(e, p, "templates");
    if( qvm_is_array(elements) ) {
        for(int i = 0; i < qvm_get_array_length(e, elements); i++ ) {
            qvm_value_t local_v = qvm_get_array(e, elements, i);
            if( qvm_is_object(local_v) ){
                we_html_object_t *html_object = (we_html_object_t *)we_get_user_data(e, local_v);
                if( html_object && html_object->destroy ) {
                    we_page_manager_remove_component(e, html_object->obj_data);
                    html_object->destroy(e, local_v, 0, NULL);
                }
                if( html_object && html_object->style_data )
                    we_free(html_object->style_data);
                we_free(html_object);
                we_set_user_data(e, local_v, NULL);
                _render_template_remove_elements(e, local_v); 
            }
        }
    }
}

void we_render_remove_page(qvm_state_t *e, qvm_value_t p) {
    if( qvm_is_object(p) ){
        we_html_object_t *html_object = (we_html_object_t *)we_get_user_data(e, p);
        if( html_object && html_object->destroy )
            html_object->destroy(e, p, 0, NULL);
        if( html_object && html_object->style_data )
            we_free(html_object->style_data);
        we_free(html_object);
        we_set_user_data(e, p, NULL);
        _render_template_remove_elements(e, p);
    }
    render_template_remove_elements(e, p);
}

static qvm_value_t _render_template_if(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(argc);
    qvm_value_t parent_hml = qvm_get_property(e, p, "$parent");
    qvm_value_t childNodes = qvm_get_property(e, p, "childNodes");
    qvm_value_t index_obj = qvm_get_parent(e, p);
    qvm_value_t localscope = qvm_get_property(e, p, "scope");
    qvm_value_t data = qvm_get_property(e, index_obj, "data");
    if ( qvm_is_undefined(localscope) ){
        localscope = data;
    }
    qvm_value_t elements = qvm_get_property(e, p, "templates");
    if( qvm_is_array(elements) ) {
        render_template_remove_elements(e, p);
        qvm_add_property(e, p, "templates", qvm_undefined);
    }

    qvm_value_t attributes = qvm_get_property(e, p, "attributes");
    qvm_value_t prop;
    if( !qvm_is_undefined(attributes) ){
        prop = qvm_get_property(e, attributes, "v-if");
        if( qvm_is_string(prop) ) {
            qvm_build_js_rule(e);
            qvm_value_t res = qvm_parse_string(e, qvm_to_string(e, prop));
            res = qvm_call(e, res, localscope, 0, NULL);
            if(qvm_to_boolean(e, res) == 0 ){
                return qvm_undefined;
            }
        }
    }

    elements = qvm_new_array(e);
    qvm_add_property(e, p, "templates", elements);
    if( !qvm_is_undefined(childNodes)) {
        for(int i = 0; i < qvm_get_array_length(e, childNodes); i++ ) {
            qvm_value_t local_v = qvm_get_array(e, childNodes, i);
            if( qvm_is_object(local_v) ){
                local_v = qvm_copy(e, local_v);
                qvm_set_array(e, elements, i, local_v);
                we_render_html_object(e, parent_hml, local_v, localscope, 1);
            }
        }
    }
    return qvm_undefined;
}

static qvm_value_t _render_template_for(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(argc);
    qvm_value_t parent_hml = qvm_get_property(e, p, "$parent");
    qvm_value_t childNodes = qvm_get_property(e, p, "childNodes");
    qvm_value_t def = we_page_get_current_default(e);
    int templates_len = qvm_get_array_length(e, childNodes);
    qvm_value_t elements = qvm_get_property(e, p, "templates");
    if( qvm_is_array(elements) ) {
        render_template_remove_elements(e, p);
        qvm_add_property(e, p, "templates", qvm_undefined);
    }

    qvm_value_t attributes = qvm_get_property(e, p, "attributes");
    if( qvm_is_undefined(attributes) ){
        return qvm_undefined;
    }

    qvm_value_t local_scope = qvm_get_property(e, p, "scope");
    qvm_value_t index_str = qvm_get_property(e, attributes, "index");
    qvm_value_t key_str = qvm_get_property(e, attributes, "key");
    qvm_value_t items_str = qvm_get_property(e, attributes, "for");

    qvm_value_t scope_items = qvm_get_property(e, local_scope, qvm_to_string(e, items_str));

    qvm_build_js_rule(e);
    qvm_value_t res = qvm_parse_string(e, qvm_to_string(e, items_str));
    res = qvm_call(e, res, def, 0, NULL);
    qvm_add_property(e, local_scope, qvm_to_string(e, items_str), res);

    if( qvm_get_array_length(e, scope_items) ) {
        elements = qvm_new_array(e);
        qvm_add_property(e, p, "templates", elements);
        if( qvm_is_array(childNodes) ) {
            for(int i = 0; i < qvm_get_array_length(e, scope_items); i++) {
                qvm_add_property(e, local_scope, qvm_to_string(e, key_str), qvm_get_array(e, scope_items, i));
                qvm_add_property(e, local_scope, qvm_to_string(e, index_str), qvm_mk_number(i));
                for(int j = 0; j < qvm_get_array_length(e, childNodes); j++ ) {
                    qvm_value_t local_v = qvm_get_array(e, childNodes, j);
                    if( qvm_is_object(local_v) ){
                        local_v = qvm_copy(e, local_v);
                        qvm_set_array(e, elements, j + i * templates_len, local_v);
                        we_render_html_object(e, parent_hml, local_v, local_scope, 1);
                    }
                }
            }
        }
    }
    return qvm_undefined;
}

void we_render_template(qvm_state_t *e, qvm_value_t parent_object, qvm_value_t object, qvm_value_t scope, int refresh) {
    qvm_value_t prop;
    qvm_value_t attributes = qvm_get_property(e, object, "attributes");
    if( qvm_is_undefined(attributes) )
        return;
    prop = qvm_get_property(e, attributes, "v-if");
    if( qvm_is_string(prop) ){
        we_binding_watcher_create(e, object, attributes, qvm_mk_string(""), _render_template_if, NULL);
        qvm_build_js_rule(e);
        qvm_value_t res = qvm_parse_string(e, qvm_to_string(e, prop));
        qvm_call(e, res, scope, 0, NULL);
        we_binding_watcher_clear_current(e);
        qvm_add_property(e, object, "$parent", parent_object);
        if ( !qvm_is_undefined(scope) ){
            qvm_add_property(e, object, "scope", scope);
        }
        _render_template_if(e, object, 0, NULL);
    } else {
        prop = qvm_get_property(e, attributes, "v-for");
        if( qvm_is_string(prop) ){
            qvm_value_t key = qvm_get_property(e, attributes, "key");
            qvm_value_t index = qvm_get_property(e, attributes, "index");
            qvm_value_t local_scope = qvm_new_object(e);
            we_binding_watcher_create(e, object, attributes, qvm_mk_string(""), _render_template_for, NULL);
            qvm_add_property(e, local_scope, qvm_to_string(e, key), qvm_undefined);
            qvm_add_property(e, local_scope, qvm_to_string(e, index), qvm_mk_number(0));
            qvm_build_js_rule(e);
            qvm_value_t res = qvm_parse_string(e, qvm_to_string(e, prop));
            res = qvm_call(e, res, scope, 0, NULL);
            qvm_add_property(e, local_scope, qvm_to_string(e, prop), res);
            we_binding_watcher_clear_current(e);
            qvm_add_property(e, object, "$parent", parent_object);
            qvm_add_property(e, object, "scope", local_scope);
            _render_template_for(e, object, 0, NULL);
        }
    }
}
