/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_RENDER_H
#define WE_RENDER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "html/we_html.h"

int we_render_run(qvm_state_t *e);
we_html_object_t *we_render_html_object(qvm_state_t *e, qvm_value_t parent_object, qvm_value_t object, qvm_value_t scope, int refresh);
void we_render_attributes_set_native(qvm_value_t attributes);
void we_render_template(qvm_state_t *e, qvm_value_t parent_object, qvm_value_t object, qvm_value_t scope, int refresh);
void we_render_remove_page(qvm_state_t *e, qvm_value_t p);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
