/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_TASK_H
#define WE_TASK_H

#ifdef __cplusplus
extern "C" {
#endif

#include "page/we_page.h"

#define WE_TASK_MAX     32

struct we_task_t;

typedef void (*we_task_cb_t)(struct we_task_t *);

typedef struct we_task_t {
    int deleted;
    int period;
    int count;
    we_task_cb_t task_cb;
    void * user_data;
    struct we_task_t *next;
} we_task_t;

we_task_t *we_task_create(we_task_cb_t task_cb, int period,  void *user_data);
void we_task_remove(we_task_t *task);
void we_task_poll(void);
void we_task_remove_all(void);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
