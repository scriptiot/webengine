#include "task/we_task.h"

static we_task_t *first_task = NULL;
static we_task_t *curr_task = NULL;

we_task_t *we_task_create(we_task_cb_t task_cb, int period,  void *user_data)
{
    we_task_t *new_task = NULL;
    new_task = we_sys_malloc(sizeof (we_task_t));
    QVM_ASSERT(new_task);
    memset(new_task, 0, sizeof (we_task_t));
    new_task->count = 0;
    new_task->period = period;
    new_task->task_cb = task_cb;
    new_task->user_data = user_data;
    if( curr_task == NULL ) {
        curr_task = first_task = new_task;
        return curr_task;
    } else {
        curr_task->next = new_task;
        curr_task = new_task;
    }
    return new_task;
}

void we_task_remove(we_task_t *task) {
    task->deleted = 1;
}

void we_task_remove_all(void) {
    we_task_t *local_task = first_task;
    while (local_task) {
        local_task->deleted = 1;
        local_task = local_task->next;
    }
}

void we_task_poll(void) {
    we_task_t *local_task = first_task;
    we_task_t *last_task = NULL;
    while (local_task) {
        if( local_task->deleted ) {
            we_task_t *next_task = local_task->next;
            if( last_task == NULL )
                first_task = curr_task = NULL;
            else {
                last_task->next = next_task;
            }
            we_sys_free(local_task);
            local_task = local_task->next;
            continue;
        }
        if( local_task->count >= local_task->period ) {
            local_task->task_cb(local_task);
            local_task->count = 0;
        } else {
            local_task->count++;
        }
        local_task = local_task->next;
    }
}

