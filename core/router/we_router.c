#include "router/we_router.h"
#include "task/we_task.h"

const char APP_DIR[] = "appdir";
const char PATH_DEFAULT[] = "we_page_launcher_index";
const char ROUTER_PAGE[] = "$page";
const char ROUTER_PAGE_URI[] = "path";
const char ROUTER_PAGE_FROM[] = "from";
const char ROUTER_PAGE_TO[] = "to";
const char ROUTER_PAGE_PARAMS[] = "params";

static we_router_t _we_router;

void we_router_init(void){
    _we_router.dir = NULL;
    _we_router.index = 0;
    _we_router.current_page = NULL;
    _we_router.previous_page = NULL;
}

we_router_t *we_router_get(void) {
    return &_we_router;
}

enum WEPageErr we_router_add(we_router_t *router, qvm_value_t object, int index){
    qvm_state_t *e = we_get_runtime();
    qvm_value_t uriValue = qvm_get_property(e, object, ROUTER_PAGE_URI);
    if( !qvm_is_string(uriValue) ) {
        return we_page_err;
    }
    char *uri = (char*)qvm_to_string(e, uriValue);
    uint32_t uri_len = strlen(uri);
    we_page_t *newSm = router->pages + index;
    newSm->index = index;
    newSm->state = UNDEFINED_STATE;
    newSm->uri = (char *)we_malloc(uri_len + 1);
    memcpy(newSm->uri, uri, uri_len);
    newSm->uri[uri_len] = 0;

    if( router->current_page ) {
        we_page_sm_change_state(router->current_page, BACKGROUND_STATE);
    }

    router->current_page = newSm;
    router->index = index;
    if( we_page_sm_change_state(newSm, INIT_STATE) != we_page_ok ) {
        we_free(newSm->uri);
        newSm->uri = NULL;
        return we_page_err;
    }
    
    qvm_add_property(e, we_page_get_current_index_object(e), ROUTER_PAGE, object);

    if( we_page_sm_change_state(newSm, SHOW_STATE) != we_page_ok ) {
        return we_page_err;
    }

    return we_page_ok;
}

enum WEPageErr we_router_push(we_router_t * router, qvm_value_t object) {
    enum WEPageErr err = we_page_err;
    qvm_state_t *e = we_get_runtime();
    qvm_value_t uriValue = qvm_get_property(e, object, ROUTER_PAGE_URI);
    if( !qvm_is_string(uriValue) ) {
        return err;
    }
    char * fromuri = router->current_page->uri;
    const char * uri = qvm_to_string(e, uriValue);

    if (!strcmp(router->current_page->uri, uri) ){
        if (router->current_page->state == SHOW_STATE){
            return we_page_ok;
        }
    }


    for(int i = 0; i < WE_PAGE_MAX; i++){
        if( router->pages[i].uri == NULL ){
            qvm_value_t from = qvm_new_string(e, fromuri);
            qvm_add_property(e, object, ROUTER_PAGE_URI, uriValue);
            qvm_add_property(e, object, ROUTER_PAGE_FROM, from);
            qvm_add_property(e, object, ROUTER_PAGE_TO, uriValue);
            err = we_router_add(router, object, i);
            if (err != we_page_ok){
                return we_page_err;
            }
            return err;
        }

        if( !strcmp(router->pages[i].uri, uri) ){
            if( router->current_page ) {
                we_page_sm_change_state(router->current_page, BACKGROUND_STATE);
            }
            we_page_t *sm = router->pages + i;

            router->current_page = sm;
            router->index = i;

            qvm_value_t from = qvm_new_string(e, fromuri);
            qvm_add_property(e, object, ROUTER_PAGE_URI, uriValue);
            qvm_add_property(e, object, ROUTER_PAGE_FROM, from);
            qvm_add_property(e, object, ROUTER_PAGE_TO, uriValue);
            qvm_value_t page_index_object = we_page_get_current_index_object(e);
            if ( !qvm_is_undefined(page_index_object) ){
                qvm_add_property(e, page_index_object, ROUTER_PAGE, object);
            }

            err = we_page_sm_change_state(sm, SHOW_STATE);
            if (err != we_page_ok){
                return we_page_err;
            }
            return err;
        }
    }
    return err;
}

enum WEPageErr we_router_replace(we_router_t * router, qvm_value_t object){
    enum WEPageErr err = we_page_err;
    qvm_state_t *e = we_get_runtime();
    qvm_value_t uriValue = qvm_get_property(e, object, ROUTER_PAGE_URI);
    if( !qvm_is_string(uriValue) ) {
        return err;
    }
    char * fromuri = router->current_page->uri;
    //destroy current page
    if( router->current_page ) {
        we_page_sm_change_state(router->current_page, DESTROY_STATE);
    }
    //add new page at current index
    qvm_value_t from = qvm_new_string(e, fromuri);
    qvm_add_property(e, object, ROUTER_PAGE_URI, uriValue);
    qvm_add_property(e, object, ROUTER_PAGE_FROM, from);
    qvm_add_property(e, object, ROUTER_PAGE_TO, uriValue);
    return we_router_add(router, object, router->index);
}

static we_task_t *task_handle = NULL;
static char launch_path[128];

static void launch_task(we_task_t *task) {
    char *args[3];
    args[0] = "";
    args[1] = "--path";
    args[2] = launch_path;
    we_restart(3, args);
    we_task_remove(task);
}

static qvm_value_t _router_system_launch(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);QVM_UNUSED(p);QVM_UNUSED(argc);QVM_UNUSED(v);
    if( argc > 0 && v != NULL && qvm_is_object(v[0]) ){
        const char *path = qvm_to_string(e, qvm_get_property(e, v[0], "path"));
        sprintf(launch_path, "%s", path);
        task_handle = we_task_create(launch_task, 10, launch_path);
    }
    return qvm_undefined;
}

static qvm_value_t _router_system_push(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);QVM_UNUSED(p);QVM_UNUSED(argc);QVM_UNUSED(v);
    if( argc > 0 && v != NULL && qvm_is_object(v[0]) ){
        we_router_push(&_we_router, v[0]);
    }
    return qvm_undefined;
}

static qvm_value_t _router_system_replace(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);QVM_UNUSED(p);QVM_UNUSED(argc);QVM_UNUSED(v);
    if( argc > 0 && qvm_is_object(v[0]) ){
        we_router_replace(&_we_router, v[0]);
    }
    return qvm_undefined;
}

int we_router_module_init(qvm_state_t * e) {
    qvm_value_t builtin = qvm_new_object(e);
    qvm_add_property(e, builtin, "push", qvm_mk_native(_router_system_push));
    qvm_add_property(e, builtin, "replace", qvm_mk_native(_router_system_replace));
    qvm_add_property(e, builtin, "launch", qvm_mk_native(_router_system_launch));
    qvm_add_module(e, "@system.router", builtin);
    return 1;
}


