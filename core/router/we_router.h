/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_ROUTER_H
#define WE_ROUTER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "page/we_page.h"

#define WE_PAGE_MAX     32

typedef struct we_router_t {
    char *dir;
    uint32_t index;
    we_page_t *previous_page;
    we_page_t *current_page;
    we_page_t pages[WE_PAGE_MAX];
} we_router_t;

void we_router_init(void);
we_router_t *we_router_get(void);
enum WEPageErr we_router_add(we_router_t *router, qvm_value_t object, int index);
enum WEPageErr we_router_push(we_router_t * router, qvm_value_t object);
enum WEPageErr we_router_replace(we_router_t * router, qvm_value_t object);

int we_router_module_init(qvm_state_t *e);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
