#include "webengine.h"
#include "router/we_router.h"
#include "fileapi/we_file.h"
#include "buffer/we_buffer.h"
#include "javascript/we_js_builtin.h"
#include "qvm_ecma.h"
#include "we_gui.h"
#include "css/we_css.h"
#include "compress/wrap_heatshrink.h"
#include "base64/we_base64.h"
#include "bget.h"
#include "image/we_image.h"
#include "graphics/we_graphics.h"
#include "network/we_socket.h"
#include "network/we_websocket.h"
#include "process/we_process.h"
#include "network/we_http_parser.h"
#include "task/we_task.h"
#include "qvm_conf.h"

static qvm_state_t *_we_runtime;
static const char *_root_dir;
static uint8_t *_we_mem = NULL;
static char _we_path[PATH_LENGTH_MAX];

int qvm_check_memory(qvm_state_t *e) {
    QVM_UNUSED(e);
    long curalloc, totfree, maxfree;
    unsigned long nget, nrel;
    bstats(&curalloc, &totfree, &maxfree, &nget, &nrel);

    float res = curalloc;
    return 100 * res / (curalloc + totfree);
}

void we_get_realpath(char *path, const char *name) {
    if( _root_dir == NULL || strlen(_root_dir) == 0 )
        sprintf(path, "%s", name);
    else {
        sprintf(path, "%s%s", we_get_root_dir(), name);
    }
}

qvm_state_t *we_get_runtime(void) {
    return _we_runtime;
}

const char *we_get_root_dir(void) {
    return _root_dir;
}

void *vm_malloc(qvm_state_t *e, size_t size) {
    QVM_UNUSED(e);
    return we_malloc(size);
}

void *vm_realloc(qvm_state_t *e, void *p, size_t size) {
    QVM_UNUSED(e);
    return we_realloc(p, size);
}

void vm_free(qvm_state_t *e, void *ptr) {
    QVM_UNUSED(e);
    we_free(ptr);
}

void *we_malloc(int size)
{
    void * m = bgetz(size);
    return m;
}

void *we_realloc(void *p, size_t size)
{
    return bgetr(p, size);
}

void we_free(void * mem)
{
    if( mem )
        brel(mem);
}

char *we_vm_open_file(qvm_state_t * e, char *filename) {
    WE_FHANDLE fd;
    size_t lSize;
    size_t br;
    char *buffer = NULL;

    lSize = (size_t)we_file_size(filename);

    fd = we_file_open(filename, "rb");
    if (fd == NULL)
        return NULL;

    buffer = (char*)qvm_malloc(e, lSize + 1);
    memset(buffer, 0, lSize + 1);
    br = we_file_read(fd, buffer, lSize);

    we_debug("we_vm_open_file buffer \n%s\n", buffer);

    if (br != lSize ){
        we_file_close(fd);
        return NULL;
    }

    buffer[lSize] = 0;
    we_file_close(fd);
    return buffer;
}

char * qvm_open_file(qvm_state_t * e, char * path)
{
    char* buffer = NULL;
    e->filename = path;
    buffer = we_vm_open_file(e, path);
    return buffer;
}

void _vm_destroy(void) {
    we_sys_free(_we_mem);
    _we_mem = NULL;
}

uint8_t we_system_memory_usage(void) {
    return (uint8_t)qvm_check_memory(_we_runtime);
}

void we_register_print(void *p) {
    qvm_register_print(p);
}

static void we_init_ecma(qvm_state_t *e) {
    qvm_ecma_init_builtins(e);
    qvm_ecma_init_object(e);
    qvm_ecma_init_console(e);
#ifdef WE_USE_ECMA_FUNCTION
    qvm_ecma_init_function(e);
#endif

#ifdef WE_USE_ECMA_ARRAY
    qvm_ecma_init_array(e);
#endif

#ifdef WE_USE_ECMA_STRING
    qvm_ecma_init_string(e);
#endif

#ifdef WE_USE_ECMA_MATH
    qvm_ecma_init_math(e);
#endif

#ifdef WE_USE_ECMA_NUMBER
    qvm_ecma_init_number(e);
#endif

#ifdef WE_USE_ECMA_BOOLEAN
    qvm_ecma_init_boolean(e);
#endif

#ifdef WE_USE_ECMA_DATE
    qvm_ecma_init_date(e);
#endif

#ifdef WE_USE_ECMA_JSON
    qvm_ecma_init_json(e);
#endif
}

qvm_value_t native_show(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    long curalloc, totfree, maxfree;
    unsigned long nget, nrel;
    bstats(&curalloc, &totfree, &maxfree, &nget, &nrel);
    printf("stack usage: %ld B\r\n", ((intptr_t)e->sp - (intptr_t)e->sp_base));
    printf("mem info: used = %ld B, max used = %ld B, total = %ld B\r\n", curalloc, bstatsmaxget() , (curalloc + totfree) );
    return qvm_undefined;
}

static int _init(int argc, char **argv, int is_restart) {
    int i = 0;
    char *router_path = NULL;
    char *arg_name;
    char *arg_value;
    int local_heap_size = WE_HEAP_SIZE;
    int local_stack_size = WE_STACK_SIZE;

    if( !is_restart )
        _root_dir = WE_CONF_ROOT_DIR;

    if( argc > 1 ) {
        for(i = 1; i < argc; i += 2) {
            arg_name = argv[i];
            arg_value = argv[i + 1];
            if( !strcmp(arg_name, "--heap") ) {
                local_heap_size = atoi(arg_value) * 1024;
            } else if( !strcmp(arg_name, "--stack") ) {
                local_stack_size = atoi(arg_value) * 1024;
            } else if( !strcmp(arg_name, "--dir") ) {
                _root_dir = arg_value;
            } else if( !strcmp(arg_name, "--path") ) {
                router_path = arg_value;
            }
        }
    }

    int mem_size = local_heap_size + local_stack_size;

    we_debug("allocate main memory\r\n");
    _we_mem = (uint8_t *)we_sys_malloc(mem_size);
    if( _we_mem == NULL ) {
        we_debug("Failed to create memory\r\n");
        return 0;
    }

    bpool(_we_mem, mem_size);

    _we_runtime = (qvm_state_t *)we_malloc(sizeof(qvm_state_t));
    if( _we_runtime == NULL ) {
        we_debug("Failed to create vm, not enough memory\r\n");
        _vm_destroy();
        return 0;
    }

    qvm_register_malloc(_we_runtime, vm_malloc);
    qvm_register_realloc(_we_runtime, vm_realloc);
    qvm_register_free(_we_runtime, vm_free);
    qvm_register_file_load(qvm_open_file);

    we_debug("init qvm\r\n");
    qvm_init(_we_runtime, local_stack_size, WE_REF_SIZE);

    qvm_add_global(_we_runtime, "show", qvm_mk_native(native_show));
    we_debug("init js builtin\r\n");
    we_js_builtin_init(_we_runtime);
    we_debug("init css module\r\n");
    we_css_module_init(_we_runtime);
    we_debug("init ecma\r\n");
    we_init_ecma(_we_runtime);
    we_debug("init gui\r\n");
    gui_init(_we_runtime);

#ifdef WE_USE_FS
    we_debug("init fs\r\n");
    we_fs_module(_we_runtime);
#endif
#ifdef WE_USE_BASE64
    we_debug("init base64\r\n");
    we_module_base64(_we_runtime);
#endif
#ifdef WE_USE_HEATSHRINK
    we_debug("init heatshrink\r\n");
    we_module_heatshrink(_we_runtime);
#endif
#ifdef WE_USE_OPENGL
    we_debug("init image graphics\r\n");
    we_module_graphics(_we_runtime);
#endif
    we_debug("init image module\r\n");
    we_image_module(_we_runtime);
#ifdef WE_USE_SOCKET
    we_debug("init socket module\r\n");
    we_socket_module(_we_runtime);
#endif
#ifdef WE_USE_WEBSOCKET
    we_debug("init websocket module\r\n");
    we_websocket_module(_we_runtime);
#endif
#ifdef WE_USE_HTTP
    we_debug("init http parser module\r\n");
    we_http_parser_module(_we_runtime);
#endif
#ifdef WE_USE_PROCESS
    we_log("init process module\r\n");
    we_process_module(_we_runtime);
#endif
#ifdef WE_USE_BUFFER
    we_log("init buffer module\r\n");
    we_buffer_module_init(_we_runtime);
#endif
    we_debug("init router module\r\n");
    we_router_module_init(_we_runtime);
    we_debug("init page\r\n");
    we_page_list_init(_we_runtime);
    sprintf(_we_path, "%sapp.js", _root_dir);
    printf("_we_path: %s\n",_we_path);
    qvm_build_js_rule(_we_runtime);
    qvm_value_t app_obj = qvm_parse(_we_runtime, "app.js");
    if( qvm_is_undefined(app_obj) ){
        we_debug("Failed to parse app.js\r\n");
        _vm_destroy();
        return 0;
    }
    we_router_init();
    we_page_insert_index_object(_we_runtime, 0, app_obj);

//    if( qvm_try(_we_runtime) ) {
//        qvm_value_t err = qvm_pop(_we_runtime);
//        qvm_report(_we_runtime, err);
//        we_debug("Failed to run app.js\r\n");
//        _vm_destroy();
//        return 0;
//    }

    qvm_call(_we_runtime, app_obj, qvm_get_global(_we_runtime), 0, NULL);
    qvm_value_t onCreateFunction = qvm_get_property(_we_runtime, app_obj, "onCreate");
    if ( qvm_is_script(onCreateFunction) ) {
        qvm_call(_we_runtime, onCreateFunction, app_obj, 0, NULL);
    }
    if( router_path ) {
        qvm_value_t router = qvm_new_object(_we_runtime);
        qvm_add_property(_we_runtime, router, "path", qvm_new_string(_we_runtime, router_path));
        we_router_add(we_router_get(), router, 0);
        return 1;
    }
    qvm_value_t config = qvm_get_property(_we_runtime, app_obj, "config");
    if( !qvm_is_undefined(config) ) {
        qvm_value_t local_path = qvm_get_property(_we_runtime, config, "router");
        if( qvm_is_object(local_path) ) {
            we_router_add(we_router_get(), local_path, 0);
        } else {
            we_debug("No entry path is defined\r\n");
        }
    }
    return 1;
}

int we_restart(int argc, char *argv[]) {
    we_gui_destroy_all();
    if( _we_mem ) {
        we_sys_free(_we_mem);
    }
    if( !_init(argc, argv, 1) )
        return 0;
    return 1;
}
/**
 * --heap    [堆大小, KB]
 * --stack   [栈大小, KB]
 * --mem     [系统内存大小, KB]
 * --dir     [运行路径]
 */
int we_start(int argc, char *argv[]) {
    if( !_init(argc, argv, 0) )
        return 0;
    return 1;
}

void we_poll(qvm_state_t *e)
{
    if ( e ){
        we_js_builtin_timer_poll(e);
        we_process_poll(e);
        we_task_poll();
    }
}

void *we_get_user_data(qvm_state_t *e, qvm_value_t object){
    qvm_value_t invoke_data = qvm_get_property(e, object, ".data");
    if( qvm_is_invoke(invoke_data) )
        return qvm_to_invoke(e, invoke_data);
    return NULL;
}

void we_set_user_data(qvm_state_t *e, qvm_value_t object, void *data){
    qvm_add_property(e, object, ".data", qvm_mk_invoke(data));
}
