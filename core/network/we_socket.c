#include "network/we_socket.h"
#include "process/we_process.h"
#include <mbedtls/ssl.h>

#ifdef WE_USE_MBEDTLS
#include "mbedtls/ssl.h"
#include "mbedtls/ssl_internal.h"
#include "mbedtls/compat-1.3.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#endif

#define SOCKET_DEFAULT_TIMEO        6

#define EVENT_CONNECTED     1
#define EVENT_DISCONNECTED  2
#define EVENT_RECV          3
#define EVENT_CLOSE         4

#ifdef __linux__

#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <errno.h>
#include <pthread.h>

#define closesocket(x) close(x)
#else
#include <Winsock2.h>
#include <WS2tcpip.h>
#include <unistd.h>
#include <pthread.h>
//#pragma comment(lib, "ws2_32.lib")
#endif

#define SOCKET_RX_BUF_SIZE  (100 * 1024)
#define SOCKET_TX_BUF_SIZE  (10 * 1024)


struct ssl_client {
    mbedtls_ssl_context ssl;
    mbedtls_ssl_config config;
    mbedtls_ctr_drbg_context ctr_drbg;
    mbedtls_entropy_context entropy;
    int fd;
};

typedef struct _socket_client_t {
    int is_https;
    int id;
    int obj;
    int cb_close;
    int cb_recv;
    int cb_connected;
    int cb_disconnected;
    pthread_t tid;
    int rx_buf_size;
    int rx_len;
    uint8_t *rx_buf;

    int tx_buf_size;
    int tx_len;
    uint8_t *tx_buf;

    struct ssl_client * ssl;
} _socket_client_t;

static int ssl_client_recv(void * ctx, unsigned char * buf, unsigned int len)
{
    struct ssl_client * client = (struct ssl_client *)ctx;
    int bytes = recv(client->fd, buf, len, 0);
    return bytes;
}

static int ssl_client_send(void * ctx, const unsigned char *buf, unsigned int len)
{
    struct ssl_client * client = (struct ssl_client *)ctx;
    return send(client->fd, buf, len, 0);
}

struct ssl_client * ssl_client_init(int fd)
{
    char *pers = "ssl_client";
    int ret;
    struct ssl_client * client = (struct ssl_client *)malloc(sizeof(struct ssl_client));
    if (!client) {
        we_log("Cannot malloc memory(ssl_client_init)\n");
        return NULL;
    }

    memset(client, 0, sizeof(struct ssl_client));

#ifdef CA_CERT
    mbedtls_x509_crt *ca_chain;
    ca_chain = (mbedtls_x509_crt *)malloc(sizeof(mbedtls_x509_crt));
    memset(ca_chain, 0, sizeof(mbedtls_x509_crt));
#endif

    mbedtls_entropy_init(&client->entropy);
    mbedtls_ctr_drbg_init(&client->ctr_drbg);
    if((ret = mbedtls_ctr_drbg_seed(&client->ctr_drbg, mbedtls_entropy_func,
                                    &client->entropy,
                                    (unsigned char *)pers, strlen(pers))) != 0)
    {
        we_log( " failed\n  ! ctr_drbg_init returned %d\n", ret );
        goto failed;
    }

    mbedtls_ssl_init(&(client->ssl));
    mbedtls_ssl_config_init(&(client->config));
    client->fd = fd;
    mbedtls_ssl_conf_rng(&(client->config), mbedtls_ctr_drbg_random, &client->ctr_drbg);

#ifdef CA_CERT
    mbedtls_x509_crt_parse(ca_chain,ca1_cert,strlen(ca1_cert));
    mbedtls_ssl_conf_ca_chain(&(client->config), ca_chain, NULL);
    mbedtls_ssl_set_hostname(&(client->ssl), HOSTNAME);
#endif

#ifdef OWN_CERT
    mbedtls_x509_crt *client_chain = NULL;
    mbedtls_pk_context *client_rsa = NULL;

    client_chain = (mbedtls_x509_crt *)malloc(sizeof(mbedtls_x509_crt));
    memset(client_chain, 0, sizeof(mbedtls_x509_crt));
    mbedtls_x509_crt_parse(client_chain,client_cert,strlen(client_cert));

    client_rsa = (mbedtls_pk_context *)malloc(sizeof(mbedtls_pk_context));
    memset(client_rsa, 0, sizeof(mbedtls_pk_context));
    mbedtls_pk_parse_key(client_rsa, client_key, strlen(client_key), NULL,0);
    mbedtls_ssl_conf_own_cert(&client->config, client_chain, client_rsa);
#endif

    //mbedtls_ssl_conf_endpoint(&(client->config), MBEDTLS_SSL_IS_CLIENT);
    // if using MBEDTLS_SSL_PRESET_SUITEB,need open SUPPORT_TLS_SUITEB in config-no-entropy.h
    mbedtls_ssl_config_defaults(&client->config, MBEDTLS_SSL_IS_CLIENT,
                                MBEDTLS_SSL_TRANSPORT_STREAM, MBEDTLS_SSL_PRESET_DEFAULT);

#ifdef CA_CERT
    mbedtls_ssl_conf_authmode(&client->config, MBEDTLS_SSL_VERIFY_REQUIRED);
#else
    mbedtls_ssl_conf_authmode(&client->config, MBEDTLS_SSL_VERIFY_OPTIONAL);
#endif

    mbedtls_ssl_conf_rng(&client->config, mbedtls_ctr_drbg_random, &client->ctr_drbg);
    //mbedtls_debug_set_threshold(1000);
    mbedtls_ssl_set_bio(&client->ssl, client,
                        ssl_client_send, ssl_client_recv, NULL);

    mbedtls_ssl_setup(&client->ssl, &client->config);
    return client;
failed:
#ifdef CA_CERT
    if(ca_chain) free(ca_chain);
#endif
    if (client) free(client);
    return NULL;
}

#include "http-parser/http_parser.h"

static void socket_handler(qvm_state_t *e, int type, void *ptr) {
    _socket_client_t *client = ptr;
    qvm_value_t obj = qvm_get_ref(e, client->obj);
    switch (type) {
    case EVENT_CLOSE:{
        qvm_value_t cb = qvm_get_ref(e, client->cb_close);
        qvm_call(e, cb, we_page_get_current_index_object(e), 1, &obj);
        break;
    }
    case EVENT_CONNECTED:{
        qvm_value_t cb = qvm_get_ref(e, client->cb_connected);
        qvm_call(e, cb, we_page_get_current_index_object(e), 1, &obj);
        break;
    }
    case EVENT_DISCONNECTED:{
        qvm_value_t cb = qvm_get_ref(e, client->cb_disconnected);
        qvm_call(e, cb, we_page_get_current_index_object(e), 1, &obj);
        break;
    }
    case EVENT_RECV:{
        qvm_value_t cb = qvm_get_ref(e, client->cb_recv);
        qvm_value_t args[2];
        args[0] = obj;
        args[1] = qvm_new_buffer(e, client->rx_buf, client->rx_len);
        we_free(client->rx_buf);
        we_free(client);
        qvm_call(e, cb, we_page_get_current_index_object(e), 2, args);
        break;
    }
    }
}

static void *socket_tls_thread_entry(void *param) {
    _socket_client_t *client = param;
    ssize_t bytes = mbedtls_ssl_write(&client->ssl->ssl, client->tx_buf, client->tx_len);
    if( bytes <= 0 ) {
        return NULL;
    }
    int32_t length = mbedtls_ssl_read(&client->ssl->ssl, client->rx_buf, client->rx_buf_size);
    if (length > 0) {
        we_log("netc[%d] data recv len=%d\n", client->id, length);
        if (client->rx_buf) {
            _socket_client_t *local_client = we_malloc(sizeof (_socket_client_t));
            if( !local_client ) {
                return NULL;
            }
            memcpy(local_client, client, sizeof (_socket_client_t));
            local_client->rx_len = length;
            local_client->rx_buf = we_malloc(length);
            if( !client->rx_buf ) {
                we_free(local_client);
                return NULL;
            }
            memcpy(local_client->rx_buf, client->rx_buf, length);
            we_process_add_handler(EVENT_RECV, socket_handler, local_client);
        }
    }
    we_process_add_handler(EVENT_CLOSE, socket_handler, client);
    return NULL;
}
//close();
static qvm_value_t we_socket_close(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v){
    QVM_UNUSED(e);QVM_UNUSED(argc);QVM_UNUSED(v);
    _socket_client_t *client = (_socket_client_t *)we_get_user_data(e,p);
    if( !client )
        return qvm_undefined;
    closesocket(client->id);
    client->id = -1;
    return  qvm_undefined;
}
//write(buffer|string, len)
static qvm_value_t we_socket_write(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v){
    ssize_t bytes = 0;
    _socket_client_t *client = (_socket_client_t *)we_get_user_data(e,p);
    if( !client )
        return qvm_mk_number(0);

    if( argc < 1 )
        return qvm_mk_number(0);

    char *buff = NULL;
    size_t len = 0;
    if( argc > 1 && qvm_is_number(v[1]))
        len = (size_t)qvm_to_int32(e, v[1]);

    if( qvm_is_buffer(v[0]) ){
        buff = qvm_to_buffer(e, v[0]);
        int local_len = qvm_get_bufferlen(e, v[0]);
        if( len == 0 )
            len = local_len;
        else
            len = len < local_len ? len:local_len;
    } else if( qvm_is_string(v[0]) ){
        buff = qvm_to_string(e, v[0]);
        int local_len = qvm_string_length(e, v[0]);
        if( len == 0 )
            len = local_len;
        else
            len = len < local_len ? len:local_len;
    }
#ifdef WE_USE_MBEDTLS
    if (client->is_https)
    {
        if( len > client->tx_buf_size )
            len = client->tx_buf_size;
        client->tx_len = len;
        memcpy(client->tx_buf, buff, len);
        if (pthread_create(&client->tid, NULL, socket_tls_thread_entry, client) == -1) {
            qvm_throw(e, qvm_error(e, "socket failed to create thread"));
            closesocket(client->id);
            we_free(client);
            return qvm_undefined;
        }
        return qvm_mk_number(bytes);
    }
#endif
    bytes = write(client->id, buff, len);
    return qvm_mk_number(bytes);
}
//read(size)
static qvm_value_t we_socket_read(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    _socket_client_t *client = (_socket_client_t *)we_get_user_data(e, p);
    if( !client )
        return qvm_mk_number(0);

    if( argc == 0 || !qvm_is_number(v[0]) )
        return qvm_mk_number(0);

    int size = qvm_to_int32(e, v[0]);
    uint8_t *addr = qvm_malloc(e, size);
    if( !addr ) {
        qvm_throw(e, qvm_error(e, "failed to allocate memory"));
    }
#ifdef WE_USE_MBEDTLS
    if (client->is_https)
    {
        ssize_t res = mbedtls_ssl_read(&client->ssl->ssl, addr, size);
        qvm_free(e, addr);
        return qvm_mk_number(res);
    }
#endif
    ssize_t res = recv(client->id, addr, size, 0);
    qvm_free(e, addr);
    return qvm_mk_number(res);
}
//on(event, callback)
static qvm_value_t we_socket_on(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    _socket_client_t *client = (_socket_client_t *)we_get_user_data(e,p);
    if( !client ) {
        qvm_throw(e, qvm_error(e, "Failed to setup socket event callback"));
        return qvm_undefined;
    }

    if( argc < 2 || !qvm_is_string(v[0]) || !qvm_is_script(v[1])) {
        qvm_throw(e, qvm_error(e, "Failed to setup socket event callback"));
        return qvm_undefined;
    }
    const char *event = qvm_to_string(e, v[0]);
    if( !strcmp(event, "recv") ) {
        client->cb_recv = qvm_set_ref(e, v[1]);
    } else if( !strcmp(event, "connected") ) {
        client->cb_connected = qvm_set_ref(e, v[1]);
    } else if( !strcmp(event, "disconnected") ) {
        client->cb_disconnected = qvm_set_ref(e, v[1]);
    } else if( !strcmp(event, "close") ) {
        client->cb_close = qvm_set_ref(e, v[1]);
    }
    return qvm_undefined;
}

static void client_destory(_socket_client_t *thiz)
{
    if (thiz == NULL)
    {
        we_log("netclient del : param is NULL, delete failed");
        return;
    }

    we_log("netc[%ld] destory begin", thiz->id);

    if (thiz->id != -1) {
        closesocket(thiz->id);
    }
}

static void client_clean(_socket_client_t *client) {
    if( client->rx_buf )
        we_free(client->rx_buf);
    if( client->tx_buf )
        we_free(client->tx_buf);
    we_free(client);
}

static void *socket_thread_entry(void *param) {
    _socket_client_t *client = (_socket_client_t *)param;
    while(1) {
        ssize_t length = recv(client->id, client->rx_buf, client->rx_buf_size, 0);
        if( length > 0 ){
            _socket_client_t *local_client = we_malloc(sizeof (_socket_client_t));
            if( !local_client ) {
                return NULL;
            }
            memcpy(local_client, client, sizeof (_socket_client_t));
            local_client->rx_len = length;
            local_client->rx_buf = we_malloc(length);
            if( !client->rx_buf ) {
                we_free(local_client);
                return NULL;
            }
            memcpy(local_client->rx_buf, client->rx_buf, length);
            we_process_add_handler(EVENT_RECV, socket_handler, local_client);
        } else if( length < 0 ) {
            break;
        }
    }
}

static qvm_value_t we_socket_destroy(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    _socket_client_t *client = (_socket_client_t *)we_get_user_data(e, p);
    if( !client ) {
        qvm_throw(e, qvm_error(e, "Failed to destroy socket"));
        return qvm_undefined;
    }
    client_clean(client);
    we_set_user_data(e, p, NULL);
    return qvm_undefined;
}

//connect(address, port, [rx_buf_size, tx_buf_size])
static qvm_value_t we_socket_connect(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    _socket_client_t *client = we_malloc(sizeof(_socket_client_t));
    if( !client ){
        qvm_throw(e, qvm_error(e, "Engine runs out of memory"));
        return qvm_undefined;
    }
    memset(client, 0, sizeof (_socket_client_t));
    client->ssl = NULL;
    client->obj = -1;
    client->id = -1;
    client->cb_recv = -1;
    client->cb_connected = -1;
    client->cb_disconnected = -1;
    client->cb_close = -1;

    const char *host = qvm_to_string(e, v[0]);

    if( argc > 2 && qvm_is_integer(v[2]) ){
        client->rx_buf_size = qvm_to_int32(e, v[2]);
    } else {
        client->rx_buf_size = SOCKET_RX_BUF_SIZE;
    }

    if( argc > 3 && qvm_is_integer(v[3]) ){
        client->tx_buf_size = qvm_to_int32(e, v[3]);
    } else {
        client->tx_buf_size = SOCKET_TX_BUF_SIZE;
    }

    client->rx_buf = we_malloc(client->rx_buf_size);
    if( !client->rx_buf ) {
        qvm_throw(e, qvm_error(e, "Engine runs out of memory"));
        client_clean(client);
        return qvm_undefined;
    }
    memset(client->rx_buf, 0, client->rx_buf_size);

    client->tx_buf = we_malloc(client->tx_buf_size);
    if( !client->tx_buf ) {
        qvm_throw(e, qvm_error(e, "Engine runs out of memory"));
        client_clean(client);
        return qvm_undefined;
    }
    memset(client->tx_buf, 0, client->tx_buf_size);

    char port_str[6] = "80";
    port_str[2] = 0;
    struct addrinfo hint;
    memset(&hint, 0, sizeof(hint));
    struct addrinfo *resolved;
    int ret;
    const char *host_addr = 0;

    if( strncmp(host, "https://", 8) == 0 ) {
        host_addr = host + 8;
        strncpy(port_str, "443", 4);
        port_str[3] = 0;
        client->is_https = 1;
    } else if( strncmp(host, "http://", 7) == 0 ){
        host_addr = host + 7;
    } else {
        strncpy(port_str, qvm_to_string(e, v[1]), qvm_string_length(e, v[1]) + 1);
        host_addr = host;
    }
    we_log("host: %s, port: %s \n", host_addr, port_str);
    ret = getaddrinfo(host_addr, port_str, &hint, &resolved);
    if (ret != 0) {
        we_log("dns name relove fail, retry at 2000ms\n");
        we_free(client);
        return qvm_undefined;
    }

    int socket_handle = socket(resolved->ai_family, SOCK_STREAM, IPPROTO_TCP);
    if( socket_handle == -1 ){
        we_log("socket init : socket create failed\n");
        client_clean(client);
        free(resolved);
        return qvm_undefined;
    }

    struct timeval timeout;
    timeout.tv_sec = SOCKET_DEFAULT_TIMEO;
    timeout.tv_usec = 0;

    setsockopt(client->id, SOL_SOCKET, SO_RCVTIMEO, (void *) &timeout,
               sizeof(timeout));
    setsockopt(client->id, SOL_SOCKET, SO_SNDTIMEO, (void *) &timeout,
               sizeof(timeout));


    int32_t res = connect(socket_handle, resolved->ai_addr, resolved->ai_addrlen);
    if (res == -1)
    {
        we_log("connect failed\n");
        we_free(client);
        return qvm_undefined;
    }
    we_log("connect succeedn");

    client->id = socket_handle;

    if ( !client->ssl && client->is_https) {
        we_log("Perform ssl_client_init...\r\n");
        client->ssl = ssl_client_init(client->id);

        if (client->ssl == NULL) {
            we_log("Try build new SSL client failed\r\n");
            client_clean(client);
            free(resolved);
            return qvm_undefined;
        }
    }

    qvm_value_t obj = qvm_new_object(e);
    we_set_user_data(e, obj, client);

    qvm_add_property(e, obj, "close", qvm_mk_native(we_socket_close));
    qvm_add_property(e, obj, "write", qvm_mk_native(we_socket_write));
    qvm_add_property(e, obj, "read", qvm_mk_native(we_socket_read));
    qvm_add_property(e, obj, "on", qvm_mk_native(we_socket_on));
    qvm_add_property(e, obj, "destroy", qvm_mk_native(we_socket_destroy));
    client->obj = qvm_set_ref(e, obj);
    we_process_add_handler(EVENT_CONNECTED, socket_handler, client);

    if( client->is_https == 0 ) {
        if (pthread_create(&client->tid, NULL, socket_thread_entry, client) == -1) {
            qvm_throw(e, qvm_error(e, "socket failed to create thread"));
            closesocket(client->id);
            client_clean(client);
            return qvm_undefined;
        }
    }
    return obj;
}

int we_socket_module(qvm_state_t *e) {
    qvm_value_t obj = qvm_new_object(e);
    qvm_add_property(e, obj, "connect", qvm_mk_native(we_socket_connect));
    qvm_add_module(e, "@system.socket", obj);
    return 1;
}
