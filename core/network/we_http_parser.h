/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_HTTP_PARSER_H
#define WE_HTTP_PARSER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "webengine.h"

int we_http_parser_module(qvm_state_t *e);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
