#ifndef WE_WEBSOCKET_H
#define WE_WEBSOCKET_H

#ifdef __cplusplus
extern "C" {
#endif

#include "webengine.h"

int we_websocket_module(qvm_state_t *e);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
