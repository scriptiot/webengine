#include "network/we_http_parser.h"
#include "http-parser/http_parser.h"

static int url_cb(http_parser *p, const char *buf, size_t len)
{
    qvm_value_t res = qvm_mk_invoke((intptr_t)p->data);
    qvm_value_t body = qvm_new_buffer(we_get_runtime(), buf, len);
    qvm_add_property(we_get_runtime(), res, "url", body);
    return 0;
}

static int status_cb(http_parser *p, const char *buf, size_t len)
{
    qvm_value_t res = qvm_mk_invoke((intptr_t)p->data);
    qvm_value_t body = qvm_new_buffer(we_get_runtime(), buf, len);
    qvm_add_property(we_get_runtime(), res, "status", body);
    return 0;
}

static int header_field_cb(http_parser *p, const char *buf, size_t len)
{
    qvm_value_t res = qvm_mk_invoke((intptr_t)p->data);
    qvm_value_t body = qvm_new_buffer(we_get_runtime(), buf, len);
    qvm_add_property(we_get_runtime(), res, "fields", body);
    return 0;
}

static int header_value_cb(http_parser *p, const char *buf, size_t len)
{
    qvm_value_t res = qvm_mk_invoke((intptr_t)p->data);
    qvm_value_t body = qvm_new_buffer(we_get_runtime(), buf, len);
    qvm_add_property(we_get_runtime(), res, "values", body);
    return 0;
}

static int body_cb(http_parser *p, const char *buf, size_t len)
{
    qvm_value_t res = qvm_mk_invoke((intptr_t)p->data);
    qvm_value_t body = qvm_new_buffer(we_get_runtime(), buf, len);
    qvm_add_property(we_get_runtime(), res, "body", body);
    return 0;
}

static http_parser_settings settings_dontcall = {
    .on_url = url_cb,
    .on_status = status_cb,
    .on_header_field = header_field_cb,
    .on_header_value = header_value_cb,
    .on_body = body_cb,
    .on_message_begin = 0,
    .on_headers_complete = 0,
    .on_message_complete = 0,
    .on_chunk_header = 0,
    .on_chunk_complete = 0
};

//execute(data)
static qvm_value_t we_http_parser_execute(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    http_parser *parser = (http_parser *)qvm_to_invoke(e, qvm_get_property(e, p, ".data"));
    if( !parser ){
        return qvm_undefined;
    }

    if( argc == 0 || !qvm_is_buffer(v[0]) ) {
        return qvm_undefined;
    }
    int nparsed;
    int recved = qvm_get_bufferlen(e, v[0]);
    nparsed = http_parser_execute(parser, &settings_dontcall, (const char *)qvm_to_buffer(e, v[0]), recved);
    qvm_add_property(e, p, "code", qvm_mk_number(parser->status_code));
    if (parser->upgrade) {
        return  QVM_TRUE;
    } else if (nparsed != recved) {
        return  QVM_FALSE;
    }
    return  QVM_TRUE;
}

static qvm_value_t we_http_parser_destroy(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(e);QVM_UNUSED(argc);QVM_UNUSED(v);
    http_parser *parser = (http_parser *)qvm_to_invoke(e, qvm_get_property(e, p, ".data"));
    if( !parser ){
        return qvm_undefined;
    }
    we_free(parser);
    return qvm_undefined;
}

//create(type)
static qvm_value_t we_http_parser_create(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(p);
    if( argc == 0 || !qvm_is_number(v[0]) ) {
        qvm_throw(e, qvm_error(e, "Http type must be defined"));
        return qvm_undefined;
    }
    int type = qvm_to_int32(e, v[0]);
    http_parser *parser = we_malloc(sizeof (http_parser));
    if( !parser ) {
        qvm_throw(e, qvm_error(e, "Engine runs out of memory"));
        return qvm_undefined;
    }
    qvm_value_t res = qvm_new_object(e);
    qvm_add_property(e, res, "execute", qvm_mk_native(we_http_parser_execute));
    qvm_add_property(e, res, "destroy", qvm_mk_native(we_http_parser_destroy));
    qvm_add_property(e, res, "code", qvm_mk_number(200));
    qvm_add_property(e, res, "body", qvm_mk_null());
    http_parser_init(parser, (enum http_parser_type)type);
    qvm_add_property(e, res, ".data", qvm_mk_invoke((intptr_t)parser));
    parser->data = (void*)res.u.object;
    return res;
}

int we_http_parser_module(qvm_state_t *e) {
    qvm_value_t builtin = qvm_new_object(e);
    qvm_add_property(e, builtin, "create", qvm_mk_native(we_http_parser_create));
    qvm_add_property(e, builtin, "REQUEST", qvm_mk_number(HTTP_REQUEST));
    qvm_add_property(e, builtin, "RESPONSE", qvm_mk_number(HTTP_RESPONSE));
    qvm_add_module(e, "@system.http_parser", builtin);
    return 1;
}
