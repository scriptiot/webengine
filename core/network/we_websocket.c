#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "we_websocket.h"
#include "process/we_process.h"

#if defined(CMAKE_BUILD)
#undef CMAKE_BUILD
#endif

#if defined(XCODE)
#include "librws.h"
#else
#include <librws.h>
#endif

#if defined(CMAKE_BUILD)
#undef CMAKE_BUILD
#endif

#define WEBSOCKET_RX_BUF_SIZE  (100 * 1024)
#define WEBSOCKET_TX_BUF_SIZE  (10 * 1024)

typedef struct _websocket_client_t {
    rws_socket _socket;
    int is_https;
    int obj;
    int cb_close;
    int cb_recv;
    int cb_connected;
    int cb_disconnected;
    unsigned int rx_buf_size;
    uint8_t *rx_buf;
    int rx_len;

    unsigned int tx_buf_size;
    uint8_t *tx_buf;
    int tx_len;
} _websocket_client_t;

typedef struct _websocket_handler_t {
    int obj;
    int callback;
    uint8_t *rx_buf;
    int rx_len;
} _websocket_handler_t;

enum {
    EVENT_CLOSE,
    EVENT_CONNECTED,
    EVENT_DATA_TEXT,
    EVENT_DATA_BINARY,
    EVENT_DISCONNECTED
};

static void socket_callback_handler(qvm_state_t *e, int obj, void *ptr) {
    qvm_value_t arg = qvm_get_ref(e, obj);
    int *callback = ptr;
    qvm_value_t cb = qvm_get_ref(e, *callback);
    we_free(ptr);
    qvm_call(e, cb, we_page_get_current_default(e), 1, &arg);
}

static void socket_data_handler(qvm_state_t *e, int type, void *ptr) {
    _websocket_handler_t *client = ptr;
    qvm_value_t obj = qvm_get_ref(e, client->obj);

    qvm_value_t cb = qvm_get_ref(e, client->callback);
    qvm_value_t args[2];
    args[0] = obj;
    if (type == EVENT_DATA_BINARY) {
        args[1] = qvm_new_buffer(e, client->rx_buf, client->rx_len);
    } else {
        args[1] = qvm_new_string(e, (const char *)client->rx_buf);
    }
    we_free(client->rx_buf);
    we_free(client);
    qvm_call(e, cb, we_page_get_current_default(e), 2, args);
}

static void on_socket_received_text(rws_socket socket, const char * data, const unsigned int length) {
    rws_error error = rws_socket_get_error(socket);
    if (error) {
        we_log("Socket receive with code, error: %i, %s", rws_error_get_code(error), rws_error_get_description(error));
        return;
    }

    _websocket_client_t *client = (_websocket_client_t *)rws_socket_get_user_object(socket);
    assert(client);

    if (length > client->rx_buf_size - 1) {
        client->rx_len = client->rx_buf_size;
        strncpy((char *)client->rx_buf, data, client->rx_buf_size - 1);
        client->rx_buf[client->rx_buf_size] = 0;
    } else {
        client->rx_len = length;
        strncpy((char *)client->rx_buf, data, length);
        client->rx_buf[length] = 0;
    }

    _websocket_handler_t *local_client = we_malloc(sizeof(_websocket_handler_t));
    local_client->obj = client->obj;
    local_client->callback = client->cb_recv;
    local_client->rx_buf = we_malloc(client->rx_len);
    if( !client->rx_buf ) {
        we_free(local_client);
        return;
    }
    memcpy(local_client->rx_buf, client->rx_buf, client->rx_len);
    local_client->rx_len = client->rx_len;

    we_process_add_handler(EVENT_DATA_TEXT, socket_data_handler, local_client);
}

static void on_socket_received_bin(rws_socket socket, const void * data, const unsigned int length) {
    rws_error error = rws_socket_get_error(socket);
    if (error) {
        we_log("Socket receive with code, error: %i, %s", rws_error_get_code(error), rws_error_get_description(error));
        return;
    }

    _websocket_client_t *client = (_websocket_client_t *)rws_socket_get_user_object(socket);
    assert(client);

    if (length > client->rx_buf_size) {
        client->rx_len = client->rx_buf_size;
    } else {
        client->rx_len = length;
    }
    memcpy(client->rx_buf, data, client->rx_len);

    _websocket_handler_t *local_client = we_malloc(sizeof(_websocket_handler_t));
    local_client->obj = client->obj;
    local_client->callback = client->cb_recv;
    local_client->rx_buf = we_malloc(client->rx_len);
    if( !client->rx_buf ) {
        we_free(local_client);
        return;
    }
    memcpy(local_client->rx_buf, client->rx_buf, client->rx_len);
    local_client->rx_len = client->rx_len;

    we_process_add_handler(EVENT_DATA_BINARY, socket_data_handler, local_client);
}

static void on_socket_connected(rws_socket socket) {
    rws_error error = rws_socket_get_error(socket);
    if (error) {
        we_log("Socket connect with code, error: %i, %s", rws_error_get_code(error), rws_error_get_description(error));
        return;
    }

    _websocket_client_t *client = (_websocket_client_t *)rws_socket_get_user_object(socket);
    assert(client);

    int *callback = we_malloc(sizeof(client->cb_connected));
    *callback = client->cb_connected;
    we_process_add_handler(client->obj, socket_callback_handler, callback);
}

static void on_socket_disconnected(rws_socket socket) {
    rws_error error = rws_socket_get_error(socket);
    if (error) {
        we_log("Socket disconnect with code, error: %i, %s", rws_error_get_code(error), rws_error_get_description(error));
        return;
    }

    _websocket_client_t *client = (_websocket_client_t *)rws_socket_get_user_object(socket);
    assert(client);

    int *callback = we_malloc(sizeof(client->cb_close));
    *callback = client->cb_close;
    we_process_add_handler(client->obj, socket_callback_handler, callback);
}

static void client_clean(_websocket_client_t *client) {
    if( client->rx_buf )
        we_free(client->rx_buf);
    if( client->tx_buf )
        we_free(client->tx_buf);
    we_free(client);
}

static qvm_value_t we_websocket_send(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    _websocket_client_t *client = (_websocket_client_t *)we_get_user_data(e, p);
    if( !client ) {
        qvm_throw(e, qvm_error(e, "Failed to send message"));
        return qvm_undefined;
    }

    if (argc < 1 || !qvm_is_string(v[0])) {
        return qvm_undefined;
    }

    const char* message = qvm_to_string(e, v[0]);
    size_t len = strlen(message);
    rws_bool ret = rws_socket_send_text(client->_socket, message);
    if (ret) {
        return qvm_mk_number(len);
    } else {
        return qvm_mk_number(-1);
    }
}

static qvm_value_t we_websocket_close(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    _websocket_client_t *client = (_websocket_client_t *)we_get_user_data(e, p);
    if( !client ) {
        qvm_throw(e, qvm_error(e, "Failed to close websocket"));
        return qvm_undefined;
    }
    rws_socket_disconnect_and_release(client->_socket);
    client->_socket = NULL;
    client_clean(client);
    we_set_user_data(e, p, NULL);
    return qvm_undefined;
}

static qvm_value_t we_websocket_on(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    _websocket_client_t *client = (_websocket_client_t *)we_get_user_data(e, p);
    if( !client ) {
        qvm_throw(e, qvm_error(e, "Failed to setup socket event callback"));
        return qvm_undefined;
    }

    if( argc < 2 || !qvm_is_string(v[0]) || !qvm_is_script(v[1])) {
        qvm_throw(e, qvm_error(e, "Failed to setup socket event callback"));
        return qvm_undefined;
    }
    const char *event = qvm_to_string(e, v[0]);
    if( !strcmp(event, "data") ) {
        client->cb_recv = qvm_set_ref(e, v[1]);
    } else if( !strcmp(event, "connected") ) {
        client->cb_connected = qvm_set_ref(e, v[1]);
    } else if( !strcmp(event, "disconnected") ) {
        client->cb_disconnected = qvm_set_ref(e, v[1]);
    } else if( !strcmp(event, "close") ) {
        client->cb_close = qvm_set_ref(e, v[1]);
    }
    return qvm_undefined;
}

static qvm_value_t we_websocket_connect(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    if (argc < 1 || !qvm_is_object(v[0])) {
        return qvm_undefined;
    }

    qvm_value_t _schema = qvm_get_property(e, v[0], "schema");
    qvm_value_t _host = qvm_get_property(e, v[0], "host");
    qvm_value_t _port = qvm_get_property(e, v[0], "port");
    qvm_value_t _path = qvm_get_property(e, v[0], "path");

    const char * schema = qvm_to_string(e, _schema);
    const char * host = qvm_to_string(e, _host);
    int32_t port = qvm_to_int32(e, _port);
    const char * path = qvm_to_string(e, _path);

    _websocket_client_t *client = we_malloc(sizeof(_websocket_client_t));
    client->rx_buf_size = WEBSOCKET_RX_BUF_SIZE;
    client->tx_buf_size = WEBSOCKET_TX_BUF_SIZE;

    client->rx_buf = we_malloc(client->rx_buf_size);
    if( !client->rx_buf ) {
        qvm_throw(e, qvm_error(e, "Engine runs out of memory"));
        client_clean(client);
        return qvm_undefined;
    }
    memset(client->rx_buf, 0, client->rx_buf_size);

    client->tx_buf = we_malloc(client->tx_buf_size);
    if( !client->tx_buf ) {
        qvm_throw(e, qvm_error(e, "Engine runs out of memory"));
        client_clean(client);
        return qvm_undefined;
    }
    memset(client->tx_buf, 0, client->tx_buf_size);

    client->_socket = rws_socket_create(); // create and store socket handle
    assert(client->_socket);

    rws_socket_set_scheme(client->_socket, schema);
    rws_socket_set_host(client->_socket, host);
    rws_socket_set_path(client->_socket, path);
    rws_socket_set_port(client->_socket, port);

    rws_socket_set_on_disconnected(client->_socket, &on_socket_disconnected);
    rws_socket_set_on_connected(client->_socket, &on_socket_connected);
    rws_socket_set_on_received_text(client->_socket, &on_socket_received_text);
    rws_socket_set_on_received_bin(client->_socket, &on_socket_received_bin);

    rws_bool ret = rws_false;

    // connection denied for client applications
    ret = rws_socket_connect(client->_socket);
    if (!ret)  {
        qvm_throw(e, qvm_error(e, "socket failed to create thread"));
        rws_socket_disconnect_and_release(client->_socket);
        client->_socket = NULL;
        client_clean(client);
        return qvm_undefined;
    }
    rws_socket_set_user_object(client->_socket, client);

    qvm_value_t obj = qvm_new_object(e);
    we_set_user_data(e, obj, client);

    qvm_add_property(e, obj, "send", qvm_mk_native(we_websocket_send));
    qvm_add_property(e, obj, "close", qvm_mk_native(we_websocket_close));
    qvm_add_property(e, obj, "on", qvm_mk_native(we_websocket_on));
    client->obj = qvm_set_ref(e, obj);

    return obj;
}

int we_websocket_module(qvm_state_t *e) {
    qvm_value_t obj = qvm_new_object(e);
    qvm_add_property(e, obj, "connect", qvm_mk_native(we_websocket_connect));
    qvm_add_module(e, "@system.websocket", obj);
    return 1;
}
