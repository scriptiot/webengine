/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef _WRAP_HEATSHRINK_H
#define _WRAP_HEATSHRINK_H

#ifdef __cplusplus
extern "C" {
#endif

#include "webengine.h"

void heatshrink_set_hsd(uint8_t p_window_sz2, uint8_t p_lookahead_sz2, uint16_t p_input_buffer_size);
int we_module_heatshrink(qvm_state_t * e);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif

