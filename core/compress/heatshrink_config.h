#ifndef HEATSHRINK_CONFIG_H
#define HEATSHRINK_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

/* Should functionality assuming dynamic allocation be used? */
#define HEATSHRINK_DYNAMIC_ALLOC 0

/* Required parameters for static configuration */
#define HEATSHRINK_STATIC_INPUT_BUFFER_SIZE (1024 * 50)
#define HEATSHRINK_STATIC_WINDOW_BITS 14
#define HEATSHRINK_STATIC_LOOKAHEAD_BITS 8

/* Turn on logging for debugging. */
#define HEATSHRINK_DEBUGGING_LOGS 0

/* Use indexing for faster compression. (This requires additional space.) */
#define HEATSHRINK_USE_INDEX 0

#if HEATSHRINK_DYNAMIC_ALLOC
    #include "webengine.h"
    /* Optional replacement of malloc/free */
    #define HEATSHRINK_MALLOC(SZ) we_malloc(SZ)
    #define HEATSHRINK_FREE(P, SZ) we_free(P)
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
