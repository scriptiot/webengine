#include "compress/compress_heatshrink.h"
#include "compress/heatshrink_encoder.h"
#include "compress/heatshrink_decoder.h"
#include "webengine.h"

static uint32_t buf_size = 128;

static uint8_t window_sz2 = HEATSHRINK_STATIC_WINDOW_BITS;
static uint8_t lookahead_sz2 = HEATSHRINK_STATIC_LOOKAHEAD_BITS;
static uint16_t input_buffer_size = HEATSHRINK_STATIC_INPUT_BUFFER_SIZE;

void heatshrink_ptr_output_cb(unsigned char ch, uint32_t *cbdata, uint32_t * count, uint32_t len) {
  unsigned char **outPtr = (unsigned char**)cbdata;
  *((*outPtr)++) = ch;
}
int heatshrink_ptr_input_cb(uint32_t *cbdata, uint32_t * count, uint32_t len) {
  HeatShrinkPtrInputCallbackInfo *info = (HeatShrinkPtrInputCallbackInfo *)cbdata;
  if (!info->len) return -1;
  info->len--;
  return *(info->ptr++);
}

void heatshrink_var_output_cb(unsigned char ch, uint32_t *cbdata, uint32_t * count, uint32_t len) {
  uint8_t * data = (uint8_t*)cbdata;
  if( *count >= len ) return;
  data[*count] = ch;
  *count += 1;
}

int heatshrink_var_input_cb(uint32_t *cbdata, uint32_t * count, uint32_t len) {
  uint8_t * data = (uint8_t*)cbdata;
  int d = -1;
  if( *count >= len ) return d;
  d = data[*count] & 0xFF;
  *count += 1;
  return d;
}

void heatshrink_set_hsd(uint8_t p_window_sz2, uint8_t p_lookahead_sz2, uint16_t p_input_buffer_size) {
    window_sz2 = p_window_sz2;
    lookahead_sz2 = p_lookahead_sz2;
    input_buffer_size = p_input_buffer_size;
}

void heatshrink_set_buf_size(uint32_t size) {
    buf_size = size;
}

/** gets data from callback, writes to callback if nonzero. Returns total length. */
uint32_t heatshrink_encode_cb(int (*in_callback)(uint32_t *cbdata, uint32_t * cbcount, uint32_t len), uint32_t *in_cbdata, uint32_t in_len, void (*out_callback)(unsigned char ch, uint32_t *cbdata, uint32_t * count, uint32_t len), uint32_t *out_cbdata, uint32_t out_len) {
  heatshrink_encoder hse;
  uint8_t *inBuf = (uint8_t *)we_malloc(buf_size);
  uint8_t *outBuf = (uint8_t *)we_malloc(buf_size);
  if( inBuf == NULL || outBuf == NULL ) {
      return 0;
  }
  heatshrink_encoder_reset(&hse);

  size_t i;
  size_t count = 0;
  size_t sunk = 0;
  size_t polled = 0;
  int lastByte = 0;
  size_t inBufCount = 0;
  size_t inBufOffset = 0;
  uint32_t bufCount = 0;
  while (lastByte >= 0 || inBufCount>0) {
    // Read data from input
    if (inBufCount==0) {
      inBufOffset = 0;
      while (inBufCount<buf_size && lastByte>=0) {
        lastByte = in_callback(in_cbdata, &bufCount, in_len);
        if (lastByte >= 0)
          inBuf[inBufCount++] = (uint8_t)lastByte;
      }
    }
    // encode
    uint8_t ok = heatshrink_encoder_sink(&hse, &inBuf[inBufOffset], inBufCount, &count) >= 0;
    inBufCount -= count;
    inBufOffset += count;
    sunk += count;
    if ((inBufCount==0) && (lastByte < 0)) {
      heatshrink_encoder_finish(&hse);
    }

    HSE_poll_res pres;
    bufCount = 0;
    do {
      pres = heatshrink_encoder_poll(&hse, outBuf, buf_size, &count);
      if (out_callback)
        for (i=0;i<count;i++)
          out_callback(outBuf[i], out_cbdata, &bufCount, out_len);
      polled += count;
    } while (pres == HSER_POLL_MORE);
    if ((inBufCount==0) && (lastByte < 0)) {
      heatshrink_encoder_finish(&hse);
    }
  }
  we_free(inBuf);
  we_free(outBuf);
  return (uint32_t)polled;
}

/** gets data from callback, writes it into callback if nonzero. Returns total length */
uint32_t heatshrink_decode_cb(int (*in_callback)(uint32_t *cbdata, uint32_t * count, uint32_t len), uint32_t *in_cbdata, uint32_t in_len, void (*out_callback)(unsigned char ch, uint32_t *cbdata, uint32_t * count, uint32_t len), uint32_t *out_cbdata, uint32_t out_len) {
  heatshrink_decoder hsd;
  hsd.buffers = (uint8_t *)we_malloc(input_buffer_size);
  hsd.input_buffer_size = input_buffer_size;
  hsd.window_sz2 = window_sz2;
  hsd.lookahead_sz2 = lookahead_sz2;

  uint8_t *inBuf = (uint8_t *)we_malloc(buf_size);
  uint8_t *outBuf = (uint8_t *)we_malloc(buf_size);
  if( inBuf == NULL || outBuf == NULL || hsd.buffers == NULL) {
      return 0;
  }
  heatshrink_decoder_reset(&hsd);

  size_t i;
  size_t count = 0;
  size_t sunk = 0;
  size_t polled = 0;
  int lastByte = 0;
  size_t inBufCount = 0;
  size_t inBufOffset = 0;
  uint32_t bufCount = 0;
  while (lastByte >= 0 || inBufCount>0) {
    // Read data from input
    if (inBufCount==0) {
      inBufOffset = 0;
      while (inBufCount<buf_size && lastByte>=0) {
        lastByte = in_callback(in_cbdata, &bufCount, in_len);
        if (lastByte >= 0)
          inBuf[inBufCount++] = (uint8_t)lastByte;
      }
    }
    // decode
    uint8_t ok = heatshrink_decoder_sink(&hsd, &inBuf[inBufOffset], inBufCount, &count) >= 0;
    inBufCount -= count;
    inBufOffset += count;
    sunk += count;
    if ((inBufCount==0) && (lastByte < 0)) {
      heatshrink_decoder_finish(&hsd);
    }

    HSE_poll_res pres;
    bufCount = 0;
    do {
      pres = (HSE_poll_res)heatshrink_decoder_poll(&hsd, outBuf, buf_size, &count);
      if (out_callback)
        for (i=0;i<count;i++)
          out_callback(outBuf[i], out_cbdata, &bufCount, out_len);
      polled += count;
    } while (pres == HSER_POLL_MORE);
    if (lastByte < 0) {
      heatshrink_decoder_finish(&hsd);
    }
  }
  we_free(inBuf);
  we_free(outBuf);
  we_free(hsd.buffers);
  return (uint32_t)polled;
}



/** gets data from array, writes to callback if nonzero. Returns total length. */
uint32_t heatshrink_encode(unsigned char *in_data, size_t in_len, void (*out_callback)(unsigned char ch, uint32_t *cbdata, uint32_t * count, uint32_t len), uint32_t *out_cbdata, uint32_t out_len) {
  HeatShrinkPtrInputCallbackInfo cbi;
  cbi.ptr = in_data;
  cbi.len = in_len;
  return heatshrink_encode_cb(heatshrink_ptr_input_cb, (uint32_t*)&cbi,in_len, out_callback, out_cbdata, out_len);
}

/** gets data from callback, writes it into array if nonzero. Returns total length */
uint32_t heatshrink_decode(int (*in_callback)(uint32_t *cbdata, uint32_t * count, uint32_t len), uint32_t *in_cbdata, uint32_t in_len, unsigned char *out_data, uint32_t out_len) {
  unsigned char *dataptr = out_data;
  return heatshrink_decode_cb(in_callback, in_cbdata, in_len, out_data?heatshrink_ptr_output_cb:NULL, out_data?(uint32_t*)&dataptr:NULL, out_len);
}

