
#include "compress/compress_heatshrink.h"
#include "compress/wrap_heatshrink.h"



qvm_value_t wrap_heatshrink_compress(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
  if( argc > 0 ){
    uint8_t *in_it = NULL;
    uint32_t in_len = 0;
    if( qvm_is_buffer(v[0]) ) {
      in_it = qvm_to_buffer(e, v[0]);
      in_len = qvm_get_bufferlen(e, v[0]);
    } else if( qvm_is_string(v[0]) ) {
      in_it = (uint8_t *)qvm_to_string(e, v[0]);
      in_len = strlen((char *)in_it);
    }

    heatshrink_set_buf_size(in_len + 1);
    uint32_t compressedSize = heatshrink_encode_cb(heatshrink_var_input_cb, (uint32_t*)in_it, in_len, NULL, NULL, 0);
    uint8_t *out_it = we_malloc(compressedSize);
    if ( !out_it ) {
      return qvm_undefined;
    }
    heatshrink_encode_cb(heatshrink_var_input_cb, (uint32_t*)in_it, in_len, heatshrink_var_output_cb, (uint32_t*)out_it, compressedSize);
    qvm_value_t outVar = qvm_new_buffer(e, out_it, compressedSize);
    we_free(out_it);
    return outVar;
  } 
  return qvm_undefined;
}


qvm_value_t wrap_heatshrink_decompress(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v) {
  if( argc > 0 ){
    uint8_t *in_it = NULL;
    uint32_t in_len = 0;
    if( qvm_is_buffer(v[0]) ) {
      in_it = qvm_to_buffer(e, v[0]);
      in_len = qvm_get_bufferlen(e, v[0]);
    } else if( qvm_is_string(v[0]) ) {
      in_it = (uint8_t *)qvm_to_string(e, v[0]);
      in_len = strlen((char *)in_it);
    }
    heatshrink_set_buf_size(in_len + 1);
    uint32_t decompressedSize = heatshrink_decode(heatshrink_var_input_cb, (uint32_t*)in_it, in_len, NULL, 0);
    heatshrink_set_buf_size(decompressedSize + 1);
    uint8_t *out_it = we_malloc(decompressedSize);
    if ( out_it == NULL ) {
      return qvm_undefined;
    }
    heatshrink_decode_cb(heatshrink_var_input_cb, (uint32_t*)in_it, in_len, heatshrink_var_output_cb, (uint32_t*)out_it, decompressedSize);
    qvm_value_t outVar = qvm_new_buffer(e, out_it, decompressedSize);
    we_free(out_it);
    return outVar;
  } 
  return qvm_undefined;
}

int we_module_heatshrink(qvm_state_t * e){
    qvm_value_t module = qvm_new_object(e);
    qvm_add_property(e, module, "compress", qvm_mk_native(wrap_heatshrink_compress ));
    qvm_add_property(e, module, "decompress", qvm_mk_native(wrap_heatshrink_decompress ));
    qvm_add_module(e, "@system.heatshrink", module);
    return 1;
}
