/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_BASE64_h
#define WE_BASE64_h

#ifdef __cplusplus
extern "C" {
#endif

#include "webengine.h"
qvm_value_t we_base64_decode(qvm_state_t *e, qvm_value_t v);
qvm_value_t we_base64_encode(qvm_state_t *e, qvm_value_t v);
int we_base64_enclen(int len);
int we_base64_declen(int len);

int we_module_base64(qvm_state_t *e);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
