#include "binding/we_binding.h"
#include "html/we_html.h"

#define BINDING_CONTENT_SIZE    1024
static char _binding_content[BINDING_CONTENT_SIZE];

static int _has_binding(const char *content, size_t len) {
    for(size_t i = 0; i < len - 1; i++) {
        if( content[i] == '{' && content[i + 1] == '{' )
            return 1;
    }
    return 0;
}

qvm_value_t we_binding_eval(qvm_state_t *e, qvm_value_t pthis, const char *content, size_t len) {
    int flag = 0;
    size_t i;
    if( len > BINDING_CONTENT_SIZE - 3 ){
        return qvm_undefined;
    }
    memset(_binding_content, 0, BINDING_CONTENT_SIZE);
    _binding_content[0] = '\'';
    memcpy(_binding_content + 1, content, len);
    for(i = 0; i < len; i++) {
        if( flag == 0 && _binding_content[i] == '{' && _binding_content[i + 1] == '{' ){
            _binding_content[i] = '\'';
            _binding_content[i + 1] = '+';
            flag = 1;
            i++;
        } else if( flag == 1 && _binding_content[i] == '}' && _binding_content[i + 1] == '}' ) {
            _binding_content[i] = '+';
            _binding_content[i + 1] = '\'';
            flag = 0;
            i++;
        }
    }
    if( flag == 1 ) {
        qvm_throw(e, qvm_error(e, "Invalid binding syntax"));
        return qvm_undefined;
    }
    _binding_content[len + 1] = '\'';
    qvm_build_js_rule(e);
    qvm_value_t res = qvm_parse_string(e, _binding_content);
    return qvm_call(e, res, pthis, 0, NULL);
}

void we_binding_compile(qvm_state_t *e, qvm_value_t obj, qvm_value_t prop, const char *key, qvm_value_t scope) {
    qvm_value_t res;
    we_html_property_reg_t * prop_reg;
    we_html_object_t * html_obj;
    html_obj = (we_html_object_t*)qvm_to_invoke(e, qvm_get_property(e, obj, ".data"));
    prop_reg = we_html_reg_property_get(html_obj, key);
    if( qvm_is_string(prop) && prop_reg && prop_reg->set_api ) {
        const char *content = qvm_to_string(e, prop);
        size_t len = strlen(content);
        if( _has_binding(content, len) ) {
            we_binding_watcher_create(e, obj, prop, qvm_mk_string("value"), prop_reg->set_api, prop_reg->get_api);
            res = we_binding_eval(e, scope, content, len);
            we_binding_watcher_clear_current(e);
        } else {
            res = prop;
        }
        prop_reg->set_api(e, obj, 1, &res);
        return;
    }
}
