/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_BINDING_H
#define WE_BINDING_H

#ifdef __cplusplus
extern "C" {
#endif


#include "webengine.h"

//watcher
qvm_value_t we_binding_watcher_get_current(qvm_state_t * e);
void we_binding_watcher_set_current(qvm_state_t * e, qvm_value_t watcher);
void we_binding_watcher_clear_current(qvm_state_t * e);
qvm_value_t we_binding_watcher_create(qvm_state_t *e,
                                     qvm_value_t obj,
                                     qvm_value_t attr, qvm_value_t attr_name,
                                     qvm_native_t set_api,
                                     qvm_native_t get_api);
//binding native
void we_binding_define_property(qvm_state_t *e, qvm_value_t def);

void we_binding_compile(qvm_state_t *e, qvm_value_t obj, qvm_value_t prop, const char *key, qvm_value_t scope);
qvm_value_t we_binding_eval(qvm_state_t *e, qvm_value_t pthis, const char *content, size_t len);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
