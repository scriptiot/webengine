#include "binding/we_binding.h"

#include "page/we_page.h"

qvm_value_t we_binding_watcher_get_current(qvm_state_t * e) {
    return qvm_get_property(e, we_page_get_current_index_object(e), "$watcher");
}

void we_binding_watcher_set_current(qvm_state_t * e, qvm_value_t watcher) {
    qvm_add_property(e, we_page_get_current_index_object(e), "$watcher", watcher);
}

void we_binding_watcher_clear_current(qvm_state_t * e) {
    qvm_add_property(e, we_page_get_current_index_object(e), "$watcher", qvm_undefined);
}

qvm_value_t we_binding_watcher_create(qvm_state_t *e,
                                      qvm_value_t obj,
                                      qvm_value_t attr,
                                      qvm_value_t name,
                                      qvm_native_t set_api,
                                      qvm_native_t get_api) {
    qvm_value_t watcher = qvm_new_object(e);
    we_binding_watcher_set_current(e, watcher);
    qvm_add_property(e, watcher, "src_obj", obj);
    qvm_add_property(e, watcher, "attr", attr);
    qvm_add_property(e, watcher, "name", name);
    if( set_api )
        qvm_add_property(e, watcher, "set", qvm_mk_native(set_api));
    if( get_api )
        qvm_add_property(e, watcher, "get", qvm_mk_native(get_api));
    return watcher;
}

