#include "binding/we_binding.h"


static qvm_value_t _binding_getter(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    qvm_value_t setter_obj = v[2];
    qvm_value_t watcher = we_binding_watcher_get_current(e);
    if( !qvm_is_undefined(watcher) ){
        qvm_value_t watchers = qvm_get_property(e, setter_obj, "$watchers");
        if( qvm_is_undefined(watchers) ) {
            watchers = qvm_add_property(e, setter_obj, "$watchers", qvm_new_array(e));
        }
        int len = qvm_get_array_length(e, watchers);
        for(int i = 0; i < len; i++) {
            if( qvm_is_undefined( qvm_get_array(e, watchers, i) ) ){
                qvm_set_array(e, watchers, i, watcher);
                return qvm_get_property(e, p, qvm_to_string(e, v[0]));
            }
        }
        qvm_set_array(e, watchers, qvm_get_array_length(e, watchers), watcher);
    }
    return qvm_get_property(e, p, qvm_to_string(e, v[0]));
}

static qvm_value_t _binding_setter(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    const char *key = qvm_to_string(e, v[0]);
    qvm_value_t new_value = v[3];
    qvm_value_t setter_obj = v[2];
    qvm_value_t value = qvm_get_property(e, p, key);

    if( qvm_equal(e, new_value, value) )
        return qvm_undefined;

    qvm_add_property(e, p, key, new_value);

    qvm_value_t watchers = qvm_get_property(e, setter_obj, "$watchers");
    if( !qvm_is_undefined(watchers) ) {
        int len = qvm_get_array_length(e, watchers);
        for(int i = 0; i < len; i++) {
            qvm_value_t watcher = qvm_get_array(e, watchers, i);
            qvm_value_t set_api_value = qvm_get_property(e, watcher, "set");
            if( qvm_is_undefined(set_api_value ) )
                continue;
            qvm_native_t set_api = qvm_to_native(e, set_api_value);
            qvm_value_t src_obj = qvm_get_property(e, watcher, "src_obj");
            if( we_get_user_data(e, src_obj) != NULL ){
                qvm_value_t args[2];
                args[0] = new_value;
                args[1] = qvm_get_property(e, watcher, "name");
                set_api(e, src_obj, 2, args);
            } else {
                qvm_set_array(e, watchers, i, qvm_undefined);
            }
        }
    }
    return qvm_undefined;
}

static void _binding_define_property(qvm_state_t *e, qvm_value_t data) {
    if( !qvm_is_object(data) )
        return;
    qvm_value_t next = qvm_first_property(e, data);
    while(qvm_is_valid(next)) {
        const char *name = qvm_get_propertyname(e, next);
        int flag = qvm_get_property_flag(e, data, name);
        if( flag == QVM_PROP_C_W_E ) {
            qvm_value_t getter = qvm_new_native(e, _binding_getter, 0);
            qvm_value_t setter = qvm_new_native(e, _binding_setter, 0);
            qvm_def_accessor(e, data, name, getter, setter, next, QVM_PROP_C_W_E);
            if( qvm_is_object(next) && !qvm_is_script(next)) {
                _binding_define_property(e, next);
            }
        }
        next = qvm_next_property(e, data, next);
    }
}

void we_binding_define_property(qvm_state_t *e, qvm_value_t def) {
    qvm_value_t data = qvm_get_property(e, def, "data");

    qvm_value_t next = qvm_first_property(e, data);
    while(qvm_is_valid(next)) {
        const char *name = qvm_get_propertyname(e, next);
        int flag = qvm_get_property_flag(e, data, name);
        if( flag == QVM_PROP_C_W_E ) {
            qvm_add_property(e, def, name, next);
            qvm_value_t getter = qvm_new_native(e, _binding_getter, 0);
            qvm_value_t setter = qvm_new_native(e, _binding_setter, 0);
            qvm_def_accessor(e, def, name, getter, setter, next, QVM_PROP_C_W_E);
            if( qvm_is_object(next) && !qvm_is_script(next)) {
                _binding_define_property(e, next);
            }
        }
        next = qvm_next_property(e, data, next);
    }
}
