/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_CSS_H
#define WE_CSS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "webengine.h"

extern qvm_value_t we_css_linear_gradient(qvm_state_t * e, qvm_value_t * p, int argc, qvm_value_t * v);
int we_css_run(qvm_state_t *e, qvm_value_t html);
int we_css_module_init(qvm_state_t *e);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
