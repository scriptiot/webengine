#include "css/we_css.h"
#include "page/we_page.h"

#include "html/we_html.h"
#include "binding/we_binding.h"

qvm_value_t we_css_get_style_object_from_string(qvm_state_t *e, const char *content) {
    QVM_UNUSED2(e, content);
    return qvm_undefined;
}

qvm_value_t we_css_get_style_object_from_file(qvm_state_t *e, const char *path) {
    QVM_UNUSED2(e, path);
    return qvm_undefined;
}

static void _css_merge_object(qvm_state_t * e, qvm_value_t style_object, qvm_value_t properties) {
    if( !qvm_is_object(properties) )
        return;
    qvm_value_t next = qvm_first_property(e, properties);
    while(qvm_is_valid(next)) {
        const char *key = qvm_get_propertyname(e, next);
        int flag = qvm_get_property_flag(e, properties, key);
        if( flag == QVM_PROP_C_W_E ) {
            qvm_add_property(e, style_object, key, next);
        }
        next = qvm_next_property(e, properties, next);
    }
}

static qvm_value_t _css_object_set_style(qvm_state_t * e, qvm_value_t html, int argc, qvm_value_t style_obj){
    QVM_UNUSED(argc);
    if( !qvm_is_object(style_obj) )
        return qvm_undefined;
    qvm_value_t next = qvm_first_property(e, style_obj);
    while(qvm_is_valid(next)) {
        we_html_property_reg_t *reg = we_html_get_style_reg(next.key);
        if( reg ) {
            if( !qvm_is_script(next) )
                reg->set_api(e, html, 1, &next);
            else {
                qvm_call(e, next, html, 0, NULL);
            }
        }
        next = qvm_next_property(e, style_obj, next);
    }
    return qvm_undefined;
}

int we_css_run(qvm_state_t *e, qvm_value_t html) {
    qvm_value_t css_object = we_page_get_current_css_object(e);
    qvm_value_t css_label = qvm_get_property(e, css_object, "label");
    qvm_value_t css_class = qvm_get_property(e, css_object, "class");
    qvm_value_t css_id = qvm_get_property(e, css_object, "id");

    if ( qvm_is_undefined(css_label) && qvm_is_undefined(css_class) && qvm_is_undefined(css_id) ) {
        return 0;
    }

    qvm_value_t attributes = qvm_get_property(e, html, "attributes");
    qvm_value_t label = qvm_get_property(e, html, "name");
    qvm_value_t _class = qvm_get_property(e, attributes, "class");
    qvm_value_t id = qvm_get_property(e, attributes, "id");
    qvm_value_t style = qvm_get_property(e, attributes, "style");

    qvm_value_t style_obj = qvm_new_object(e);
    if ( !qvm_is_undefined(label) ){
        qvm_value_t css_label_properties = qvm_get_property(e, css_label, qvm_to_string(e, label));
        _css_merge_object(e, style_obj, css_label_properties);
    }

    if ( !qvm_is_undefined(_class) ){
        qvm_value_t css_class_properties = qvm_get_property(e, css_class, qvm_to_string(e, _class));
        _css_merge_object(e, style_obj, css_class_properties);
    }

    if ( !qvm_is_undefined(id) ){
        qvm_value_t css_id_properties = qvm_get_property(e, css_id, qvm_to_string(e, id));
        if ( !qvm_is_undefined(id) ){
            _css_merge_object(e, style_obj, css_id_properties);
        }
    }

    if ( qvm_is_string(style) ){
        const char *style_content = qvm_to_string(e, style);
        int len = strlen(style_content) + strlen("temp {") + strlen("}") + 1;
        char *content = we_malloc(len);
        QVM_ASSERT(content);
        memcpy(content, "temp {", strlen("temp {"));
        memcpy(content + strlen("temp {"), style_content, strlen(style_content));
        memcpy(content + strlen("temp {") + strlen(style_content), "}", 1);
        content[len] = 0;
        qvm_build_css_rule(e);
        qvm_value_t css_obj = qvm_parse_string(e, content);
        we_free(content);
        qvm_call(e, css_obj, css_obj, 0, NULL);
        qvm_value_t local_class = qvm_get_property(e, css_obj, "label");
        qvm_value_t local_style_obj = qvm_get_property(e, local_class, "temp");
        _css_merge_object(e, style_obj, local_style_obj);
    }
    _css_object_set_style(e, html, 0, style_obj);
    return 1;
}

int we_css_module_init(qvm_state_t *e) {
    QVM_UNUSED(e);
    return 1;
}
