#include "fileapi/we_file.h"
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>

WE_FHANDLE we_file_open(const char *path, const char *mode) {
    char realpath[256];
    we_get_realpath(realpath, path);
    return fopen(realpath, mode);
}

void we_file_close(WE_FHANDLE fd) {
    fclose(fd);
}

size_t we_file_read(WE_FHANDLE fd, void *buf, size_t btr) {
    return fread(buf, 1, btr, fd);
}

size_t we_file_write(WE_FHANDLE fd, void *buf, size_t btw) {
    return fwrite(buf, 1, btw, fd);
}

int32_t we_file_rename(const char *oldname, const char *newname) {
    char real_old_name[256], real_new_name[256];
    we_get_realpath(real_old_name, oldname);
    we_get_realpath(real_new_name, newname);
    return rename(real_old_name, real_new_name);
}

int32_t we_file_remove(const char *path) {
    char realpath[256];
    we_get_realpath(realpath, path);
    return remove(realpath);
}

// int stat(const char *path, struct stat *struct_stat);
int we_file_stat(const char *path, struct stat *info) {
    char realpath[256];
    we_get_realpath(realpath, path);
    return stat(realpath, info);
}

int32_t we_file_seek(WE_FHANDLE fd, int32_t offset, int32_t mode) {
    return fseek(fd, offset, mode);
}

long we_file_tell(WE_FHANDLE fd) {
    return ftell(fd);
}

long we_file_size(const char *path) {
    WE_FHANDLE file = we_file_open(path, "rb");
    if( file == NULL )
        return 0;
    fseek (file , 0 , SEEK_END);
    long lsize = we_file_tell(file);
    fseek (file , 0 , SEEK_SET);
    we_file_close(file);
    return lsize;
}

int32_t we_file_exist(const char *path) {
    WE_FHANDLE file = we_file_open(path, "rb");
    if( file == NULL)
        return 0;
    we_file_close(file);
    return 1;
}

//fs.read(fd, buffer, offset, length, position, callback)
static qvm_value_t we_fs_read(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    if (argc < 6 || qvm_is_object(v[0]) == 0 || qvm_is_buffer(v[1]) == 0 || qvm_is_integer(v[2]) == 0 || qvm_is_integer(v[3]) == 0|| qvm_is_integer(v[4]) == 0|| qvm_is_script(v[5]) == 0) {
        return qvm_undefined;
    }

    WE_FHANDLE fd = (WE_FHANDLE)qvm_to_invoke(e, qvm_get_property(e, v[0], ".data"));
    uint8_t* buf = qvm_to_buffer(e, v[1]);
    size_t rsize = qvm_to_int32(e, v[3]);

    size_t read_size = we_file_read(fd, buf, rsize);

    qvm_value_t args[3];
    args[0] = qvm_mk_null();
    args[1] = qvm_mk_number(read_size);
    args[2] = qvm_new_buffer(e, buf, rsize);
    qvm_call(e, v[5], we_page_get_current_default(e), 3, args);

    return qvm_undefined;
}

//fs.write(fd, buffer[, offset[, length[, position]]], callback)
static qvm_value_t we_fs_write(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    if (argc < 6 || qvm_is_object(v[0]) == 0 || qvm_is_buffer(v[1]) == 0 || qvm_is_integer(v[2]) == 0 || qvm_is_integer(v[3]) == 0|| qvm_is_integer(v[4]) == 0|| qvm_is_script(v[5]) == 0) {
        return qvm_undefined;
    }

    WE_FHANDLE fd = (WE_FHANDLE)qvm_to_invoke(e, qvm_get_property(e, v[0], ".data"));
    uint8_t* buf = qvm_to_buffer(e, v[1]);
    size_t wsize = qvm_to_int32(e, v[3]);

    size_t write_size = we_file_write(fd, buf, wsize);

    qvm_value_t args[3];
    args[0] = qvm_mk_null();
    args[1] = qvm_mk_number(write_size);
    args[2] = qvm_new_buffer(e, buf, wsize);
    qvm_call(e, v[5], we_page_get_current_default(e), 3, args);

    return qvm_undefined;
}

//fs.stat(path[, options], callback)
static qvm_value_t we_fs_stat(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    if (argc < 2 || qvm_is_string(v[0]) == 0 || qvm_is_script(v[1]) == 0) {
        return qvm_undefined;
    }

    const char* path = qvm_to_string(e, v[0]);
    struct stat info;
    int res = we_file_stat(path, &info);

    qvm_value_t args[2];
    if (res != 0 && errno != 0) {
        args[0] = qvm_new_string(e, strerror(errno));
        args[1] = qvm_mk_undefined();
    } else {
        /*  Refer to nodejs fs module interface standard:
         *  http://nodejs.cn/api/fs.html#fsstatpath-options-callback
         *  Currently only these properties are set, size / dev / mode
         *  More properties visit:
         *  https://man7.org/linux/man-pages/man0/sys_stat.h.0p.html */
        qvm_value_t stat_obj = qvm_new_object(e);
        qvm_add_property(e, stat_obj, "size", qvm_mk_number(info.st_size));
        qvm_add_property(e, stat_obj, "dev", qvm_mk_number(info.st_dev));
        qvm_add_property(e, stat_obj, "mode", qvm_mk_number(info.st_mode));

        args[0] = qvm_mk_null();
        args[1] = stat_obj;
    }
    qvm_call(e, v[1], we_page_get_current_default(e), 2, args);

    return qvm_undefined;
}

//fs.rename(oldPath, newPath, callback)
static qvm_value_t we_fs_rename(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    if (argc < 3 || qvm_is_string(v[0]) == 0 || qvm_is_string(v[1]) == 0 || qvm_is_script(v[2]) == 0) {
        return qvm_undefined;
    }

    const char* old_name = qvm_to_string(e, v[0]);
    const char* new_name = qvm_to_string(e, v[1]);
    int res = we_file_rename(old_name, new_name);

    qvm_value_t args[1];
    if (res == EOF && errno != 0) {
        args[0] = qvm_mk_string(strerror(errno));
    } else {
        args[0] = qvm_mk_null();
    }
    qvm_call(e, v[2], we_page_get_current_default(e), 1, args);

    return qvm_undefined;
}

//fs.rm(path[, options], callback)
static qvm_value_t we_fs_remove(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    if (argc < 2 || qvm_is_object(v[0]) == 0 || qvm_is_script(v[1]) == 0) {
        return qvm_undefined;
    }

    const char* path = qvm_to_string(e, v[0]);
    int res = we_file_remove(path);

    qvm_value_t args[1];
    if (res == EOF && errno != 0) {
        args[0] = qvm_mk_string(strerror(errno));
    } else {
        args[0] = qvm_mk_null();
    }
    qvm_call(e, v[1], we_page_get_current_default(e), 1, args);

    return qvm_undefined;
}

//fs.close(fd[, callback])
static qvm_value_t we_fs_close(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    if (argc < 2 || qvm_is_object(v[0]) == 0 || qvm_is_script(v[1]) == 0) {
        return qvm_undefined;
    }

    WE_FHANDLE fd = (WE_FHANDLE)qvm_to_invoke(e, qvm_get_property(e, v[0], ".data"));
    int res = 0;
    we_file_close(fd);

    qvm_value_t args[1];
    if (res == EOF && errno != 0) {
        args[0] = qvm_mk_string(strerror(errno));
    } else {
        args[0] = qvm_mk_null();
    }
    qvm_call(e, v[1], we_page_get_current_default(e), 1, args);

    return qvm_undefined;
}

//fs.open(path[, flags[, mode]], callback)
static qvm_value_t we_fs_open(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    if (argc < 3 || qvm_is_string(v[0]) == 0 || qvm_is_string(v[1]) == 0 || qvm_is_script(v[2]) == 0) {
        return qvm_undefined;
    }
    const char* path = qvm_to_string(e, v[0]);
    const char* mode = qvm_to_string(e, v[1]);
    WE_FHANDLE fd = we_file_open(path, mode);

    qvm_value_t res = qvm_new_object(e);
    qvm_value_t args[2];

    if (fd == NULL && errno != 0) {
        args[0] = qvm_mk_string(strerror(errno));
        args[1] = qvm_mk_undefined();
    } else {
        qvm_add_property(e, res, ".data", qvm_mk_invoke((void*)fd));
        args[0] = qvm_mk_null();
        args[1] = res;
    }
    qvm_call(e, v[2], we_page_get_current_default(e), 2, args);

    return qvm_undefined;
}

int we_fs_module(qvm_state_t *e) {
    qvm_value_t obj = qvm_new_object(e);
    qvm_add_property(e, obj, "open", qvm_mk_native(we_fs_open));
    qvm_add_property(e, obj, "close", qvm_mk_native(we_fs_close));
    qvm_add_property(e, obj, "read", qvm_mk_native(we_fs_read));
    qvm_add_property(e, obj, "write", qvm_mk_native(we_fs_write));
    qvm_add_property(e, obj, "rename", qvm_mk_native(we_fs_rename));
    qvm_add_property(e, obj, "rm", qvm_mk_native(we_fs_remove));
    qvm_add_property(e, obj, "stat", qvm_mk_native(we_fs_stat));
    qvm_add_module(e, "@system.fs", obj);
    return 1;
}
