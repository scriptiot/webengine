/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_FILE_H
#define WE_FILE_H

#include "webengine.h"

#define  WE_FHANDLE FILE*

#ifdef __cplusplus
extern "C" {
#endif

extern WE_FHANDLE we_file_open(const char *path, const char *mode);
extern void we_file_close(WE_FHANDLE fd);
extern int32_t we_file_remove(const char *path);
extern size_t we_file_read(WE_FHANDLE fd, void *buf, size_t btr);
extern size_t we_file_write(WE_FHANDLE fd, void *buf, size_t btw);
extern int32_t we_file_seek(WE_FHANDLE fd, int32_t offset, int32_t mode);
extern long we_file_tell(WE_FHANDLE fd);
extern long we_file_size(const char *path);
extern int32_t we_file_rename(const char *oldname, const char *newname);
extern int32_t we_file_exist(const char *path);
int we_fs_module(qvm_state_t *e);

#ifdef __cplusplus
}
#endif

#endif
