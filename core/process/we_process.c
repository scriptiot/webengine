#include "process/we_process.h"

#define WE_PROCESS_QUEUE_SIZE   32

static qvm_value_t process_queue;
static qvm_value_t argument_queue;

static we_process_info_t process_infos[WE_PROCESS_QUEUE_SIZE];

//process.nextTick(callback, [...args]);
static qvm_value_t _process_nextTick(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    if( argc > 0 && qvm_is_script(v[0]) ) {
        int len = qvm_get_array_length(e, argument_queue);
        for(int i = 0; i < len; i++){
            qvm_value_t value = qvm_get_array(e, argument_queue, i);
            if( qvm_is_undefined(value) ){
                qvm_set_array(e, process_queue, i, v[0]);
                if( argc > 1 ) {
                    qvm_value_t arguments = qvm_new_array(e);
                    for(int i = 0; i < argc - 1; i++){
                        qvm_set_array(e, arguments, i, *(v + 1 + i));
                    }
                    qvm_set_array(e, argument_queue, i, arguments);
                } else {
                    qvm_set_array(e, argument_queue, i, qvm_mk_null());
                }
            }
        }
    }
    return qvm_undefined;
}

void we_process_poll(qvm_state_t *e) {
    for(int i = 0; i < WE_PROCESS_QUEUE_SIZE; i++) {
        if( process_infos[i].handler != NULL ){
            process_infos[i].handler(e, process_infos[i].type, process_infos[i].ptr);
            process_infos[i].handler = NULL;
            process_infos[i].ptr = NULL;
        }
    }

    int len = qvm_get_array_length(e, process_queue);
    for(int index = 0; index < len; index++) {
        qvm_value_t callback = qvm_get_array(e, process_queue, index);
        if( !qvm_is_script(callback) )
            continue;
        qvm_value_t arguments = qvm_get_array(e, argument_queue, index);
        if( qvm_is_array(arguments) ) {
            int argc = qvm_get_array_length(e, arguments);
            qvm_value_t args[argc];
            for(int i = 0; i < argc; i++){
                args[i] = qvm_get_array(e, arguments, i + 1);
            }
            qvm_call(e, callback, qvm_get_current_scope(e), argc, args);
        } else {
            qvm_call(e, callback, qvm_get_current_scope(e), 0, NULL);
        }
        qvm_set_array(e, process_queue, index, qvm_undefined);
        qvm_set_array(e, argument_queue, index, qvm_undefined);
    }
}

void we_process_add_handler(int type, we_process_handler handler, void *ptr) {
    for(int i = 0; i < WE_PROCESS_QUEUE_SIZE; i++) {
        if( process_infos[i].handler == NULL ){
            process_infos[i].handler = handler;
            process_infos[i].ptr = ptr;
            process_infos[i].type = type;
            break;
        }
    }
}

void we_process_next_tick(qvm_state_t *e, qvm_value_t callback, int argc, qvm_value_t *v) {
    qvm_value_t args[argc + 1];
    args[0] = callback;
    for(uint32_t i = 0; i < argc; i++) {
        args[1 + i] = v[i];
    }
    _process_nextTick(e, qvm_undefined, argc + 1, args);
}

int we_process_module(qvm_state_t *e) {
    process_queue = qvm_new_array(e);
    qvm_push(e, process_queue);
    argument_queue = qvm_new_array(e);
    qvm_push(e, argument_queue);
    for(int i = 0; i < WE_PROCESS_QUEUE_SIZE; i++) {
        process_infos[i].handler = NULL;
    }

    qvm_value_t builtin = qvm_new_object(e);
    qvm_add_property(e, builtin, "nextTick", qvm_mk_native(_process_nextTick));
    qvm_add_module(e, "@system.process", builtin);
    return 1;
}

