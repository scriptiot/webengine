/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_PROCESS_H
#define WE_PROCESS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "webengine.h"

typedef struct we_process_info_t {
    int type;
    we_process_handler handler;
    void* ptr;
} we_process_info_t;

void we_process_add_handler(int type, we_process_handler handler, void *ptr);
void we_process_next_tick(qvm_state_t *e, qvm_value_t callback, int argc, qvm_value_t *v);
void we_process_poll(qvm_state_t *e);
int we_process_module(qvm_state_t *e);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
