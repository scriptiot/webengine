#include "javascript/we_js_builtin.h"
#include "page/we_page.h"


static qvm_value_t we_js_builtin_dom_close(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(e);QVM_UNUSED(p);QVM_UNUSED(argc);QVM_UNUSED(v);
    return qvm_undefined;
}

static qvm_value_t _document_querySelector(qvm_state_t *e, qvm_value_t p, const char *name) {
    qvm_value_t attributes = qvm_get_property(e, p, "attributes");
    qvm_value_t id = qvm_get_property(e, attributes, "id");
    if( qvm_is_string(id) && !strcmp(name, qvm_to_string(e, id)) ) {
        return p;
    }
    qvm_value_t elements = qvm_get_property(e, p, "childNodes");
    if ( !qvm_is_array(elements) ){
        return qvm_undefined;
    }
    int len = qvm_get_array_length(e, elements);
    for (int j = 0; j < len; j++) {
        qvm_value_t ret = _document_querySelector(e, qvm_get_array(e, elements, j), name);
        if ( !qvm_is_undefined(ret) ) {
            return ret;
        }
    }
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_getElementById(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(p);
    if( argc < 1 || !qvm_is_string(v[0]) ){
        return qvm_undefined;
    }
    qvm_value_t index_object = we_page_get_current_index_object(e);
    qvm_value_t html_object  = we_page_get_current_html_object(e);
    if ( qvm_is_undefined(index_object) || qvm_is_undefined(html_object) ) {
        return qvm_undefined;
    }

    return _document_querySelector(e, html_object, qvm_to_string(e, v[0]));
}

static qvm_value_t we_js_builtin_dom_getElementsByName(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(e);QVM_UNUSED(p);QVM_UNUSED(argc);QVM_UNUSED(v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_getElementsByTagName(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(e);QVM_UNUSED(p);QVM_UNUSED(argc);QVM_UNUSED(v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_open(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(e);QVM_UNUSED(p);QVM_UNUSED(argc);QVM_UNUSED(v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_write(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(e);QVM_UNUSED(p);QVM_UNUSED(argc);QVM_UNUSED(v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_writeln(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED(e);QVM_UNUSED(p);QVM_UNUSED(argc);QVM_UNUSED(v);
    return qvm_undefined;
}

qvm_value_t we_js_builtin_dom_create(qvm_state_t *e) {
    qvm_value_t document = qvm_new_object(e);
    qvm_push(e, document);
    qvm_add_property(e, document, "close", qvm_mk_native(we_js_builtin_dom_close) );
    qvm_add_property(e, document, "getElementById", qvm_mk_native(we_js_builtin_dom_getElementById) );
    qvm_add_property(e, document, "getElementsByName", qvm_mk_native(we_js_builtin_dom_getElementsByName) );
    qvm_add_property(e, document, "getElementsByTagName", qvm_mk_native(we_js_builtin_dom_getElementsByTagName) );
    qvm_add_property(e, document, "open", qvm_mk_native(we_js_builtin_dom_open) );
    qvm_add_property(e, document, "write", qvm_mk_native(we_js_builtin_dom_write) );
    qvm_add_property(e, document, "writeln", qvm_mk_native(we_js_builtin_dom_writeln) );
    return document;
}
