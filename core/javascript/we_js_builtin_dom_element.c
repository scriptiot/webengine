#include "javascript/we_js_builtin.h"  

static qvm_value_t we_js_builtin_dom_element_appendChild(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED4(e, p, argc, v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_element_cloneNode(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED4(e, p, argc, v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_element_compareDocumentPosition(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED4(e, p, argc, v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_element_getAttribute(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED4(e, p, argc, v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_element_getAttributeNode(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED4(e, p, argc, v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_element_getElementsByTagName(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED4(e, p, argc, v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_element_hasAttribute(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED4(e, p, argc, v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_element_hasAttributes(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED4(e, p, argc, v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_element_hasChildNodes(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED4(e, p, argc, v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_element_insertBefore(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED4(e, p, argc, v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_element_isDefaultNamespace(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED4(e, p, argc, v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_element_isEqualNode(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED4(e, p, argc, v);
    return qvm_undefined;
}

static qvm_value_t we_js_builtin_dom_element_isSameNode(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    QVM_UNUSED4(e, p, argc, v);
    return qvm_undefined;
}

int we_js_builtin_dom_element_init(qvm_state_t *e, qvm_value_t elements) {
    qvm_value_t context = qvm_new_object(e);
    qvm_add_property(e, context, "accessKey", qvm_undefined);
    qvm_add_property(e, context, "appendChild", qvm_mk_native(we_js_builtin_dom_element_appendChild));
    qvm_add_property(e, context, "cloneNode", qvm_mk_native(we_js_builtin_dom_element_cloneNode));
    qvm_add_property(e, context, "compareDocumentPosition", qvm_mk_native(we_js_builtin_dom_element_compareDocumentPosition));
    qvm_add_property(e, context, "getAttribute", qvm_mk_native(we_js_builtin_dom_element_getAttribute));
    qvm_add_property(e, context, "getAttributeNode", qvm_mk_native(we_js_builtin_dom_element_getAttributeNode));
    qvm_add_property(e, context, "getElementsByTagName", qvm_mk_native(we_js_builtin_dom_element_getElementsByTagName));
    qvm_add_property(e, context, "hasAttribute", qvm_mk_native(we_js_builtin_dom_element_hasAttribute));
    qvm_add_property(e, context, "hasAttributes", qvm_mk_native(we_js_builtin_dom_element_hasAttributes));
    qvm_add_property(e, context, "hasChildNodes", qvm_mk_native(we_js_builtin_dom_element_hasChildNodes));
    qvm_add_property(e, context, "insertBefore", qvm_mk_native(we_js_builtin_dom_element_insertBefore));
    qvm_add_property(e, context, "isDefaultNamespace", qvm_mk_native(we_js_builtin_dom_element_isDefaultNamespace));
    qvm_add_property(e, context, "isEqualNode", qvm_mk_native(we_js_builtin_dom_element_isEqualNode));
    qvm_add_property(e, context, "isSameNode", qvm_mk_native(we_js_builtin_dom_element_isSameNode));
    qvm_add_property(e, elements, "childNodes", context);
    return 1;
}
