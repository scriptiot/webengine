#include "javascript/we_js_builtin.h"
#include "binding/we_binding.h"

#define ONESHOT             1
#define ONESHOT_TRIGGERED   2

static qvm_value_t timer_callbacks;

typedef struct js_timer_t {
    int oneshot;
    int period;
    int count;
    int id;
} js_timer_t;

static qvm_value_t _js_builtin_force_update(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t * v){
    return qvm_undefined;
}

static void addTimer(qvm_state_t * e, js_timer_t *timer, int period, qvm_value_t cb) {
    timer->count = 0;
    timer->id = qvm_set_ref(e, cb);
    timer->period = period;
    int len = qvm_get_array_length(e, timer_callbacks);
    for(int i = 0; i < len; i++) {
        qvm_value_t value = qvm_get_array(e, timer_callbacks, i);
        if( qvm_is_undefined(value) ) {
            qvm_set_array(e, timer_callbacks, i, qvm_mk_invoke(timer));
            break;
        }
    }
    qvm_set_array(e, timer_callbacks, len, qvm_mk_invoke(timer));
}

static qvm_value_t setTimeout(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t *v){
    if( argc != 2 || !qvm_is_script(v[0]) || !qvm_is_number(v[1]) )
        return qvm_undefined;
    js_timer_t *timer = we_malloc(sizeof (js_timer_t));
    QVM_ASSERT(timer);
    timer->oneshot = ONESHOT;
    addTimer(e, timer, qvm_to_int32(e, v[1]), v[0]);
    return qvm_mk_number(timer->id);
}

static qvm_value_t clearTimeout(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t *v){
    int id = qvm_to_int32(e, v[0]);
    int len = qvm_get_array_length(e, timer_callbacks);
    for(int i = 0; i < len; i++) {
        qvm_value_t value = qvm_get_array(e, timer_callbacks, i);
        if( qvm_is_invoke(value) ) {
            js_timer_t *timer = qvm_to_invoke(e, value);
            if( timer->id == id ) {
                qvm_clear_ref(e, timer->id);
                qvm_set_array(e, timer_callbacks, i, qvm_undefined);
                we_free(timer);
                break;
            }
        }
    }
    return qvm_undefined;
}

static qvm_value_t setInterval(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t *v){
    if( argc != 2 || !qvm_is_script(v[0]) || !qvm_is_number(v[1]) )
        return qvm_undefined;
    js_timer_t *timer = we_malloc(sizeof (js_timer_t));
    QVM_ASSERT(timer);
    timer->oneshot = 0;
    addTimer(e, timer, qvm_to_int32(e, v[1]), v[0]);
    return qvm_mk_number(timer->id);
}

static qvm_value_t clearInterval(qvm_state_t * e, qvm_value_t p, int argc, qvm_value_t *v){
    return clearTimeout(e, p, argc, v);
}

void we_js_builtin_timer_poll(qvm_state_t *e) {
    int len = qvm_get_array_length(e, timer_callbacks);
    for(int i = 0; i < len; i++) {
        qvm_value_t value = qvm_get_array(e, timer_callbacks, i);
        if( qvm_is_invoke(value) ) {
            js_timer_t *timer = qvm_to_invoke(e, value);
            if( timer->oneshot == ONESHOT_TRIGGERED )
                continue;
            if( timer->count >= timer->period ) {
                if( timer->oneshot ) {
                    timer->oneshot = ONESHOT_TRIGGERED;
                } else {
                    timer->count = 0;
                }
                qvm_value_t cb = qvm_get_ref(e, timer->id);
                qvm_call(e, cb, we_page_get_current_default(e), 0, NULL);
            } else {
                timer->count++;
            }
        }
    }
}

int we_js_builtin_init(qvm_state_t *e) {
    timer_callbacks = qvm_new_array(e);
    qvm_push(e, timer_callbacks);
    qvm_value_t document = we_js_builtin_dom_create(e);
    qvm_add_global(e, "document", document);
    qvm_add_global(e, "forceUpdate", qvm_mk_native(_js_builtin_force_update ));

    qvm_add_global(e, "setTimeout", qvm_mk_native(setTimeout ));
    qvm_add_global(e, "clearTimeout", qvm_mk_native(clearTimeout ));
    qvm_add_global(e, "setInterval", qvm_mk_native(setInterval ));
    qvm_add_global(e, "clearInterval", qvm_mk_native(clearInterval ));
    return 1;
}
