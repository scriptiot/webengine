/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_JS_BUILTIN_H
#define WE_JS_BUILTIN_H

#ifdef __cplusplus
extern "C" {
#endif

#include "webengine.h"

qvm_value_t we_js_builtin_dom_create(qvm_state_t *e);
int we_js_builtin_init(qvm_state_t *e);
void we_js_builtin_timer_poll(qvm_state_t *e);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
