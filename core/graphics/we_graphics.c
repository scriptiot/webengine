#include "graphics/we_graphics.h"

#include <math.h>
#ifdef WE_USE_OPENGL
#include <stdio.h>
#include <GL/glx.h>
#include <GL/gl.h>
#include "picotk.h"

extern void tk_init(int width, int height, int bits_per_pixels, void *buffer);

void we_graphics_3d_init(qvm_state_t *e,int width, int height, int bits_per_pixels, void *framebuffer) {
    tk_init(width, height, bits_per_pixels, framebuffer);
}

void we_graphics_3d_loop(qvm_state_t *e, qvm_value_t *callback){
    qvm_call(e, callback[0], we_page_get_current_default(e), 0, NULL);
}

extern void tkSwapBuffers(void);

static qvm_value_t wrap_gltkSwapBuffers(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 0 ) return qvm_undefined;
    tkSwapBuffers();
    return qvm_undefined;
}

static qvm_value_t wrap_glglEnable(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int code = qvm_to_int32(e, v[0]);
    glEnable(code);
    return qvm_undefined;
}

static qvm_value_t wrap_glglDisable(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int code = qvm_to_int32(e, v[0]);
    glDisable(code);
    return qvm_undefined;
}

static qvm_value_t wrap_glglShadeModel(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int mode = qvm_to_int32(e, v[0]);
    glShadeModel(mode);
    return qvm_undefined;
}

static qvm_value_t wrap_glglCullFace(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int mode = qvm_to_int32(e, v[0]);
    glCullFace(mode);
    return qvm_undefined;
}

static qvm_value_t wrap_glglPolygonMode(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    int face = qvm_to_int32(e, v[0]);
    int mode = qvm_to_int32(e, v[1]);
    glPolygonMode(face, mode);
    return qvm_undefined;
}

static qvm_value_t wrap_glglBegin(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int type = qvm_to_int32(e, v[0]);
    glBegin(type);
    return qvm_undefined;
}

static qvm_value_t wrap_glglEnd(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 0 ) return qvm_undefined;
    glEnd();
    return qvm_undefined;
}

static qvm_value_t wrap_glglVertex2f(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    float x = qvm_to_number(e, v[0]);
    float y = qvm_to_number(e, v[1]);
    glVertex2f(x, y);
    return qvm_undefined;
}

static qvm_value_t wrap_glglVertex3f(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 3 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    float x = qvm_to_number(e, v[0]);
    float y = qvm_to_number(e, v[1]);
    float z = qvm_to_number(e, v[2]);
    glVertex3f(x, y, z);
    return qvm_undefined;
}

static qvm_value_t wrap_glglVertex3fv(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_invoke(v[0])) return qvm_undefined;
    void* x = qvm_to_invoke(e, v[0]);
    glVertex3fv(x);
    return qvm_undefined;
}

static qvm_value_t wrap_glglVertex4f(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 4 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    if(!qvm_is_number(v[3])) return qvm_undefined;
    float x = qvm_to_number(e, v[0]);
    float y = qvm_to_number(e, v[1]);
    float z = qvm_to_number(e, v[2]);
    float r = qvm_to_number(e, v[3]);
    glVertex4f(x, y, z, r);
    return qvm_undefined;
}

static qvm_value_t wrap_glglColor3f(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 3 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    float x = qvm_to_number(e, v[0]);
    float y = qvm_to_number(e, v[1]);
    float z = qvm_to_number(e, v[2]);
    glColor3f(x, y, z);
    return qvm_undefined;
}

static qvm_value_t wrap_glglColor3fv(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_invoke(v[0])) return qvm_undefined;
    void* x = qvm_to_invoke(e, v[0]);
    glColor3fv(x);
    return qvm_undefined;
}

static qvm_value_t wrap_glglNormal3f(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 3 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    float x = qvm_to_number(e, v[0]);
    float y = qvm_to_number(e, v[1]);
    float z = qvm_to_number(e, v[2]);
    glNormal3f(x, y, z);
    return qvm_undefined;
}

static qvm_value_t wrap_glglNormal3fv(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_invoke(v[0])) return qvm_undefined;
    void* x = qvm_to_invoke(e, v[0]);
    glNormal3fv(x);
    return qvm_undefined;
}

static qvm_value_t wrap_glglTexCoord2f(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    float x = qvm_to_number(e, v[0]);
    float y = qvm_to_number(e, v[1]);
    glTexCoord2f(x, y);
    return qvm_undefined;
}

static qvm_value_t wrap_glglTexCoord2fv(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_invoke(v[0])) return qvm_undefined;
    void* x = qvm_to_invoke(e, v[0]);
    glTexCoord2fv(x);
    return qvm_undefined;
}

static qvm_value_t wrap_glglTexCoord4f(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 4 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    if(!qvm_is_number(v[3])) return qvm_undefined;
    float x = qvm_to_number(e, v[0]);
    float y = qvm_to_number(e, v[1]);
    float z = qvm_to_number(e, v[2]);
    float r = qvm_to_number(e, v[3]);
    glTexCoord4f(x, y, z, r);
    return qvm_undefined;
}

static qvm_value_t wrap_glglEdgeFlag(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int flag = qvm_to_int32(e, v[0]);
    glEdgeFlag(flag);
    return qvm_undefined;
}

static qvm_value_t wrap_glglMatrixMode(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int mode = qvm_to_int32(e, v[0]);
    glMatrixMode(mode);
    return qvm_undefined;
}

static qvm_value_t wrap_glglLoadMatrixf(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_invoke(v[0])) return qvm_undefined;
    void* m = qvm_to_invoke(e, v[0]);
    glLoadMatrixf(m);
    return qvm_undefined;
}

static qvm_value_t wrap_glglLoadIdentity(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 0 ) return qvm_undefined;
    glLoadIdentity();
    return qvm_undefined;
}

static qvm_value_t wrap_glglMultMatrixf(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_invoke(v[0])) return qvm_undefined;
    void* m = qvm_to_invoke(e, v[0]);
    glMultMatrixf(m);
    return qvm_undefined;
}

static qvm_value_t wrap_glglPushMatrix(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 0 ) return qvm_undefined;
    glPushMatrix();
    return qvm_undefined;
}

static qvm_value_t wrap_glglPopMatrix(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 0 ) return qvm_undefined;
    glPopMatrix();
    return qvm_undefined;
}

static qvm_value_t wrap_glglRotatef(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 4 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    if(!qvm_is_number(v[3])) return qvm_undefined;
    float angle = qvm_to_number(e, v[0]);
    float x = qvm_to_number(e, v[1]);
    float y = qvm_to_number(e, v[2]);
    float z = qvm_to_number(e, v[3]);
    glRotatef(angle, x, y, z);
    return qvm_undefined;
}

static qvm_value_t wrap_glglTranslatef(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 3 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    float x = qvm_to_number(e, v[0]);
    float y = qvm_to_number(e, v[1]);
    float z = qvm_to_number(e, v[2]);
    glTranslatef(x, y, z);
    return qvm_undefined;
}

static qvm_value_t wrap_glglScalef(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 3 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    float x = qvm_to_number(e, v[0]);
    float y = qvm_to_number(e, v[1]);
    float z = qvm_to_number(e, v[2]);
    glScalef(x, y, z);
    return qvm_undefined;
}

static qvm_value_t wrap_glglViewport(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 4 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    if(!qvm_is_number(v[3])) return qvm_undefined;
    int x = qvm_to_int32(e, v[0]);
    int y = qvm_to_int32(e, v[1]);
    int width = qvm_to_int32(e, v[2]);
    int height = qvm_to_int32(e, v[3]);
    glViewport(x, y, width, height);
    return qvm_undefined;
}

static qvm_value_t wrap_glglFrustum(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 6 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    if(!qvm_is_number(v[3])) return qvm_undefined;
    if(!qvm_is_number(v[4])) return qvm_undefined;
    if(!qvm_is_number(v [5])) return qvm_undefined;
    double left = qvm_to_number(e, v[0]);
    double right = qvm_to_number(e, v[1]);
    double bottom = qvm_to_number(e, v[2]);
    double top = qvm_to_number(e, v[3]);
    double near = qvm_to_number(e, v[4]);
    double far = qvm_to_number(e, v[5]);
    glFrustum(left, right, bottom, top, near, far);
    return qvm_undefined;
}

static qvm_value_t wrap_glglGenLists(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int range = qvm_to_int32(e, v[0]);
    return qvm_mk_number(glGenLists(range));
}

static qvm_value_t wrap_glglIsList(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int list = qvm_to_int32(e, v[0]);
    return qvm_mk_number(glIsList(list));
}

static qvm_value_t wrap_glglNewList(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    int list = qvm_to_int32(e, v[0]);
    int mode = qvm_to_int32(e, v[1]);
    glNewList(list, mode);
    return qvm_undefined;
}

static qvm_value_t wrap_glglEndList(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 0 ) return qvm_undefined;
    glEndList();
    return qvm_undefined;
}

static qvm_value_t wrap_glglCallList(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int list = qvm_to_int32(e, v[0]);
    glCallList(list);
    return qvm_undefined;
}

static qvm_value_t wrap_glglClear(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int mask = qvm_to_int32(e, v[0]);
    glClear(mask);
    return qvm_undefined;
}

static qvm_value_t wrap_glglClearColor(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 4 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    if(!qvm_is_number(v[3])) return qvm_undefined;
    float r = qvm_to_number(e, v[0]);
    float g = qvm_to_number(e, v[1]);
    float b = qvm_to_number(e, v[2]);
    float a = qvm_to_number(e, v[3]);
    glClearColor(r, g, b, a);
    return qvm_undefined;
}

static qvm_value_t wrap_glglClearDepth(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    double depth = qvm_to_number(e, v[0]);
    glClearDepth(depth);
    return qvm_undefined;
}

static qvm_value_t wrap_glglRenderMode(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int mode = qvm_to_int32(e, v[0]);
    return qvm_mk_number(glRenderMode(mode));
}

static qvm_value_t wrap_glglSelectBuffer(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_invoke(v[1])) return qvm_undefined;
    int size = qvm_to_int32(e, v[0]);
    void* buf = qvm_to_invoke(e, v[1]);
    glSelectBuffer(size, buf);
    return qvm_undefined;
}

static qvm_value_t wrap_glglInitNames(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 0 ) return qvm_undefined;
    glInitNames();
    return qvm_undefined;
}

static qvm_value_t wrap_glglPushName(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int name = qvm_to_int32(e, v[0]);
    glPushName(name);
    return qvm_undefined;
}

static qvm_value_t wrap_glglPopName(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 0 ) return qvm_undefined;
    glPopName();
    return qvm_undefined;
}

static qvm_value_t wrap_glglLoadName(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int name = qvm_to_int32(e, v[0]);
    glLoadName(name);
    return qvm_undefined;
}

static qvm_value_t wrap_glglGenTextures(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_invoke(v[1])) return qvm_undefined;
    int n = qvm_to_int32(e, v[0]);
    void* textures = qvm_to_invoke(e, v[1]);
    glGenTextures(n, textures);
    return qvm_undefined;
}

static qvm_value_t wrap_glglDeleteTextures(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_invoke(v[1])) return qvm_undefined;
    int n = qvm_to_int32(e, v[0]);
    void* textures = qvm_to_invoke(e, v[1]);
    glDeleteTextures(n, textures);
    return qvm_undefined;
}

static qvm_value_t wrap_glglBindTexture(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    int target = qvm_to_int32(e, v[0]);
    int texture = qvm_to_int32(e, v[1]);
    glBindTexture(target, texture);
    return qvm_undefined;
}

static qvm_value_t wrap_glglTexImage2D(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 9 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    if(!qvm_is_number(v[3])) return qvm_undefined;
    if(!qvm_is_number(v[4])) return qvm_undefined;
    if(!qvm_is_number(v [5])) return qvm_undefined;
    if(!qvm_is_number(v[6])) return qvm_undefined;
    if(!qvm_is_number(v[7])) return qvm_undefined;
    if(!qvm_is_invoke(v[8])) return qvm_undefined;
    int target = qvm_to_int32(e, v[0]);
    int level = qvm_to_int32(e, v[1]);
    int components = qvm_to_int32(e, v[2]);
    int width = qvm_to_int32(e, v[3]);
    int height = qvm_to_int32(e, v[4]);
    int border = qvm_to_int32(e, v[5]);
    int format = qvm_to_int32(e, v[6]);
    int type = qvm_to_int32(e, v[7]);
    void* pixels = qvm_to_invoke(e, v[8]);
    glTexImage2D(target, level, components, width, height, border, format, type, pixels);
    return qvm_undefined;
}

static qvm_value_t wrap_glglTexEnvi(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 3 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    int target = qvm_to_int32(e, v[0]);
    int pname = qvm_to_int32(e, v[1]);
    int param = qvm_to_int32(e, v[2]);
    glTexEnvi(target, pname, param);
    return qvm_undefined;
}

static qvm_value_t wrap_glglTexParameteri(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 3 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    int target = qvm_to_int32(e, v[0]);
    int pname = qvm_to_int32(e, v[1]);
    int param = qvm_to_int32(e, v[2]);
    glTexParameteri(target, pname, param);
    return qvm_undefined;
}

static qvm_value_t wrap_glglPixelStorei(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    int pname = qvm_to_int32(e, v[0]);
    int param = qvm_to_int32(e, v[1]);
    glPixelStorei(pname, param);
    return qvm_undefined;
}

static qvm_value_t wrap_glglMaterialfv(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 3 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_invoke(v[2])) return qvm_undefined;
    int mode = qvm_to_int32(e, v[0]);
    int type = qvm_to_int32(e, v[1]);
    void* vv = qvm_to_invoke(e, v[2]);
    glMaterialfv(mode, type, vv);
    return qvm_undefined;
}

static qvm_value_t wrap_glglMaterialf(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 3 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    int mode = qvm_to_int32(e, v[0]);
    int type = qvm_to_int32(e, v[1]);
    float vv = qvm_to_number(e, v[2]);
    glMaterialf(mode, type, vv);
    return qvm_undefined;
}

static qvm_value_t wrap_glglColorMaterial(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    int mode = qvm_to_int32(e, v[0]);
    int type = qvm_to_int32(e, v[1]);
    glColorMaterial(mode, type);
    return qvm_undefined;
}

static qvm_value_t wrap_glglLightfv(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 3 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_invoke(v[2])) return qvm_undefined;
    int light = qvm_to_int32(e, v[0]);
    int type = qvm_to_int32(e, v[1]);
    void* vv = qvm_to_invoke(e, v[2]);
    glLightfv(light, type, vv);
    return qvm_undefined;
}

static qvm_value_t wrap_glglLightf(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 3 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    int light = qvm_to_int32(e, v[0]);
    int type = qvm_to_int32(e, v[1]);
    float vv = qvm_to_number(e, v[2]);
    glLightf(light, type, vv);
    return qvm_undefined;
}

static qvm_value_t wrap_glglLightModeli(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    int pname = qvm_to_int32(e, v[0]);
    int param = qvm_to_int32(e, v[1]);
    glLightModeli(pname, param);
    return qvm_undefined;
}

static qvm_value_t wrap_glglLightModelfv(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_invoke(v[1])) return qvm_undefined;
    int pname = qvm_to_int32(e, v[0]);
    void* param = qvm_to_invoke(e, v[1]);
    glLightModelfv(pname, param);
    return qvm_undefined;
}

static qvm_value_t wrap_glglFlush(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 0 ) return qvm_undefined;
    glFlush();
    return qvm_undefined;
}

static qvm_value_t wrap_glglHint(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    int target = qvm_to_int32(e, v[0]);
    int mode = qvm_to_int32(e, v[1]);
    glHint(target, mode);
    return qvm_undefined;
}

static qvm_value_t wrap_glglGetIntegerv(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_invoke(v[1])) return qvm_undefined;
    int pname = qvm_to_int32(e, v[0]);
    void* params = qvm_to_invoke(e, v[1]);
    glGetIntegerv(pname, params);
    return qvm_undefined;
}

static qvm_value_t wrap_glglGetFloatv(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_invoke(v[1])) return qvm_undefined;
    int pname = qvm_to_int32(e, v[0]);
    void* vv = qvm_to_invoke(e, v[1]);
    glGetFloatv(pname, vv);
    return qvm_undefined;
}

static qvm_value_t wrap_glglFrontFace(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int mode = qvm_to_int32(e, v[0]);
    glFrontFace(mode);
    return qvm_undefined;
}

static qvm_value_t wrap_glglEnableClientState(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int array = qvm_to_int32(e, v[0]);
    glEnableClientState(array);
    return qvm_undefined;
}

static qvm_value_t wrap_glglDisableClientState(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int array = qvm_to_int32(e, v[0]);
    glDisableClientState(array);
    return qvm_undefined;
}

static qvm_value_t wrap_glglArrayElement(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int i = qvm_to_int32(e, v[0]);
    glArrayElement(i);
    return qvm_undefined;
}

static qvm_value_t wrap_glglVertexPointer(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 4 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    if(!qvm_is_invoke(v[3])) return qvm_undefined;
    int size = qvm_to_int32(e, v[0]);
    int type = qvm_to_int32(e, v[1]);
    int stride = qvm_to_int32(e, v[2]);
    void* pointer = qvm_to_invoke(e, v[3]);
    glVertexPointer(size, type, stride, pointer);
    return qvm_undefined;
}

static qvm_value_t wrap_glglColorPointer(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 4 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    if(!qvm_is_invoke(v[3])) return qvm_undefined;
    int size = qvm_to_int32(e, v[0]);
    int type = qvm_to_int32(e, v[1]);
    int stride = qvm_to_int32(e, v[2]);
    void* pointer = qvm_to_invoke(e, v[3]);
    glColorPointer(size, type, stride, pointer);
    return qvm_undefined;
}

static qvm_value_t wrap_glglNormalPointer(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 3 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_invoke(v[2])) return qvm_undefined;
    int type = qvm_to_int32(e, v[0]);
    int stride = qvm_to_int32(e, v[1]);
    void* pointer = qvm_to_invoke(e, v[2]);
    glNormalPointer(type, stride, pointer);
    return qvm_undefined;
}

static qvm_value_t wrap_glglTexCoordPointer(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 4 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    if(!qvm_is_number(v[2])) return qvm_undefined;
    if(!qvm_is_invoke(v[3])) return qvm_undefined;
    int size = qvm_to_int32(e, v[0]);
    int type = qvm_to_int32(e, v[1]);
    int stride = qvm_to_int32(e, v[2]);
    void* pointer = qvm_to_invoke(e, v[3]);
    glTexCoordPointer(size, type, stride, pointer);
    return qvm_undefined;
}

static qvm_value_t wrap_glglPolygonOffset(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 2 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    if(!qvm_is_number(v[1])) return qvm_undefined;
    float factor = qvm_to_number(e, v[0]);
    float units = qvm_to_number(e, v[1]);
    glPolygonOffset(factor, units);
    return qvm_undefined;
}

static qvm_value_t wrap_glglDebug(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_number(v[0])) return qvm_undefined;
    int mode = qvm_to_int32(e, v[0]);
    glDebug(mode);
    return qvm_undefined;
}

static qvm_value_t wrap_glglInit(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 1 ) return qvm_undefined;
    if(!qvm_is_invoke(v[0])) return qvm_undefined;
    void* zbuffer = qvm_to_invoke(e, v[0]);
    glInit(zbuffer);
    return qvm_undefined;
}

static qvm_value_t wrap_glglClose(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    if( argc != 0 ) return qvm_undefined;
    glClose();
    return qvm_undefined;
}

static int _3d_graphics_init(qvm_state_t *e){
#if WE_USE_OPENGL
    qvm_value_t builtin = qvm_new_object(e);
    qvm_add_property(e, builtin, "tkSwapBuffers", qvm_mk_native(wrap_gltkSwapBuffers));
    qvm_add_property(e, builtin, "glEnable", qvm_mk_native(wrap_glglEnable));
    qvm_add_property(e, builtin, "glDisable", qvm_mk_native(wrap_glglDisable));
    qvm_add_property(e, builtin, "glShadeModel", qvm_mk_native(wrap_glglShadeModel));
    qvm_add_property(e, builtin, "glCullFace", qvm_mk_native(wrap_glglCullFace));
    qvm_add_property(e, builtin, "glPolygonMode", qvm_mk_native(wrap_glglPolygonMode));
    qvm_add_property(e, builtin, "glBegin", qvm_mk_native(wrap_glglBegin));
    qvm_add_property(e, builtin, "glEnd", qvm_mk_native(wrap_glglEnd));
    qvm_add_property(e, builtin, "glVertex2f", qvm_mk_native(wrap_glglVertex2f));
    qvm_add_property(e, builtin, "glVertex3f", qvm_mk_native(wrap_glglVertex3f));
    qvm_add_property(e, builtin, "glVertex3fv", qvm_mk_native(wrap_glglVertex3fv));
    qvm_add_property(e, builtin, "glVertex4f", qvm_mk_native(wrap_glglVertex4f));
    qvm_add_property(e, builtin, "glColor3f", qvm_mk_native(wrap_glglColor3f));
    qvm_add_property(e, builtin, "glColor3fv", qvm_mk_native(wrap_glglColor3fv));
    qvm_add_property(e, builtin, "glNormal3f", qvm_mk_native(wrap_glglNormal3f));
    qvm_add_property(e, builtin, "glNormal3fv", qvm_mk_native(wrap_glglNormal3fv));
    qvm_add_property(e, builtin, "glTexCoord2f", qvm_mk_native(wrap_glglTexCoord2f));
    qvm_add_property(e, builtin, "glTexCoord2fv", qvm_mk_native(wrap_glglTexCoord2fv));
    qvm_add_property(e, builtin, "glTexCoord4f", qvm_mk_native(wrap_glglTexCoord4f));
    qvm_add_property(e, builtin, "glEdgeFlag", qvm_mk_native(wrap_glglEdgeFlag));
    qvm_add_property(e, builtin, "glMatrixMode", qvm_mk_native(wrap_glglMatrixMode));
    qvm_add_property(e, builtin, "glLoadMatrixf", qvm_mk_native(wrap_glglLoadMatrixf));
    qvm_add_property(e, builtin, "glLoadIdentity", qvm_mk_native(wrap_glglLoadIdentity));
    qvm_add_property(e, builtin, "glMultMatrixf", qvm_mk_native(wrap_glglMultMatrixf));
    qvm_add_property(e, builtin, "glPushMatrix", qvm_mk_native(wrap_glglPushMatrix));
    qvm_add_property(e, builtin, "glPopMatrix", qvm_mk_native(wrap_glglPopMatrix));
    qvm_add_property(e, builtin, "glRotatef", qvm_mk_native(wrap_glglRotatef));
    qvm_add_property(e, builtin, "glTranslatef", qvm_mk_native(wrap_glglTranslatef));
    qvm_add_property(e, builtin, "glScalef", qvm_mk_native(wrap_glglScalef));
    qvm_add_property(e, builtin, "glViewport", qvm_mk_native(wrap_glglViewport));
    qvm_add_property(e, builtin, "glFrustum", qvm_mk_native(wrap_glglFrustum));
    qvm_add_property(e, builtin, "glGenLists", qvm_mk_native(wrap_glglGenLists));
    qvm_add_property(e, builtin, "glIsList", qvm_mk_native(wrap_glglIsList));
    qvm_add_property(e, builtin, "glNewList", qvm_mk_native(wrap_glglNewList));
    qvm_add_property(e, builtin, "glEndList", qvm_mk_native(wrap_glglEndList));
    qvm_add_property(e, builtin, "glCallList", qvm_mk_native(wrap_glglCallList));
    qvm_add_property(e, builtin, "glClear", qvm_mk_native(wrap_glglClear));
    qvm_add_property(e, builtin, "glClearColor", qvm_mk_native(wrap_glglClearColor));
    qvm_add_property(e, builtin, "glClearDepth", qvm_mk_native(wrap_glglClearDepth));
    qvm_add_property(e, builtin, "glRenderMode", qvm_mk_native(wrap_glglRenderMode));
    qvm_add_property(e, builtin, "glSelectBuffer", qvm_mk_native(wrap_glglSelectBuffer));
    qvm_add_property(e, builtin, "glInitNames", qvm_mk_native(wrap_glglInitNames));
    qvm_add_property(e, builtin, "glPushName", qvm_mk_native(wrap_glglPushName));
    qvm_add_property(e, builtin, "glPopName", qvm_mk_native(wrap_glglPopName));
    qvm_add_property(e, builtin, "glLoadName", qvm_mk_native(wrap_glglLoadName));
    qvm_add_property(e, builtin, "glGenTextures", qvm_mk_native(wrap_glglGenTextures));
    qvm_add_property(e, builtin, "glDeleteTextures", qvm_mk_native(wrap_glglDeleteTextures));
    qvm_add_property(e, builtin, "glBindTexture", qvm_mk_native(wrap_glglBindTexture));
    qvm_add_property(e, builtin, "glTexImage2D", qvm_mk_native(wrap_glglTexImage2D));
    qvm_add_property(e, builtin, "glTexEnvi", qvm_mk_native(wrap_glglTexEnvi));
    qvm_add_property(e, builtin, "glTexParameteri", qvm_mk_native(wrap_glglTexParameteri));
    qvm_add_property(e, builtin, "glPixelStorei", qvm_mk_native(wrap_glglPixelStorei));
    qvm_add_property(e, builtin, "glMaterialfv", qvm_mk_native(wrap_glglMaterialfv));
    qvm_add_property(e, builtin, "glMaterialf", qvm_mk_native(wrap_glglMaterialf));
    qvm_add_property(e, builtin, "glColorMaterial", qvm_mk_native(wrap_glglColorMaterial));
    qvm_add_property(e, builtin, "glLightfv", qvm_mk_native(wrap_glglLightfv));
    qvm_add_property(e, builtin, "glLightf", qvm_mk_native(wrap_glglLightf));
    qvm_add_property(e, builtin, "glLightModeli", qvm_mk_native(wrap_glglLightModeli));
    qvm_add_property(e, builtin, "glLightModelfv", qvm_mk_native(wrap_glglLightModelfv));
    qvm_add_property(e, builtin, "glFlush", qvm_mk_native(wrap_glglFlush));
    qvm_add_property(e, builtin, "glHint", qvm_mk_native(wrap_glglHint));
    qvm_add_property(e, builtin, "glGetIntegerv", qvm_mk_native(wrap_glglGetIntegerv));
    qvm_add_property(e, builtin, "glGetFloatv", qvm_mk_native(wrap_glglGetFloatv));
    qvm_add_property(e, builtin, "glFrontFace", qvm_mk_native(wrap_glglFrontFace));
    qvm_add_property(e, builtin, "glEnableClientState", qvm_mk_native(wrap_glglEnableClientState));
    qvm_add_property(e, builtin, "glDisableClientState", qvm_mk_native(wrap_glglDisableClientState));
    qvm_add_property(e, builtin, "glArrayElement", qvm_mk_native(wrap_glglArrayElement));
    qvm_add_property(e, builtin, "glVertexPointer", qvm_mk_native(wrap_glglVertexPointer));
    qvm_add_property(e, builtin, "glColorPointer", qvm_mk_native(wrap_glglColorPointer));
    qvm_add_property(e, builtin, "glNormalPointer", qvm_mk_native(wrap_glglNormalPointer));
    qvm_add_property(e, builtin, "glTexCoordPointer", qvm_mk_native(wrap_glglTexCoordPointer));
    qvm_add_property(e, builtin, "glPolygonOffset", qvm_mk_native(wrap_glglPolygonOffset));
    qvm_add_property(e, builtin, "glDebug", qvm_mk_native(wrap_glglDebug));
    qvm_add_property(e, builtin, "glInit", qvm_mk_native(wrap_glglInit));
    qvm_add_property(e, builtin, "glClose", qvm_mk_native(wrap_glglClose));
#endif
    qvm_add_module(e, "@system.g3d", builtin);
    return 1;
}

static void get_stroke_style(qvm_state_t *e, qvm_value_t p) {
    float r, g, b, a;
    qvm_value_t vStyle = qvm_get_property(e, p, "strokeStyle");
    int32_t rgba = qvm_to_int32(e, vStyle);
    a = (rgba >> 24) & 0xFF;
    r = (rgba >> 16) & 0xFF;
    g = (rgba >> 8) & 0xFF;
    b = rgba & 0xFF;
    r = r / 256;
    g = g / 256;
    b = b / 256;
    a = a / 256;
    glColor3f(r, g, b);
}

static void get_fill_style(qvm_state_t *e, qvm_value_t p) {
    float r, g, b, a;
    qvm_value_t vStyle = qvm_get_property(e, p, "fillStyle");
    int32_t rgba = qvm_to_int32(e, vStyle);
    a = (rgba >> 24) & 0xFF;
    r = (rgba >> 16) & 0xFF;
    g = (rgba >> 8) & 0xFF;
    b = rgba & 0xFF;
    r = r / 256;
    g = g / 256;
    b = b / 256;
    a = a / 256;
    glColor3f(r, g, b);
}

static double origin_x(we_html_canvas_t *canvas, int x) {
    double res = x + canvas->tx;
    double half = canvas->w/2;
    return (res - half) / half;
}

static double origin_y(we_html_canvas_t *canvas, int y) {
    double res = y + canvas->ty;
    double half = canvas->h/2;
    return (half - res) / half;
}

static double translate_x(we_html_canvas_t *canvas, int x) {
    double res = x;
    double half = canvas->w/2;
    return (res) / half;
}

static double translate_y(we_html_canvas_t *canvas, int y) {
    double res = y;
    double half = canvas->h/2;
    return (res) / half;
}

static void refresh(we_html_canvas_t *canvas, int x, int y){
    glLoadIdentity();
    glTranslatef(origin_x(canvas, x), origin_y(canvas, y), 0);
    glRotatef(canvas->angle, 0, 0, -1);
}

static qvm_value_t wrap_2d_stroke(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    QVM_UNUSED(e); QVM_UNUSED(argc); QVM_UNUSED(v);
    we_html_canvas_t *canvas = (we_html_canvas_t *)we_get_user_data(e,p);
    if( canvas ) {
        tkSwapBuffers();
        canvas->stroke(canvas);
    }
    return qvm_undefined;
}
//rect(x, y, w, h)
static qvm_value_t wrap_2d_rect(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v) {
    we_html_canvas_t *canvas = (we_html_canvas_t *)we_get_user_data(e,p);
    if( !canvas )
        return qvm_undefined;
    int x = qvm_to_int32(e, v[0]);
    int y = qvm_to_int32(e, v[1]);
    int w = qvm_to_int32(e, v[2]);
    int h = qvm_to_int32(e, v[3]);
    get_stroke_style(e, p);
    glBegin(GL_LINE_LOOP);
    glVertex3f(translate_x(canvas, x), translate_y(canvas, y), 0);
    glVertex3f(translate_x(canvas, x + w), translate_y(canvas, y), 0);
    glVertex3f(translate_x(canvas, x + w), translate_y(canvas, y - h), 0);
    glVertex3f(translate_x(canvas, x), translate_y(canvas, y - h), 0);

    glEnd();
    return qvm_undefined;
}

static qvm_value_t wrap_2d_fillRect(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v){
    we_html_canvas_t *canvas = (we_html_canvas_t *)we_get_user_data(e,p);
    if( !canvas )
        return qvm_undefined;
    int x = qvm_to_int32(e, v[0]);
    int y = qvm_to_int32(e, v[1]);
    int w = qvm_to_int32(e, v[2]);
    int h = qvm_to_int32(e, v[3]);
    get_fill_style(e, p);
    refresh(canvas, x, y);
    glBegin(GL_POLYGON);
    glVertex3f(translate_x(canvas, 0), translate_y(canvas, 0), 0);
    glVertex3f(translate_x(canvas, w), translate_y(canvas, y), 0);
    glVertex3f(translate_x(canvas, w), translate_y(canvas, -h), 0);
    glVertex3f(translate_x(canvas, 0), translate_y(canvas, -h), 0);
    glEnd();
    canvas->stroke(canvas);
    return qvm_undefined;
}

static qvm_value_t wrap_2d_strokeRect(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v){
    wrap_2d_rect(e, p, argc, v);
    wrap_2d_stroke(e, p, argc, v);
    return qvm_undefined;
}

static qvm_value_t wrap_2d_clearRect(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    return qvm_undefined;
}
//moveTo(x, y)
static qvm_value_t wrap_2d_moveTo(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    we_html_canvas_t *canvas = (we_html_canvas_t *)we_get_user_data(e,p);
    if( !canvas )
        return qvm_undefined;
    int x = qvm_to_int32(e, v[0]);
    int y = qvm_to_int32(e, v[1]);
    canvas->x = x;
    canvas->y = y;
    return qvm_undefined;
}
//lineTo(x, y)
static qvm_value_t wrap_2d_lineTo(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    we_html_canvas_t *canvas = (we_html_canvas_t *)we_get_user_data(e,p);
    if( !canvas )
        return qvm_undefined;
    int x = qvm_to_int32(e, v[0]);
    int y = qvm_to_int32(e, v[1]);
    get_stroke_style(e, p);
    refresh(canvas, canvas->x, canvas->y);
    glBegin(GL_LINES);
    glVertex3f(translate_x(canvas, 0), translate_y(canvas, 0), 0);
    glVertex3f(translate_x(canvas, x - canvas->x), translate_y(canvas, y - canvas->y), 0);
    glEnd();
    return qvm_undefined;
}

static void drawArc(we_html_canvas_t *canvas, double x,double y, double radius, double start_angle,double end_angle, int segments,int fill)
{
    if (fill)
    {
        glBegin(GL_TRIANGLE_FAN);
    }
    else
    {
        glBegin(GL_LINE_STRIP);
    }

    double delta_angle = 3.1415926/ segments;

    for (double i=start_angle;i<=end_angle;i+=delta_angle)
    {
        double vx=x+radius * cos(i);
        double vy=y+radius * sin(i);
        glVertex2f(translate_x(canvas, vx), translate_y(canvas, vy));
    }
    glEnd();
}
//arc(x, y, r, sAngle, eAngle, segments)
static qvm_value_t wrap_2d_arc(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v){
    we_html_canvas_t *canvas = (we_html_canvas_t *)we_get_user_data(e,p);
    if( !canvas )
        return qvm_undefined;
    int x = qvm_to_int32(e, v[0]);
    int y = qvm_to_int32(e, v[1]);
    int r = qvm_to_int32(e, v[2]);
    float sa = qvm_to_number(e, v[3]);
    float ea = qvm_to_number(e, v[4]);
    int segments = 100;
    if( argc > 5 && qvm_is_number(v [5]) ) {
        segments = qvm_to_int32(e, v[5]);
    }
    get_stroke_style(e, p);
    drawArc(canvas, x, y, r, sa, ea, segments, 0);
    return qvm_undefined;
}

//rotate(angle)
static qvm_value_t wrap_2d_rotate(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v){
    we_html_canvas_t *canvas = (we_html_canvas_t *)we_get_user_data(e,p);
    if( !canvas )
        return qvm_undefined;
    double angle = qvm_to_number(e, v[0]);
    angle = angle * 180 / M_PI;
    canvas->angle = angle;
    return qvm_undefined;
}

//translate(x, y)
static qvm_value_t wrap_2d_translate(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v){
    we_html_canvas_t *canvas = (we_html_canvas_t *)we_get_user_data(e,p);
    if( !canvas )
        return qvm_undefined;
    canvas->tx = qvm_to_int32(e, v[0]);
    canvas->ty = qvm_to_int32(e, v[1]);
    return qvm_undefined;
}

static qvm_value_t wrap_canvas_getContext(qvm_state_t *e, qvm_value_t p, int argc,qvm_value_t *v) {
    if( argc > 0 && qvm_is_string(v[0]) ) {
        const char *str = qvm_to_string(e, v[0]);
        if( !strcmp(str, "2d") ) {
            return qvm_get_property(e, p, ".ctx2d");
        }
    }
    return qvm_undefined;
}

we_html_canvas_t *we_canvas_init(qvm_state_t *e, qvm_value_t p, int color_depth, void (*stroke)(we_html_canvas_t *)) {
    qvm_value_t attributes = qvm_get_property(e, p, "attributes");
    if( qvm_is_undefined(attributes) )
        return NULL;

    qvm_value_t wv = qvm_get_property(e, attributes, "width");
    if( qvm_is_undefined(wv) || (!qvm_is_number(wv) && !qvm_is_string(wv) ) )
        return NULL;

    qvm_value_t hv = qvm_get_property(e, attributes, "height");
    if( qvm_is_undefined(hv) || (!qvm_is_number(hv) && !qvm_is_string(hv) ) )
        return NULL;

    qvm_value_t ctx2d = qvm_new_object(e);
    qvm_add_property(e, ctx2d, "stroke", qvm_mk_native(wrap_2d_stroke));
    qvm_add_property(e, ctx2d, "rect", qvm_mk_native(wrap_2d_rect));
    qvm_add_property(e, ctx2d, "fillRect", qvm_mk_native(wrap_2d_fillRect));
    qvm_add_property(e, ctx2d, "strokeRect", qvm_mk_native(wrap_2d_strokeRect));
    qvm_add_property(e, ctx2d, "clearRect", qvm_mk_native(wrap_2d_clearRect));
    qvm_add_property(e, ctx2d, "moveTo", qvm_mk_native(wrap_2d_moveTo));
    qvm_add_property(e, ctx2d, "lineTo", qvm_mk_native(wrap_2d_lineTo));
    qvm_add_property(e, ctx2d, "arc", qvm_mk_native(wrap_2d_arc));
    qvm_add_property(e, ctx2d, "rotate", qvm_mk_native(wrap_2d_rotate));
    qvm_add_property(e, ctx2d, "translate", qvm_mk_native(wrap_2d_translate));
    qvm_add_property(e, ctx2d, "strokeStyle", qvm_mk_number(0));
    qvm_add_property(e, ctx2d, "fillStyle", qvm_mk_number(0));

    we_html_canvas_t *html_canvas = we_malloc(sizeof(we_html_canvas_t));
    if( !html_canvas ){
        qvm_throw(e, qvm_error(e, "Canvas runs out memory"));
        return NULL;
    }
    if( qvm_is_number(wv))
        html_canvas->w = qvm_to_int32(e, wv);
    else {
        html_canvas->w = atoi(qvm_to_string(e, wv));
    }

    if( qvm_is_number(hv))
        html_canvas->h = qvm_to_int32(e, hv);
    else {
        html_canvas->h = atoi(qvm_to_string(e, hv));
    }

    html_canvas->buffer = we_malloc(html_canvas->w * html_canvas->h * color_depth / 8);
    if( !html_canvas->buffer ) {
        we_free(html_canvas);
        return NULL;
    }
    html_canvas->obj = we_html_object_get_obj_data(p);
    html_canvas->stroke = stroke;

    we_set_user_data(e, ctx2d, html_canvas);

    qvm_add_property(e, p, "getContext", qvm_mk_native(wrap_canvas_getContext));
    qvm_add_property(e, p, ".ctx2d", ctx2d);

    we_graphics_3d_init(e, html_canvas->w, html_canvas->h, color_depth, html_canvas->buffer);
    glClearColor(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, html_canvas->w, html_canvas->h);
    glTranslatef(-1, 1, 0);
    return html_canvas;
}


int we_module_graphics(qvm_state_t *e) {
    return _3d_graphics_init(e);
}
#endif
