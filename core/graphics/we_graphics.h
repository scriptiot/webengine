/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_GRAPHICS_H
#define WE_GRAPHICS_H

#include "webengine.h"

void we_graphics_3d_init(qvm_state_t *e, int width, int height, int bits_per_pixels, void *framebuffer);
void we_graphics_3d_loop(qvm_state_t *e, qvm_value_t *callback);

int we_module_graphics(qvm_state_t *e);

#endif
