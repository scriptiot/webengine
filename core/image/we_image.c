#include "image/we_image.h"
#include "base64/we_base64.h"
#include "image/lodepng.h"
#include "fileapi/we_file.h"
#include "image/lodepng.h"

#define DEFAULT_IMAGE_CACHE_SIZE    16
static qvm_value_t image_cache;

//append(path, alpha)
static qvm_value_t _we_image_append(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    if( argc < 1 || !qvm_is_string(v[0]) )
        return qvm_undefined;

    int alpha = 1;
    if( argc > 1 ){
        alpha = qvm_to_int32(e, v[1]);
    }

    const char *path = qvm_to_string(e, v[0]);
    if( !we_file_exist(path) ) {
        return qvm_undefined;
    }
    int size = we_file_size(path);
    if( size == 0 ){
        return qvm_undefined;
    }

#ifdef WE_USE_LODEPNG
    if( strstr(path, ".png") ) {
        we_image_info_t *info = we_malloc(sizeof (we_image_info_t));
        if( !info ){
            qvm_throw(e, qvm_error(e, "Engine is running ouf of memory"));
            return qvm_undefined;
        }
        info->type = WE_IMAGE_TYPE_PNG;

        uint8_t *png_data;
        uint32_t error;
        signed char * png_decoded;
        uint32_t png_width;
        uint32_t png_height;

        png_data = we_malloc(size);
        if( !png_data ){
            we_free(info);
            qvm_throw(e, qvm_error(e, "Engine is running ouf of memory"));
            return qvm_undefined;
        }
        WE_FHANDLE fd =we_file_open(path, "rb");
        if( fd == NULL ) {
            we_free(info);
            we_free(png_data);
            return qvm_undefined;
        }
        size_t br = we_file_read(fd, png_data, size);

        error = lodepng_decode32(&png_decoded, &png_width, &png_height, png_data, size);
        if( error ) {
            char err_data[128];
            sprintf(err_data, "error %u: %s\n", error, lodepng_error_text(error));
            qvm_throw(e, qvm_error(e, err_data));
            we_free(info);
            we_free(png_data);
            we_file_close(fd);
            return qvm_undefined;
        }

        we_file_close(fd);

        info->raw_data = png_data;
        info->decoded_data = png_decoded;

        if( info->is_converted == 0){
            info->is_converted = 1;
            we_convert_color_depth(info->decoded_data, png_width * png_height, alpha);
        }
        qvm_value_t obj = qvm_new_object(e);
        qvm_add_property(e, obj, "type", qvm_mk_string("png"));
        qvm_add_property(e, obj, "width", qvm_mk_number(png_width));
        qvm_add_property(e, obj, "height", qvm_mk_number(png_height));
        qvm_add_property(e, obj, ".data", qvm_mk_invoke(info));
        qvm_add_property(e, image_cache, qvm_to_string(e, v[0]), obj);
        return obj;
    }
#endif
    return qvm_undefined;
}

//remove(path)
static qvm_value_t _we_image_remove(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    if( argc < 1 || !qvm_is_string(v[0]) )
        return qvm_undefined;
    qvm_value_t obj = qvm_get_property(e, image_cache, qvm_to_string(e, v[0]));
    if( qvm_is_object(obj) ){
        qvm_value_t data = qvm_get_property(e, obj, ".data");
        we_image_info_t *info = NULL;
        if( qvm_is_invoke(data) ) {
            info = (we_image_info_t *)qvm_to_invoke(e, data);
        }
        if( info && info->destroyer ){
            info->destroyer(info->user_data);
        }
        we_free(info->raw_data);
        we_free(info->decoded_data);
        we_free(info);
        qvm_add_property(e, obj, ".data", qvm_mk_undefined());
        qvm_add_property(e, image_cache, qvm_to_string(e, v[0]), qvm_undefined);
    }
    return qvm_undefined;
}

//get(path)
static qvm_value_t _we_image_get(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v) {
    if( argc < 1 || !qvm_is_string(v[0]) )
        return qvm_undefined;
    return qvm_get_property(e, image_cache, qvm_to_string(e, v[0]));
}

qvm_value_t we_image_append(qvm_state_t *e, const char *path) {
    qvm_value_t image_obj = we_image_get(e, path);
    if( qvm_is_object(image_obj) ) {
        return image_obj;
    }
    qvm_value_t args = qvm_mk_string(path);
    return _we_image_append(e, qvm_undefined, 1, &args);
}

qvm_value_t we_image_get(qvm_state_t *e, const char *path) {
    qvm_value_t args = qvm_mk_string(path);
    return _we_image_get(e, qvm_undefined, 1, &args);
}

qvm_value_t we_image_remove(qvm_state_t *e, const char *path) {
    qvm_value_t args = qvm_mk_string(path);
    return _we_image_remove(e, qvm_undefined, 1, &args);
}

void we_image_clear_cache(qvm_state_t *e) {
    qvm_value_t next = qvm_first_property(e, image_cache);
    while(qvm_is_valid(next)) {
        const char *name = qvm_get_propertyname(e, next);
        we_image_remove(e, name);
    }
}

int we_image_module(qvm_state_t *e) {
    image_cache = qvm_new_object(e);
    qvm_push(e, image_cache);
    qvm_add_property(e, image_cache, "append", qvm_mk_native(_we_image_append ));
    qvm_add_property(e, image_cache, "remove", qvm_mk_native(_we_image_remove ));
    qvm_add_property(e, image_cache, "get", qvm_mk_native(_we_image_get ));
    qvm_add_module(e, "@system.image", image_cache);
    qvm_pop(e);
    return 1;
}
