/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_IMAGE_H
#define WE_IMAGE_H

#include "webengine.h"

#define WE_IMAGE_TYPE_PNG   0
#define WE_IMAGE_TYPE_JPG   1

typedef struct we_image_info_t {
    int type;
    int is_converted;
    void *raw_data;
    void *decoded_data;
    void (*destroyer)(void *user_data);
    void *user_data;
} we_image_info_t;

qvm_value_t we_image_append(qvm_state_t *e, const char *path);
qvm_value_t we_image_get(qvm_state_t *e, const char *path);
qvm_value_t we_image_remove(qvm_state_t *e, const char *path);
void we_image_clear_cache(qvm_state_t *e);
int we_image_module(qvm_state_t *e);

extern void we_convert_color_depth(uint8_t *img, uint32_t px_cnt, int alpha);

#endif

