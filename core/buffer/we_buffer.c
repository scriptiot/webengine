#include "we_buffer.h"

static qvm_value_t we_buffer_class_instantiate(qvm_state_t *e, uint32_t size);

static int HexToOct(int h) {
    int re = 0;
    int k = 16;
    int n = 1;
    while (h != 0)
    {
        re += (h % 10) * n;
        h /= 10;
        n *= k;
    }
    return re;
}

static void ByteToHexStr(const unsigned char* source, char* dest, int sourceLen) {
    short i;
    unsigned char highByte, lowByte;

    for (i = 0; i < sourceLen; i++) {
        highByte = source[i] >> 4;
        lowByte = source[i] & 0x0f;
        highByte += 0x30;

        if (highByte > 0x39) dest[i * 2] = highByte + 0x07;
        else dest[i * 2] = highByte;

        lowByte += 0x30;

        if (lowByte > 0x39) dest[i * 2 + 1] = lowByte + 0x07;
        else dest[i * 2 + 1] = lowByte;
    }

    return ;
}

//new Buffer(array)
//new Buffer(buffer)
//new Buffer(size)
//new Buffer(str[, encoding])
static qvm_value_t we_buffer_class_new(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(p);
    qvm_value_t buf_obj = qvm_undefined;
    uint32_t length = 0;

    if (qvm_is_integer(v[0])) {
        length = qvm_to_int32(e, v[0]);
        buf_obj = we_buffer_class_instantiate(e, length);
    } else if (qvm_is_array(v[0])) {
        length = qvm_get_array_length(e, v[0]);
        buf_obj = we_buffer_class_instantiate(e, length);
        qvm_value_t item;
        uint8_t *buffer = qvm_to_buffer(e, buf_obj);
        for (uint32_t i = 0; i < length; i++) {
            item = qvm_get_array(e, v[0], i);
            if (qvm_is_number(item)) {
                buffer[i] = (uint8_t)qvm_to_int32(e, item);
            }
        }
    } else if (qvm_is_buffer(v[0])) {
        length = qvm_get_bufferlen(e, v[0]);
        buf_obj = we_buffer_class_instantiate(e, length);
        uint8_t *target = qvm_to_buffer(e, v[0]);
        memcpy(qvm_to_buffer(e, buf_obj), target, length);
    } else if (qvm_is_string(v[0])) {
        if (argc > 1 && qvm_is_string(v[1]) && !strcmp(qvm_to_string(e, v[1]), "hex")) {
            length = 0;
            uint8_t *p = qvm_to_buffer(e, v[0]);
            char t[2] = {0};
            while (*p != '\0' && *p) {
                strncpy(t, (const char *)p, 2);
                if (atoi(t))
                    length++;
                p += 2;
            }

            buf_obj = we_buffer_class_instantiate(e, length);
            uint8_t *buffer = qvm_to_buffer(e, buf_obj);

            char n;
            p = (uint8_t *)qvm_to_string(e, v[0]);
            uint32_t cnt = 0;
            while (*p != '\0' && *p) {
                strncpy(t, (const char *)p, 2);
                n = (char)HexToOct(atoi(t));
                buffer[cnt] = n;
                p += 2;
                cnt++;
            }
        } else {
            length = qvm_string_length(e, v[0]);
            buf_obj = we_buffer_class_instantiate(e, length);
            memcpy(qvm_to_buffer(e, buf_obj), qvm_to_string(e, v[0]), length); // 这样写是否正确？
        }
    }

    return buf_obj;
}

//Buffer.byteLength(str, encoding)
//encoding: hex | bytes(default)
static qvm_value_t we_buffer_byteLength(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(p);

    if (argc < 1) {
        return qvm_undefined;
    }

    if (qvm_is_string(v[0]) == 1) {
        return qvm_mk_number(qvm_string_length(e, v[0]));
    }

    if (qvm_is_buffer(v[0]) == 1) {
        return qvm_mk_number(qvm_get_bufferlen(e, v[0]));
    }

    return qvm_undefined;
}

//Buffer.concat(list)
static qvm_value_t we_buffer_concat(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(p);
    if (argc < 1 || qvm_is_array(v[0]) == 0) {
        return qvm_undefined;
    }

    uint32_t len = qvm_get_array_length(e, v[0]);
    uint32_t count = 0;
    uint32_t total_length = 0;

    qvm_value_t item;
    for (uint32_t i = 0; i < len; i++) {
        item = qvm_get_array(e, v[0], i);
        if (qvm_is_buffer(item)) {
            total_length += qvm_get_bufferlen(e, item);
            count++;
        }
    }

    qvm_value_t buffer = we_buffer_class_instantiate(e, total_length);
    uint32_t i = 0, length = 0, current_length = 0;
    while (i < count) {
        current_length += length;
        item = qvm_get_array(e, v[0], i);
        length = qvm_get_bufferlen(e, item);
        memcpy(qvm_to_buffer(e, buffer) + current_length, qvm_to_buffer(e, item), length);
        i++;
    }

    return buffer;
}

static qvm_value_t we_buffer_alloc(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(p);
    if (argc >= 1 && qvm_is_integer(v[0])) {
        int size = qvm_to_int32(e, v[0]);
        return we_buffer_class_instantiate(e, size);
    }

    return qvm_undefined;
}

//Buffer.from(array)
//Buffer.from(buffer)
//Buffer.from(string[,encoding])
//Buffer.from(arrayBuffer[, byteOffset[, length]])
static qvm_value_t we_buffer_from(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    return we_buffer_class_new(e, p, argc, v);
}

//Buffer.isBuffer(buffer)
static qvm_value_t we_buffer_isBuffer(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);
    QVM_UNUSED(p);
    if (argc < 1 || qvm_is_buffer(v[0]) == 0) {
        return QVM_FALSE;
    }

    if (qvm_is_buffer(v[0]))
        return QVM_TRUE;

    return QVM_FALSE;
}

//buf.compare(otherBuffer)
static qvm_value_t we_buffer_class_compare(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);
    if (argc < 1 || qvm_is_buffer(v[0]) == 0) {
        return qvm_undefined;
    }

    uint32_t p_len = qvm_get_bufferlen(e, p);
    uint32_t v_len = qvm_get_bufferlen(e, v[0]);
    uint8_t *p_addr = qvm_to_buffer(e, p);
    uint8_t *v_addr = qvm_to_buffer(e, v[0]);

    /* return -1 if first lower than second
     * return 1 if second lower than first
     * return 0 if first equal second */
    if (p_len == v_len) {
        for(uint32_t i = 0; i < p_len; i++) {
            if (p_addr[i] < v_addr[i]) return qvm_mk_number(-1);
            else if (p_addr[i] > v_addr[i]) return qvm_mk_number(1);
        }
        return qvm_mk_number(0);
    } else if (p_len < v_len) { // length different
        return qvm_mk_number(-1); // return 1 if first less than second
    } else { // p_len > v_len
        return qvm_mk_number(1); // return -1 if first rather than second
    }

    return qvm_undefined;
}

//buf.copy(targetBuffer[, targetStart[, sourceStart[, sourceEnd]]])
static qvm_value_t evm_module_buffer_class_copy(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    QVM_UNUSED(e);
    if (argc < 1 || qvm_is_buffer(v[0]) == 0) {
        return qvm_undefined;
    }

    uint8_t *source = qvm_to_buffer(e, p);
    uint8_t *target = qvm_to_buffer(e, v[0]);
    uint32_t target_length = qvm_get_bufferlen(e, v[0]);
    uint32_t source_start = 0;
    uint32_t source_end = qvm_get_bufferlen(e, p);

    uint32_t target_start = 0;
    if (argc >= 1 && qvm_is_integer(v[1]))
        target_start = qvm_to_int32(e, v[1]);
    if (argc >= 2 && qvm_is_integer(v[2]))
        source_start = qvm_to_int32(e, v[2]);
    if (argc >= 3 && qvm_is_integer(v[3]))
        source_end = qvm_to_int32(e, v[3]);

    if (source_start > source_end && target_length < (source_end - source_start)) {
        return qvm_undefined;
    }

    memcpy(target + target_start, source, source_end - source_start);
    return qvm_mk_number(source_end - source_start);
}

//buf.equals(otherBuffer)
static qvm_value_t we_buffer_class_equals(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    if (argc < 1 || qvm_is_buffer(v[0]) == 0) {
        return qvm_undefined;
    }

    qvm_value_t result = we_buffer_class_compare(e, p, argc, v);
    return qvm_to_int32(e, result) == 0 ? QVM_TRUE : QVM_FALSE;
}

//buf.fill(value)
static qvm_value_t we_buffer_class_fill(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    if (argc < 1 || qvm_is_integer(v[0]) == 0) {
        return qvm_undefined;
    }

    uint32_t value = qvm_to_int32(e, v[0]);
    uint8_t *buffer = qvm_to_buffer(e, p);

    uint32_t length = qvm_get_bufferlen(e, p);
    for(uint32_t i = 0; i < length; i++) {
        buffer[i] = value;
    }
    return p;
}

//buf.slice([start[, end]])
static qvm_value_t we_buffer_class_slice(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    // 根据开始下标和结束下标获取某一段buffer
    uint32_t start = 0;
    uint32_t end = qvm_get_bufferlen(e, p);
    if (argc >= 1 && qvm_is_integer(v[0])) {
        start = qvm_to_int32(e, v[0]);
    }
    if (argc >= 2 && qvm_is_integer(v[1])) {
        end = qvm_to_int32(e, v[1]);
    }
    if (start > end) {
        return qvm_undefined;
    }

    uint32_t length = end - start;
    qvm_value_t buf = we_buffer_class_instantiate(e, length);
    uint8_t *target = qvm_to_buffer(e, buf);
    uint8_t *source = qvm_to_buffer(e, p);

    uint32_t cnt = 0;
    for(uint32_t i = start; i < end; i++) {
        target[cnt] = *(source + i);
        cnt++;
    }

    return buf;
}

//buf.toString([encoding[, start[, end]]])
static qvm_value_t evm_module_buffer_class_toString(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    uint32_t start = 0;
    uint32_t end = qvm_get_bufferlen(e, p);

    const char* encoding = "utf8";
    if (argc >= 1 && qvm_is_string(v[0]))
        encoding = qvm_to_string(e, v[0]);

    if (argc >= 2 && qvm_is_integer(v[1]))
        start = qvm_to_int32(e, v[1]);

    if (argc >= 3 && qvm_is_integer(v[2]))
        end = qvm_to_int32(e, v[2]);

    if (start > end) {
        return qvm_undefined;
    }

    uint32_t len = end - start;
    uint8_t* addr = qvm_to_buffer(e, p);
    qvm_value_t result;

    if (strcmp(encoding, "hex") == 0) {
        len = len * 2;
        char buf[len + 1];
        memset(buf, 0, len);
        ByteToHexStr(addr, buf, len / 2);
        buf[len] = 0;
        result = qvm_new_lstring(e, buf + start, len + 1);
    } else {              
        result = qvm_new_string(e, (const char *)(addr + start));
    }
    return result;
}

//buf.write(string[, offset[, length]])
static qvm_value_t evm_module_buffer_class_write(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    if (argc < 1 || qvm_is_string(v[0]) == 0) {
        return qvm_undefined;
    }

    uint32_t offset = 0;
    uint32_t length = qvm_string_length(e, v[0]);

    if (argc > 1 && qvm_is_integer(v[1]))
        offset = qvm_to_int32(e, v[1]);

    if (argc > 2 && qvm_is_integer(v[2]))
        length = qvm_to_int32(e, v[2]);

    const char* source = qvm_to_string(e, v[0]);
    uint8_t* target = qvm_to_buffer(e, p);
    memcpy(target + offset, source, length);
    return qvm_mk_number(length);
}

//buf.writeUInt8(value, offset[, noAssert])
static qvm_value_t evm_module_buffer_class_writeUInt8(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    if (argc < 1 || qvm_is_integer(v[0]) == 0) {
        return qvm_undefined;
    }

    int offset = 0;
    if (argc > 1 && qvm_is_integer(v[1])) {
        offset = qvm_to_int32(e, v[1]);
    }

    if (offset > qvm_get_bufferlen(e, p)) {
        we_log("buffer subscript index overflow\n");
        return qvm_undefined;
    }

    uint8_t *buf = qvm_to_buffer(e, p);
    buf[offset] = qvm_to_int32(e, v[0]);
    return qvm_mk_number(offset + 1);
}

//buf.readUInt8(offset[, noAssert])
static qvm_value_t we_buffer_class_readUInt8(qvm_state_t *e, qvm_value_t p, int argc, qvm_value_t *v)
{
    uint32_t offset = 0;
    if (argc > 0 && qvm_is_integer(v[0])) {
        offset = qvm_to_int32(e, v[0]);
    }

    uint8_t *buf = qvm_to_buffer(e, p);
    uint8_t value = (uint8_t)buf[offset];

    return qvm_mk_number(value);
}

static qvm_value_t we_buffer_class_instantiate(qvm_state_t *e, uint32_t size)
{
    uint8_t* buf = we_malloc(size);
    qvm_value_t obj = qvm_new_buffer(e, buf, size); // 这里有待商榷，应该是创建一个object才对，然后buffer挂到.data属性下面

    qvm_add_property(e, obj, "length", qvm_mk_number(size));
    qvm_add_property(e, obj, "compare", qvm_mk_native(we_buffer_class_compare));
    qvm_add_property(e, obj, "copy", qvm_mk_native(evm_module_buffer_class_copy));
    qvm_add_property(e, obj, "equals", qvm_mk_native(we_buffer_class_equals));
    qvm_add_property(e, obj, "fill", qvm_mk_native(we_buffer_class_fill));
    qvm_add_property(e, obj, "slice", qvm_mk_native(we_buffer_class_slice));
    qvm_add_property(e, obj, "toString", qvm_mk_native(evm_module_buffer_class_toString));
    qvm_add_property(e, obj, "write", qvm_mk_native(evm_module_buffer_class_write));
    qvm_add_property(e, obj, "writeUInt8", qvm_mk_native(evm_module_buffer_class_writeUInt8));
    qvm_add_property(e, obj, "readUInt8", qvm_mk_native(we_buffer_class_readUInt8));

    return obj;
}

int we_buffer_module_init(qvm_state_t *e) {
    qvm_value_t obj = qvm_new_object(e);
    qvm_add_property(e, obj, "alloc", qvm_mk_native(we_buffer_alloc));
    qvm_add_property(e, obj, "byteLength", qvm_mk_native(we_buffer_byteLength));
    qvm_add_property(e, obj, "concat", qvm_mk_native(we_buffer_concat));
    qvm_add_property(e, obj, "from", qvm_mk_native(we_buffer_from));
    qvm_add_property(e, obj, "isBuffer", qvm_mk_native(we_buffer_isBuffer));
    qvm_add_module(e, "@system.buffer", obj);
    return 1;
}
