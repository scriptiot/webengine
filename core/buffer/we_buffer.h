/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_BINDING_H
#define WE_BINDING_H

#ifdef __cplusplus
extern "C" {
#endif


#include "webengine.h"

int we_buffer_module_init(qvm_state_t *e);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
