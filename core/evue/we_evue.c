#include "evue/we_evue.h"
#include "fileapi/we_file.h"

static char _temp_path[PATH_LENGTH_MAX];

static enum WEPageErr _evue_parse_token(we_evue_t * p, char *str, int len, uint32_t *index)
{
    char ch;
    int str_index = 0;

    do {
        ch = p->html_file[(*index) + str_index];
        if( ch == 0) {
            return we_page_ok;
        }
        if( ch != str[str_index] )
            return we_page_ok;
        str_index++;
    } while ( str_index < len);
    *index += len;
    return we_page_err;
}

static enum WEPageErr _evue_html_find_label(we_evue_t *sm, char *label, char *end_label) {
    uint32_t index = 0;
    uint32_t flag = 0;
    uint32_t str_flag = 0;
    uint32_t start_index = 0;
    while( index < sm->html_file_len ) {
        if( str_flag == 2 ) {
            if( sm->html_file[index] == '"') {
                str_flag = 0;
            }
            (index)++;
            continue;
        }

        if( str_flag == 1 ) {
            if( sm->html_file[index] == '\'') {
                str_flag = 0;
            }
            (index)++;
            continue;
        }

        if( _evue_parse_token(sm, "\"", 1, &index) ) {
            str_flag = 2;
            continue;
        }

        if( _evue_parse_token(sm, "'", 1, &index) ) {
            str_flag = 1;
            continue;
        }

        if( flag == 0 && _evue_parse_token(sm, label, strlen(label), &index) ) {
            flag = 1;
            start_index = index;
            if( !strcmp(label, "<html>") ) {
                sm->html_content = sm->html_file + index;
            } else if( !strcmp(label, "<style>") ) {
                sm->style_content = sm->html_file + index;
            } else if( !strcmp(label, "<script>") ) {
                sm->script_content = sm->html_file + index;
            }
            continue;
        }

        if( flag == 1 && _evue_parse_token(sm, end_label, strlen(label), &index) ) {
            if( !strcmp(label, "<html>") ) {
                sm->html_content_len = index - start_index - strlen(label);
            } else if( !strcmp(label, "<style>") ) {
                sm->style_content_len = index - start_index - strlen(label);
            } else if( !strcmp(label, "<script>") ) {
                sm->script_content_len = index - start_index - strlen(label);
            }
            return we_page_ok;
        }
        (index)++;
    }
    return we_page_err;
}

static enum WEPageErr _evue_parse_html(we_evue_t *sm) {
    //find <html> label
    if( we_page_err == _evue_html_find_label(sm, "<html>", "</html>") ) {
        return we_page_err;
    }

    _evue_html_find_label(sm, "<style>", "</style>");
    _evue_html_find_label(sm, "<script>", "</script>");
    if( sm->html_content ){
        sm->html_content[sm->html_content_len] = 0;
    }
    if( sm->script_content ){
        sm->script_content[sm->script_content_len] = 0;
    }
    if( sm->style_content ){
        sm->style_content[sm->style_content_len] = 0;
    }
    return we_page_ok;
}

void we_evue_free(we_evue_t *evue) {
    if( evue->html_file ) {
        we_free(evue->html_file);
        evue->html_file = NULL;
    } else {
        if( evue->html_content )
            we_free(evue->html_content);
        if( evue->script_content )
            we_free(evue->script_content);
        if( evue->style_content )
            we_free(evue->style_content);
    }
}

enum WEPageErr we_evue_open(const char *uri, we_evue_t *sm) {
    sprintf(_temp_path, "%s.evue" , uri);
    we_log("evue path : %s \n", _temp_path);
    WE_FHANDLE fp;
    size_t size = (size_t)we_file_size(_temp_path);
    fp = we_file_open(_temp_path, "rb");
    if( fp == NULL ) {
        we_log("open evue path : %s failed", _temp_path);
        return we_page_err;
    }
    sm->html_file = (char *)we_malloc(size + 1);
    sm->html_file_len = size;
    sm->html_file[size] = 0;

    we_file_read(fp, sm->html_file, size);
    we_file_close(fp);

    if( _evue_parse_html(sm) != we_page_ok ) {
        we_free(sm->html_file);
        return we_page_err;
    }

    return we_page_ok;
}
