/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_EVUE_H
#define WE_EVUE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "page/we_page.h"

typedef struct we_evue_t {
    char *html_file;
    int html_file_len;
    char *html_content;
    int html_content_len;
    char *style_content;
    int style_content_len;
    char *script_content;
    int script_content_len;
} we_evue_t;

void we_evue_free(we_evue_t *evue);
enum WEPageErr we_evue_open(const char *uri, we_evue_t *evue);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
