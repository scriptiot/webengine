/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_MD5_H
#define WE_MD5_H

#ifdef __cplusplus
extern "C" {
#endif

#include "webengine.h"
#include <stdint.h>
#include <stdbool.h>

bool we_md5_check(uint8_t *msg, uint32_t msg_length, uint8_t *md5);
bool we_md5_compare(uint8_t *src, uint8_t *des, int len);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
