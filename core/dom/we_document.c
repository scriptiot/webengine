#include "dom/we_document.h"

qvm_value_t we_dom_get_element_by_id(qvm_state_t *e, qvm_value_t *object, const char *id) {
    QVM_UNUSED3(e, object, id);
    return qvm_undefined;
}

qvm_value_t we_dom_get_elements_by_name(qvm_state_t *e, qvm_value_t *object, const char *id) {
    QVM_UNUSED3(e, object, id);
    return qvm_undefined;
}

qvm_value_t we_dom_get_elements_by_tag_name(qvm_state_t *e, qvm_value_t *object, const char *id) {
    QVM_UNUSED3(e, object, id);
    return qvm_undefined;
}

void we_dom_open(qvm_state_t *e, qvm_value_t *object, const char *mimetype, const char *replace) {
    QVM_UNUSED4(e, object, mimetype, replace);
}

void we_dom_close(qvm_state_t *e, qvm_value_t *object) {
    QVM_UNUSED2(e, object);
}

void we_dom_write(qvm_state_t *e, qvm_value_t *object, int argc, qvm_value_t *argv) {
    QVM_UNUSED4(e, object, argc, argv);
}

void we_dom_writeln(qvm_state_t *e, qvm_value_t *object, int argc, qvm_value_t *argv) {
    QVM_UNUSED4(e, object, argc, argv);
}
