#include "page/we_page.h"

#include "router/we_router.h"

static qvm_value_t _page_list;

qvm_value_t we_page_get_current_index_object(qvm_state_t *e) {
    we_router_t *router = we_router_get();
    return we_page_get_index_object(e, router->index);
}

qvm_value_t we_page_get_current_css_object(qvm_state_t *e) {
    we_router_t *router = we_router_get();
    qvm_value_t index_object = we_page_get_index_object(e, router->index);
    return qvm_get_property(e, index_object, "css_object");
}

qvm_value_t we_page_get_current_html_object(qvm_state_t *e) {
    we_router_t *router = we_router_get();
    qvm_value_t index_object = we_page_get_index_object(e, router->index);
    return qvm_get_property(e, index_object, "html_object");
}

qvm_value_t we_page_get_current_top_html_object(qvm_state_t *e) {
    qvm_value_t html_object = we_page_get_current_html_object(e);
    qvm_value_t first_node = qvm_get_property(e, html_object, "childNodes");
    return qvm_get_array(e, first_node, 0);
}

qvm_value_t we_page_get_current_default(qvm_state_t *e) {
    qvm_value_t index_object = we_page_get_current_index_object(e);
    return qvm_get_property(e, index_object, "default");
}

qvm_value_t we_page_get_current_data(qvm_state_t *e) {
    qvm_value_t def = we_page_get_current_default(e);
    return qvm_get_property(e, def, "data");
}

void we_page_insert_index_object(qvm_state_t *e, int index, qvm_value_t v) {
    qvm_set_array(e, _page_list, index, v);
} 

qvm_value_t we_page_get_index_object(qvm_state_t *e, int index) {
    return qvm_get_array(e, _page_list, index);
} 

int we_page_list_init(qvm_state_t *e) {
    _page_list = qvm_new_array(e);
    qvm_push(e, _page_list);
    return 1;
}
