/*
 *  Copyright (c) 2022, 王喆、丁江锋
 *  Version	: 1.0
 *  github	: https://github.com/scriptiot
 *  gitee	: https://gitee.com/scriptiot
 *  License: 个人免费，企业授权
 */
#ifndef WE_PAGE_H
#define WE_PAGE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "webengine.h"

enum WEPageErr
{
    we_page_ok = 0,
    we_page_err,
    we_page_no_file,
};

enum {
    UNDEFINED_STATE = -1,
    INIT_STATE,
    READY_STATE,
    SHOW_STATE,
    BACKGROUND_STATE,
    DESTROY_STATE,
    END_STATE
};

#define PATH_LENGTH_MAX 256

extern const char APP_DIR[];
extern const char PATH_DEFAULT[];
extern const char ROUTER_PAGE[];
extern const char ROUTER_PAGE_URI[];
extern const char ROUTER_PAGE_FROM[];
extern const char ROUTER_PAGE_TO[];
extern const char ROUTER_PAGE_PARAMS[];
extern const char ROUTER_PAGE_PATH[];

extern const char PAGE_LIFECYCLE_ON_INIT[];
extern const char PAGE_LIFECYCLE_CALLBACK_ON_READY[];
extern const char PAGE_LIFECYCLE_CALLBACK_ON_SHOW[];
extern const char PAGE_LIFECYCLE_CALLBACK_ON_HIDE[];
extern const char PAGE_LIFECYCLE_CALLBACK_ON_DESTROY[];

typedef struct we_page_t{
    uint32_t index;
    int32_t state;
    char *uri;
} we_page_t;

void we_page_sm_init(qvm_state_t * e, we_page_t * sm, const char* uri);
enum WEPageErr we_page_sm_set_current_state(we_page_t * sm, int32_t state);
int32_t we_page_sm_get_current_state(we_page_t * sm);
enum WEPageErr we_page_sm_change_state(we_page_t * sm, int new_state);
enum WEPageErr we_page_sm_render_page(we_page_t * sm);
enum WEPageErr we_page_sm_show_page(we_page_t * sm);
enum WEPageErr we_page_sm_hide_page(we_page_t * sm);
enum WEPageErr we_page_sm_invoke_page_life_cycle_callback(we_page_t * sm, const char * name);
enum WEPageErr we_page_sm_release_history_page_resource(we_page_t * sm);
void we_page_sm_delete_view_model_properties(we_page_t * sm);

enum WEPageErr we_page_state_handle_init_state(we_page_t *sm);
enum WEPageErr we_page_state_handle_ready_state(we_page_t * sm);
enum WEPageErr we_page_state_handle_show_state(we_page_t * sm);
enum WEPageErr we_page_state_handle_background_state(we_page_t * sm);
enum WEPageErr we_page_state_handle_destroy_state(we_page_t * sm);

qvm_value_t we_page_get_current_index_object(qvm_state_t *e);
qvm_value_t we_page_get_current_css_object(qvm_state_t *e);
qvm_value_t we_page_get_current_html_object(qvm_state_t *e);
qvm_value_t we_page_get_current_top_html_object(qvm_state_t *e);
qvm_value_t we_page_get_current_default(qvm_state_t *e);
qvm_value_t we_page_get_current_data(qvm_state_t *e);
int we_page_list_init(qvm_state_t *e);

void we_page_insert_index_object(qvm_state_t *e, int index, qvm_value_t v);
qvm_value_t we_page_get_index_object(qvm_state_t *e, int index);

void we_page_register_basic(qvm_native_t show, qvm_native_t hide);

int we_page_manager_create_components(qvm_state_t *e);

qvm_value_t we_page_load_evue_file(const char *filename);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
