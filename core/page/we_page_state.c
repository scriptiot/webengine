#include "page/we_page.h"

const char PAGE_LIFECYCLE_ON_INIT[] = "onInit";
const char PAGE_LIFECYCLE_CALLBACK_ON_READY[] = "onReady";
const char PAGE_LIFECYCLE_CALLBACK_ON_SHOW[] = "onShow";
const char PAGE_LIFECYCLE_CALLBACK_ON_HIDE[] = "onHide";
const char PAGE_LIFECYCLE_CALLBACK_ON_DESTROY[] = "onDestroy";

enum WEPageErr we_page_state_handle_init_state(we_page_t *sm) {
    enum WEPageErr err = we_page_err;
    int curr_state = we_page_sm_get_current_state(sm);
    if( curr_state == UNDEFINED_STATE ){
        we_log("undefined state -> init state\n");
        err = we_page_sm_set_current_state(sm, INIT_STATE);
        if (err != we_page_ok)
            return err;

        err = we_page_sm_invoke_page_life_cycle_callback(sm, PAGE_LIFECYCLE_ON_INIT);
        if (err != we_page_ok)
            return err;

        err = we_page_sm_change_state(sm, READY_STATE);
        if (err != we_page_ok)
            return err;
    } else {
        we_log("current state(%d) is invalid when changing undefined state to init state.", curr_state);
    }
    return err;
}

enum WEPageErr we_page_state_handle_ready_state(we_page_t * sm){
    enum WEPageErr err = we_page_err;
    int curr_state = we_page_sm_get_current_state(sm);
    if (curr_state == INIT_STATE) {
        we_log("init state -> ready state\n");
        err = we_page_sm_render_page(sm);
        if (err != we_page_ok)
            return err;

        err = we_page_sm_invoke_page_life_cycle_callback(sm, PAGE_LIFECYCLE_CALLBACK_ON_READY);
        if (err != we_page_ok)
            return err;

        err = we_page_sm_set_current_state(sm, READY_STATE);
        if (err != we_page_ok)
            return err;
    } else {
        we_log("current state(%d) is invalid when changing init state to ready state.",
                    curr_state);
    }
    return err;
}

enum WEPageErr we_page_state_handle_show_state(we_page_t * sm){
    enum WEPageErr err = we_page_err;
    int curr_state = we_page_sm_get_current_state(sm);
    if ((curr_state == READY_STATE) || (curr_state == BACKGROUND_STATE)) {
        we_log("current state(%d) -> show state\n", curr_state);
        err = we_page_sm_show_page(sm);
        if (err != we_page_ok)
            return err;

        err = we_page_sm_invoke_page_life_cycle_callback(sm, PAGE_LIFECYCLE_CALLBACK_ON_SHOW);
        if (err != we_page_ok)
            return err;

        err = we_page_sm_set_current_state(sm, SHOW_STATE);
        if (err != we_page_ok)
            return err;

    } else {
        we_log("current state(%d) is invalid when changing ready state to show state.",
                    curr_state);
    }
    return err;
}

enum WEPageErr we_page_state_handle_background_state(we_page_t * sm){
    enum WEPageErr err = we_page_err;
    int curr_state = we_page_sm_get_current_state(sm);
    if ((curr_state == READY_STATE) || (curr_state == SHOW_STATE)) {
        we_log("current state(%d) -> background state\n", curr_state);
        err = we_page_sm_hide_page(sm);
        if (err != we_page_ok)
            return err;

        err = we_page_sm_invoke_page_life_cycle_callback(sm, PAGE_LIFECYCLE_CALLBACK_ON_HIDE);
        if (err != we_page_ok)
            return err;

        err = we_page_sm_set_current_state(sm, BACKGROUND_STATE);
        if (err != we_page_ok)
            return err;
    } else {
        we_log("current state(%d) is invalid when changing show state to background state.",
                    curr_state);
    }
    return err;
}

enum WEPageErr we_page_state_handle_destroy_state(we_page_t * sm){
    enum WEPageErr err = we_page_err;
    int curr_state = we_page_sm_get_current_state(sm);
    // any normal state can jump to destory state
    if ( (curr_state >= INIT_STATE) ) {
        we_log("current state(%d) -> destroy state\n", curr_state);
        err = we_page_sm_invoke_page_life_cycle_callback(sm, PAGE_LIFECYCLE_CALLBACK_ON_DESTROY);
        if (err != we_page_ok)
            return err;

        err = we_page_sm_release_history_page_resource(sm);
        if (err != we_page_ok)
            return err;

        err = we_page_sm_set_current_state(sm, DESTROY_STATE);
        if (err != we_page_ok)
            return err;
    } else {
        we_log("current state(%d) is invalid when changing show state to destroy state.",
                    curr_state);
    }
    return err;
}
