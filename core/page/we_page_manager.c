#include "page/we_page.h"

static qvm_value_t _page_manager_get_components(qvm_state_t *e) {
    qvm_value_t index_object = we_page_get_current_index_object(e);
    return qvm_get_property(e, index_object, "$components");
}

static qvm_value_t _page_manager_get_objs(qvm_state_t *e) {
    qvm_value_t index_object = we_page_get_current_index_object(e);
    return qvm_get_property(e, index_object, "$objs");
}

int we_page_manager_create_components(qvm_state_t *e) {
    qvm_value_t index_object = we_page_get_current_index_object(e);
    if( qvm_is_undefined(index_object) ) {
        return 0;
    } 
    qvm_add_property(e, index_object, "$components", qvm_new_array(e));
    qvm_add_property(e, index_object, "$objs", qvm_new_array(e));
    return 1;
}

int we_page_manager_add_component(qvm_state_t *e, void *addr, qvm_value_t html_object) {
    qvm_value_t components = _page_manager_get_components(e);
    if( qvm_is_undefined(components) ) {
        return 0;
    }
    qvm_value_t objs = _page_manager_get_objs(e);
    if( qvm_is_undefined(objs) ) {
        return 0;
    }

    int len = qvm_get_array_length(e, objs);
    for(int i = 0; i < len; i++) {
        qvm_value_t obj = qvm_get_array(e, objs, i);
        if( qvm_is_invoke(obj) && qvm_to_invoke(e, obj) == addr ) {
            qvm_set_array(e, components, i, html_object);
            return 1;
        }
    }

    qvm_set_array(e, objs, len, qvm_mk_invoke(addr));
    qvm_set_array(e, components, len, html_object);
    return 0;
}

int we_page_manager_remove_component(qvm_state_t *e, void *addr) {
    qvm_value_t components = _page_manager_get_components(e);
    if( qvm_is_undefined(components) ) {
        return 0;
    }
    qvm_value_t objs = _page_manager_get_objs(e);
    if( qvm_is_undefined(objs) ) {
        return 0;
    }

    int len = qvm_get_array_length(e, objs);
    for(int i = 0; i < len; i++) {
        qvm_value_t obj = qvm_get_array(e, objs, i);
        if( qvm_is_invoke(obj) && qvm_to_invoke(e, obj) == addr ) {
            qvm_set_array(e, components, i, qvm_undefined);
            return 1;
        }
    }
    return 0;
}

qvm_value_t we_page_manager_find_component(qvm_state_t *e, void *addr) {
    qvm_value_t components = _page_manager_get_components(e);
    if( qvm_is_undefined(components) ) {
        return qvm_undefined;
    }
    qvm_value_t objs = _page_manager_get_objs(e);
    if( qvm_is_undefined(objs) ) {
        return qvm_undefined;
    }

    int len = qvm_get_array_length(e, objs);
    for(int i = 0; i < len; i++) {
        qvm_value_t obj = qvm_get_array(e, objs, i);
        if( qvm_is_invoke(obj) && qvm_to_invoke(e, obj) == addr ) {
            return qvm_get_array(e, components, i);
        }
    }
    return qvm_undefined;
}
