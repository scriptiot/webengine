#include "page/we_page.h"
#include "evue/we_evue.h"
#include "binding/we_binding.h"
#include "rendering/we_render.h"


static we_evue_t _evue;
static qvm_native_t _page_show;
static qvm_native_t _page_hide;

void we_page_register_basic(qvm_native_t show, qvm_native_t hide) {
    _page_show = show;
    _page_hide = hide;
}

void we_page_sm_init(qvm_state_t * e, we_page_t * sm, const char* uri) {
    sm->state = UNDEFINED_STATE;
    sm->uri = (char *)uri;
    sm->uri = NULL;
}

enum WEPageErr we_page_sm_set_current_state(we_page_t * sm, int32_t state){
    if (state <= UNDEFINED_STATE || state >= END_STATE) {
        we_log("error input state:%d\n", state);
        return we_page_err;
    }
    sm->state = state;
    return we_page_ok;
}

int32_t we_page_sm_get_current_state(we_page_t * sm){
    return sm->state;
}

enum WEPageErr we_page_sm_change_state(we_page_t * sm, int new_state){
    enum WEPageErr err = we_page_err;
    if ((new_state <= UNDEFINED_STATE) || (new_state >= END_STATE)) {
        we_log("error input state:%d", new_state);
        return err;
    }

    switch (new_state) {
    case INIT_STATE:        err = we_page_state_handle_init_state(sm);break;
    case READY_STATE:       err = we_page_state_handle_ready_state(sm);break;
    case SHOW_STATE:        err = we_page_state_handle_show_state(sm);break;
    case BACKGROUND_STATE:  err = we_page_state_handle_background_state(sm);break;
    case DESTROY_STATE:     err = we_page_state_handle_destroy_state(sm);break;
    default:
        break;
    }

    return err;
}

enum WEPageErr we_page_sm_render_page(we_page_t * sm){
    we_log("render page\r\n");
    qvm_state_t *e = we_get_runtime();
    enum WEPageErr err = we_evue_open(sm->uri, &_evue);
    if( err != we_page_ok ) {
        we_log("render page code: %d\r\n", err);
        return we_page_err;
    }
    qvm_build_js_rule(e);
    qvm_value_t index_object = qvm_parse_string(e, _evue.script_content);
    qvm_call(e, index_object, index_object, 0, NULL);
    if ( qvm_is_undefined(index_object) ) {
        we_log("Eval JS file failed\r\n");
        we_evue_free(&_evue);
        return we_page_err;
    }

    we_page_insert_index_object(e, sm->index, index_object);

    qvm_build_xml_rule(e);
    qvm_value_t html_object = qvm_parse_string(e, _evue.html_content);
    qvm_call(e, html_object, html_object, 0, NULL);
    if ( qvm_is_undefined(html_object) ) {
        we_log("Eval HTML file failed\r\n");
        we_evue_free(&_evue);
        return we_page_err;
    }
    qvm_add_property(e, index_object, "html_object", html_object);

    qvm_build_css_rule(e);
    qvm_value_t css_object = qvm_parse_string(e, _evue.style_content);
    qvm_call(e, css_object, css_object, 0, NULL);
    if (qvm_is_undefined(css_object)) {
        we_log("Eval CSS file failed\r\n");
        we_evue_free(&_evue);
        return we_page_err;
    }

    qvm_add_property(e, index_object, "css_object", css_object);
    we_evue_free(&_evue);
    we_render_run(e);
    return we_page_ok;
}

enum WEPageErr we_page_sm_show_page(we_page_t * sm){
    QVM_UNUSED(sm);
    qvm_state_t *e = we_get_runtime();
    qvm_value_t first_node = we_page_get_current_top_html_object(e);
    _page_show(e, first_node, 0, NULL);
    //qvm_gc(e);
    we_log("show page\r\n");
    we_log("Sysmem usage: %d percent\r\n", we_system_memory_usage() );
    return we_page_ok;
}

enum WEPageErr we_page_sm_hide_page(we_page_t * sm) {
    qvm_state_t *e = we_get_runtime();
    qvm_value_t first_node = we_page_get_current_top_html_object(e);
    _page_hide(e, first_node, 0, NULL);

    we_log("hide page\r\n");
    return we_page_ok;
}

enum WEPageErr we_page_sm_invoke_page_life_cycle_callback(we_page_t * sm, const char * name){
    if ((name == NULL) || (strlen(name) == 0)) {
        we_log("input parameter is invalid when invoking page life cycle callback.\n");
        return we_page_err;
    }
    qvm_state_t *e = we_get_runtime();
    qvm_value_t function = qvm_get_property(e, we_page_get_current_default(e), name);
    if ( qvm_is_script(function) ){
        qvm_call(e, function, we_page_get_current_default(e), 0, NULL);
    }
    return we_page_ok;
}

enum WEPageErr we_page_sm_release_history_page_resource(we_page_t * sm){
    qvm_state_t * e = we_get_runtime();
    we_render_remove_page(e, we_page_get_current_html_object(e));
    qvm_gc(e);
    we_log("release page\r\n");
    return we_page_ok;
}

qvm_value_t we_page_load_evue_file(const char *filename) {
    qvm_state_t *e = we_get_runtime();
    enum WEPageErr err = we_evue_open(filename, &_evue);
    if( err != we_page_ok ) {
        we_log("render page code: %d\r\n", err);
        return qvm_undefined;
    }

    qvm_build_js_rule(e);
    qvm_value_t index_object = qvm_parse_string(e, _evue.script_content);
    qvm_call(e, index_object, index_object, 0, NULL);
    if ( qvm_is_undefined(index_object) ) {
        we_log("Eval JS file failed\r\n");
        we_evue_free(&_evue);
        return qvm_undefined;
    }

    qvm_build_xml_rule(e);
    qvm_value_t html_object = qvm_parse_string(e, _evue.html_content);
    qvm_call(e, html_object, html_object, 0, NULL);
//    if ( qvm_is_undefined(html_object) ) {
//        we_log("Eval HTML file failed\r\n");
//        we_evue_free(&_evue);
//        return qvm_undefined;
//    }
    qvm_add_property(e, index_object, "html_object", html_object);


    qvm_build_css_rule(e);
    qvm_value_t css_object = qvm_parse_string(e, _evue.style_content);
    qvm_call(e, css_object, css_object, 0, NULL);
    if (qvm_is_undefined(css_object)) {
        we_log("Eval CSS file failed\r\n");
        we_evue_free(&_evue);
        return qvm_undefined;
    }
    qvm_add_property(e, index_object, "css_object", css_object);
    we_evue_free(&_evue);
    return index_object;
}
