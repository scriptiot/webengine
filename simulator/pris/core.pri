HEADERS += \
    $$PWD/../../core/binding/we_binding.h \
    $$PWD/../../core/buffer/we_buffer.h \
    $$PWD/../../core/css/we_css.h \
    $$PWD/../../core/dom/we_document.h \
    $$PWD/../../core/evue/we_evue.h \
    $$PWD/../../core/fileapi/we_file.h \
    $$PWD/../../core/html/we_html.h \
    $$PWD/../../core/javascript/we_js_builtin.h \
    $$PWD/../../core/network/we_websocket.h \
    $$PWD/../../core/page/we_page.h \
    $$PWD/../../core/rendering/we_render.h \
    $$PWD/../../core/router/we_router.h \
    $$PWD/../../core/compress/wrap_heatshrink.h \
    $$PWD/../../core/compress/heatshrink_encoder.h \
    $$PWD/../../core/compress/heatshrink_decoder.h \
    $$PWD/../../core/compress/heatshrink_config.h \
    $$PWD/../../core/compress/heatshrink_common.h \
    $$PWD/../../core/compress/compress_heatshrink.h \
    $$PWD/../../core/base64/we_base64.h \
    $$PWD/../../core/epk/we_epk.h \
    $$PWD/../../core/md5/we_md5.h

SOURCES += \
    $$PWD/../../core/buffer/we_buffer.c \
    $$PWD/../../core/image/we_image.c \
    $$PWD/../../core/image/lodepng.c \
    $$PWD/../../core/network/we_websocket.c

SOURCES += \
    $$PWD/../../core/process/we_process.c

SOURCES += \
    $$PWD/../../core/network/we_socket.c \
    $$PWD/../../core/network/we_http_parser.c \
    $$PWD/../../core/network/http-parser/http_parser.c

SOURCES += \
    $$PWD/../../core/binding/we_binding.c \
    $$PWD/../../core/binding/we_binding_native.c \
    $$PWD/../../core/binding/we_binding_watcher.c \
    $$PWD/../../core/css/we_css.c \
    $$PWD/../../core/dom/we_document.c \
    $$PWD/../../core/evue/we_evue.c \
    $$PWD/../../core/fileapi/we_file.c \
    $$PWD/../../core/html/we_html_object.c \
    $$PWD/../../core/html/we_html_register_ui.c \
    $$PWD/../../core/javascript/we_js_builtin.c \
    $$PWD/../../core/javascript/we_js_builtin_dom.c \
    $$PWD/../../core/javascript/we_js_builtin_dom_element.c \
    $$PWD/../../core/page/we_page.c \
    $$PWD/../../core/page/we_page_manager.c \
    $$PWD/../../core/page/we_page_state.c \
    $$PWD/../../core/page/we_page_state_machine.c \
    $$PWD/../../core/rendering/we_render.c \
    $$PWD/../../core/rendering/we_render_template.c \
    $$PWD/../../core/router/we_router.c \
    $$PWD/../../core/compress/wrap_heatshrink.c \
    $$PWD/../../core/compress/heatshrink_encoder.c \
    $$PWD/../../core/compress/heatshrink_decoder.c \
    $$PWD/../../core/compress/compress_heatshrink.c \
    $$PWD/../../core/base64/we_base64.c \
    $$PWD/../../core/md5/we_md5.c \
    $$PWD/../../core/task/we_task.c \
    $$PWD/../../core/webengine.c



