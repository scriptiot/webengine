DEFINES += WE_USE_OPENGL

SOURCES += \
    $$PWD/../../core/graphics/we_graphics.c \
    $$PWD/../../core/graphics/we_tk.c \
    $$PWD/../../core/graphics/PicoGL/src/api.c \
    $$PWD/../../core/graphics/PicoGL/src/arrays.c \
    $$PWD/../../core/graphics/PicoGL/src/clear.c \
    $$PWD/../../core/graphics/PicoGL/src/clip.c \
    $$PWD/../../core/graphics/PicoGL/src/error.c \
    $$PWD/../../core/graphics/PicoGL/src/get.c \
    $$PWD/../../core/graphics/PicoGL/src/image_util.c \
    $$PWD/../../core/graphics/PicoGL/src/init.c \
    $$PWD/../../core/graphics/PicoGL/src/light.c \
    $$PWD/../../core/graphics/PicoGL/src/list.c \
    $$PWD/../../core/graphics/PicoGL/src/matrix.c \
    $$PWD/../../core/graphics/PicoGL/src/memory.c \
    $$PWD/../../core/graphics/PicoGL/src/misc.c \
    $$PWD/../../core/graphics/PicoGL/src/msghandling.c \
    $$PWD/../../core/graphics/PicoGL/src/oscontext.c \
    $$PWD/../../core/graphics/PicoGL/src/select.c \
    $$PWD/../../core/graphics/PicoGL/src/specbuf.c \
    $$PWD/../../core/graphics/PicoGL/src/texture.c \
    $$PWD/../../core/graphics/PicoGL/src/vertex.c \
    $$PWD/../../core/graphics/PicoGL/src/zbuffer.c \
    $$PWD/../../core/graphics/PicoGL/src/zdither.c \
    $$PWD/../../core/graphics/PicoGL/src/zline.c \
    $$PWD/../../core/graphics/PicoGL/src/zmath.c \
    $$PWD/../../core/graphics/PicoGL/src/ztriangle.c \
    $$PWD/../../core/graphics/PicoGL/src/glu/glu.c \
    $$PWD/../../core/graphics/PicoGL/src/glu/glu_cylinder.c \
    $$PWD/../../core/graphics/PicoGL/src/glu/glu_disk.c \
    $$PWD/../../core/graphics/PicoGL/src/glu/glu_perspective.c \
    $$PWD/../../core/graphics/PicoGL/src/glu/glu_sphere.c \
    $$PWD/../../core/graphics/PicoGL/src/glu/glu_torus.c
