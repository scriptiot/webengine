INCLUDEPATH +=

HEADERS += \
    $$PWD/../../../component/lvgl7/lv_conf.h \
    $$PWD/../../../component/lvgl7/style/lvgl_style_utils.h \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_basic.h

SOURCES += \
    $$PWD/../../../component/lvgl7/style/lvgl_style_align-items.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_background-color.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_border.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_color.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_display.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_fade.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_flex-direction.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_font.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_gradient.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_index.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_margin.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_opacity.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_padding.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_position.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_size.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_text.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_utils.c \
    $$PWD/../../../component/lvgl7/style/lvgl_style_draggable.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_basic.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_button.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_canvas.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_div.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_image.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_imagebutton.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_input.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_list.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_listitem.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_option.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_progress.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_select.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_slider.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_tab.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_tabview.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_text.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_line.c \
    $$PWD/../../../component/lvgl7/ui/evue/lvgl_html_textarea.c
