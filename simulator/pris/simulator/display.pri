HEADERS += \
#    $$PWD/../../display/ILI9341.h \
#    $$PWD/../../display/R61581.h \
#    $$PWD/../../display/SHARP_MIP.h \
#    $$PWD/../../display/SSD1963.h \
#    $$PWD/../../display/ST7565.h \
#    $$PWD/../../display/UC1610.h \
#    $$PWD/../../display/drm.h \
#    $$PWD/../../display/fbdev.h \
    $$PWD/../../display/monitor.h

SOURCES += \
#    $$PWD/../../display/ILI9341.c \
#    $$PWD/../../display/R61581.c \
#    $$PWD/../../display/SHARP_MIP.c \
#    $$PWD/../../display/SSD1963.c \
#    $$PWD/../../display/ST7565.c \
#    $$PWD/../../display/UC1610.c \
#    $$PWD/../../display/drm.c \
#    $$PWD/../../display/fbdev.c \
    $$PWD/../../display/monitor.c \
    $$PWD/../../logo/Icon.c
