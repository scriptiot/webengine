HEADERS += \
#    $$PWD/../../indev/AD_touch.h \
#    $$PWD/../../indev/FT5406EE8.h \
#    $$PWD/../../indev/XPT2046.h \
#    $$PWD/../../indev/evdev.h \
    $$PWD/../../indev/keyboard.h \
#    $$PWD/../../indev/libinput_drv.h \
    $$PWD/../../indev/mouse.h \
    $$PWD/../../indev/mousewheel.h

SOURCES += \
#    $$PWD/../../indev/AD_touch.c \
#    $$PWD/../../indev/FT5406EE8.c \
#    $$PWD/../../indev/XPT2046.c \
#    $$PWD/../../indev/evdev.c \
    $$PWD/../../indev/keyboard.c \
#    $$PWD/../../indev/libinput.c \
    $$PWD/../../indev/mouse.c \
    $$PWD/../../indev/mousewheel.c
