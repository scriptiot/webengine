INCLUDEPATH += $$PWD/../../librws

SOURCES += \
	$$PWD/../../librws/src/librws.c \
	$$PWD/../../librws/src/rws_common.c \
	$$PWD/../../librws/src/rws_error.c \
	$$PWD/../../librws/src/rws_frame.c \
	$$PWD/../../librws/src/rws_list.c \
	$$PWD/../../librws/src/rws_memory.c \
	$$PWD/../../librws/src/rws_socketpriv.c \
	$$PWD/../../librws/src/rws_socketpub.c \
	$$PWD/../../librws/src/rws_string.c \
	$$PWD/../../librws/src/rws_thread.c

HEADERS += \
	$$PWD/../../librws/librws.h \
	$$PWD/../../librws/src/rws_common.h \
	$$PWD/../../librws/src/rws_error.h \
	$$PWD/../../librws/src/rws_frame.h \
	$$PWD/../../librws/src/rws_list.h \
	$$PWD/../../librws/src/rws_memory.h \
	$$PWD/../../librws/src/rws_socket.h \
	$$PWD/../../librws/src/rws_string.h \
	$$PWD/../../librws/src/rws_thread.h

DEFINES += RWS_BUILD
