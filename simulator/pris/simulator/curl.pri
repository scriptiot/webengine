HEADERS += \
    $$PWD/../../curl/curl-7.75.0_3-win64-mingw/include/curl/curl.h \
    $$PWD/../../curl/curl-7.75.0_3-win64-mingw/include/curl/curlver.h \
    $$PWD/../../curl/curl-7.75.0_3-win64-mingw/include/curl/easy.h \
    $$PWD/../../curl/curl-7.75.0_3-win64-mingw/include/curl/mprintf.h \
    $$PWD/../../curl/curl-7.75.0_3-win64-mingw/include/curl/multi.h \
    $$PWD/../../curl/curl-7.75.0_3-win64-mingw/include/curl/options.h \
    $$PWD/../../curl/curl-7.75.0_3-win64-mingw/include/curl/stdcheaders.h \
    $$PWD/../../curl/curl-7.75.0_3-win64-mingw/include/curl/system.h \
    $$PWD/../../curl/curl-7.75.0_3-win64-mingw/include/curl/typecheck-gcc.h \
    $$PWD/../../curl/curl-7.75.0_3-win64-mingw/include/curl/urlapi.h \
    $$PWD/../../curl/evue_curl.h

SOURCES += \
    $$PWD/../../curl/evue_curl.c
