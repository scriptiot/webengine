DEFINES += QVM_RUNTIME_DEBUG

SOURCES += \
    $$PWD/../../../../quickvm/extension/ecma/qvm_ecma.c \
    $$PWD/../../../../quickvm/extension/ecma/qvm_ecma_builtin.c \
    $$PWD/../../../../quickvm/extension/ecma/qvm_ecma_console.c \
    $$PWD/../../../../quickvm/extension/ecma/qvm_ecma_object.c \
    $$PWD/../../../../quickvm/extension/ecma/qvm_ecma_boolean.c \
    $$PWD/../../../../quickvm/extension/ecma/qvm_ecma_number.c \
    $$PWD/../../../../quickvm/extension/ecma/qvm_ecma_string.c \
    $$PWD/../../../../quickvm/extension/ecma/qvm_ecma_math.c \
    $$PWD/../../../../quickvm/extension/ecma/qvm_ecma_json.c \
    $$PWD/../../../../quickvm/extension/ecma/qvm_ecma_function.c \
    $$PWD/../../../../quickvm/extension/ecma/qvm_ecma_date.c \
    $$PWD/../../../../quickvm/extension/ecma/qvm_ecma_array.c

SOURCES += \
    $$PWD/../../../../quickvm/src/quickvm.c \
    $$PWD/../../../../quickvm/src/quickvm_utils.c \
    $$PWD/../../../../quickvm/src/quickvm_native.c \
    $$PWD/../../../../quickvm/src/quickvm_gc.c \
    $$PWD/../../../../quickvm/src/quickvm_xml_parser.c \
    $$PWD/../../../../quickvm/src/quickvm_css_parser.c \
    $$PWD/../../../../quickvm/src/quickvm_js_parser.c
