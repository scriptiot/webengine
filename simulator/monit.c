/*
 * @Author: your name
 * @Date: 2021-05-22 20:57:29
 * @LastEditTime: 2021-05-22 21:10:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /ewebengine/simulator/monit.c
 */
// 1.获取输出路径和输入路径
// 2.启动线程监控输入路径
// 3.根据md5或者文件修改时间判断文件是否修改
// 4.如发生变化，则调用系统shell命令python3 nxp.py ./xxx ./xxx
// 5.刷新当前页面

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <SDL2/SDL.h>

extern bool we_md5_check(uint8_t *msg, uint32_t msg_length, uint8_t *md5);

static char *src_dir = NULL; // evue directory
static char *conf_file = NULL; // nxp guide config file
static uint8_t * last_modify_md5 = NULL;
static long last_modidy_ts = 0;

char * ReadFile(char * path, int *length)
{
    FILE * pfile;
    char * data;

    pfile = fopen(path, "rb");
    if (pfile == NULL)
    {
        return NULL;
    }
    fseek(pfile, 0, SEEK_END);
    *length = ftell(pfile);
    data = (char *)malloc((*length + 1) * sizeof(char));
    rewind(pfile);
    *length = fread(data, 1, *length, pfile);
    data[*length] = '\0';
    fclose(pfile);
    return data;
}


int GetFileModifyTime(char * fpath)
{
    FILE * fp;
    int fd;
    struct stat buf;
    fp = fopen(fpath, "r");
    if(NULL != fp )
    {
        fd = fileno(fp);
        fstat(fd, &buf);
        int size = buf.st_size; // get file size (byte)
        long modify_time = buf.st_mtime; // latest modification time (seconds passed from 01/01/00:00:00 1970 UTC)
        printf("size = %d\n",size);
        printf("modify_time = %ld\n",modify_time);
        fclose(fp);
        return modify_time;
    }
    printf("function error\n");
    return 0;
}

void monit_init(int argc, char *argv[]){
    int i = 0;
    char *arg_name;
    char *arg_value;

    if( argc > 1 ) {
        for(i = 1; i < argc; i += 2) {
            arg_name = argv[i];
            arg_value = argv[i + 1];

            if( !strcmp(arg_name, "--src") ) {
                src_dir = arg_value;
            } else if( !strcmp(arg_name, "--dst") ) {
                conf_file = arg_value;
            }
        }
    }
}

// 重新生成evue代码
int generate_code() {
    int ret = 0;
    char * cmd_string = "python3 ../tools/nxp_evue/nxp.py %s .%s";
    char string[strlen(cmd_string) + strlen(src_dir) + strlen(conf_file)];
    sprintf(string, cmd_string, conf_file, src_dir);
    printf("%s\r\n", string);
    ret = system(string); // ls一个没有的文件  ret = 512
    return ret;
}

// refresh 刷新页面
void refresh_page() {

}

// 监控目录
int monit_dir(void * data) {
    (void)data;
    while(1) {
        // 获取当前目录修改时间
        long mts = GetFileModifyTime(conf_file);

        // 如果发生改变，则调用python重新生成evue文件
        if (mts != last_modidy_ts) {
            if (last_modify_md5 == NULL) {
                generate_code();
                refresh_page();
                return 1;
            }

            int result = 0;
            int file_len = 0;
            char * data = ReadFile(conf_file, &file_len);

            if (we_md5_check((uint8_t *)data, file_len, last_modify_md5)) {
                generate_code();
                refresh_page();
                result = 1;
            }
            free(data);
            return result;
        }

        // 刷新页面        
        SDL_Delay(5);
    }
    return 0;
}

void monit_main(int argc, char *argv[]) {
    if (src_dir != NULL && conf_file != NULL) {
        monit_init(argc, argv);
        // 启动一个线程
        SDL_CreateThread(monit_dir, "tick", NULL);
    }
}
