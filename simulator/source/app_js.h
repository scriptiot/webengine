#ifndef APP_JS_H
#define APP_JS_H


#include "stdint.h"

#define EVUE_SRC_CNT  8

typedef struct evue_src_code{
    const char *path;
    int addr;
    uint32_t len;
}evue_src_code_t;

extern void we_src_init(evue_src_code_t *src);

#endif
