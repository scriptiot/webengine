TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

#QMAKE_CFLAGS += -Wall -Wshadow -Wundef -Wuninitialized -Wno-missing-braces
QMAKE_LINK = gcc

#INCLUDEPATH += $$PWD/../../../quickvm/private
#INCLUDEPATH += $$PWD/../../../quickvm/inc

INCLUDEPATH += $$PWD/../../webengine/core
INCLUDEPATH += $$PWD/../../webengine/include
INCLUDEPATH += $$PWD/../../webengine/malloc
INCLUDEPATH += $$PWD/../../webengine/modules/cjson

INCLUDEPATH += $$PWD/../../evm/inc

INCLUDEPATH += $$PWD/../gui/lvgl7
INCLUDEPATH += $$PWD/../gui/lvgl7/lvgl
INCLUDEPATH += $$PWD/../component
INCLUDEPATH += $$PWD/../component/lvgl7
INCLUDEPATH += $$PWD
INCLUDEPATH += $$PWD/curl/curl-7.75.0_3-win64-mingw/include

INCLUDEPATH += $$PWD/../../webengine/core/network/mbedtls/inc
INCLUDEPATH += $$PWD/../../webengine/core/network/http-parser

DEFINES += WE_USE_ECMA_MATH
DEFINES += WE_USE_ECMA_JSON
DEFINES += WE_USE_ECMA_STRING
DEFINES += WE_USE_ECMA_ARRAY
DEFINES += WE_USE_LODEPNG
DEFINES += WE_USE_MBEDTLS
DEFINES += WE_USE_DEBUG
DEFINES += WE_USE_WEBSOCKET
DEFINES += WE_USE_HTTP
DEFINES += WE_USE_SOCKET
DEFINES += WE_USE_PROCESS
DEFINES += WE_USE_FS
DEFINES += WE_USE_BUFFER
DEFINES += WE_COLOR_DEPTH=16
DEFINES += WE_USE_CANVAS
DEFINES += WE_USE_ECMA_DATE

DEFINES += LV_CONF_INCLUDE_SIMPLE
DEFINES += USE_MONITOR
DEFINES += USE_MOUSE
DEFINES += LV_PNG_USE_LV_FILESYSTEM=1

INCLUDEPATH += $$PWD/../../webengine/core/graphics/PicoGL/src
INCLUDEPATH += $$PWD/../../webengine/core/graphics/PicoGL/include
INCLUDEPATH += $$PWD/../../webengine/core/graphics/PicoGL/src

LIBS += -lm

win32 {
    INCLUDEPATH += ./
    LIBS += -L$$PWD/mbedtls -lmbedtls -lmbedx509 -lmbedcrypto
    LIBS += -lwinmm -lwsock32 -lws2_32
    LIBS += -L$$PWD/curl/curl-7.75.0_3-win64-mingw/bin -lcurl-x64
    LIBS += -L$$PWD -lSDL2
    LIBS += -L$$PWD/../../evm/lib/x86/mingw -lqvmcore -lqvmecma -lqvmjs -lqvmxml -lqvmcss
}

unix {
    LIBS += -L/usr/lib/x86_64-linux-gnu -lpthread -lSDL2 -lcurl -lmbedtls -lmbedx509 -lmbedcrypto
    LIBS += -L$$PWD/../../evm/lib/x86/linux -lqvmcore -lqvmecma -lqvmjs -lqvmxml -lqvmcss
}

include($$PWD/pris/core.pri)
include($$PWD/pris/malloc.pri)
include($$PWD/pris/component.pri)
include($$PWD/pris/gui.pri)
include($$PWD/pris/simulator.pri)
include($$PWD/pris/graphics.pri)

SOURCES += \
    main.c
