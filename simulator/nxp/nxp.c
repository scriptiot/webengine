/*
 * @Author: your name
 * @Date: 2021-05-22 20:57:29
 * @LastEditTime: 2021-05-22 21:10:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /ewebengine/simulator/monit.c
 */
// 1.获取输出路径和输入路径
// 2.启动线程监控输入路径
// 3.根据md5或者文件修改时间判断文件是否修改
// 4.如发生变化，则调用系统shell命令python3 nxp.py ./xxx ./xxx
// 5.刷新当前页面

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include "fileapi/we_file.h"
#include "nxp.h"
#include "md5/we_md5.h"

extern bool we_md5_check(uint8_t *msg, uint32_t msg_length, uint8_t *md5);

static char *src_dir = NULL; // evue directory
static char *guider_file = NULL; // nxp guide config file
static long last_modidy_ts = 0;
static int isRefresh = 0;

bool isFileExists(const char* path){
    WE_FHANDLE fd = we_file_open(path, "rb");
    if (fd == NULL){
        return false;
    }
    we_file_close(fd);
    return true;
}

char * readFile(const char* path){
    WE_FHANDLE file;
    size_t lSize;
    size_t br;
    char *buffer = NULL;
    lSize = (size_t)we_file_size(path);
    file = we_file_open(path, "rb");
    if (file == NULL){
        return NULL;
    }

    buffer = malloc(lSize);
    br = we_file_read(file, buffer, lSize);
    if (br != lSize ){
        we_file_close(file);
        return NULL;
    }

    buffer[lSize] = 0;
    we_file_close(file);
    return buffer;
}

int GetFileModifyTime(char * fpath)
{
    FILE * fp;
    int fd;
    struct stat buf;
    fp = fopen(fpath, "r");
    if(NULL != fp )
    {
        fd = fileno(fp);
        fstat(fd, &buf);
        long modify_time = buf.st_mtime; // latest modification time (seconds passed from 01/01/00:00:00 1970 UTC)
        fclose(fp);
        return modify_time;
    }
    return 0;
}

void monit_init(int argc, char *argv[]){
    int i = 0;
    char *arg_name;
    char *arg_value;

    if( argc > 1 ) {
        for(i = 1; i < argc; i += 2) {
            arg_name = argv[i];
            arg_value = argv[i + 1];

            if( !strcmp(arg_name, "--dir") ) {
                src_dir = arg_value;
            } else if( !strcmp(arg_name, "--guider") ) {
                guider_file = arg_value;
            }
        }
    }
}

// 重新生成evue代码
int generate_code() {
    int ret = 0;
    char cmd[256] = {0};
    char currentDir[256] = {0};
    char nxp_dir [256] = {0};
    char nxp_exe [256] = {0};
    getcwd(currentDir, sizeof(currentDir));
#if defined (CONFIG_EVM_DEBUG)
    sprintf(nxp_dir, "%s/%s/", currentDir, "../bin/x86_64-window-mingw/tools/nxp_evue/");
#else
    sprintf(nxp_dir, "%s/%s/", currentDir, "tools/nxp_evue/");
#endif
    sprintf(nxp_exe, "%s/nxp.exe", nxp_dir);

    bool isExists = isFileExists(nxp_exe);
    if (isExists){
        sprintf(cmd, "%s %s %s", nxp_exe, guider_file, src_dir);
        ret = system(cmd);
    }
    return ret;
}

// refresh 刷新页面
void refresh_page() {
    isRefresh = 1;
}

// 监控目录
static int monit_dir(void * param) {
    (void )(param);
    uint8_t last_modify_md5[32];
    while(1) {
        // 获取当前目录修改时间
        long mts = GetFileModifyTime(guider_file);
        // 如果发生改变，则调用python重新生成evue文件
        if (mts != last_modidy_ts) {
            last_modidy_ts = mts;
            printf("oldtime: %ld modify_time = %ld\n", last_modidy_ts, mts);
//            generate_code();
            uint8_t guider_md5[32];
            char * data = readFile(guider_file);
            we_md5_check((uint8_t *)data, strlen(data), guider_md5);
            bool isNotChanged = we_md5_compare(guider_md5, last_modify_md5, 32);
            memcpy(last_modify_md5, guider_md5, 32);
            printf("isChanged: %b\n", isNotChanged);
            if (!isNotChanged) {
                generate_code();
                refresh_page();
            }
            free(data);
        }else{
            isRefresh = 0;
        }
        // 刷新页面
        SDL_Delay(500);
    }
    return 0;
}

void nxp_monitor(int argc, char *argv[]) {
     monit_init(argc, argv);
     SDL_CreateThread(monit_dir, "nxp_monitor", NULL);
}

int isPageRefresh()
{
    return isRefresh;
}
