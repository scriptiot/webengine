#ifndef EVM_CONF_H
#define EVM_CONF_H

#include <stdio.h>
#include "quickvm.h"

#define WE_CONF_ROOT_DIR    "C:"

#define LV_RAMFS_DIR        "R:/ramfs"

#define WE_HEAP_SIZE          (1000 * 1024)
#define WE_STACK_SIZE         (sizeof(qvm_value_t)*1000)
#define WE_REF_SIZE           50

#define PATH_LENGTH_MAX 256

#endif
