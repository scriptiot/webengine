#include <SDL2/SDL.h>
#include <stdlib.h>
#include <unistd.h>
#include "lvgl.h"
#include "display/monitor.h"
#include "indev/mouse.h"
#include "webengine.h"
#include "router/we_router.h"
#include "qvm_conf.h"
#include "source/app_js.h"
#include "fileapi/we_file.h"
#include "nxp/nxp.h"
#include "router/we_router.h"

static uint32_t WINDOW_WIDTH = LV_HOR_RES_MAX;
static uint32_t WINDOW_Height = LV_VER_RES_MAX;

uint32_t get_window_width(){return WINDOW_WIDTH;}
void set_window_width(uint32_t w){WINDOW_WIDTH = w;}
uint32_t get_window_height(){return WINDOW_Height;}
void set_window_height(uint32_t h){WINDOW_Height = h;}

extern int lv_pcfs_init(void);

void lv_size_init_cmd(int argc, char *argv[]){
    int i = 0;
    char *arg_name;
    char *arg_value;
    uint32_t width = LV_HOR_RES_MAX;
    uint32_t height = LV_VER_RES_MAX;
    if( argc > 1 ) {
        for(i = 1; i < argc; i += 2) {
            arg_name = argv[i];
            arg_value = argv[i + 1];

            if( !strcmp(arg_name, "--width") ) {
                width = atoi(arg_value);
            } else if( !strcmp(arg_name, "--height") ) {
                height = atoi(arg_value);
            }
        }
    }
    set_window_width(width);
    set_window_height(height);
}

int gui_init_ex(qvm_state_t *e) {
    QVM_UNUSED(e);
    return 1;
}

static int tick_thread(void * data)
{
    (void)data;

    while(1) {
        SDL_Delay(5);
        lv_tick_inc(5);
    }

    return 0;
}

static lv_disp_buf_t disp_buf;

static int global_argc;
static char **global_argv;

static void lv_disp_set_drv_size(lv_disp_drv_t * driver, uint32_t width, uint32_t height){
    driver->hor_res = width;
    driver->ver_res = height;
    return;
}

static void hal_init(void)
{
#if defined (WIN32) || defined (WIN64) || defined (__linux)
    lv_color_t *buf_1 = (lv_color_t *)malloc(get_window_width() *get_window_height() * sizeof (lv_color_t));
    lv_color_t *buf_2 = (lv_color_t *)malloc(get_window_width() *get_window_height() * sizeof (lv_color_t));
    lv_disp_buf_init(&disp_buf, buf_1, buf_2, get_window_width() *get_window_height());
#else
    static lv_color_t buf_1[LV_HOR_RES_MAX * LV_VER_RES_MAX];
    static lv_color_t buf_2[LV_HOR_RES_MAX * LV_VER_RES_MAX];
    lv_disp_buf_init(&disp_buf, buf_1, buf_2, LV_HOR_RES_MAX * LV_VER_RES_MAX);
#endif
    lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    lv_disp_set_drv_size(&disp_drv, get_window_width(), get_window_height());
    disp_drv.flush_cb = monitor_flush;
    disp_drv.buffer = &disp_buf;

    lv_disp_drv_register(&disp_drv);

    monitor_init(NULL);

    mouse_init();
    lv_indev_drv_t indev_drv;
    lv_indev_drv_init(&indev_drv);
    indev_drv.type = LV_INDEV_TYPE_POINTER;
    indev_drv.read_cb = mouse_read;
    lv_indev_drv_register(&indev_drv);

    SDL_CreateThread(tick_thread, "tick", NULL);
}

void qvm_assert (const char *assertion, const char *file, const char *function, const uint32_t line){
    printf ("AssertionError: '%s' failed at %s(%s):%lu.\n",
                       assertion,
                       file,
                       function,
                       (unsigned long) line);
    char s[255];
    sprintf(s, "[ERROR][%s]:[%s]:[%d]\n", file, function, line);
    while(1);
}

void * we_sys_malloc(size_t size) {
    return malloc(size);
}

void we_sys_free(void *mem){
    free(mem);
}

#undef main
int main(int argc, char *argv[])
{
    evue_src_code_t src[EVUE_SRC_CNT];
    we_src_init(src);
    for(int i=0;i<EVUE_SRC_CNT;i++)
        printf("%s 0x%x %d\n",src[i].path, src[i].addr,src[i].len);

    global_argc = argc;
    global_argv = argv;
    lv_size_init_cmd(argc, argv);
    lv_init();
    hal_init();
    int err = we_start(argc, argv);
    if (err == 0){
        we_error("Failed to run webengine!\r\n");
        return 0;
    }
    while(1) {
        lv_task_handler();
        usleep(1000);
        we_poll(we_get_runtime());
    }
    return 0;
}
