#include <stdio.h>
#include <curl/curl.h>
#include "evue_curl.h"


#define ACCEPT "Accept: application/json"
#define CONTENT_TYPE "Content-Type: application/json"
#define CHARSET "charsets: utf-8"


int curl_evm(qvm_state_t* e, qvm_value_t request, write_cb_t cb, void *chunk)
{
    qvm_value_t eurl = qvm_get_property(e, request, "url");
    qvm_value_t emethod = qvm_get_property(e, request, "method");
    qvm_value_t epostfield = qvm_get_property(e, request, "data");
    qvm_value_t eheaders = qvm_get_property(e, request, "headers");
    qvm_value_t etimeout = qvm_get_property(e, request, "timeout");

    const char *url = NULL;
    if (qvm_is_string(eurl)){
        url = qvm_to_string(e, eurl);
    }

    const char *method = NULL;
    if (qvm_is_string(emethod)){
        method = qvm_to_string(e, emethod);
    }

    const char *postfield = NULL;
    if (qvm_is_string(epostfield)){
        postfield = qvm_to_string(e, epostfield);
    }

    struct curl_slist *headers = NULL;
    if (qvm_is_array(eheaders)){
        int len = qvm_get_array_length(e, eheaders);
        for(uint8_t i = 0; i < len ; i++){
            qvm_value_t item = qvm_get_array(e, eheaders, i );
            if (qvm_is_string(item)){
                const char * header = qvm_to_string(e, item);
                headers = curl_slist_append(headers, header);
            }
        }
    }else if (qvm_is_string(eheaders)){
        const char * header = qvm_to_string(e, eheaders);
        headers = curl_slist_append(headers, header);
    }else{
        headers = curl_slist_append(headers, ACCEPT);
        headers = curl_slist_append(headers, CONTENT_TYPE);
        headers = curl_slist_append(headers, CHARSET);
    }



    CURL *curl;
    CURLcode res = CURLE_OK;
    long http_code = 0;

//    curl_global_init(CURL_GLOBAL_DEFAULT);

    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        curl_easy_setopt(curl, CURLOPT_URL, url);
        if (!strcmp(method, "POST") && postfield){
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS,postfield);
        }
        if(!strcmp(method,"POST"))
            curl_easy_setopt(curl, CURLOPT_POST, 1);
        if(!strcmp(method,"PUT"))
            curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");

        if (qvm_is_number(etimeout)){
            int32_t timeout = qvm_to_int32(e, etimeout);
            curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout);
        }

        /* send all data to this function  */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, cb);

        /* we pass our 'chunk' struct to the callback function */
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, chunk);

        /* some servers don't like requests that are made without a user-agent
        field, so we provide one */
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");

        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);

        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
        printf("HTTP_CODE: %ld\n\n", http_code);

        /* Check for errors */
        if(res != CURLE_OK){
            fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        }
        /* always cleanup */
        curl_slist_free_all(headers);
        curl_easy_cleanup(curl);
    }

//    curl_global_cleanup();

    return res;
}




int curl_raw(const char *url, const char *method, const char *postfield, struct curl_slist *headers, write_cb_t cb, void *chunk)
{
    CURL *curl;
    CURLcode res = CURLE_OK;
    long http_code = 0;

//    curl_global_init(CURL_GLOBAL_DEFAULT);

    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        curl_easy_setopt(curl, CURLOPT_URL, url);
        if (!strcmp(method, "POST") && postfield){
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS,postfield);
        }
        if(!strcmp(method,"POST"))
            curl_easy_setopt(curl, CURLOPT_POST, 1);
        if(!strcmp(method,"PUT"))
            curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");

        /* send all data to this function  */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, cb);

        /* we pass our 'chunk' struct to the callback function */
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, chunk);

        /* some servers don't like requests that are made without a user-agent
        field, so we provide one */
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");

        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);

        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
        printf("HTTP_CODE: %ld\n\n", http_code);

        /* Check for errors */
        if(res != CURLE_OK){
            fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        }
        /* always cleanup */
        curl_slist_free_all(headers);
        curl_easy_cleanup(curl);
    }

//    curl_global_cleanup();

    return res;
}

int curl(const char *url, const char *method, const char *postfield, const char *header, write_cb_t cb, void * chunk){


    struct curl_slist *headers=NULL;
    headers = curl_slist_append(headers, ACCEPT);
    headers = curl_slist_append(headers, CONTENT_TYPE);
    headers = curl_slist_append(headers, CHARSET);
    if(header && *header != '\0'){
        headers = curl_slist_append(headers, header);
    }
    return curl_raw(url, method, postfield, headers, cb, chunk);
}
