var socket = require('@system.socket');
// var process = require('@system.process');
// var parser = require("@system.http_parser");

function onRecv(ctx, data) {
    print("======onRecv======")
    // print(ctx)
    print("data ======>:");
    print(data) // 这里是有值的，只是print这个方法没有对buffer输出做格式化处理，因此输出为空字符串
    print(typeof data)
    // var obj = parser.create(parser.RESPONSE);
    // obj.execute(data);
    // print(obj);
    ctx.close();
}

function onConnected(ctx) {
    print("======onConnected======")
    print(ctx);
}


function onDisconnected(ctx) {
    print("======onDisconnected======")
    print(ctx);
}

function request(options){
    var method = options.method || 'GET';
    var path = options.path || '/';
    var headers = '';
    var port = 80;

    if( options.port ) {
        port = options.port;
    }

    if( options.headers ){
        for(var i = 0; i < options.headers.length; i++) {
            headers += options.headers[i];
        } 
    }
    headers += method + ' ' + path + 'HTTP/1.1\r\n';
    headers += 'Host: ' + options.host + '\r\n';
    headers += "User-Agent: EVM HTTP Agent\r\n";
    headers += "Accept: */*\r\n";

    client = socket.connect(options.host, port);
    client.options = options;
    client.on('recv', onRecv);
    client.on('connected', onConnected);
    client.on('disconnected', onDisconnected);
    client.write(headers);
}

export default {
    request: request
}
