config = {
    router: {
        path: 'evue_launcher'
    }
}

image_cache = require('@system.image');
image_cache.append('test.png')

function onCreate() {
    print("Application onCreate");
}

function onDestroy() {
    print("Application onDestroy");
}
