socket = require('@system.socket');
http_parser = require('@system.http_parser');

function onRecv(so, data) {
    parser = http_parser.create(http_parser.RESPONSE);
    var ret = parser.execute(data);
    if( ret == true && so.options.callback ){
        so.options.callback(parser.code, parser.body);
    }
}

function onConnected(so) {

}

function onDisconnected(so) {

}

function onClose(so) {
    console.log('close');
    so.close();
    so.destroy();
}

function request(options){
    var method = options.method || 'GET';
    var url = options.url || '/';
    var headers = '';
    var port = 80;

    if( options.port ) {
        port = options.port;
    }

    headers += method + ' ' + url + ' ' + 'HTTP/1.1\r\n';

    if( options.headers ){
        for(var i in options.headers) {
            headers += i + ": " + options.headers[i] + "\r\n";
        } 
    }
    
    headers += "User-Agent: EVM HTTP Agent\r\n\r\n";

    client = socket.connect(url, port);
    client.options = options;
    client.on('recv', onRecv);
    client.on('connected', onConnected);
    client.on('disconnected', onDisconnected);
    client.on('close', onClose);
    client.write(headers);  
}

