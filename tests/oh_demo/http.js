socket = require('@system.socket');
process = require('@system.process');

function onRecv(so, data) {
    
}

function onConnected(so) {

}


function onDisconnected(so) {

}

function request(options){
    var method = options.method || 'GET';
    var url = options.url || '/';
    var headers = '';
    var port = 80;

    if( options.port ) {
        port = options.port;
    }

    if( options.headers ){
        for(var i = 0; i < options.headers.length; i++) {
            headers += options.headers[i];
        } 
    }
    headers += method + ' ' + url + 'HTTP/1.1\r\n';
    headers += 'Host: ' + options.host + '\r\n';
    headers += "User-Agent: EVM HTTP Agent\r\n";
    headers += "Accept: */*\r\n";

    client = socket.connect(url, port);
    client.options = options;
    client.on('recv', onRecv);
    client.on('connected', onConnected);
    client.on('disconnected', onDisconnected);
    client.write(headers);
}

