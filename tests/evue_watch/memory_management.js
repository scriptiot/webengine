router = require("@system.router");
fs = require("@system.fs");
watch = require("@system.watch");

/**
 * 内存清理
 * 删除最久启动的应用文件及json配置
 * @returns {null|any}
 * 流程
 * ***************************************************
 *
 *                                                          -->是，不做处理
 *                                      -->判断当前启动的应用是
 *                                         否是未启动时间最长的应用
 *                                                          -->不是，直接删除文件并删除json信息
 *                               -->超过
 * 启动应用--->检测内存是否超过70%
 *                               -->未超过
 *                                     -->不做处理
 * **************************************************
 */
function memoryClean(appId) {
  var useSpace = watch.getUsedSpaceSize();
  var notUseSpace = watch.getFreeSpaceSize();
  var sum = useSpace + notUseSpace;
  var limit = sum * 0.82;
  //判断已使用空间是否超过70%
  if (limit > useSpace) {
    return null;
  } else {
    if (fs.exists("appstore_application.json")) {
      var json = fs.readFile("appstore_application.json");
      var info = JSON.parse(json);
      var nCommonlyUsedApp = info["nCommonlyUsedApp"];
      if (nCommonlyUsedApp == "") {
        return null;
      } else {
        //应用json文件
        var jsonFile = nCommonlyUsedApp["jsonFile"];
        //应用id
        var id = nCommonlyUsedApp["id"];
        //启动的是json中记录的最久未启动的应用，不做处理
        if (id == appId) {
          return null;
        } else {
          if (jsonFile == undefined) {
            return null;
          }
          //删除最久未启动应用文件
          this.deleteFile(jsonFile + ".json");
          //更新已安装应用json
          // this.updateJson(appId, info);
        }
      }
    }
  }
}

/**
 * 删除文件
 * @param jsonFileName
 */
function deleteFile(jsonFileName) {
  if (fs.exists(jsonFileName)) {
    var json = fs.readFile(jsonFileName);
    var info = JSON.parse(json);
    var files = info["files"];
    for (var i = 0; i < files.length; i++) {
      var fileName = files[i]["name"];
      fs.remove(fileName);
    }
  }
}

/**
 * 更新json
 * @param jsonFileName
 */
function updateJson(id, info) {
  var installApps = info["installApps"];
  var newInstallApps = [];
  var newTab = [];
  for (var i = 0; i < installApps.length; i++) {
    //一页tab
    var tab = installApps[i];
    for (var j = 0; j < tab.length; j++) {
      if (newTab.length == 4) {
        newInstallApps.push(newTab);
        newTab = [];
      }
      var app = tab[j];
      if (app["id"] == id) {
        continue;
      } else {
        newTab.push(app);
      }
    }
    //最后一次循环添加进列表
    if (i == installApps.length - 1 && newTab.length != 0) {
      newInstallApps.push(newTab);
    }
  }
  if (newInstallApps.length == 0) {
    info["installApps"] = [];
  } else {
    info["installApps"] = newInstallApps;
  }
  fs.writeFile("appstore_application.json", JSON.stringify(info));
}

/**
 * 卸载应用
 * @param id  应用id
 */
function deleteApp(id) {
  var json = fs.readFile("appstore_application.json");
  var info = JSON.parse(json);
  //读取要删除应用的epkjson文件，删除文件
  var apps = info["installApps"];
  var jsonFileName = "";
  for (var i = 0; i < apps.length; i++) {
    var tab = apps[i];
    for (var j = 0; j < tab.length; j++) {
      var aid = tab[j]["id"];
      if (aid == id) {
        jsonFileName = tab[j]["fileJsonName"];
        fs.remove(tab[j]["icon"]);
      }
    }
  }
  this.deleteFile(jsonFileName + ".json");
  setTimeout(function () {
    //更改应用市场配置文件删除应用信息
    this.updateJson(id, info);
  }, 100);
}
