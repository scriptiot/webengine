var config = {
    router: {
        path: "evue_launcher",
    },
};

var http = require("@system.http");
var fs = require("@system.fs");
var watch = require("@system.watch");
var os = require("@system.os");

function startMonitor() {
    if (os.platform() == "win64" || os.platform() == "linux") {
        watch.setImei("1398621891311111");
        watch.monitor(3000);
    } else {
        watch.monitor(3000);
    }
}

function onCreate() {
    // startMonitor()
    watch.setImei("1398621891311111");
    if (fs.exists("store_index.evue") == false) {
        var headers = [
            "Accept: application/json",
            "Content-Type: application/json",
            "charsets: utf-8",
        ];
        http.request({
            method: "POST",
            url: "http://store.evmiot.com/api/v1/evm_store/download/apps",
            headers: headers,
            timeout: 10000,
            data: JSON.stringify({
                uuid: "4fefa0ec-e484-11eb-b9cc-00163e02d869",
                imei: watch.imei(),
                byId: 1,
            }),
            responseType: "epk",
            callback: function (res, len, args) {},
            error: function (code, args) {},
        });
    }
}
