router = require("@system.router");
fs = require("@system.fs");
http = require("@system.http");
watch = require("@system.watch");
process = require("@system.process");
audio = require("@system.audio");

/**
 * 获取json文件内容
 * @param url    json名称
 */
function getJsonInfo(url) {
  if (fs.exists(url)) {
    var json = fs.readFile(url);
    var info = JSON.parse(json);
    return info;
  }
  return null;
}

/**
 * 下载epk安装包
 * 自动解压
 * @param url  下载地址
 */
function installEpk(id) {
  headers = [
    "Accept: application/json",
    "Content-Type: application/json",
    "charsets: utf-8",
  ];
  http.request({
    method: "POST",
    url: "http://store.evmiot.com/api/v1/evm_store/download/apps",
    headers: headers,
    timeout: 10000,
    data: JSON.stringify({
      uuid: id,
      imei: watch.imei(),
      byId: 1,
    }),
    responseType: "epk",
    callback: function (res, len, args) {},
    error: function (code, args) {},
  });
}

/**
 * 下载付费二维码
 * @param dis
 */
function getPayImage(domin, dis) {
  var imei = watch.imei();
  http.request({
    method: "GET",
    url: domin + "asrMarket/balance?imei=" + imei + "&dis=" + dis + "",
    headers: [],
    timeout: 1000,
    callback: function (res) {
      var result = JSON.parse(res);
      console.log("请求图片链接返回");
      console.log(res);
      if (result["code"] == 200) {
        http.request({
          method: "GET",
          url: result["user"]["payUrl"],
          headers: [],
          timeout: 100,
          filename: "yysc_pay_ewm.png",
          callback: function (res) {},
          error: function (code) {},
        });
      }
    },
    error: function (code) {
      // this.text='暂无数据';
    },
  });
}

/**
 * 下载图片
 * @param list   图片集合
 */
function getImage(list) {
  if (list.length == 0) {
    return;
  }
  for (var i = 0; i < list.length; i++) {
    var info = list[i];
    downloadImage(info["iconUrl"], info["icon"]);
  }
}

/**
 * 下载图片到本地
 * @param url   下载链接
 * @param fileName   图片名称
 */
function downloadImage(url, fileName) {
  http.request({
    method: "GET",
    url: url,
    headers: [],
    timeout: 100,
    filename: fileName,
    callback: function (res) {},
    error: function (code) {},
  });
}

/**
 * 下载语音到本地
 * @param url   下载链接
 * @param fileName   文件名称
 */
function downloadFile(url, fileName) {
  http.request({
    method: "GET",
    url: url,
    headers: [],
    timeout: 10000,
    responseType: "blob",
    filename: fileName,
    callback: function (res) {
      if (fs.exists(fileName)) {
        audio.play(fileName, function (time) {});
      }
    },
    error: function (code) {},
  });
}

/**
 * 删除图片文件
 * @param list  图片文件名称
 */
function deleteImage(list) {
  for (var i = 0; i < list.length; i++) {
    var path = list[i];
    fs.remove(path);
  }
}

/**
 * 获取登录状态
 * */
function login(domin, type, voucher) {
  http.request({
    method: "POST",
    url: domin + "api/urlTicket",
    data: JSON.stringify({
      imei: watch.imei(),
      type: voucher,
      action: type,
    }),
    headers: [],
    timeout: 1000,
    callback: function (res) {
      var result = JSON.parse(res);
      var state = result["status"];
      if (state == 0) {
        downloadImage(result["qrCodeUrl"], "kdgs_qrcodeA.png");
      } else {
        var info = getJsonInfo("kdgs_application.json");
        info["isVip"] = result["member"]["is_vip"];
        info["name"] = result["member"]["nickname"];
        info["icon"] = result["member"]["avatar"];
        info["token"] = result["newUserToken"];
        var vipData = result["member"]["vip_valid_date"];
        if (vipData == null || vipData == "") {
          vipData = "1";
        }
        info["vipValidDate"] = vipData;
        info["loginStatus"] = 1;
        fs.writeFile("kdgs_application.json", JSON.stringify(info));
      }
    },
    error: function (code) {},
  });
}

/**
 * 上传文件到服务器
 * @param url   上传接口链接
 * @param fileName   文件名称
 */
function uploadFile(url, fileName, imei) {
  var data = fs.readFile(fileName, "rb");
  var headers = [
    "Accept: application/octet-stream",
    "Content-Type: application/octet-stream",
    "imei:" + imei,
  ];
  var res = http.request({
    method: "POST",
    url: url,
    headers: headers,
    timeout: 10000,
    data: data,
    callback: function (res, len) {
      var result = JSON.parse(res);
      if (result["code"] == 200) {
        router.push({
          path: "kdgs_1_searchResults",
          params: {
            str: result["msg"],
          },
        });
      } else {
        // fs.remove(fileName);
      }
    },
    error: function (code) {},
  });
}
