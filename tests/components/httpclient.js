function encodeURIComponent(i) {
    return i;
}

function encodeURI(params) {
    let newData = '';
    for (let k in params) {
        if (params.hasOwnProperty(k) === true) {
            newData += encodeURIComponent(k) + '=' + encodeURIComponent(params[k]) + '&';
        }
    }
    return newData;
}

function apiAxios(method, url, params, response) {
    url = (method === 'GET' && params != "" ? url + "?" + encodeURI(params) : url)
    data = (method != 'GET' ? JSON.stringify(params) : null)
    http_send(method, url, data, response)
}

export default {
    get: function(url, params, response) {
        return apiAxios('GET', url, params, response)
    },
    post: function(url, params, response) {
        return apiAxios('POST', url, params, response)
    },
    put: function(url, params, response) {
        return apiAxios('PUT', url, params, response)
    },
    delete: function(url, params, response) {
        return apiAxios('DELETE', url, params, response)
    },
    jsonp: function(url, params, response) {
        return apiAxios('JSONP', url, params, response)
    },
}