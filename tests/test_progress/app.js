config = {
    router: {
        path: 'evue_launcher'
    }
}

image_cache = require('../demo_320x240/node_modules/@system.image');
image_cache.append('test.png')

function onCreate() {
    print("Application onCreate");
}

function onDestroy() {
    print("Application onDestroy");
}
